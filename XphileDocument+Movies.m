/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/

#import "FileEntity.h"
#import "ImageEntity.h"
#import "NSString+Additions.h"
#import "QTMovie+Additions.h"
#import "XphileDocument+Movies.h"
#import "WaitController.h"



@implementation XphileDocument (XphileDocument_Movies)


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) createMovie
	{
	[self showMovieSheet: imageWindow];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) showMovieSheet: (NSWindow*) window
	{
	if (!movieSheet)
		[NSBundle loadNibNamed: @"Movie" owner: self];

	[movieLabel setStringValue: UntitledMovieString];
	[movieLabel selectText: self];
	[NSApp beginSheet: movieSheet modalForWindow: window modalDelegate: self didEndSelector: @selector(movieSheetDidEnd: returnCode: contextInfo:) contextInfo: nil];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) closeMovieSheet: (id) sender
	{
	ASSERTNOTNIL(movieSheet);
	
    [NSApp endSheet: movieSheet returnCode: [sender tag]];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) movieSheetDidEnd: (NSWindow*) sheet returnCode: (NSInteger) returnCode contextInfo: (void* __unused) contextInfo
	{
	[sheet orderOut: self];
	if (returnCode==NSAlertFirstButtonReturn)
		{
		[self createMovieWithLabel: [movieLabel stringValue]];
		}
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) createMovieWithLabel: (NSString*) label
	{
		
// Store selected images in temporary folder.

	NSUInteger count=[[imageArrayController selectedObjects] count];
	if (count==0)
		{
		return;
		}

	NSString *tempImagesPath=[NSString temporaryPathName];ASSERTNOTNIL(tempImagesPath);
	NSError *error=nil;
	if (![[NSFileManager defaultManager] createDirectoryAtPath: tempImagesPath withIntermediateDirectories: YES attributes: nil error: &error])
		{
		NSLog(@"Error: %@", [error description]);
		}
	[imageArrayController exportImagesTo: tempImagesPath];
	
// Create movie.

	WaitController *waitController=[[WaitController alloc] initWithStatusString: CreatingMovieString];ASSERTNOTNIL(waitController);
	[waitController showWindow: self];			
	[waitController setProgressBarMax: count];

	NSString *tempMoviePath=[NSString temporaryPathName];ASSERTNOTNIL(tempMoviePath);
	QTMovie *qt=[QTMovie editableMovieWithFile: tempMoviePath];ASSERTNOTNIL(qt);
	NSDictionary *attrs=[NSDictionary dictionaryWithObject: @"jpeg" forKey: QTAddImageCodecType];ASSERTNOTNIL(attrs);
	NSUInteger movieFrameRate=[[NSUserDefaults standardUserDefaults] integerForKey: MovieFrameRateKey];

	NSDirectoryEnumerator *enumerator=[[NSFileManager defaultManager] enumeratorAtPath: tempImagesPath];ASSERTNOTNIL(enumerator);
	NSString *imagePath;
	count=0;
	while ((imagePath=[enumerator nextObject]))
		{
		NSString *fullPath=[tempImagesPath stringByAppendingPathComponent: imagePath];ASSERTNOTNIL(fullPath);
		NSImage *im=[[[NSImage alloc] initWithContentsOfFile: fullPath] autorelease];ASSERTNOTNIL(im);
		[qt addImage: im forDuration: QTMakeTime(100/movieFrameRate, 100) withAttributes: attrs];
		
		[waitController setProgressBarValue: ++count];
		[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];
		}
		
	[waitController setIndeterminate];
	[waitController setStatusString: StoringMovieString];
	[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];

// Finalise the movie and delete the temporary images.

	[qt updateMovieFile];
	if (![[NSFileManager defaultManager] removeItemAtPath: tempImagesPath error: &error])
		{
		NSLog(@"Error: %@", [error description]);
		}

// Get the order index of the last item in the file array.

	NSInteger orderIndex;
	FileEntity *fileEntity=[[fileArrayController arrangedObjects] lastObject];
	if (fileEntity==nil)
		orderIndex=-1;
	else
		orderIndex=[[fileEntity valueForKey: @"order"] integerValue];
	
// Add movie as a file entity and delete temporary movie.
	
	ImageEntity *firstImageEntity=[[imageArrayController selectedObjects] objectAtIndex: 0];ASSERTNOTNIL(firstImageEntity);
	StudyEntity	*currentStudy=[firstImageEntity valueForKeyPath: @"study"];ASSERTNOTNIL(currentStudy);
	
	fileEntity=[NSEntityDescription insertNewObjectForEntityForName: @"File" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(fileEntity);
	[fileEntity setValue: currentStudy forKey: @"study"];
	[fileEntity setValue: [NSNumber numberWithUnsignedInteger:++orderIndex] forKey: @"order"];
	[fileEntity setMediaFromMovie: qt];
	[fileEntity setValue: [label stringByAppendingPathExtension: @"mov"] forKey: @"fileName"];
	if (![[NSFileManager defaultManager] removeItemAtPath: tempMoviePath error: &error])
		{
		NSLog(@"Error: %@", [error description]);
		}
	
	[waitController dismiss];
	[waitController release];
	
	[fileArrayController setSelectedObjects: [NSArray arrayWithObject: fileEntity]];
	[imageWindow makeFirstResponder: fileTableView];
	}
	

@end
