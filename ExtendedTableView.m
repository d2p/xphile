/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "ExtendedTableView.h"


@implementation NSTableView (ExtendedTableView)

// Used to prevent double-clicking in table header.
// Modified from http://www.cocoabuilder.com/archive/message/cocoa/2004/5/11/106845

- (BOOL) isClickInHeader
	{
	NSTableHeaderView *header=[self headerView];ASSERTNOTNIL(header);
	NSEvent *event=[NSApp currentEvent];ASSERTNOTNIL(event);
	NSPoint location=[event locationInWindow];
	location=[[header superview] convertPoint: location fromView: nil];
	if ([header hitTest: location])
       return (YES);
	
	return (NO);
	}

@end
