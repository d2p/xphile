/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


// This portion of the codebase is a stop-gap measure.  A better quality, more modular rule editor is required.
// which would allow for text searches etc.



#import "XphileDocument+Albums.h"
#import "XphileDocument+ImportMIRC.h"
#import "XphileDocument+Toolbar.h"


@implementation XphileDocument (XphileDocument_Albums)


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Random Album

- (NSString*) uniqueRandomAlbumName
	{
	NSArray									*resultArray;
	NSString								*albumName;
	NSInteger										index	=	0;

	do
		{
		albumName=[NSString stringWithFormat: @"%@ %ld", RandomCasesString, (long) ++index];ASSERTNOTNIL(albumName);
		
		NSString *predicateString=[NSString stringWithFormat: @"label like[c] '%@'", albumName];ASSERTNOTNIL(predicateString);
		NSPredicate *predicate=[NSPredicate predicateWithFormat: predicateString];ASSERTNOTNIL(predicate);
		NSFetchRequest *request=[[[NSFetchRequest alloc] init] autorelease];ASSERTNOTNIL(request);
		NSEntityDescription *entity=[NSEntityDescription entityForName: @"Album" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(entity);
		[request setEntity: entity];
		[request setPredicate: predicate];
		NSError *error=nil;
		resultArray=[[self managedObjectContext] executeFetchRequest: request error: &error];
		if (resultArray == nil)
			{
			NSLog(@"Error: %@", [error description]);
			}
		} while ([resultArray count] > 0);
		
	return albumName;
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) createRandomAlbum: (id __unused) sender
	{

// Record whether the option key is down.

	BOOL isOptionKey=(([[NSApp currentEvent] modifierFlags] & NSAlternateKeyMask) != 0);
	
// Calculate maximum number of cases to choose (containing images / movies).
	
	NSMutableArray *condArray=[[[browserArrayController arrangedObjects] mutableCopy] autorelease];ASSERTNOTNIL(condArray);
	NSUInteger maxItems=[condArray count];

	if (maxItems == 0)
		return;

	NSUInteger index=0;
	do
		{
		ConditionEntity *aCondition=[condArray objectAtIndex: index];ASSERTNOTNIL(aCondition);
		if ([aCondition numberOfImages] == 0 && [aCondition numberOfMovies] == 0)
			[condArray removeObject: aCondition];
		else
			index++;
		} while (index < [condArray count]);
	
	maxItems=index;
	NSUInteger numberToChoose=20;
	if (numberToChoose > maxItems)
		numberToChoose=maxItems;

	if (maxItems == 0)
		return;

// Create new album.

	AlbumEntity *newAlbum=[NSEntityDescription insertNewObjectForEntityForName: @"Album" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(newAlbum);
	[newAlbum setValue: [self uniqueRandomAlbumName] forKey: @"label"];
	NSMutableSet *newAlbumSet=[newAlbum mutableSetValueForKeyPath: @"conditions"];ASSERTNOTNIL(newAlbumSet);

// Populate new album with random cases.
	
	do
		{
		index=[self randomInt: maxItems] - 1;
		ConditionEntity *newCondition=[condArray objectAtIndex: index];ASSERTNOTNIL(newCondition);
		[newAlbumSet addObject: newCondition];
		} while ([newAlbumSet count] < numberToChoose);
	
// If the option key is down, reset browser and select new album and enter full screen.

	if (isOptionKey)
		{
		[self resetBrowser: self];
		[albumArrayController performSelector: @selector(setSelectedObjects:) withObject: [NSArray arrayWithObject: newAlbum] afterDelay: 0.5];
		[self enterFullScreen: self];
		}
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Smart Album

- (void) editSmartAlbum: (id) sender
	{
	if (!smartAlbumSheet)
		[NSBundle loadNibNamed: @"Album" owner: self];

// Manually bind the label.
	
	NSDictionary *bindingOptionsDict=[NSDictionary dictionaryWithObject: [NSNumber numberWithBool: NO] forKey: NSContinuouslyUpdatesValueBindingOption];ASSERTNOTNIL(bindingOptionsDict);
	[smartAlbumLabel bind: NSValueBinding toObject: albumArrayController withKeyPath: @"selection.label" options: bindingOptionsDict];

// Set the interface.

	[self setUpMenusForCurrentAlbum];
	[NSApp beginSheet: smartAlbumSheet modalForWindow: [self windowForSheet] modalDelegate: self didEndSelector: @selector(smartAlbumSheetDidEnd: returnCode: contextInfo:) contextInfo: sender];
	}
	
//*** Should record current selection here.

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) closeAlbumSheet: (id __unused) sender
	{
		ASSERTNOTNIL(smartAlbumSheet);

	[smartAlbumSheet makeFirstResponder: nil];	
    [NSApp endSheet: smartAlbumSheet];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) smartAlbumSheetDidEnd: (NSWindow*) sheet returnCode: (NSInteger __unused) returnCode contextInfo: (id) sender
	{
		
// Manually unbind the label text field.

	[smartAlbumLabel unbind: NSValueBinding];

// Build predicate, based on menu choices:  all=and | any=or.

	NSPredicate *predicate1=[self predicateFromLine1];ASSERTNOTNIL(predicate1);
	NSPredicate *predicate2=[self predicateFromLine2];ASSERTNOTNIL(predicate2);
	NSPredicate *predicate3=[self predicateFromLine3];ASSERTNOTNIL(predicate3);
	
	NSPredicate *compoundPredicate=nil;
	switch ([smartAlbumCompoundMenu indexOfSelectedItem])
		{
		case SmartAlbumCompoundTypeNone:
		case SmartAlbumCompoundTypeAll:
			compoundPredicate=[NSCompoundPredicate andPredicateWithSubpredicates: [NSArray arrayWithObjects: predicate1, predicate2, predicate3, nil]];
			break;
			
		case SmartAlbumCompoundTypeAny:
			compoundPredicate=[NSCompoundPredicate orPredicateWithSubpredicates: [NSArray arrayWithObjects: predicate1, predicate2, predicate3, nil]];
			break;
		}
	ASSERTNOTNIL(compoundPredicate);
	NSData *predicateData=nil;
	if (![self predicateEquatesToZero: compoundPredicate])
		{
		predicateData=[NSKeyedArchiver archivedDataWithRootObject: compoundPredicate];ASSERTNOTNIL(predicateData);
		}

	AlbumEntity *album=[[albumArrayController selectedObjects] objectAtIndex: 0];ASSERTNOTNIL(album);
	[album setValue: predicateData forKeyPath: @"itsPredicate"];

// Dismiss the sheet.

	[sheet orderOut: self];
	if (sender==albumArrayController)
		[albumArrayController setSelectionIndexes: [NSIndexSet indexSet]];

// Update the display.
	
	[self updateForAlbumSelection];

// Remove all simple linked conditions, if smart album.

	if ([album isSmartAlbum])
		{
		NSMutableSet *conditions=[[albumArrayController selection] mutableSetValueForKeyPath: @"conditions"];ASSERTNOTNIL(conditions);
		[conditions removeAllObjects];
		}
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Update Smart Album Menu Items

- (IBAction) updateAlbumSheetForSearchType: (id) sender
	{
	if (sender==smartAlbumCompoundMenu)
		[self updateSmartAlbumCompoundMenu];
		
	if (sender==smartAlbumSearchTypeMenu1)
		[self updateSmartAlbumMenusLine1];
		
	if (sender==smartAlbumSearchTypeMenu2)
		[self updateSmartAlbumMenusLine2];
		
	if (sender==smartAlbumSearchTypeMenu3)
		[self updateSmartAlbumMenusLine3];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) setUpMenusForCurrentAlbum
	{

// Get current predicate.  If nil, set interface to nil and exit.

	AlbumEntity *album=[albumArrayController selection];ASSERTNOTNIL(album);
	NSData *data=[album valueForKeyPath: @"itsPredicate"];
	if (data==nil)
		{
		[smartAlbumCompoundMenu selectItemAtIndex: SmartAlbumCompoundTypeNone];[self updateSmartAlbumCompoundMenu];
		return;
		}
	
	NSCompoundPredicate *compoundPredicate=[NSKeyedUnarchiver unarchiveObjectWithData: data];ASSERTNOTNIL(compoundPredicate);

// Decide if all or any (AND or OR) and set divider accordingly.

	if ([compoundPredicate compoundPredicateType]==NSAndPredicateType)
		[smartAlbumCompoundMenu selectItemAtIndex: SmartAlbumCompoundTypeAll];
	else
		[smartAlbumCompoundMenu selectItemAtIndex: SmartAlbumCompoundTypeAny];

// Update UI for this compound menu choice.
	
	[self updateSmartAlbumCompoundMenu];
				
// Divide predicate with dividerStr.

	NSArray *subPredicatesArray=[compoundPredicate subpredicates];ASSERTNOTNIL(subPredicatesArray);

// Set up outlets into arrays.

	NSMutableArray *smartAlbumSearchTypeMenuArray=[NSMutableArray arrayWithCapacity: 3];ASSERTNOTNIL(smartAlbumSearchTypeMenuArray);
	[smartAlbumSearchTypeMenuArray addObject: smartAlbumSearchTypeMenu1];
	[smartAlbumSearchTypeMenuArray addObject: smartAlbumSearchTypeMenu2];
	[smartAlbumSearchTypeMenuArray addObject: smartAlbumSearchTypeMenu3];

	NSMutableArray *smartAlbumOperatorMenuArray=[NSMutableArray arrayWithCapacity: 3];ASSERTNOTNIL(smartAlbumOperatorMenuArray);
	[smartAlbumOperatorMenuArray addObject: smartAlbumOperatorMenu1];
	[smartAlbumOperatorMenuArray addObject: smartAlbumOperatorMenu2];
	[smartAlbumOperatorMenuArray addObject: smartAlbumOperatorMenu3];

	NSMutableArray *smartAlbumOptionMenuArray=[NSMutableArray arrayWithCapacity: 3];ASSERTNOTNIL(smartAlbumOptionMenuArray);
	[smartAlbumOptionMenuArray addObject: smartAlbumOptionMenu1];
	[smartAlbumOptionMenuArray addObject: smartAlbumOptionMenu2];
	[smartAlbumOptionMenuArray addObject: smartAlbumOptionMenu3];
	
// Loop through the smart album menu lines.

	NSInteger								smartAlbumMenuLine;
	
	for (smartAlbumMenuLine=0; smartAlbumMenuLine<=2; smartAlbumMenuLine++)
		{
		NSString *str=[[subPredicatesArray objectAtIndex: smartAlbumMenuLine] predicateFormat];ASSERTNOTNIL(str);
		if ([str isEqualToString: @"TRUEPREDICATE"] || [str isEqualToString: @"FALSEPREDICATE"])
			{
			[[smartAlbumSearchTypeMenuArray objectAtIndex: smartAlbumMenuLine] selectItemAtIndex: SmartAlbumSearchTypeIgnore];
			[self updateSmartAlbumMenusForLine: smartAlbumMenuLine];
			}
		else
			{
			NSArray *components=[str componentsSeparatedByString: @" "];ASSERTNOTNIL(components);
			NSString *searchTypeString=[components objectAtIndex: 0];ASSERTNOTNIL(searchTypeString);
			NSString *operatorString=[components objectAtIndex: 1];ASSERTNOTNIL(operatorString);
			NSString *optionString=[components objectAtIndex: 2];ASSERTNOTNIL(optionString);
			
			if ([searchTypeString isEqualToString: @"system"])
				[[smartAlbumSearchTypeMenuArray objectAtIndex: smartAlbumMenuLine] selectItemAtIndex: SmartAlbumSearchTypeSystem];
			else if ([searchTypeString isEqualToString: @"pathology"])
				[[smartAlbumSearchTypeMenuArray objectAtIndex: smartAlbumMenuLine] selectItemAtIndex: SmartAlbumSearchTypePathology];
			else if ([searchTypeString isEqualToString: @"conditionRegion"])
				[[smartAlbumSearchTypeMenuArray objectAtIndex: smartAlbumMenuLine] selectItemAtIndex: SmartAlbumSearchTypeConditionRegion];
			else if ([searchTypeString isEqualToString: @"patient.gender"])
				[[smartAlbumSearchTypeMenuArray objectAtIndex: smartAlbumMenuLine] selectItemAtIndex: SmartAlbumSearchTypeGender];
			[self updateSmartAlbumMenusForLine: smartAlbumMenuLine];
			
			if ([operatorString isEqualToString: @"=="])
				[[smartAlbumOperatorMenuArray objectAtIndex: smartAlbumMenuLine] selectItemAtIndex: SmartAlbumOperatorTypeIs];
			else if ([operatorString isEqualToString: @"!="])
				[[smartAlbumOperatorMenuArray objectAtIndex: smartAlbumMenuLine] selectItemAtIndex: SmartAlbumOperatorTypeIsNot];

			[[smartAlbumOptionMenuArray objectAtIndex: smartAlbumMenuLine] selectItemAtIndex: [optionString integerValue]];
			}
		}

	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) updateSmartAlbumMenusForLine: (NSInteger) line
	{

// Temporary director for the setUpMenusForCurrentAlbum loop.  Should be designed out eventually.

	switch (line)
		{
		case 0:
			[self updateSmartAlbumMenusLine1];
			break;
			
		case 1:
			[self updateSmartAlbumMenusLine2];
			break;
			
		case 2:
			[self updateSmartAlbumMenusLine3];
			break;
			
		default:
			NSLog(@"No such smart album menu line");
			break;
		}
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) updateSmartAlbumMenusLine1
	{
	ASSERTNOTNIL(smartAlbumOperatorMenu1);
	ASSERTNOTNIL(smartAlbumOptionMenu1);
	
	NSInteger smartAlbumSearchType=[smartAlbumSearchTypeMenu1 indexOfSelectedItem];

	[smartAlbumOperatorMenu1 setHidden: NO];
	[smartAlbumOptionMenu1 setHidden: NO];

	[smartAlbumOptionMenu1 removeAllItems];

	switch (smartAlbumSearchType)
		{
		NSArray						*menuArray=nil;
		
		case SmartAlbumSearchTypeIgnore:
			[smartAlbumOptionMenu1 setHidden: YES];
			[smartAlbumOperatorMenu1 setHidden: YES];
			break;

		case SmartAlbumSearchTypeSystem:
			menuArray=[[NSUserDefaults standardUserDefaults] objectForKey: SystemArrayKey];ASSERTNOTNIL(menuArray);
			[smartAlbumOptionMenu1 addItemsWithTitles: menuArray];
			break;
			
		case SmartAlbumSearchTypePathology:
			menuArray=[[NSUserDefaults standardUserDefaults] objectForKey: PathologyArrayKey];ASSERTNOTNIL(menuArray);
			[smartAlbumOptionMenu1 addItemsWithTitles: menuArray];
			break;
			
		case SmartAlbumSearchTypeConditionRegion:
			menuArray=[[NSUserDefaults standardUserDefaults] objectForKey: RegionArrayKey];ASSERTNOTNIL(menuArray);
			[smartAlbumOptionMenu1 addItemsWithTitles: menuArray];
			break;
			
		case SmartAlbumSearchTypeGender:
			menuArray=[[NSUserDefaults standardUserDefaults] objectForKey: GenderArrayKey];ASSERTNOTNIL(menuArray);
			[smartAlbumOptionMenu1 addItemsWithTitles: menuArray];
			break;
		}
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) updateSmartAlbumMenusLine2
	{
	ASSERTNOTNIL(smartAlbumOperatorMenu2);
	ASSERTNOTNIL(smartAlbumOptionMenu2);
	
	NSInteger smartAlbumSearchType=[smartAlbumSearchTypeMenu2 indexOfSelectedItem];

	[smartAlbumOperatorMenu2 setHidden: NO];
	[smartAlbumOptionMenu2 setHidden: NO];

	[smartAlbumOptionMenu2 removeAllItems];

	switch (smartAlbumSearchType)
		{
		NSArray						*menuArray=nil;
		
		case SmartAlbumSearchTypeIgnore:
			[smartAlbumOptionMenu2 setHidden: YES];
			[smartAlbumOperatorMenu2 setHidden: YES];
			break;

		case SmartAlbumSearchTypeSystem:
			menuArray=[[NSUserDefaults standardUserDefaults] objectForKey: SystemArrayKey];ASSERTNOTNIL(menuArray);
			[smartAlbumOptionMenu2 addItemsWithTitles: menuArray];
			break;
			
		case SmartAlbumSearchTypePathology:
			menuArray=[[NSUserDefaults standardUserDefaults] objectForKey: PathologyArrayKey];ASSERTNOTNIL(menuArray);
			[smartAlbumOptionMenu2 addItemsWithTitles: menuArray];
			break;
			
		case SmartAlbumSearchTypeConditionRegion:
			menuArray=[[NSUserDefaults standardUserDefaults] objectForKey: RegionArrayKey];ASSERTNOTNIL(menuArray);
			[smartAlbumOptionMenu2 addItemsWithTitles: menuArray];
			break;
			
		case SmartAlbumSearchTypeGender:
			menuArray=[[NSUserDefaults standardUserDefaults] objectForKey: GenderArrayKey];ASSERTNOTNIL(menuArray);
			[smartAlbumOptionMenu2 addItemsWithTitles: menuArray];
			break;
		}
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) updateSmartAlbumMenusLine3
	{
	ASSERTNOTNIL(smartAlbumOperatorMenu3);
	ASSERTNOTNIL(smartAlbumOptionMenu3);
	
	NSInteger smartAlbumSearchType=[smartAlbumSearchTypeMenu3 indexOfSelectedItem];

	[smartAlbumOperatorMenu3 setHidden: NO];
	[smartAlbumOptionMenu3 setHidden: NO];

	[smartAlbumOptionMenu3 removeAllItems];

	switch (smartAlbumSearchType)
		{
		NSArray						*menuArray=nil;
		
		case SmartAlbumSearchTypeIgnore:
			[smartAlbumOptionMenu3 setHidden: YES];
			[smartAlbumOperatorMenu3 setHidden: YES];
			break;

		case SmartAlbumSearchTypeSystem:
			menuArray=[[NSUserDefaults standardUserDefaults] objectForKey: SystemArrayKey];ASSERTNOTNIL(menuArray);
			[smartAlbumOptionMenu3 addItemsWithTitles: menuArray];
			break;
			
		case SmartAlbumSearchTypePathology:
			menuArray=[[NSUserDefaults standardUserDefaults] objectForKey: PathologyArrayKey];ASSERTNOTNIL(menuArray);
			[smartAlbumOptionMenu3 addItemsWithTitles: menuArray];
			break;
			
		case SmartAlbumSearchTypeConditionRegion:
			menuArray=[[NSUserDefaults standardUserDefaults] objectForKey: RegionArrayKey];ASSERTNOTNIL(menuArray);
			[smartAlbumOptionMenu3 addItemsWithTitles: menuArray];
			break;
			
		case SmartAlbumSearchTypeGender:
			menuArray=[[NSUserDefaults standardUserDefaults] objectForKey: GenderArrayKey];ASSERTNOTNIL(menuArray);
			[smartAlbumOptionMenu3 addItemsWithTitles: menuArray];
			break;
		}
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) updateSmartAlbumCompoundMenu
	{

// If changing to simple album, disable the search type menus.
	
	if ([smartAlbumCompoundMenu indexOfSelectedItem] == SmartAlbumCompoundTypeNone)
		{
		[smartAlbumSearchTypeMenu1 selectItemAtIndex: SmartAlbumSearchTypeIgnore];[self updateSmartAlbumMenusLine1];
		[smartAlbumSearchTypeMenu2 selectItemAtIndex: SmartAlbumSearchTypeIgnore];[self updateSmartAlbumMenusLine2];
		[smartAlbumSearchTypeMenu3 selectItemAtIndex: SmartAlbumSearchTypeIgnore];[self updateSmartAlbumMenusLine3];
		}
		
	[smartAlbumSearchTypeMenu1 setEnabled: ([smartAlbumCompoundMenu indexOfSelectedItem] != SmartAlbumCompoundTypeNone)];
	[smartAlbumSearchTypeMenu2 setEnabled: ([smartAlbumCompoundMenu indexOfSelectedItem] != SmartAlbumCompoundTypeNone)];
	[smartAlbumSearchTypeMenu3 setEnabled: ([smartAlbumCompoundMenu indexOfSelectedItem] != SmartAlbumCompoundTypeNone)];
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Predicates

- (NSPredicate*) ignorePredicate
	{

// If the compound will be OR, we don't want all records to match, so use FALSEPREDICATE otherwise TRUEPREDICATE.

	if ([smartAlbumCompoundMenu indexOfSelectedItem]==SmartAlbumCompoundTypeAny)
		{
		return [NSPredicate predicateWithFormat: @"FALSEPREDICATE"];
		}

	return [NSPredicate predicateWithFormat: @"TRUEPREDICATE"];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (BOOL) predicateEquatesToZero: (NSPredicate*) predicate
	{
	if ([[predicate predicateFormat] isEqualToString: @"TRUEPREDICATE AND TRUEPREDICATE AND TRUEPREDICATE"])
		{
		return YES;
		}
	if ([[predicate predicateFormat] isEqualToString: @"FALSEPREDICATE OR FALSEPREDICATE OR FALSEPREDICATE"])
		{
		return YES;
		}
	
	return NO;
	}
	
	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSPredicate*) predicateFromAlbumSearchType: (SmartAlbumSearchType) albumSearchType
  albumOperatorType: (SmartAlbumOperatorType) albumOperatorType
  albumOption: (NSUInteger) albumOption
	{
	NSString *predicateString=@"";ASSERTNOTNIL(predicateString);

	switch (albumSearchType)
		{
		case SmartAlbumSearchTypeIgnore:
			return [self ignorePredicate];
			break;
		
		case SmartAlbumSearchTypeSystem:
			predicateString=[predicateString stringByAppendingString: @"system"];
			break;
			
		case SmartAlbumSearchTypePathology:
			predicateString=[predicateString stringByAppendingString: @"pathology"];
			break;
			
		case SmartAlbumSearchTypeConditionRegion:
			predicateString=[predicateString stringByAppendingString: @"conditionRegion"];
			break;

		case SmartAlbumSearchTypeGender:
			predicateString=[predicateString stringByAppendingString: @"patient.gender"];
			break;
		}

	switch (albumOperatorType)
		{
		case SmartAlbumOperatorTypeIs:
			predicateString=[predicateString stringByAppendingString: @" == "];
			break;
			
		case SmartAlbumOperatorTypeIsNot:
			predicateString=[predicateString stringByAppendingString: @" != "];
			break;
		}

	predicateString=[predicateString stringByAppendingFormat: @"%ld", albumOption];
	NSPredicate *predicate=[NSPredicate predicateWithFormat: predicateString];ASSERTNOTNIL(predicate);

	return (predicate);
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSPredicate*) predicateFromLine1
	{
	return [self predicateFromAlbumSearchType: [smartAlbumSearchTypeMenu1 indexOfSelectedItem]
	  albumOperatorType: [smartAlbumOperatorMenu1 indexOfSelectedItem] albumOption: [smartAlbumOptionMenu1 indexOfSelectedItem]];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSPredicate*) predicateFromLine2
	{
	return [self predicateFromAlbumSearchType: [smartAlbumSearchTypeMenu2 indexOfSelectedItem]
	  albumOperatorType: [smartAlbumOperatorMenu2 indexOfSelectedItem] albumOption: [smartAlbumOptionMenu2 indexOfSelectedItem]];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSPredicate*) predicateFromLine3
	{
	return [self predicateFromAlbumSearchType: [smartAlbumSearchTypeMenu3 indexOfSelectedItem]
	  albumOperatorType: [smartAlbumOperatorMenu3 indexOfSelectedItem] albumOption: [smartAlbumOptionMenu3 indexOfSelectedItem]];
	}



	

@end
