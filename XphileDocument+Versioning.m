/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2014 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "AlbumEntity.h"
#import "FileEntity.h"
#import "ImageEntity.h"
#import "StudyEntity.h"
#import "WaitController.h"
#import "XphileDocument+Versioning.h"


@implementation XphileDocument (XphileDocument_Versioning)


- (void) updateAlbumFromVersion: (NSInteger) oldVersion oldAlbum: (AlbumEntity*) oldAlbum newAlbum: (AlbumEntity*) newAlbum
	{
	[newAlbum setValue: [oldAlbum valueForKeyPath: @"label"] forKeyPath: @"label"];
	if (oldVersion==1)																		// Only simple albums in version 1.
		[newAlbum setValue: nil forKeyPath: @"itsPredicate"];
	else
		[newAlbum setValue: [oldAlbum valueForKeyPath: @"itsPredicate"] forKeyPath: @"itsPredicate"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
	
- (void) updatePatientFromVersion: (NSInteger __unused) oldVersion oldPatient: (PatientEntity*) oldPatient newPatient: (PatientEntity*) newPatient
	{	
	[newPatient setValue: [oldPatient valueForKeyPath: @"identifier"] forKeyPath: @"identifier"];
	[newPatient setValue: [oldPatient valueForKeyPath: @"dateOfBirth"] forKeyPath: @"dateOfBirth"];
	[newPatient setValue: [oldPatient valueForKeyPath: @"gender"] forKeyPath: @"gender"];

// New since version 3.
	
	[newPatient setValue: nil forKeyPath: @"linkedToPatients"];
	[newPatient setValue: nil forKeyPath: @"linkedFromPatients"];
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) updateConditionFromVersion: (NSInteger) oldVersion oldCondition: (ConditionEntity*) oldCondition newCondition: (ConditionEntity*) newCondition
	{
	[newCondition setValue: [oldCondition valueForKeyPath: @"conditionDescription"] forKeyPath: @"conditionDescription"];
	[newCondition setValue: [oldCondition valueForKeyPath: @"conditionRegion"] forKeyPath: @"conditionRegion"];
	[newCondition setValue: [oldCondition valueForKeyPath: @"dateOfOnset"] forKeyPath: @"dateOfOnset"];

	NSInteger pathologyIndex=[[oldCondition valueForKeyPath: @"pathology"] integerValue];
	if (oldVersion==1)																		// Allow for Degenerative and Normal pathology codes not present
		{																					// in version 1.
		if (pathologyIndex>=14) pathologyIndex++;
		if (pathologyIndex>=5) pathologyIndex++;
		}
	[newCondition setValue: [NSNumber numberWithInteger:pathologyIndex] forKeyPath: @"pathology"];
	
	NSInteger oldSystemIndex=[[oldCondition valueForKeyPath: @"system"] integerValue];
	if (oldVersion==2)
		{
		switch (oldSystemIndex)
			{
			case 5:																			// Previously Genitourinary
				if ([[newCondition valueForKeyPath: @"conditionRegion"] integerValue]==5)		// Abdomen -> Urinary
					oldSystemIndex=14;
				else if ([[newCondition valueForKeyPath: @"conditionRegion"] integerValue]==6)	// Pelvis -> Genital Tract (Male) or Genital Tract (Female)
					{																		// This is best guess, though eg bladder will be incorrectly assigned
					if ([[newCondition valueForKeyPath: @"patient.gender"] integerValue]==1)
						oldSystemIndex=6;
					else if ([[newCondition valueForKeyPath: @"patient.gender"] integerValue]==2)
						oldSystemIndex=5;
					else
						oldSystemIndex=0;													// Otherwise no idea, set to -
					}
				break;
			case 6:																			// Gyanecological -> Genital Tract (Female)
				oldSystemIndex=5;
				break;
			case 14:																		// Vascular -> Vascular
				oldSystemIndex=15;
				break;
			default:																		// Keep unchanged.
				break;
			}
		}
	[newCondition setValue: [NSNumber numberWithInteger:oldSystemIndex] forKeyPath: @"system"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) updateStudyFromVersion: (NSInteger __unused) oldVersion oldStudy: (StudyEntity*) oldStudy newStudy: (StudyEntity*) newStudy
	{
		
	[newStudy setValue: [oldStudy valueForKeyPath: @"dateOfStudy"] forKeyPath: @"dateOfStudy"];
	[newStudy setValue: [oldStudy valueForKeyPath: @"modality"] forKeyPath: @"modality"];
	[newStudy setValue: [oldStudy valueForKeyPath: @"studyDescription"] forKeyPath: @"studyDescription"];
	[newStudy setValue: [oldStudy valueForKeyPath: @"studyRegion"] forKeyPath: @"studyRegion"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) updateImageFromVersion: (NSInteger) oldVersion oldImage: (ImageEntity*) oldImage newImage: (ImageEntity*) newImage
	{
	[newImage setValue: [oldImage valueForKeyPath: @"imageData"] forKeyPath: @"imageData"];

	if (oldVersion==1)
		{
		[newImage setValue: [NSNumber numberWithInteger:0] forKeyPath: @"order"];
		[newImage setValue: @"" forKeyPath: @"imageDescription"];
		[newImage setValue: @"" forKeyPath: @"fileName"];
		}
		
	if (oldVersion==2)
		{
		[newImage setValue: [oldImage valueForKeyPath: @"order"] forKeyPath: @"order"];
		[newImage setValue: [oldImage valueForKeyPath: @"imageDescription"] forKeyPath: @"imageDescription"];
		[newImage setValue: [oldImage valueForKeyPath: @"fileName"] forKeyPath: @"fileName"];
		}
	}

	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) updateFileFromVersion: (NSInteger) oldVersion oldFile: (FileEntity*) oldFile newFile: (FileEntity*) newFile
	{
	[newFile setValue: [oldFile valueForKeyPath: @"fileData"] forKeyPath: @"fileData"];

	if (oldVersion==1)
		{
		[newFile setValue: [NSNumber numberWithInteger:0] forKeyPath: @"order"];
		[newFile setValue: @"" forKeyPath: @"fileDescription"];
		[newFile setValue: @"" forKeyPath: @"fileName"];
		}
		
	if (oldVersion==2)
		{
		[newFile setValue:  [oldFile valueForKeyPath: @"order"] forKeyPath: @"order"];
		[newFile setValue: [oldFile valueForKeyPath: @"fileDescription"] forKeyPath: @"fileDescription"];
		[newFile setValue: [oldFile valueForKeyPath: @"fileName"] forKeyPath: @"fileName"];
		}
	}
	

@end

