/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/



#import "ConditionEntity.h"


@interface MIRCXMLDocument : NSXMLDocument

- (void) addStandardInformationTo: (NSXMLElement*) root;
- (void) addTitleInformationTo: (NSXMLElement*) root withTitle: (NSString*) title;
- (void) addAuthorInformationTo: (NSXMLElement*) root;

- (void) addAbstractTo: (NSXMLElement*) root withDetail: (NSString*) abstractDetail;
- (void) addHistoryTo: (NSXMLElement*) root withCondition: (ConditionEntity*) condition;
- (void) addFindingsTo: (NSXMLElement*) root withDetail: (NSString*) findingsDetail;
- (void) addDiscussionTo: (NSXMLElement*) root withDetail: (NSString*) discussionDetail;
- (void) addNotesTo: (NSXMLElement*) root withDetail: (NSString*) notesDetail;

- (void) addSystemTo: (NSXMLElement*) root withDetail: (NSString*) systemString;
- (void) addPathologyTo: (NSXMLElement*) root withDetail: (NSString*) pathologyString;
- (void) addAnatomyTo: (NSXMLElement*) root withDetail: (NSString*) anatomyString;
- (void) addCategoryTo: (NSXMLElement*) root withCondition: (ConditionEntity*) condition;

- (void) addImageTo: (NSXMLElement*) imageRoot withName: (NSString*) imageName withModality: (NSString*) modalityString;

@end
