/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "AddOneTransformer.h"
#import "AgeTransformer.h"
#import "ApplicationController.h"
#import "CaseSingularPluralTransformer.h"
#import "DataToImageTransformer.h"
#import "ImagesSelectedTransformer.h"
#import "IsSingleTransformer.h"
#import "NSImage+Additions.h"
#import "OsirixXMLRPC.h"
#import "PatientEntity.h"
#import "PatientSingularPluralTransformer.h"
#import "ReturnToSpacesTransformer.h"
#import "StudiesAvailableTransformer.h"
#import "SuppressZeroTransformer.h"
#import "VersionManager.h"
#import "XphileDocument.h"
#import "XphileDocument+Debug.h"



@implementation ApplicationController


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Initialisation and Termination

+ (void) initialize
	{

// Transformers.

	id transformer=[[[AddOneTransformer alloc] init] autorelease];
	[NSValueTransformer setValueTransformer: transformer forName: @"AddOneTransformer"]; 

	transformer=[[[SuppressZeroTransformer alloc] init] autorelease];
	[NSValueTransformer setValueTransformer: transformer forName: @"SuppressZeroTransformer"]; 

	transformer=[[[IsSingleTransformer alloc] init] autorelease];
	[NSValueTransformer setValueTransformer: transformer forName: @"IsSingleTransformer"]; 

	transformer=[[[CaseSingularPluralTransformer alloc] init] autorelease];
	[NSValueTransformer setValueTransformer: transformer forName: @"CaseSingularPluralTransformer"]; 

	transformer=[[[ImagesSelectedTransformer alloc] init] autorelease];
	[NSValueTransformer setValueTransformer: transformer forName: @"ImagesSelectedTransformer"]; 

	transformer=[[[PatientSingularPluralTransformer alloc] init] autorelease];
	[NSValueTransformer setValueTransformer: transformer forName: @"PatientSingularPluralTransformer"]; 

	transformer=[[[ReturnToSpacesTransformer alloc] init] autorelease];
	[NSValueTransformer setValueTransformer: transformer forName: @"ReturnToSpacesTransformer"];

	transformer=[[[StudiesAvailableTransformer alloc] init] autorelease];
	[NSValueTransformer setValueTransformer: transformer forName: @"StudiesAvailableTransformer"]; 

	transformer=[[[AgeTransformer alloc] init] autorelease];
	[NSValueTransformer setValueTransformer: transformer forName: @"AgeTransformer"]; 

	transformer=[[[TimeSinceTransformer alloc] init] autorelease];
	[NSValueTransformer setValueTransformer: transformer forName: @"TimeSinceTransformer"]; 

	transformer=[[[TimeCourseTransformer alloc] init] autorelease];
	[NSValueTransformer setValueTransformer: transformer forName: @"TimeCourseTransformer"]; 

	transformer=[[[DataToImageTransformer alloc] init] autorelease];
    [NSValueTransformer setValueTransformer: transformer forName: @"DataToImageTransformer"];

// Set up the user defaults.

	NSMutableDictionary *myDefaults=[NSMutableDictionary dictionary];ASSERTNOTNIL(myDefaults);

// General preferences.

	[myDefaults setObject: @NO forKey: OpenRecentDocumentAtStartupKey];
	[myDefaults setObject: @NO forKey: AutomaticallyCheckUpdatesKey];
	
	[myDefaults setObject: @"/Applications/Preview.app" forKey: ImageEditorPathKey];
	[myDefaults setObject: @"/Applications/QuickTime Player.app" forKey: MovieEditorPathKey];
	[myDefaults setObject: @YES forKey: AutomaticallyPlayMoviesKey];
	[myDefaults setObject: @NO forKey: FullScreenBlankImageKey];
	
	[myDefaults setObject: [NSDate date] forKey: UpdateLastCheckedKey];
	[myDefaults setObject: @0 forKey: NumberOfLaunchesKey];

	[myDefaults setObject: @0.8 forKey: JPEGCompressionFactorKey];
	[myDefaults setObject: @(2*1024*1024) forKey: MaximumImageSizeKey];
	[myDefaults setObject: @3 forKey: MovieFrameRateKey];

// Categories.

	NSArray *genderArray=[NSArray arrayWithObjects: @"-", MaleString, FemaleString, nil];ASSERTNOTNIL(genderArray);
	[myDefaults setObject: genderArray forKey: GenderArrayKey];

	NSArray *modalityArray=[NSArray arrayWithObjects: @"-", CRString, CTString, IMString, MGString, MRString, NMString, RFString, USString, XAString, nil];ASSERTNOTNIL(modalityArray);
	[myDefaults setObject: modalityArray forKey: ModalityArrayKey];

	NSArray *systemArray=[NSArray arrayWithObjects: AnyString, BreastString, CardiacString, EndocrineString, GastrointestinalString, GenitalTractFemaleString, GenitalTractMaleString,
	  HaematopoieticString, HeadAndNeckString, HepatobiliaryString, MusculoskeletalString, NeurologicalString, ObstetricString, RespiratoryString, UrinaryTractString, VascularString, nil];
	ASSERTNOTNIL(systemArray);
	[myDefaults setObject: systemArray forKey: SystemArrayKey];

	NSArray *pathologyArray=[NSArray arrayWithObjects: AnyString, ArtifactString, AutoimmuneString, CongenitalString, DevelopmentalString, DegenerativeString, DrugRelatedString, EndocrineString,
	  IatrogenicString, IdiopathicString, InfectiveString, InflammatoryString, MetabolicString, NeoplasticString, NeurogenicString, NormalString, TraumaticString, VascularString, nil];ASSERTNOTNIL(pathologyArray);
	[myDefaults setObject: pathologyArray forKey: PathologyArrayKey];

	NSArray *regionArray=[NSArray arrayWithObjects: AnyString, HeadString, NeckString, SpineString, ChestString, AbdomenString, PelvisString, UpperLimbString, LowerLimbString, nil];
	ASSERTNOTNIL(regionArray);
	[myDefaults setObject: regionArray forKey: RegionArrayKey];

// Sites.
// For arrays of dictionaries, make sure "Handle as compound content" is set in the array controller.
	
	NSMutableArray *siteArray=[NSMutableArray arrayWithCapacity: 0];ASSERTNOTNIL(siteArray);
	NSDictionary *siteDict=[NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"AJR", @"http://www.ajronline.org", nil]
	  forKeys: [NSArray arrayWithObjects: @"label", @"address", nil]];ASSERTNOTNIL(siteDict);
	[siteArray addObject: siteDict];
	siteDict=[NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"BJR", @"http://bjr.birjournals.org/", nil]
	  forKeys: [NSArray arrayWithObjects: @"label", @"address", nil]];ASSERTNOTNIL(siteDict);
	[siteArray addObject: siteDict];
	siteDict=[NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"eMedicine (Radiology)", @"http://www.emedicine.com/radio/topiclist.htm", nil]
	  forKeys: [NSArray arrayWithObjects: @"label", @"address", nil]];ASSERTNOTNIL(siteDict);
	[siteArray addObject: siteDict];
	siteDict=[NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"Medline", @"http://www.ncbi.nlm.nih.gov/entrez/", nil]
	  forKeys: [NSArray arrayWithObjects: @"label", @"address", nil]];ASSERTNOTNIL(siteDict);
	[siteArray addObject: siteDict];
	siteDict=[NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"Pediatric Radiology", @"http://link.springer.com/journal/247", nil]
	  forKeys: [NSArray arrayWithObjects: @"label", @"address", nil]];ASSERTNOTNIL(siteDict);
	[siteArray addObject: siteDict];
	siteDict=[NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"Radiographics", @"http://radiographics.rsnajnls.org/", nil]
	  forKeys: [NSArray arrayWithObjects: @"label", @"address", nil]];ASSERTNOTNIL(siteDict);
	[siteArray addObject: siteDict];
	siteDict=[NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"Radiology", @"http://radiology.rsnajnls.org/", nil]
	  forKeys: [NSArray arrayWithObjects: @"label", @"address", nil]];ASSERTNOTNIL(siteDict);
	[siteArray addObject: siteDict];
	siteDict=[NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"Radiopedia", @"http://radiopaedia.org/", nil]
	  forKeys: [NSArray arrayWithObjects: @"label", @"address", nil]];ASSERTNOTNIL(siteDict);
	[siteArray addObject: siteDict];
	siteDict=[NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"RSNA MIRC", @"http://mircwiki.rsna.org", nil]
	  forKeys: [NSArray arrayWithObjects: @"label", @"address", nil]];ASSERTNOTNIL(siteDict);
	[siteArray addObject: siteDict];
	siteDict=[NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"Searching Radiology", @"http://www.searchingradiology.com/", nil]
	  forKeys: [NSArray arrayWithObjects: @"label", @"address", nil]];ASSERTNOTNIL(siteDict);
	[siteArray addObject: siteDict];
	[myDefaults setObject: siteArray forKey: @"SitesArray"];

// MIRC author settings.
	
	[myDefaults setObject: @"" forKey: MIRCAuthorKey];
	[myDefaults setObject: @"" forKey: MIRCAffiliationKey];
	[myDefaults setObject: @"" forKey: MIRCContactKey];

// MIRC server settings.
	
	[myDefaults setObject: @"" forKey: MIRCIPAddressKey];
	[myDefaults setObject: @"" forKey: MIRCPortNumberKey];
	[myDefaults setObject: @"" forKey: MIRCStorageServiceKey];
	[myDefaults setObject: @"" forKey: MIRCUserNameKey];
	[myDefaults setObject: @"" forKey: MIRCPasswordKey];
	[myDefaults setObject: @"50" forKey: MIRCQueryMaximumResultsKey];

// MIRC image settings.
	
	[myDefaults setObject: [NSNumber numberWithInteger:800] forKey: MIRCImageWidthKey];
	[myDefaults setObject: [NSNumber numberWithInteger:600] forKey: MIRCImageHeightKey];

// OsiriX XML-RPC.
// For arrays of dictionaries, make sure "Handle as compound content" is set in the array controller.

	NSMutableArray *osirixXMLArray=[NSMutableArray arrayWithCapacity: 0];ASSERTNOTNIL(osirixXMLArray);
	NSDictionary *osirixXMLDict=[NSDictionary dictionaryWithObjectsAndKeys: @"127.0.0.1", OsirixXMLRPCAddressKey, @"8080", OsirixXMLRPCPortKey,
	  [NSNumber numberWithBool: YES], OsirixXMLRPCActiveKey, nil];ASSERTNOTNIL(osirixXMLDict);
	[osirixXMLArray addObject: osirixXMLDict];
	[myDefaults setObject: osirixXMLArray forKey: @"OsirixXMLArray"];

	[myDefaults setObject: @"YES" forKey: SetOsirixToFrontOnOpenKey];

// Keynote export settings.

	[myDefaults setObject: @"YES" forKey: IncludeConditionSlidesKey];
	[myDefaults setObject: @"NO" forKey: IncludeStudySlidesKey];

// Write out the dictionary to the defaults database.

	[[NSUserDefaults standardUserDefaults] registerDefaults: myDefaults];
	[[NSUserDefaultsController sharedUserDefaultsController] setInitialValues: myDefaults];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) dealloc
	{
	[[NSNotificationCenter defaultCenter] removeObserver: self name: VersionCheckSuccessfulNotification object: nil];
	[[NSNotificationCenter defaultCenter] removeObserver: self name: VersionCheckUnsuccessfulNotification object: nil];

	[super dealloc];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) awakeFromNib
	{

// Set up OsiriX XML-RPC table default sorting.

	NSSortDescriptor *sortDescriptor=[[[NSSortDescriptor alloc] initWithKey: OsirixXMLRPCAddressKey ascending: YES] autorelease];ASSERTNOTNIL(sortDescriptor);
	[osirixXMLRPCTable setSortDescriptors: [NSArray arrayWithObject: sortDescriptor]];

#ifndef Debug
	return;
#else
// Set up the Debug menu.

	NSMenu *mainMenu=[NSApp mainMenu];
	NSMenu *debugMenu=[[[NSMenu allocWithZone: [NSMenu menuZone]] initWithTitle: @"Debug"] autorelease];
	NSMenuItem *debugItem=(NSMenuItem*) [mainMenu addItemWithTitle: @"Debug" action: nil keyEquivalent: @""];
	[debugItem setSubmenu: debugMenu];
	
	[debugMenu addItemWithTitle: @"Populate with Random Patients" action: @selector(debugBatchNewPatient:) keyEquivalent: @""];
	[debugMenu addItemWithTitle: @"Import from FileMaker" action: @selector(doImportFromFileMaker:) keyEquivalent: @""];
	[debugMenu addItem: [NSMenuItem separatorItem]];
	[debugMenu addItemWithTitle: @"Log Responder Chain" action: @selector(debugResponderChain:) keyEquivalent: @""];
	[debugMenu addItemWithTitle: @"Remove Orphans" action: @selector(debugRemoveOrphans:) keyEquivalent: @""];
	[debugMenu addItemWithTitle: @"Show BLOBs > 1 MB" action: @selector(debugShowLargeBlobs:) keyEquivalent: @""];
	[debugMenu addItem: [NSMenuItem separatorItem]];
	[debugMenu addItemWithTitle: @"Redraw Album Table" action: @selector(debugRedrawAlbumTable:) keyEquivalent: @""];
#endif
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) applicationWillFinishLaunching: (NSNotification* __unused) aNotification
	{	
	preferencesWindowController=nil;
	
// Sent before application: openFile notification.

	hasOpenedFile=NO;
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) application: (NSApplication* __unused) theApplication openFile: (NSString*) filename
	{
	hasOpenedFile=YES;						// Don't auto open file in applicationDidFinishLaunching.
	BOOL __block success=NO;
	
	[[NSDocumentController sharedDocumentController] openDocumentWithContentsOfURL: [NSURL fileURLWithPath: filename] display: YES
	  completionHandler: ^(NSDocument * document, BOOL __unused wasOpen, NSError __unused *error)
		{
		if (document != nil)
			{
			success=YES;
			}
		}];
	
	return success;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

/// Sent after application:openFile notification.

- (void) applicationDidFinishLaunching: (NSNotification* __unused) aNotification
	{
	if ([[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"] rangeOfString: @"a"].length>0)
		{
		[[NSApp dockTile] setBadgeLabel: @"α"];
		}
	if ([[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"] rangeOfString: @"b"].length>0)
		{
		[[NSApp dockTile] setBadgeLabel: @"β"];
		}
	[[NSApp dockTile] performSelector: @selector(setBadgeLabel:) withObject: @"" afterDelay: 30];

// Set up links for OsiriX plugins.

	[self linkToOsirixPlugins];
	
	NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];ASSERTNOTNIL(defaults);
	NSInteger numLaunches=[defaults integerForKey: NumberOfLaunchesKey];
	
// On the 20th launch, advise about autoUpdating.

	if (numLaunches==20)
		{
		if (![[NSUserDefaults standardUserDefaults] boolForKey: AutomaticallyCheckUpdatesKey])
			{
			if ([[NSAlert alertWithMessageText: AutomaticallyCheckForUpdatesString defaultButton: CheckString alternateButton: DontCheckString otherButton: @""
			  informativeTextWithFormat: AutomaticallyCheckForUpdatesInfoString] runModal]==NSOKButton)
				{
				[defaults setBool: YES forKey: AutomaticallyCheckUpdatesKey];
				}
			}
		}
		
// On the 10th launch advise if launching from disk image.

	if (numLaunches==10)
		{
		NSString *diPath=[[NSBundle mainBundle] bundlePath];ASSERTNOTNIL(diPath);
		diPath=[diPath stringByDeletingLastPathComponent];
		diPath=[diPath stringByAppendingPathComponent: @".RunningFromDiskImage"];

		if ([[NSFileManager defaultManager] fileExistsAtPath: diPath])
			{
			[[NSAlert alertWithMessageText: RunningFromDiskImageString defaultButton: ContinueString alternateButton: @"" otherButton: @"" informativeTextWithFormat: RunningFromDiskImageInfoString] runModal];
			}
		}
				
// Open recent document after short delay.

	[self performSelector: @selector(openRecentDocument) withObject: nil afterDelay: 0.0];
	[self performSelector: @selector(autoCheckForNewVersion) withObject: nil afterDelay: 0.1];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) openRecentDocument
	{
	
// Open most recent document if preference and !shift key and haven't already opened document from Finder.

	BOOL isShiftKey=(([[NSApp currentEvent] modifierFlags] & NSShiftKeyMask) != 0);
	if ([[[NSUserDefaultsController sharedUserDefaultsController] defaults] boolForKey: OpenRecentDocumentAtStartupKey] && !isShiftKey && !hasOpenedFile)
		{
		NSArray *recentDocArray=[[NSDocumentController sharedDocumentController] recentDocumentURLs];ASSERTNOTNIL(recentDocArray);
		if ([recentDocArray count]!=0)
			{
			[aboutWindowController showStatusString: OpeningMostRecentDocumentString];
			NSURL *recentDocURL=[recentDocArray objectAtIndex: 0];ASSERTNOTNIL(recentDocURL);
			[[NSDocumentController sharedDocumentController] openDocumentWithContentsOfURL: recentDocURL display: YES
			  completionHandler: ^(NSDocument __unused *document, BOOL __unused wasOpen, NSError __unused *error) {}];
			}
		}
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) applicationWillTerminate: (NSNotification* __unused) aNotification
	{
	
// Clear the drag pasteboard.

	NSPasteboard *dragPasteBoard=[NSPasteboard pasteboardWithName: NSDragPboard];ASSERTNOTNIL(dragPasteBoard);
	[dragPasteBoard declareTypes: [NSArray array] owner: self];

// Update number of launches.

	NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];ASSERTNOTNIL(defaults);
	NSInteger numLaunches=[defaults integerForKey: NumberOfLaunchesKey];
	[defaults setInteger: ++numLaunches forKey: NumberOfLaunchesKey];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) applicationShouldOpenUntitledFile: (NSApplication* __unused) sender
	{
	return (![[NSUserDefaults standardUserDefaults] boolForKey: OpenRecentDocumentAtStartupKey]);
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) menuNeedsUpdate: (NSMenu*) menu
	{

// Sites menu updating.
// Remove all existing menu items.
	
	NSUInteger count=[menu numberOfItems];
	NSUInteger index;
	for (index=0; index<count; index++)
		[menu removeItemAtIndex: 0];

// Loop through and add new items.
	
	NSArray *sitesArray=[sitesArrayController arrangedObjects];ASSERTNOTNIL(sitesArray);
	count=[sitesArray count];
	for (index=0; index<count; index++)
		{
		NSDictionary *dict=[sitesArray objectAtIndex: index];ASSERTNOTNIL(dict);
		NSString *label=[dict objectForKey: @"label"];
		if (label==nil)
			label=@"-";
		[menu addItemWithTitle: label action: @selector(doSitesMenu:) keyEquivalent: @""];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Xphile Menu

- (IBAction) doSplashAboutBox: (id) sender
	{
	if (!aboutWindowController)
		{
		aboutWindowController=[[AboutController alloc] init];ASSERTNOTNIL(aboutWindowController);
		}

	if (sender==self)
		{
		[aboutWindowController window];
		}
	[aboutWindowController showWindow: self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doPreferences: (id __unused) sender
	{
	if (preferencesWindowController==nil)
		{
		preferencesWindowController=[[NSWindowController alloc] initWithWindowNibName: @"Preferences"];ASSERTNOTNIL(preferencesWindowController);
		}
	[[preferencesWindowController window] makeKeyAndOrderFront: self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Version Check

- (void) autoCheckForNewVersion
	{
	if ([[NSUserDefaults standardUserDefaults] boolForKey: AutomaticallyCheckUpdatesKey])
		{
		NSDate *lastCheckedVersionDate=[[NSUserDefaults standardUserDefaults] objectForKey: UpdateLastCheckedKey];

		NSInteger versionCheckInterval=7;							// Release version checked weekly.
		if ([[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"] rangeOfString: @"a"].length>0)
			{
			versionCheckInterval=1;									// Alpha checked daily.
			}
		if ([[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"] rangeOfString: @"b"].length>0)
			{
			versionCheckInterval=1;									// Beta checked daily.
			}

		if (fabs([lastCheckedVersionDate timeIntervalSinceNow]) > versionCheckInterval*(60*60*24))
			{
			[self doCheckForNewVersion: self];
			}
		}
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doCheckForNewVersion: (id) sender
	{
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(versionCheckSuccessful:) name: VersionCheckSuccessfulNotification object: nil];
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(versionCheckUnsuccessful:) name: VersionCheckUnsuccessfulNotification object: nil];

	NSURL *versionsURL=[NSURL URLWithString: @"http://versions.ddp.org.nz/xphile.xml"];ASSERTNOTNIL(versionsURL);
	if (sender==self)
		{
		[aboutWindowController showStatusString: CheckingForNewVersionString];
		[[VersionManager versionManager] checkVersionFromURL: versionsURL verbose: NO];
		}
	else
		{
		[[VersionManager versionManager] checkVersionFromURL: versionsURL verbose: YES];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) versionCheckSuccessful: (NSNotification* __unused) notification
	{	
	[aboutWindowController showStatusString: VersionCheckSuccessfulString];
	[[NSUserDefaults standardUserDefaults] setObject: [NSDate date] forKey: UpdateLastCheckedKey];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) versionCheckUnsuccessful: (NSNotification* __unused) notification
	{
	[aboutWindowController showStatusString: VersionCheckFailedString];
	}



//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Editors

- (IBAction) doChooseImageEditor: (id __unused) sender
	{
	
// Choose application.

	NSOpenPanel *openPanel=[NSOpenPanel openPanel];ASSERTNOTNIL(openPanel);
	[openPanel setPrompt: ChooseString];
	[openPanel setTitle: ChooseImageEditorString];
	[openPanel setCanChooseDirectories: NO];
    [openPanel setAllowedFileTypes: [NSArray arrayWithObject: @"app"]];
	if ([openPanel runModal]==NSCancelButton)
		{
		return;
		}
	
	[[NSUserDefaults standardUserDefaults] setObject: [[openPanel URL] path] forKey: ImageEditorPathKey];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doChooseMovieEditor: (id __unused) sender
	{
	
// Choose application.

	NSOpenPanel *openPanel=[NSOpenPanel openPanel];ASSERTNOTNIL(openPanel);
	[openPanel setPrompt: ChooseString];
	[openPanel setTitle: ChooseMovieEditorString];
	[openPanel setCanChooseDirectories: NO];
	[openPanel setAllowedFileTypes: [NSArray arrayWithObject: @"app"]];
	if ([openPanel runModal]==NSCancelButton)
		{
		return;
		}
	
	[[NSUserDefaults standardUserDefaults] setObject: [[openPanel URL] path] forKey: MovieEditorPathKey];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - MIRC

- (IBAction) doMircQuery: (id __unused) sender
	{
	if (!mircQueryController)
		{
		mircQueryController=[[MIRCQueryController alloc] init];ASSERTNOTNIL(mircQueryController);
		}
	[mircQueryController showWindow: self];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doMircBrowse: (id __unused) sender
	{
	
// Set up the browse URL.

	NSString *mircURLString=[NSString stringWithFormat: @"http://%@:%@/mirc/query",
	  [[NSUserDefaults standardUserDefaults] valueForKey: MIRCIPAddressKey],
	  [[NSUserDefaults standardUserDefaults] valueForKey: MIRCPortNumberKey]];
	ASSERTNOTNIL(mircURLString);
	
	[[NSWorkspace sharedWorkspace] openURL: [NSURL URLWithString: mircURLString]];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Sites Menu

- (IBAction) doSitesMenu: (id) sender
	{
	NSInteger index=[[sender menu] indexOfItem: sender];
	NSArray *sitesArray=[sitesArrayController arrangedObjects];
	if (sitesArray==nil)
		return;
		
	NSDictionary *dict=[sitesArray objectAtIndex: index];ASSERTNOTNIL(dict);
	NSString *address=[dict objectForKey: @"address"];
	if (address==nil)
		return;
	
	NSURL *url=[NSURL URLWithString: address];ASSERTNOTNIL(url);
	[[NSWorkspace sharedWorkspace] openURL: url];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - OsiriX Plugins

- (void) linkToOsirixPlugins
	{
	BOOL osirixOrHorosExists=NO;
		
	BOOL isDirectory;
	NSString *osirixAppSupportFolder=[@"~/Library/Application Support/Horos" stringByExpandingTildeInPath];ASSERTNOTNIL(osirixAppSupportFolder);
	if ([[NSFileManager defaultManager] fileExistsAtPath: osirixAppSupportFolder isDirectory: &isDirectory] && isDirectory)
		{
		osirixOrHorosExists=YES;
		}
	else
		{
		osirixAppSupportFolder=[@"~/Library/Application Support/OsiriX" stringByExpandingTildeInPath];ASSERTNOTNIL(osirixAppSupportFolder);
		if ([[NSFileManager defaultManager] fileExistsAtPath: osirixAppSupportFolder isDirectory: &isDirectory] && isDirectory)
			{
			osirixOrHorosExists=YES;
			}
		}
	
	if (!osirixOrHorosExists)
		{
		return;
		}

// Get plug-in path and ensure folder exists.

	NSString *osirixPluginsFolder=[osirixAppSupportFolder stringByAppendingPathComponent: @"Plugins"];ASSERTNOTNIL(osirixPluginsFolder);
	if (![[NSFileManager defaultManager] fileExistsAtPath: osirixPluginsFolder isDirectory: &isDirectory] || !isDirectory)
		{
		NSLog(@"Create plugins folder at %@", osirixPluginsFolder);
		NSError *error;
		if (! [[NSFileManager defaultManager] createDirectoryAtPath: osirixPluginsFolder withIntermediateDirectories: YES attributes: nil error: &error])
			{
			NSLog(@"Failed to create plugins folder: %@", [error localizedDescription]);
			return;
			}
		}
	
// Delete currently linked osirix plugins.

	NSString *aPluginPath=[osirixPluginsFolder stringByAppendingFormat: @"/%@.%@", @"ExportToXphileDatabase", @"osirixplugin"];ASSERTNOTNIL(aPluginPath);
	if ([[NSFileManager defaultManager] fileExistsAtPath: aPluginPath])
		{
		NSError *error=nil;
		if (![[NSFileManager defaultManager] removeItemAtPath: aPluginPath error: &error])
			{
			NSLog(@"%@", [error description]);
			}
		}
	aPluginPath=[osirixPluginsFolder stringByAppendingFormat: @"/%@.%@", @"ExportToXphileStudy", @"osirixplugin"];ASSERTNOTNIL(aPluginPath);
	if ([[NSFileManager defaultManager] fileExistsAtPath: aPluginPath])
		{
		NSError *error=nil;
		if (![[NSFileManager defaultManager] removeItemAtPath: aPluginPath error: &error])
			{
			NSLog(@"%@", [error description]);
			}
		}

// Link Database OsiriX plugin.
		
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);
	NSTask *linkTask=[[[NSTask alloc] init] autorelease];ASSERTNOTNIL(linkTask);

	NSString *exportFromDatabasePath=[[NSBundle mainBundle] pathForResource: @"ExportToXphileDatabase" ofType: @"osirixplugin" inDirectory: @"OsiriX Plugins"];
	ASSERTNOTNIL(exportFromDatabasePath);
	[linkTask setLaunchPath: @"/bin/ln"];
	[linkTask setArguments: @[@"-sf", exportFromDatabasePath, osirixPluginsFolder]];
	[linkTask setStandardInput: [NSPipe pipe]];										// See http://stackoverflow.com/questions/3444178/nstask-nspipe-objective-c-command-line-help
	
	[linkTask launch];
	while ([linkTask isRunning]) [NSThread sleepForTimeInterval: 0.01];
	[linkTask interrupt];
	
	[pool release];

// Link Study OsiriX plugin.
		
	pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);
	linkTask=[[[NSTask alloc] init] autorelease];ASSERTNOTNIL(linkTask);

	NSString *exportFromStudyPath=[[NSBundle mainBundle] pathForResource: @"ExportToXphileStudy" ofType: @"osirixplugin" inDirectory: @"OsiriX Plugins"];
	ASSERTNOTNIL(exportFromStudyPath);
	[linkTask setLaunchPath: @"/bin/ln"];
	[linkTask setArguments: @[@"-sf", exportFromStudyPath, osirixPluginsFolder]];
	[linkTask setStandardInput: [NSPipe pipe]];										// See http://stackoverflow.com/questions/3444178/nstask-nspipe-objective-c-command-line-help
	
	[linkTask launch];
	while ([linkTask isRunning]) [NSThread sleepForTimeInterval: 0.01];
	[linkTask interrupt];
	
	[pool release];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Help Menu

- (IBAction) doHelp: (id __unused) sender
	{
	NSString *path=[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: UserGuideString];ASSERTNOTNIL(path);
	path=[path stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
	[[NSWorkspace sharedWorkspace] openURL: [NSURL URLWithString: [NSString stringWithFormat: @"file://%@", path]]];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doReleaseNotes: (id __unused) sender
	{
	NSString *path=[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: @"Release Notes.html"];ASSERTNOTNIL(path);
	path=[path stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
	[[NSWorkspace sharedWorkspace] openURL: [NSURL URLWithString: [NSString stringWithFormat: @"file://%@", path]]];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doOpenExample: (id __unused) sender
	{
	NSString *path=[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: @"Example Library.xphile"];ASSERTNOTNIL(path);
	NSURL *url=[NSURL fileURLWithPath: path];ASSERTNOTNIL(url);
	
	[[NSDocumentController sharedDocumentController] openDocumentWithContentsOfURL: url display: YES
	  completionHandler: ^(NSDocument *document, BOOL __unused wasOpen, NSError *error)
		{
		if (document == nil) NSLog(@"Error: %@", [error description]);
		}];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doEmailDeveloper: (id __unused) sender
	{
	NSString *str=[NSString stringWithFormat: @"mailto:xphile@ddp.org.nz?subject=Technical Support Query for %@ %@",
	  [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleName"],
	  [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"]];ASSERTNOTNIL(str);
	str=[str stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
	[[NSWorkspace sharedWorkspace] openURL: [NSURL URLWithString: str]];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doGoToWebsite: (id __unused) sender
	{
	NSString *str=@"http://xphile.ddp.org.nz";ASSERTNOTNIL(str);
	[[NSWorkspace sharedWorkspace] openURL: [NSURL URLWithString: str]];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Menu Nib String Localisations

- (NSString*) minimiseMenuTitle
	{
	return MinimiseString;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
	
- (NSString*) anonymiseAllMenuTitle
	{
	return AnonymiseAllString;
	}

	
@end
