/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "AlbumArrayController.h"
#import "AlbumEntity.h"
#import "BrowserTableView.h"
#import "FileEntity.h"
#import "ImageEntity.h"
#import "KeynoteXMLDocument.h"
#import "XphileDocument+Albums.h"
#import "WaitController.h"


@implementation AlbumArrayController


- (void) add: (id) sender
	{
	[super add: sender];

	XphileDocument *myDoc=[[NSDocumentController sharedDocumentController] documentForWindow: [sender window]];ASSERTNOTNIL(myDoc);
	[myDoc performSelector: @selector(editSmartAlbum:) withObject: self afterDelay: 0.0];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Required to Complete Data Source Delegate Informal Protocol

- (NSInteger) numberOfRowsInTableView: (NSTableView* __unused) aTableView
	{	
	return 0;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (id) tableView: (NSTableView* __unused) aTableView objectValueForTableColumn: (NSTableColumn* __unused) aTableColumn row: (NSInteger __unused) rowIndex
	{	
	return nil;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Table Destination Drop Routines

- (NSDragOperation) tableView: (NSTableView*) tv validateDrop: (id <NSDraggingInfo>) info proposedRow: (NSInteger)
  row proposedDropOperation: (NSTableViewDropOperation __unused) operation
	{
	
// Don't valiate if the originating window is different.

	if ([[info draggingSource] window] != [tv window])
		return (NSDragOperationNone);

// Don't valiate a drop onto a smart album.
		
	if (row < (NSInteger) [[self arrangedObjects] count] && row >= 0)
		{
		AlbumEntity *album=[[self arrangedObjects] objectAtIndex: row];
		if (album != nil)
			{
			if ([album isSmartAlbum])
				return (NSDragOperationNone);
			}
		}
		
    return NSDragOperationEvery;    
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) tableView: (NSTableView* __unused) aTableView acceptDrop: (id <NSDraggingInfo>) info row: (NSInteger) rowIndex
  dropOperation: (NSTableViewDropOperation __unused) operation
	{
		
// Get the managed object context.

	NSManagedObjectContext *moc=[self managedObjectContext];ASSERTNOTNIL(moc);

// If we've dropped on a pre-existing album, choose it, else create a new one.

	AlbumEntity *album=nil;
	if (rowIndex > ((NSInteger) [[self arrangedObjects] count]-1) || rowIndex==-1)
		{
		album=[NSEntityDescription insertNewObjectForEntityForName: @"Album" inManagedObjectContext: moc];ASSERTNOTNIL(album);
		}
	else
		{
		album=[[self arrangedObjects] objectAtIndex: rowIndex];ASSERTNOTNIL(album);
		}

// Get the moURIArray from the pasteboard.

    NSPasteboard *pboard=[info draggingPasteboard];ASSERTNOTNIL(pboard);
    NSData *moURIArrayData=[pboard dataForType: InDocumentBrowserDragType];ASSERTNOTNIL(moURIArrayData);	
	NSArray *moURIArray=[NSKeyedUnarchiver unarchiveObjectWithData: moURIArrayData];ASSERTNOTNIL(moURIArray);

// Convert the moURI to an moID and then to its respective managed object.
// Add these sequentially to the new album, by adding the album	to the condition's albums.
	
	NSUInteger index;
	for (index=0; index<[moURIArray count]; index++)
		{
		NSURL *moURI=[moURIArray objectAtIndex: index];ASSERTNOTNIL(moURI);
		NSManagedObjectID *moID=[[moc persistentStoreCoordinator] managedObjectIDForURIRepresentation: moURI];ASSERTNOTNIL(moID);
		NSManagedObject *condition=[moc objectRegisteredForID: moID];ASSERTNOTNIL(condition);
		NSMutableSet *albumsSet=[condition mutableSetValueForKeyPath: @"albums"];ASSERTNOTNIL(albumsSet);
		[albumsSet addObject: album];
		}

	return (YES);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Import / Export Routines

- (BOOL) exportImagesTo: (NSString*) basePath
	{
	BOOL								allSucceeded				=	YES;
	NSSet								*conditions					=	nil;
	

	NSAssert([[self selectedObjects] count]>0, @"No album is selected");

// Get the currently selected album.
// If smart album, use predicate to generate the condition set, otherwise get the set directly.

	AlbumEntity *album=[[self selectedObjects] objectAtIndex: 0];ASSERTNOTNIL(album);
	if ([album isSmartAlbum])
		{
		NSData *predicateData=[album valueForKeyPath: @"itsPredicate"];
		if (predicateData!=nil)
			{
			NSPredicate *smartAlbumPredicate=[NSKeyedUnarchiver unarchiveObjectWithData: predicateData];ASSERTNOTNIL(smartAlbumPredicate);
			NSManagedObjectContext *moc=[self managedObjectContext];ASSERTNOTNIL(moc);
			NSFetchRequest *request=[[[NSFetchRequest alloc] init] autorelease];ASSERTNOTNIL(request);
			NSEntityDescription *entity=[NSEntityDescription entityForName: @"Condition" inManagedObjectContext: moc];ASSERTNOTNIL(entity);
			[request setEntity: entity];
			[request setPredicate: smartAlbumPredicate];
			NSError *error=nil;
			NSArray *array=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(array);
			conditions=[NSSet setWithArray: array];
			}
		}
	else
		{
		conditions=[album valueForKeyPath: @"conditions"];
		}
		
	if (conditions==nil)
		return YES;

	WaitController *waitController=[[WaitController alloc] initWithStatusString: ExportingImagesString];ASSERTNOTNIL(waitController);
	[waitController showWindow: self];			
	[waitController setProgressBarMax: [conditions count]];

// Loop through the conditions.

	NSInteger conditionIndex=0;
	NSEnumerator *conditionEnumerator = [conditions objectEnumerator];ASSERTNOTNIL(conditionEnumerator);

	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);	
	ConditionEntity *aCondition;
	while ((aCondition=[conditionEnumerator nextObject]))
		{
		conditionIndex++;

		[waitController setProgressBarValue: conditionIndex];
		[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];	
		
		NSSet *studies=[aCondition valueForKeyPath: @"studies"];

// Loop through the studies.

		NSEnumerator *studyEnumerator = [studies objectEnumerator];
		StudyEntity *aStudy;
		while ((aStudy=[studyEnumerator nextObject]))
			{
			NSSet *images=[aStudy valueForKeyPath: @"images"];
			if (images)
				{
				NSArray *imagesArray=[NSArray arrayWithArray: [images allObjects]];ASSERTNOTNIL(imagesArray);
				NSSortDescriptor *imageSortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"order" ascending: YES] autorelease];ASSERTNOTNIL(imageSortDescriptor);
				imagesArray=[imagesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: imageSortDescriptor]];

// Loop through the images.

				NSUInteger imageIndex;
				for (imageIndex=0; imageIndex<[imagesArray count]; imageIndex++)
					{
					ImageEntity *anImage=[imagesArray objectAtIndex: imageIndex];ASSERTNOTNIL(anImage);
					[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];

					if (anImage)
						{
						NSString *path=[anImage uniqueFileNameForFolder: basePath];ASSERTNOTNIL(path);
						NSData *data=[anImage valueForKeyPath: @"imageData"];ASSERTNOTNIL(data);
						NSData *rawData=[NSUnarchiver unarchiveObjectWithData: data];ASSERTNOTNIL(rawData);
						[rawData writeToFile: path atomically: YES];
						}
					}
				}

			NSSet *movies=[aStudy valueForKeyPath: @"files"];
			if (movies)
				{
				NSArray *moviesArray=[NSArray arrayWithArray: [movies allObjects]];ASSERTNOTNIL(moviesArray);
				NSSortDescriptor *movieSortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"order" ascending: YES] autorelease];ASSERTNOTNIL(movieSortDescriptor);
				moviesArray=[moviesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: movieSortDescriptor]];

// Loop through the movies.

				NSUInteger movieIndex;
				for (movieIndex=0; movieIndex<[moviesArray count]; movieIndex++)
					{
					FileEntity *aMovie=[moviesArray objectAtIndex: movieIndex];ASSERTNOTNIL(aMovie);
					[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];

					if (aMovie)
						{
						NSString *path=[aMovie uniqueFileNameForFolder: basePath];ASSERTNOTNIL(path);
						NSData *data=[aMovie valueForKeyPath: @"fileData"];ASSERTNOTNIL(data);
						if (![data writeToFile: path atomically: YES])
							allSucceeded=NO;
						}
					}
				}
			}
		
		if (conditionIndex % 10 == 9)
			{
			[pool release];
			pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);	
			}
		}

	[pool release];
	[waitController release];
	return (allSucceeded);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) exportToKeynoteFile: (NSString*) basePath
	{
	BOOL								allSucceeded				=	YES;
	NSArray								*conditionsArray			=	nil;
	

	if ([[self selectedObjects] count]==0)
		return (NO);
	
// Get the currently selected album.
// If smart album, use predicate to generate the condition set, otherwise get the set directly.

	AlbumEntity *album=[[self selectedObjects] objectAtIndex: 0];ASSERTNOTNIL(album);
	if ([album isSmartAlbum])
		{
		NSData *predicateData=[album valueForKeyPath: @"itsPredicate"];
		if (predicateData!=nil)
			{
			NSPredicate *smartAlbumPredicate=[NSKeyedUnarchiver unarchiveObjectWithData: predicateData];ASSERTNOTNIL(smartAlbumPredicate);
			NSManagedObjectContext *moc=[self managedObjectContext];ASSERTNOTNIL(moc);
			NSFetchRequest *request=[[[NSFetchRequest alloc] init] autorelease];
			NSEntityDescription *entity=[NSEntityDescription entityForName: @"Condition" inManagedObjectContext: moc];
			[request setEntity: entity];
			[request setPredicate: smartAlbumPredicate];
			NSError *error=nil;
			conditionsArray=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(conditionsArray);
			}
		}
	else
		{
		NSSet *conditionsSet=[album valueForKeyPath: @"conditions"];ASSERTNOTNIL(conditionsSet);
		conditionsArray=[conditionsSet allObjects];ASSERTNOTNIL(conditionsArray);
		}

	NSSortDescriptor *conditionSortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"conditionDescription" ascending: YES] autorelease];ASSERTNOTNIL(conditionSortDescriptor);
	conditionsArray=[conditionsArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: conditionSortDescriptor]];ASSERTNOTNIL(conditionsArray);

// Create XML document and set up a simple theme.

	NSXMLElement *root=[NSXMLElement elementWithName: @"presentation"];ASSERTNOTNIL(root);
	[root setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"36" forKey: @"version"]];

	KeynoteXMLDocument *xmlDoc = [[KeynoteXMLDocument alloc] initWithRootElement: root];ASSERTNOTNIL(xmlDoc);
	[xmlDoc setVersion: @"1.0"];
	[xmlDoc setCharacterEncoding: @"UTF-8"];
	[xmlDoc addDefaultThemeTo: root];
	
	NSXMLElement *slideListElement=[NSXMLElement elementWithName: @"slide-list"];ASSERTNOTNIL(slideListElement);
	[root addChild: slideListElement];

	NSXMLElement *uiStateElement=[NSXMLElement elementWithName: @"ui-state"];ASSERTNOTNIL(uiStateElement);
	[root addChild: uiStateElement];

	WaitController *waitController=[[WaitController alloc] initWithStatusString: ExportingToKeynoteString];ASSERTNOTNIL(waitController);
	[waitController showWindow: self];			
	[waitController setProgressBarMax: [conditionsArray count]];
	
// Loop through the conditions.

	NSInteger conditionIndex=0;
	NSEnumerator *conditionEnumerator=[conditionsArray objectEnumerator];ASSERTNOTNIL(conditionEnumerator);
	ConditionEntity *aCondition;

	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);	
	while ((aCondition=[conditionEnumerator nextObject]))
		{
		conditionIndex++;

		[waitController setProgressBarValue: conditionIndex];
		[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];	

		if ([[[NSUserDefaultsController sharedUserDefaultsController] defaults] boolForKey: IncludeConditionSlidesKey])
			[xmlDoc addNewConditionSlideToList: slideListElement forCondition: aCondition];

		NSSet *studies=[aCondition valueForKeyPath: @"studies"];
		if (studies)
			{
			NSArray *studiesArray=[NSArray arrayWithArray: [studies allObjects]];ASSERTNOTNIL(studiesArray);
			NSSortDescriptor *studySortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"dateOfStudy" ascending: YES] autorelease];ASSERTNOTNIL(studySortDescriptor);
			studiesArray=[studiesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: studySortDescriptor]];

// Loop through the studies.

			NSUInteger studyIndex;
			for (studyIndex=0; studyIndex<[studiesArray count]; studyIndex++)
				{
				StudyEntity *aStudy=[studiesArray objectAtIndex: studyIndex];ASSERTNOTNIL(aStudy);
				if ([[[NSUserDefaultsController sharedUserDefaultsController] defaults] boolForKey: IncludeStudySlidesKey])
					[xmlDoc addNewStudySlideToList: slideListElement forStudy: aStudy];
				
				NSSet *images=[aStudy valueForKeyPath: @"images"];
				if (images)
					{
					NSArray *imagesArray=[NSArray arrayWithArray: [images allObjects]];ASSERTNOTNIL(imagesArray);
					NSSortDescriptor *imageSortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"order" ascending: YES] autorelease];ASSERTNOTNIL(imageSortDescriptor);
					imagesArray=[imagesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: imageSortDescriptor]];

// Loop through the images.

					NSUInteger imageIndex;
					for (imageIndex=0; imageIndex<[imagesArray count]; imageIndex++)
						{
						ImageEntity *anImage=[imagesArray objectAtIndex: imageIndex];ASSERTNOTNIL(anImage);
						[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];

						if (anImage)
							{
							NSString *path=[anImage uniqueFileNameForFolder: basePath];ASSERTNOTNIL(path);
							NSData *data=[anImage valueForKeyPath: @"imageData"];ASSERTNOTNIL(data);
							NSData *rawData=[NSUnarchiver unarchiveObjectWithData: data];ASSERTNOTNIL(rawData);
							[rawData writeToFile: path atomically: YES];

							NSImage *im=[[[NSImage alloc] initWithData: rawData] autorelease];ASSERTNOTNIL(im);
							[xmlDoc addNewImageSlideToList: slideListElement withName: [path lastPathComponent] nativeSize: [im size]];
							}
						}
					}

				NSSet *movies=[aStudy valueForKeyPath: @"files"];
				if (movies)
					{
					NSArray *moviesArray=[NSArray arrayWithArray: [movies allObjects]];ASSERTNOTNIL(moviesArray);
					NSSortDescriptor *movieSortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"order" ascending: YES] autorelease];ASSERTNOTNIL(movieSortDescriptor);
					moviesArray=[moviesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: movieSortDescriptor]];

// Loop through the movies.

					NSUInteger movieIndex;
					for (movieIndex=0; movieIndex<[moviesArray count]; movieIndex++)
						{
						FileEntity *aMovie=[moviesArray objectAtIndex: movieIndex];ASSERTNOTNIL(aMovie);
						[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];

						if (aMovie)
							{							
							FileEntity *file=[moviesArray objectAtIndex: movieIndex];ASSERTNOTNIL(file);
							NSString *path=[file uniqueFileNameForFolder: basePath];ASSERTNOTNIL(path);
							QTMovie *qtMovie=[file qtMovie];ASSERTNOTNIL(qtMovie);
							NSValue *value=[qtMovie attributeForKey: QTMovieNaturalSizeAttribute];ASSERTNOTNIL(value);
							if ([qtMovie writeToFile: path withAttributes: nil])
								[xmlDoc addNewMovieSlideToList: slideListElement withName: [path lastPathComponent] nativeSize: [value sizeValue]];
							else
								allSucceeded=NO;
							}
						}
					}
				}
			}

		if (conditionIndex % 10 == 9)
			{
			[pool release];
			pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);	
			}
		}
	[pool release];

// Write the XML document.
	
	[[NSFileManager defaultManager] createFileAtPath: [basePath stringByAppendingPathComponent: @"presentation.apxl"]
	  contents: [xmlDoc XMLDataWithOptions: NSXMLNodePrettyPrint] attributes: nil];
	[xmlDoc release];

	[waitController release];
	return (allSucceeded);
	}
	


@end
