/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


/*
NOTES

Set delegate and datasource of tableview to this controller.
Set itsTableView outlet.
Modified from mmalc's controller page at http://homepage.mac.com/mmalc/CocoaExamples/controllers.html
*/


#import "DragAndDropArrayController.h"


@implementation DragAndDropArrayController

- (void) awakeFromNib
	{

// Register for drag and drop

    NSArray *typesArray=[NSArray arrayWithObject: MovedRowsType];ASSERTNOTNIL(typesArray);
	[itsTableView registerForDraggedTypes: typesArray];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) selectAll
	{
	NSUInteger count=[[self arrangedObjects] count];
	NSRange allSelectedRange=NSMakeRange(0, count);
	NSIndexSet *selectionIndexes=[NSIndexSet indexSetWithIndexesInRange: allSelectedRange];ASSERTNOTNIL(selectionIndexes);
	[self setSelectionIndexes: selectionIndexes];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Required to Complete Data Source Delegate Informal Protocol

- (NSInteger) numberOfRowsInTableView: (NSTableView* __unused) aTableView
	{	
	return 0;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (id) tableView: (NSTableView* __unused) aTableView objectValueForTableColumn: (NSTableColumn* __unused) aTableColumn row: (NSInteger __unused) rowIndex
	{
	return nil;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Externally Called

- (BOOL) tableView: (NSTableView* __unused) tv writeRowsWithIndexes: (NSIndexSet*) rowIndexes toPasteboard: (NSPasteboard*) pboard
	{
	ASSERTNOTNIL(rowIndexes);
	ASSERTNOTNIL(pboard);

// Declare pasteboard types.

    NSArray *typesArray=[NSArray arrayWithObject: MovedRowsType];ASSERTNOTNIL(typesArray);
	[pboard declareTypes: typesArray owner: self];

// Add rows array for local move

	[pboard setData: [NSKeyedArchiver archivedDataWithRootObject: rowIndexes] forType: MovedRowsType];

    return YES;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSDragOperation) tableView: (NSTableView*) tv validateDrop: (id <NSDraggingInfo>) info proposedRow: (NSInteger) row
  proposedDropOperation: (NSTableViewDropOperation __unused) operation
	{
	ASSERTNOTNIL(info);

// If drag source is self, it's a move.

	NSDragOperation dragOp=NSDragOperationCopy;
	if ([info draggingSource] == itsTableView)
		dragOp=NSDragOperationMove;

// Put the object at, not over the current row (cf NSTableViewDropOn)

	[tv setDropRow: row dropOperation: NSTableViewDropAbove];

	return (dragOp);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) tableView: (NSTableView* __unused) aTableView acceptDrop: (id <NSDraggingInfo>) info row: (NSInteger) row
  dropOperation: (NSTableViewDropOperation __unused) operation
	{
	ASSERTNOTNIL(info);
	
    if (row<0)
		row=0;

 // If drag source is self, it's a move

	if ([info draggingSource]==itsTableView)
		{
		NSIndexSet *indexSet=[NSKeyedUnarchiver unarchiveObjectWithData: [[info draggingPasteboard] dataForType: MovedRowsType]];ASSERTNOTNIL(indexSet);
		
		[self moveObjectsInArrangedObjectsFromIndexes: indexSet toIndex: row];
		
// Set selected rows to those that were just moved.  Need to work out what moved where to determine proper selection.

		NSInteger rowsAbove=[self rowsAboveRow: row inIndexSet: indexSet];
		
		NSRange range=NSMakeRange(row - rowsAbove, [indexSet count]);
		indexSet=[NSIndexSet indexSetWithIndexesInRange: range];
		[self setSelectionIndexes: indexSet];

		return YES;
		}

	return NO;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Internal Routines

- (void) moveObjectsInArrangedObjectsFromIndexes: (NSIndexSet*) indexSet toIndex: (NSUInteger) insertIndex
	{
    NSArray *objects = [self arrangedObjects];ASSERTNOTNIL(objects);
	NSUInteger index = [indexSet lastIndex];
	
	while (index!=NSNotFound)
		{
		NSInteger removeIndex;
		NSInteger aboveInsertIndexCount = 0;
		
		if (index >= insertIndex)
			{
			removeIndex=index+aboveInsertIndexCount;
			aboveInsertIndexCount++;
			}
		else
			{
			removeIndex=index;
			insertIndex--;
			}

		id object=[objects objectAtIndex: removeIndex];ASSERTNOTNIL(object);
		[self removeObjectAtArrangedObjectIndex: removeIndex];
		[self insertObject: object atArrangedObjectIndex: insertIndex];
		
		index=[indexSet indexLessThanIndex: index];
		}

	[[[NSDocumentController sharedDocumentController] currentDocument] updateChangeCount: NSChangeDone];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSInteger) rowsAboveRow: (NSInteger) row inIndexSet: (NSIndexSet*) indexSet
	{
    NSInteger currentIndex = [indexSet firstIndex];
    NSUInteger i = 0;

    while (currentIndex!=NSNotFound)
		{
		if (currentIndex < row)
			{
			i++;
			}
		currentIndex=[indexSet indexGreaterThanIndex:currentIndex];
		}
		
    return i;
	}


@end
