/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "FileEntity.h"
#import "NSString+Additions.h"


@implementation FileEntity



- (void) setMediaFromMovie: (QTMovie*) qtMovie
	{

// Save a flattened version in the temporary folder.

	NSString *tempPath=[NSString temporaryPathName];ASSERTNOTNIL(tempPath);
	NSDictionary *movieAttributes=@{QTMovieFlatten: @YES};ASSERTNOTNIL(movieAttributes);
	if (![qtMovie writeToFile: tempPath withAttributes: movieAttributes])
		return;

// Read file data directly from the flattened temporary store.

	NSData *data=[NSData dataWithContentsOfFile: tempPath];ASSERTNOTNIL(data);
	[self setValue: data forKey: @"fileData"];
	NSError *error=nil;
	if (![[NSFileManager defaultManager] removeItemAtPath: tempPath error: &error])
		{
		NSLog(@"Error: %@", [error description]);
		}

	[super setMediaFromPath: @""];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setMediaFromPath: (NSString*) path
	{
	NSError							*error;


	QTMovie *qtMovie=[QTMovie movieWithFile: path error: &error];ASSERTNOTNIL(qtMovie);

// Save a flattened version in the temporary folder.

	NSString *tempPath=[NSString temporaryPathName];ASSERTNOTNIL(tempPath);
	NSDictionary *movieAttributes=@{QTMovieFlatten: @YES};ASSERTNOTNIL(movieAttributes);
	if (![qtMovie writeToFile: tempPath withAttributes: movieAttributes])
		{
		return;
		}

// Read file data directly from the flattened temporary store.

	NSData *data=[NSData dataWithContentsOfFile: tempPath];ASSERTNOTNIL(data);
	[self setValue: data forKey: @"fileData"];
	if (![[NSFileManager defaultManager] removeItemAtPath: tempPath error: &error])
		{
		NSLog(@"Error: %@", [error description]);
		}

	[super setMediaFromPath: path];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (QTMovie*) qtMovie
	{
	[self willAccessValueForKey: @"qtMovie"];
	QTMovie *aMovie=[self primitiveValueForKey: @"qtMovie"];
	[self didAccessValueForKey: @"qtMovie"];
	
	if (aMovie==nil)
		{
		NSData *fileData=[self valueForKey: @"fileData"];
		if (fileData != nil)
			{
			NSError *error=nil;
			aMovie=[QTMovie movieWithData: fileData error: &error];
			if (aMovie == nil)
				{
				NSLog(@"Error: %@", [error description]);
				}
			[self setPrimitiveValue: aMovie forKey: @"qtMovie"];
			}
		}
		
	return aMovie;
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSData*) rawData
	{
	NSData *data=[self valueForKeyPath: @"fileData"];ASSERTNOTNIL(data);
	return (data);
	}
	
	
@end
