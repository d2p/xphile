/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "MagnifyPath.h"
#import "NSImageView+Additions.h"



@implementation MagnifyPath

+ (MagnifyPath*) markerForView: (NSView*) aView
	{
	return [[[MagnifyPath alloc] initWithView: aView] autorelease];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) drawSelection
	{	

// Ensure > minimum size.
		
	NSInteger minDimension=3;
	NSImage *im=[targetView image];
	if (selectedRect.size.width > minDimension && selectedRect.size.height > minDimension && im != nil)
		{
		NSRect imageRect=NSMakeRect(0, 0, [im size].width, [im size].height);
		NSRect actualImageRect=[targetView actualRectForImageView];
		
		CGFloat deltaRawToDisplayedX=NSWidth(imageRect) - NSWidth(actualImageRect);
		CGFloat deltaRawToDisplayedY=NSHeight(imageRect) - NSHeight(actualImageRect);

// Only draw magnified version if true image is bigger than that currently being shown.

		if (deltaRawToDisplayedX > 0 || deltaRawToDisplayedY > 0)
			{
			CGFloat rangeX=NSMaxX(actualImageRect) - NSWidth(selectedRect);
			CGFloat scaleX=(selectedRect.origin.x) / rangeX;
			if (scaleX < 0.0) scaleX=0.0;
			if (scaleX > 1.0) scaleX=1.0;

			CGFloat rangeY=NSMaxY(actualImageRect) - NSHeight(selectedRect);
			CGFloat scaleY=(selectedRect.origin.y) / rangeY;
			if (scaleY < 0.0) scaleY=0.0;
			if (scaleY > 1.0) scaleY=1.0;

			CGFloat offsetX=scaleX * deltaRawToDisplayedX - actualImageRect.origin.x;
			CGFloat offsetY=scaleY * deltaRawToDisplayedY - actualImageRect.origin.y;
			NSRect offsetRect=NSOffsetRect(selectedRect, offsetX, offsetY);

			[im drawInRect: selectedRect fromRect: offsetRect operation: NSCompositeCopy fraction: 1.0];
			}
		}

	[super drawSelection];
	}


@end
