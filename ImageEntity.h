/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "ApplicationController.h"
#import "ConditionEntity.h"
#import "CroppableView.h"
#import "MediaEntity.h"


// Localised strings.

#define ImageEntityLocalizedString(str)					NSLocalizedStringFromTable((str), @"ImageEntity", (str))

#define ImageTooLargeString								ImageEntityLocalizedString(@"This image is too large to import into Xphile")



@interface ImageEntity : MediaEntity

@property (nonatomic, readonly, copy) NSString *suffixString;

- (void) setImageFromData: (NSData*) imageData;
@property (nonatomic, copy) NSImage *image;
- (void) reStoreImageAsJPEG;
@property (nonatomic, readonly, copy) NSData *imageDataForMIRC;

- (void) flipHorizontal;
- (void) flipVertical;

- (void) rotateBy: (CGFloat) degrees;
- (void) rotate90;
- (void) rotate180;
- (void) rotate270;

- (void) cropFromView: (CroppableView*) cropView;

@end
