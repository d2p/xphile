/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "MIRCQueryController.h"
#import "AboutController.h"



#define OpenRecentDocumentAtStartupKey							@"OpenRecentDocumentAtStartup"
#define AutomaticallyCheckUpdatesKey							@"AutomaticallyCheckUpdates"
#define UpdateLastCheckedKey									@"UpdateLastChecked"
#define NumberOfLaunchesKey										@"NumberOfLaunches"

#define ImageEditorPathKey										@"ImageEditorPath"
#define MovieEditorPathKey										@"MovieEditorPath"
#define AutomaticallyPlayMoviesKey								@"AutomaticallyPlayMovies"

#define MIRCAuthorKey											@"MIRCAuthor"
#define MIRCAffiliationKey										@"MIRCAffiliation"
#define MIRCContactKey											@"MIRCContact"

#define MIRCIPAddressKey										@"MIRCIPAddress"
#define MIRCPortNumberKey										@"MIRCPortNumber"
#define MIRCStorageServiceKey									@"MIRCStorageService"
#define MIRCUserNameKey											@"MIRCUserName"
#define MIRCPasswordKey											@"MIRCPassword"
#define MIRCQueryMaximumResultsKey								@"QueryMaximumResults"

#define MIRCImageWidthKey										@"MIRCImageWidth"
#define MIRCImageHeightKey										@"MIRCImageHeight"

#define MaximumImageSizeKey										@"MaximumImageSize"
#define MovieFrameRateKey										@"MovieFrameRate"

#define IncludeConditionSlidesKey								@"IncludeConditionSlides"
#define IncludeStudySlidesKey									@"IncludeStudySlides"



// Localised strings.

#define ApplicationControllerLocalizedString(str)				NSLocalizedStringFromTable((str), @"ApplicationController", (str))

#define MinimiseString											ApplicationControllerLocalizedString(@"Minimise")
#define AnonymiseAllString										ApplicationControllerLocalizedString(@"Anonymise All")
#define UserGuideString											ApplicationControllerLocalizedString(@"Xphile User Guide.pdf")

#define AnyString												ApplicationControllerLocalizedString(@"-")

#define BreastString											ApplicationControllerLocalizedString(@"Breast")
#define CardiacString											ApplicationControllerLocalizedString(@"Cardiac")
#define EndocrineString											ApplicationControllerLocalizedString(@"Endocrine")
#define GenitalTractFemaleString								ApplicationControllerLocalizedString(@"Genital Tract (Female)")
#define GenitalTractMaleString									ApplicationControllerLocalizedString(@"Genital Tract (Male)")
#define GastrointestinalString									ApplicationControllerLocalizedString(@"Gastrointestinal")
#define HaematopoieticString									ApplicationControllerLocalizedString(@"Haematopoietic")
#define HeadAndNeckString										ApplicationControllerLocalizedString(@"Head and Neck")
#define HepatobiliaryString										ApplicationControllerLocalizedString(@"Hepatobiliary")
#define MusculoskeletalString									ApplicationControllerLocalizedString(@"Musculoskeletal")
#define NeurologicalString										ApplicationControllerLocalizedString(@"Neurological")
#define ObstetricString											ApplicationControllerLocalizedString(@"Obstetric")
#define RespiratoryString										ApplicationControllerLocalizedString(@"Respiratory")
#define UrinaryTractString										ApplicationControllerLocalizedString(@"Urinary Tract")
#define VascularString											ApplicationControllerLocalizedString(@"Vascular")

#define MaleString												ApplicationControllerLocalizedString(@"Male")
#define FemaleString											ApplicationControllerLocalizedString(@"Female")

#define CRString												ApplicationControllerLocalizedString(@"CR")
#define CTString												ApplicationControllerLocalizedString(@"CT")
#define IMString												ApplicationControllerLocalizedString(@"IM")
#define MGString												ApplicationControllerLocalizedString(@"MG")
#define MRString												ApplicationControllerLocalizedString(@"MR")
#define NMString												ApplicationControllerLocalizedString(@"NM")
#define RFString												ApplicationControllerLocalizedString(@"RF")
#define USString												ApplicationControllerLocalizedString(@"US")
#define XAString												ApplicationControllerLocalizedString(@"XA")

#define ArtifactString											ApplicationControllerLocalizedString(@"Artifact")
#define AutoimmuneString										ApplicationControllerLocalizedString(@"Autoimmune")
#define CongenitalString										ApplicationControllerLocalizedString(@"Congenital")
#define DevelopmentalString										ApplicationControllerLocalizedString(@"Developmental")
#define DegenerativeString										ApplicationControllerLocalizedString(@"Degenerative")
#define DrugRelatedString										ApplicationControllerLocalizedString(@"Drug Related")
#define IatrogenicString										ApplicationControllerLocalizedString(@"Iatrogenic")
#define IdiopathicString										ApplicationControllerLocalizedString(@"Idiopathic")
#define InfectiveString											ApplicationControllerLocalizedString(@"Infective")
#define InflammatoryString										ApplicationControllerLocalizedString(@"Inflammatory")
#define MetabolicString											ApplicationControllerLocalizedString(@"Metabolic")
#define NeoplasticString										ApplicationControllerLocalizedString(@"Neoplastic")
#define NeurogenicString										ApplicationControllerLocalizedString(@"Neurogenic")
#define NormalString											ApplicationControllerLocalizedString(@"Normal")
#define TraumaticString											ApplicationControllerLocalizedString(@"Traumatic")

#define HeadString												ApplicationControllerLocalizedString(@"Head")
#define NeckString												ApplicationControllerLocalizedString(@"Neck")
#define SpineString												ApplicationControllerLocalizedString(@"Spine")
#define ChestString												ApplicationControllerLocalizedString(@"Chest")
#define AbdomenString											ApplicationControllerLocalizedString(@"Abdomen")
#define PelvisString											ApplicationControllerLocalizedString(@"Pelvis")
#define UpperLimbString											ApplicationControllerLocalizedString(@"Upper Limb")
#define LowerLimbString											ApplicationControllerLocalizedString(@"Lower Limb")

#define OpeningMostRecentDocumentString							ApplicationControllerLocalizedString(@"Opening most recent document...")
#define CheckingForNewVersionString								ApplicationControllerLocalizedString(@"Checking for new version...")
#define VersionCheckSuccessfulString							ApplicationControllerLocalizedString(@"Version check successful")
#define VersionCheckFailedString								ApplicationControllerLocalizedString(@"Version check failed")

#define ChooseString											ApplicationControllerLocalizedString(@"Choose")
#define ChooseImageEditorString									ApplicationControllerLocalizedString(@"Choose Image Editor")
#define ChooseMovieEditorString									ApplicationControllerLocalizedString(@"Choose Movie Editor")

#define AutomaticallyCheckForUpdatesString						ApplicationControllerLocalizedString(@"Automatically Check For Updates?")
#define CheckString												ApplicationControllerLocalizedString(@"Check")
#define DontCheckString											ApplicationControllerLocalizedString(@"Don't Check")
#define AutomaticallyCheckForUpdatesInfoString					ApplicationControllerLocalizedString(@"You can set Xphile to check for new versions every week.\n\nNo personal identifying information is transmitted.\n\n")

#define RunningFromDiskImageString								ApplicationControllerLocalizedString(@"Application Not Installed")
#define ContinueString											ApplicationControllerLocalizedString(@"Continue")
#define RunningFromDiskImageInfoString							ApplicationControllerLocalizedString(@"Xphile has been launched from its disk image.  It is recommended that the Xphile application is installed into the Applications folder.\n\nSee the User Guide for further information.")

#define SuccessString											ApplicationControllerLocalizedString(@"Completed")
#define FailureString											ApplicationControllerLocalizedString(@"Failed")



@interface ApplicationController : NSObject
	{
	IBOutlet id													osirixXMLRPCTable;
	
	NSWindowController											*preferencesWindowController;
	AboutController												*aboutWindowController;
	MIRCQueryController											*mircQueryController;
	NSArrayController											*sitesArrayController;
	BOOL														hasOpenedFile;
	}

- (IBAction) doSplashAboutBox: (id) sender;
- (IBAction) doPreferences: (id) sender;

- (IBAction) doCheckForNewVersion: (id) sender;

- (IBAction) doChooseImageEditor: (id) sender;
- (IBAction) doChooseMovieEditor: (id) sender;

- (IBAction) doMircQuery: (id) sender;
- (IBAction) doMircBrowse: (id) sender;

- (IBAction) doSitesMenu: (id) sender;

- (void) linkToOsirixPlugins;

- (IBAction) doHelp: (id) sender;
- (IBAction) doReleaseNotes: (id) sender;
- (IBAction) doOpenExample: (id) sender;
- (IBAction) doEmailDeveloper: (id) sender;
- (IBAction) doGoToWebsite: (id) sender;

@property (nonatomic, readonly, copy) NSString *minimiseMenuTitle;
@property (nonatomic, readonly, copy) NSString *anonymiseAllMenuTitle;

@end
