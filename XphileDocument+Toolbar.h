/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "XphileDocument.h"


#define AddCaseKey										@"AddCase"
#define RemoveCaseKey									@"RemoveCase"
#define EditCaseKey										@"EditCase"
#define ShowImagesKey									@"ShowImages"
#define FullScreenKey									@"FullScreen"
#define PatientListKey									@"PatientList"
#define RandomCasesKey									@"RandomCases"
#define OpenInOsirixKey									@"OpenInOsirix"
#define ExportImagesKey									@"ExportImages"
#define ExportToKeynoteKey								@"ExportToKeynote"
#define ExportToMIRCKey									@"ExportToMIRC"
#define ResetBrowserKey									@"ResetBrowser"
#define ToolbarSearchKey								@"ToolbarSearch"

#define AddCaseString									XphileDocumentLocalizedString(@"Add Case")
#define RemoveCaseString								XphileDocumentLocalizedString(@"Remove Case")
#define ResetBrowserString								XphileDocumentLocalizedString(@"Reset Browser")
#define EditCaseString									XphileDocumentLocalizedString(@"Edit Case")
#define DisplayImagesString								XphileDocumentLocalizedString(@"Display Images")
#define FullScreenString								XphileDocumentLocalizedString(@"Full Screen")
#define PatientListString								XphileDocumentLocalizedString(@"Patient List")
#define OpenInOsirixString								XphileDocumentLocalizedString(@"Open In OsiriX")
#define RandomCasesString								XphileDocumentLocalizedString(@"Random Cases")
#define ExportImagesString								XphileDocumentLocalizedString(@"Export Images")
#define ExportToKeynoteString							XphileDocumentLocalizedString(@"Export to Keynote")
#define ExportToMIRCString								XphileDocumentLocalizedString(@"Export to MIRC")
#define SearchString									XphileDocumentLocalizedString(@"Search")

#define KeynoteBundleIdentifier							@"com.apple.iWork.Keynote"


@interface XphileDocument (XphileDocument_Toolbar)

- (void) setUpToolbar;

- (IBAction) resetBrowser: (id) sender;
- (IBAction) addCase: (id) sender;
- (IBAction) removeCase: (id) sender;
- (void) removeCaseAlertDidEnd: (NSAlert*) alert returnCode: (NSInteger) returnCode contextInfo: (NSArray*) selectionArray;
- (IBAction) editCase: (id) sender;
- (IBAction) showPatientList: (id) sender;
- (void) showImages: (id) sender;
- (void) showImageWindow;
- (IBAction) openInOsirix: (id) sender;


@end