/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "MediaArrayController.h"
#import "MediaTableView.h"


@implementation MediaTableView


- (void) awakeFromNib
	{

// Set our drag mask to allow between application copy drags.

	[self setDraggingSourceOperationMask: NSDragOperationAll forLocal: YES];
	[self setDraggingSourceOperationMask: NSDragOperationCopy forLocal: NO];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Responders

- (BOOL) acceptsFirstResponder
	{
	return YES;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) resignFirstResponder
	{
	[self deselectAll: self];

	return [super resignFirstResponder];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) mouseDown: (NSEvent*) theEvent
	{	
	if ([theEvent clickCount]>1)
		{
		[self doEdit: self];
		return;
		}

	[super mouseDown: theEvent];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) keyDown: (NSEvent*) theEvent
	{
	
// Both imageTableView and fileTableView can become first responders and trap keyDowns before they can travel up the responder chain.
// imageWindow then doesn't get a chance to use noResponderFor to prevent null action key beeps.
// This routine checks for space, tab and up/down arrows, passing them along only when appropriate.
	
	NSUInteger key=[theEvent keyCode];
	switch (key)
		{
		case 48:								// Tab
			[super keyDown: theEvent];
			break;

		case 125:								// Up/down arrow
		case 126:
			if ([self numberOfRows]>0)
				[super keyDown: theEvent];
			break;

		default:
			break;
		}
	
	if ([[theEvent characters] isEqualToString: @" "])
		[super keyDown: theEvent];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (IBAction) chooseMedia: (id) sender
	{
	[[self delegate] performSelector: @selector(chooseMedia:) withObject: sender];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doEdit: (id __unused) sender
	{

// Overridden.
	}


@end
