/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "MediaEntity.h"


@implementation MediaEntity

- (id) valueForUndefinedKey: (NSString*) key
	{
	NSLog(@"%@: no such key - %@", [self description], key);
	return (nil);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) awakeFromInsert
	{
	static NSInteger						itsOrderIndex	=	0;
	
	
	[super awakeFromInsert];

	[self setPrimitiveValue: @(++itsOrderIndex) forKey: @"order"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) setMediaFromPath: (NSString*) path
	{
	ASSERTNOTNIL(path);

	NSString *fileName=[path lastPathComponent];ASSERTNOTNIL(fileName);
	if (fileName==nil || [fileName length]==0)
		fileName=@"?";

	[self setValue: fileName forKey: @"fileName"];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) defaultFileName
	{
	NSString *name=[self valueForKey: @"fileName"];ASSERTNOTNIL(name);
	return (name);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) uniqueFileNameForFolder: (NSString*) folderPath
	{
	NSString *fileName=[self defaultFileName];ASSERTNOTNIL(fileName);
	NSString *path=[NSString stringWithString: folderPath];ASSERTNOTNIL(path);
	path=[path stringByAppendingPathComponent: fileName];
	NSString *extension=[path pathExtension];ASSERTNOTNIL(extension);
	NSString *workingPath=[path stringByDeletingPathExtension];ASSERTNOTNIL(workingPath);

	NSInteger index=0;
	do
		{
		index++;
		NSString *padding=@"";
		if (index<10)
			padding=@"0000";
		else if (index<100)
			padding=@"000";
		else if (index<1000)
			padding=@"00";
		else if (index<10000)
			padding=@"0";
		ASSERTNOTNIL(padding);
		path=[NSString stringWithFormat: @"%@_%@%ld.%@", workingPath, padding, (long) index, extension];ASSERTNOTNIL(path);
		} while ([[NSFileManager defaultManager] fileExistsAtPath: path]);

	return (path);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSData*) rawData
	{

// Override.

	return (nil);
	}

@end
