/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "XphileDocument.h"


typedef enum : NSUInteger
	{
	SmartAlbumSearchTypeIgnore				=	0,
	SmartAlbumSearchTypeSystem,
	SmartAlbumSearchTypePathology,
	SmartAlbumSearchTypeConditionRegion,
	SmartAlbumSearchTypeGender
	} SmartAlbumSearchType;

typedef enum : NSUInteger
	{
	SmartAlbumOperatorTypeIs				=	0,
	SmartAlbumOperatorTypeIsNot
	} SmartAlbumOperatorType;


typedef enum : NSUInteger
	{
	SmartAlbumCompoundTypeNone				=	0,
	SmartAlbumCompoundTypeAll,
	SmartAlbumCompoundTypeAny
	} SmartAlbumCompoundType;



@interface XphileDocument (XphileDocument_Albums)

- (IBAction) createRandomAlbum: (id) sender;

- (void) editSmartAlbum: (id) sender;
- (void) setUpMenusForCurrentAlbum;
- (IBAction) closeAlbumSheet: (id) sender;

- (IBAction) updateAlbumSheetForSearchType: (id) sender;
- (void) updateSmartAlbumMenusForLine: (NSInteger) line;
- (void) updateSmartAlbumMenusLine1;
- (void) updateSmartAlbumMenusLine2;
- (void) updateSmartAlbumMenusLine3;
- (void) updateSmartAlbumCompoundMenu;

- (BOOL) predicateEquatesToZero: (NSPredicate*) predicate;

- (NSPredicate*) predicateFromLine1;
- (NSPredicate*) predicateFromLine2;
- (NSPredicate*) predicateFromLine3;

@end
