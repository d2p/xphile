/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/




#import "ConditionEntity.h"
#import "DisplayCasesTextField.h"
#import "VersionManager.h"
#import "WaitController.h"



@implementation DisplayCasesTextField

- (void) mouseUp: (NSEvent*) theEvent
	{
	if ([theEvent clickCount] != 2)
		return;
	
	WaitController *waitController=nil;
	NSDate *startDate=[NSDate date];ASSERTNOTNIL(startDate);

	NSDictionary *bindingDict=[self infoForBinding: @"displayPatternValue1"];ASSERTNOTNIL(bindingDict);
	NSArrayController *ac=[bindingDict valueForKey: NSObservedObjectKey];ASSERTNOTNIL(ac);
	NSArray *conditionsArray=[ac arrangedObjects];ASSERTNOTNIL(conditionsArray);
	
	NSInteger numImages=0;
	NSInteger numMovies=0;
	NSInteger numCases=0;

	ConditionEntity *aCondition;
	NSEnumerator *conditionEnumerator=[conditionsArray objectEnumerator];ASSERTNOTNIL(conditionEnumerator);
	while ((aCondition=[conditionEnumerator nextObject]))
		{
		if (!waitController && [[NSDate date] timeIntervalSinceDate: startDate] > AllowedTimeBeforeShowingWaitDialog)
			{
			waitController=[[WaitController alloc] initWithStatusString: EvaluatingSelectionString];ASSERTNOTNIL(waitController);
			[waitController showWindow: self];			
			[waitController setIndeterminate];
			}
		if (waitController)
			{
			[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];	
			}

		numCases++;
		numImages+=[aCondition numberOfImages];
		numMovies+=[aCondition numberOfMovies];
		}

	[waitController release];
	[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];	
	
	[[NSAlert alertWithMessageText: DisplayedCasesContainString defaultButton: OKString alternateButton: @"" otherButton: @""
	  informativeTextWithFormat: DisplayedCasesMediaString, numCases, numImages, numMovies] runModal];
	}

@end
