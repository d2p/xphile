/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "ApplicationController.h"
#import "BrowserArrayController.h"
#import "ConditionEntity.h"
#import "FileEntity.h"
#import "KeynoteXMLDocument.h"
#import "MIRCXMLDocument.h"
#import "XphileDocument.h"
#import "PatientEntity.h"
#import "WaitController.h"
#import "ZipManager.h"



@implementation BrowserArrayController


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Decoupled Fetch


- (void) fetch: (id) sender
	{

// Decouple sort from database, to allow sql to fetch without relying on cocoa-dependent sort routines.
// Save and restore.

	storedSortArray=[[[self sortDescriptors] copy] autorelease];ASSERTNOTNIL(storedSortArray);
	[self setSortDescriptors: @[]];
	
	[super fetch: sender];

	[self performSelector: @selector(setSortDescriptors:) withObject: storedSortArray afterDelay: 0.1];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Table Source Drag Routines


- (BOOL) tableView: (NSTableView* __unused) aTableView writeRowsWithIndexes: (NSIndexSet*) rowIndexes toPasteboard: (NSPasteboard*) pboard 
	{
		
// Initially just place the row indexes on the drag pasteboard, and promise everything else.

	[pboard declareTypes: @[BrowserRowIndexesDragType, InDocumentBrowserDragType, BetweenDocumentBrowserDragType, NSFilesPromisePboardType]
	  owner: self];

	NSData *rowData=[NSKeyedArchiver archivedDataWithRootObject: rowIndexes];ASSERTNOTNIL(rowData);
	[pboard setData: rowData forType: BrowserRowIndexesDragType];

	[pboard setPropertyList: @[@"jpg"] forType: NSFilesPromisePboardType];

	return YES;
	}

	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) pasteboard: (NSPasteboard*) sender provideDataForType: (NSString*) type
	{
	if ([sender dataForType: BrowserRowIndexesDragType] == nil)
		return;

	NSIndexSet *rowIndexes=[NSKeyedUnarchiver unarchiveObjectWithData: [sender dataForType: BrowserRowIndexesDragType]];
	if (rowIndexes==nil)
		return;

	if ([type isEqualToString: InDocumentBrowserDragType])
		{
		NSMutableArray *moURIArray=[NSMutableArray array];ASSERTNOTNIL(moURIArray);
		NSInteger currentIndex=[rowIndexes firstIndex];
		while (currentIndex != NSNotFound)
			{
			ConditionEntity *condition=[[self arrangedObjects] objectAtIndex: currentIndex];ASSERTNOTNIL(condition);
			NSURL *moURI=[[condition objectID] URIRepresentation];ASSERTNOTNIL(moURI);
			[moURIArray addObject: moURI];
			currentIndex=[rowIndexes indexGreaterThanIndex: currentIndex];
			}

		NSData *inData=[NSKeyedArchiver archivedDataWithRootObject: moURIArray];ASSERTNOTNIL(inData);
		[sender setData: inData forType: InDocumentBrowserDragType];
		return;
		}

	if ([type isEqualToString: BetweenDocumentBrowserDragType])
		{
		NSMutableArray *conditionDictionaryArray=[NSMutableArray array];ASSERTNOTNIL(conditionDictionaryArray);
		NSInteger currentIndex=[rowIndexes firstIndex];
		while (currentIndex != NSNotFound)
			{
			ConditionEntity *condition=[[self arrangedObjects] objectAtIndex: currentIndex];ASSERTNOTNIL(condition);
			NSDictionary *conditionDict=[condition dictionaryRepresentation];ASSERTNOTNIL(conditionDict);
			[conditionDictionaryArray addObject: conditionDict];
			currentIndex=[rowIndexes indexGreaterThanIndex: currentIndex];
			}

		NSData *betweenData=[NSKeyedArchiver archivedDataWithRootObject: conditionDictionaryArray];ASSERTNOTNIL(betweenData);
		[sender setData: betweenData forType: BetweenDocumentBrowserDragType];
		return;
		}
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSArray*) tableView: (NSTableView* __unused) aTableView namesOfPromisedFilesDroppedAtDestination: (NSURL*) dropDestination
  forDraggedRowsWithIndexes: (NSIndexSet*) indexSet
	{
	
// Set up the array file names written.

	NSMutableArray *fileNameArray=[NSMutableArray array];ASSERTNOTNIL(fileNameArray);

// Get the base bath from the URL.

	NSString *basePath=[dropDestination path];ASSERTNOTNIL(basePath);

// Iterate the selected items and write them at base path.

	NSInteger index=[indexSet firstIndex];
	while (index!=NSNotFound)
		{
		ConditionEntity *aCondition=[[self arrangedObjects] objectAtIndex: index];ASSERTNOTNIL(aCondition);
		NSArray *mediaArray=[aCondition mediaArray];ASSERTNOTNIL(mediaArray);
		NSUInteger i;
		for (i=0; i<[mediaArray count]; i++)
			{
			MediaEntity *aMedium=[mediaArray objectAtIndex: i];ASSERTNOTNIL(aMedium);
				{
				NSString *path=[aMedium uniqueFileNameForFolder: basePath];ASSERTNOTNIL(path);
				[fileNameArray addObject: [path lastPathComponent]];
				[[aMedium rawData] writeToFile: path atomically: YES];
				}
			}
		index=[indexSet indexGreaterThanIndex: index];
		}

	return (fileNameArray);
	}
		
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Table Destination Drop Routines


- (NSDragOperation) tableView: (NSTableView*) tv validateDrop: (id <NSDraggingInfo>) info proposedRow: (NSInteger __unused) row
  proposedDropOperation: (NSTableViewDropOperation) op
	{
	
// Only accept a drop above, not on.	
	
	if (op==NSTableViewDropOn)
		return NSDragOperationNone;
	
// Asserts.

	ASSERTNOTNIL(info);
	
// Only allow between-document and external OsiriX data.

	NSPasteboard* pboard=[info draggingPasteboard];   
	if ([[pboard types] containsObject: BetweenDocumentBrowserDragType])
		{
		if (tv != [info draggingSource])				// Ensure not the same document.
			return NSDragOperationEvery;
		return NSDragOperationNone;    
		}
		
	if ([[pboard types] containsObject: OsiriXDragType])
		return NSDragOperationEvery;

    return NSDragOperationNone;    
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) tableView: (NSTableView*) aTableView acceptDrop: (id <NSDraggingInfo>) info row: (NSInteger __unused) rowIndex
  dropOperation: (NSTableViewDropOperation __unused) operation
	{
	ASSERTNOTNIL(info);

// Ensure there is either between-document or external OsiriX data first.

	NSPasteboard* pboard=[info draggingPasteboard];ASSERTNOTNIL(pboard);

	if ([pboard dataForType: OsiriXDragType] != nil)
		{
		NSArray *osirixDataArray=[NSUnarchiver unarchiveObjectWithData: [pboard dataForType: OsiriXDragType]];
		if (osirixDataArray != nil)
			return [self tableView: aTableView acceptOsiriXData: osirixDataArray]; 
		}
		
	if ([pboard dataForType: BetweenDocumentBrowserDragType] != nil)
		{
		NSArray *xphileDataArray=[NSKeyedUnarchiver unarchiveObjectWithData: [pboard dataForType: BetweenDocumentBrowserDragType]];
		if (xphileDataArray != nil)
			return [self tableView: aTableView acceptBetweenDocumentData: xphileDataArray]; 
		}
		
	return NO;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) tableView: (NSTableView* __unused) aTableView acceptOsiriXData: (NSArray*) osirixDataArray
	{
	
// Get the managed object context.

	NSManagedObjectContext *moc=[self managedObjectContext];ASSERTNOTNIL(moc);

// Loop through the data array grabbing a dictionary each time, creating a patient record and populating it.		
	
	NSArray *modalityStringsArray=[[NSUserDefaults standardUserDefaults] valueForKey: ModalityArrayKey];ASSERTNOTNIL(modalityStringsArray);
	
	NSUInteger index;
	for (index=0; index<[osirixDataArray count]; index++)
		{
		PatientEntity				*patient = nil;
		
// Check for an existing patient with this id, otherwise create a new patient record.

		NSDictionary *osirixData=[osirixDataArray objectAtIndex: index];ASSERTNOTNIL(osirixData);
		NSString *idString=[osirixData valueForKey: @"Patient ID"];
		if (idString!=nil)
			{
			NSString *predicateString=[NSString stringWithFormat: @"identifier like[c] '%@'", idString];ASSERTNOTNIL(predicateString);
			NSPredicate *predicate=[NSPredicate predicateWithFormat: predicateString];ASSERTNOTNIL(predicate);
			NSFetchRequest *request=[[[NSFetchRequest alloc] init] autorelease];ASSERTNOTNIL(request);
			NSEntityDescription *entity=[NSEntityDescription entityForName: @"Patient" inManagedObjectContext: moc];ASSERTNOTNIL(entity);
			[request setEntity: entity];
			[request setPredicate: predicate];
			NSError *error=nil;
			NSArray *array=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(array);
			if (array == nil)
				{
				NSLog(@"Error: %@", [error description]);
				}
	
			if ([array count]>0)
				{
				patient=[array objectAtIndex: 0];
				}
			else
				{
				patient=[NSEntityDescription insertNewObjectForEntityForName: @"Patient" inManagedObjectContext: moc];
				}
			ASSERTNOTNIL(patient);
			}

// Create new condition and study.

		ConditionEntity *condition=[NSEntityDescription insertNewObjectForEntityForName: @"Condition" inManagedObjectContext: moc];ASSERTNOTNIL(condition);
		[condition setValue: patient forKey: @"patient"];
		StudyEntity *study=[NSEntityDescription insertNewObjectForEntityForName: @"Study" inManagedObjectContext: moc];ASSERTNOTNIL(study);
		[study setValue: condition forKey: @"condition"];

// Populate patient data.
		
		if (idString!=nil)
			[patient setValue: idString forKeyPath: @"identifier"];
		
		NSDate *dob=[osirixData valueForKey: @"Patients DOB"];
		if (dob!=nil)
			[patient setValue: dob forKeyPath: @"dateOfBirth"];

		NSString *genderString=[osirixData valueForKey: @"Patients Sex"];
		if (genderString!=nil)
			{
			if ([genderString isEqualToString: @"M"])
				[patient setValue: [NSNumber numberWithInteger:1] forKeyPath: @"gender"];
			else if ([genderString isEqualToString: @"F"])
				[patient setValue: [NSNumber numberWithInteger:2] forKeyPath: @"gender"];
			else
				[patient setValue: [NSNumber numberWithInteger:0] forKeyPath: @"gender"];
			}

// Populate condition data.

		NSDate *conditionDate=[osirixData valueForKey: @"Study Date"];
		if (conditionDate!=nil)
			[condition setValue: conditionDate forKey: @"dateOfOnset"];
		NSString *commentString=[osirixData valueForKey: @"Comment"];
		if (commentString!=nil)
			[condition setValue: commentString forKey: @"conditionDescription"];
			
// Populate study data.

		NSDate *studyDate=[osirixData valueForKey: @"Study Date"];
		if (studyDate!=nil)
			[study setValue: studyDate forKey: @"dateOfStudy"];
		
		NSString *modalityString=[osirixData valueForKey: @"Modality"];
		if ([modalityString isEqualToString: @"DX"])								// Temporary conversion until DX is directly supported.
			modalityString=@"CR";
		NSInteger modalityIndex=[modalityStringsArray indexOfObject: modalityString];
		if (modalityIndex==NSNotFound)
			modalityIndex=0;
		[study setValue: [NSNumber numberWithInteger: modalityIndex] forKey: @"modality"];
		
		NSString *referrerString=[osirixData valueForKey: @"Referring Physician"];
		if (referrerString==nil) referrerString=@"Unknown referrer";ASSERTNOTNIL(referrerString);
		
		NSString *performerString=[osirixData valueForKey: @"Performing Physician"];
		if (performerString==nil) performerString=@"Unknown physician";ASSERTNOTNIL(performerString);
		
		NSString *institutionString=[osirixData valueForKey: @"Institution"];
		if (institutionString==nil) institutionString=@"Unknown institution";ASSERTNOTNIL(institutionString);
		
		NSString *studyTypeString=[osirixData valueForKey: @"Study Description"];
		if (studyTypeString==nil) studyTypeString=@"Unknown study type";ASSERTNOTNIL(studyTypeString);
		
		NSString *studyDescriptionString=[NSString stringWithFormat: OsirixStudyDescriptionString,
		  referrerString, institutionString, studyTypeString, performerString];ASSERTNOTNIL(studyDescriptionString);
		[study setValue: studyDescriptionString forKey: @"studyDescription"];
		}

// Force update of browser display.

	[moc processPendingChanges];
//	[[NSNotificationCenter defaultCenter] postNotificationName: ResetBrowserNotification object: self];
	return (YES);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) tableView: (NSTableView* __unused) aTableView acceptBetweenDocumentData: (NSArray*) dataArray
	{
		
// Get the managed object context.

	NSManagedObjectContext *moc=[self managedObjectContext];ASSERTNOTNIL(moc);

// Loop through the data array grabbing a dictionary each time, creating a patient record and populating it.		
		
	NSUInteger index;
	for (index=0; index<[dataArray count]; index++)
		{
		PatientEntity				*patient = nil;
		
// Check for an existing patient with this id, otherwise create a new patient record.

		NSDictionary *dict=[dataArray objectAtIndex: index];ASSERTNOTNIL(dict);
		NSString *idString=[dict valueForKey: @"identifier"];
		if (idString!=nil)
			{
			NSString *predicateString=[NSString stringWithFormat: @"identifier like[c] '%@'", idString];ASSERTNOTNIL(predicateString);
			NSPredicate *predicate=[NSPredicate predicateWithFormat: predicateString];ASSERTNOTNIL(predicate);
			NSFetchRequest *request=[[[NSFetchRequest alloc] init] autorelease];ASSERTNOTNIL(request);
			NSEntityDescription *entity=[NSEntityDescription entityForName: @"Patient" inManagedObjectContext: moc];ASSERTNOTNIL(entity);
			[request setEntity: entity];
			[request setPredicate: predicate];
			NSError *error=nil;
			NSArray *array=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(array);
			if (array == nil)
				{
				NSLog(@"Error: %@", [error description]);
				}
	
			if ([array count]>0)
				patient=[array objectAtIndex: 0];
			else
				patient=[NSEntityDescription insertNewObjectForEntityForName: @"Patient" inManagedObjectContext: moc];
			ASSERTNOTNIL(patient);
			}

// Create new condition and study.

		ConditionEntity *condition=[NSEntityDescription insertNewObjectForEntityForName: @"Condition" inManagedObjectContext: moc];ASSERTNOTNIL(condition);
		[condition setValue: patient forKey: @"patient"];

// Populate patient data.
				
		[patient setValue: [dict valueForKey: @"identifier"] forKey: @"identifier"];
		[patient setValue: [dict valueForKey: @"dateOfBirth"] forKey: @"dateOfBirth"];
		[patient setValue: [dict valueForKey: @"gender"] forKey: @"gender"];

// Populate condition data.

		[condition setValue: [dict valueForKey: @"system"] forKey: @"system"];
		[condition setValue: [dict valueForKey: @"pathology"] forKey: @"pathology"];
		[condition setValue: [dict valueForKey: @"conditionRegion"] forKey: @"conditionRegion"];
		[condition setValue: [dict valueForKey: @"conditionDescription"] forKey: @"conditionDescription"];
		[condition setValue: [dict valueForKey: @"dateOfOnset"] forKey: @"dateOfOnset"];
			
// Populate study data.

		NSArray *studiesArray=[dict valueForKey: @"studiesArray"];ASSERTNOTNIL(studiesArray);
		NSUInteger studyIndex;
		for (studyIndex=0; studyIndex<[studiesArray count]; studyIndex++)
			{
			StudyEntity *aStudy=[studiesArray objectAtIndex: studyIndex];ASSERTNOTNIL(aStudy);

			StudyEntity *newStudy=[NSEntityDescription insertNewObjectForEntityForName: @"Study" inManagedObjectContext: moc];ASSERTNOTNIL(newStudy);
			[newStudy setValue: condition forKey: @"condition"];
			
			[newStudy setValue: [aStudy valueForKey: @"dateOfStudy"] forKey: @"dateOfStudy"];
			[newStudy setValue: [aStudy valueForKey: @"modality"] forKey: @"modality"];
			[newStudy setValue: [aStudy valueForKey: @"studyRegion"] forKey: @"studyRegion"];
			[newStudy setValue: [aStudy valueForKey: @"studyDescription"] forKey: @"studyDescription"];

// Populate image data.

			NSArray *imagesArray=[aStudy valueForKey: @"imagesArray"];ASSERTNOTNIL(imagesArray);
			NSUInteger imageIndex;
			for (imageIndex=0; imageIndex<[imagesArray count]; imageIndex++)
				{
				ImageEntity *anImage=[imagesArray objectAtIndex: imageIndex];ASSERTNOTNIL(anImage);

				ImageEntity *newImage=[NSEntityDescription insertNewObjectForEntityForName: @"Image" inManagedObjectContext: moc];ASSERTNOTNIL(newImage);
				[newImage setValue: newStudy forKey: @"study"];

				[newImage setValue: [anImage valueForKey: @"imageData"] forKey: @"imageData"];
				[newImage setValue: [anImage valueForKey: @"imageDescription"] forKey: @"imageDescription"];
				[newImage setValue: [anImage valueForKey: @"order"] forKey: @"order"];
				[newImage setValue: [anImage valueForKey: @"fileName"] forKey: @"fileName"];
				}

// Populate file data.

			NSArray *filesArray=[aStudy valueForKey: @"filesArray"];ASSERTNOTNIL(filesArray);
			NSUInteger fileIndex;
			for (fileIndex=0; fileIndex<[filesArray count]; fileIndex++)
				{
				FileEntity *aFile=[filesArray objectAtIndex: fileIndex];ASSERTNOTNIL(aFile);

				FileEntity *newFile=[NSEntityDescription insertNewObjectForEntityForName: @"File" inManagedObjectContext: moc];ASSERTNOTNIL(newFile);
				[newFile setValue: newStudy forKey: @"study"];

				[newFile setValue: [aFile valueForKey: @"fileData"] forKey: @"fileData"];
				[newFile setValue: [aFile valueForKey: @"fileDescription"] forKey: @"fileDescription"];
				[newFile setValue: [aFile valueForKey: @"order"] forKey: @"order"];
				[newFile setValue: [aFile valueForKey: @"fileName"] forKey: @"fileName"];
				}
			}
		}
		
	return (YES);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Table Miscellaneous

- (BOOL) tableView: (NSTableView* __unused) tableView shouldShowCellExpansionForTableColumn: (NSTableColumn* __unused) tableColumn row: (NSInteger __unused) row
	{
	return NO;
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Import / Export Routines

- (BOOL) exportImagesTo: (NSString*) basePath
	{
	BOOL								allSucceeded				=	YES;


	if ([[self selectedObjects] count]==0)
		return (NO);

	WaitController *waitController=[[WaitController alloc] initWithStatusString: ExportingImagesString];ASSERTNOTNIL(waitController);
	[waitController showWindow: self];			
	[waitController setProgressBarMax: [[self selectedObjects] count]];

	NSUInteger conditionIndex;
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);	

	for (conditionIndex=0; conditionIndex<[[self selectedObjects] count]; conditionIndex++)
		{
		[waitController setProgressBarValue: conditionIndex];
		[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];	
			
		ConditionEntity *condition=[[self selectedObjects] objectAtIndex: conditionIndex];ASSERTNOTNIL(condition);
		NSSet *studies=[condition valueForKeyPath: @"studies"];

		NSEnumerator *studyEnumerator = [studies objectEnumerator];
		StudyEntity *aStudy;
		while ((aStudy=[studyEnumerator nextObject]))
			{
			NSSet *images=[aStudy valueForKeyPath: @"images"];
			if (images)
				{
				NSArray *imagesArray=[NSArray arrayWithArray: [images allObjects]];ASSERTNOTNIL(imagesArray);
				NSSortDescriptor *imageSortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"order" ascending: YES] autorelease];ASSERTNOTNIL(imageSortDescriptor);
				imagesArray=[imagesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: imageSortDescriptor]];

// Loop through the images.

				NSUInteger imageIndex;
				for (imageIndex=0; imageIndex<[imagesArray count]; imageIndex++)
					{
					ImageEntity *anImage=[imagesArray objectAtIndex: imageIndex];ASSERTNOTNIL(anImage);
					[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];

					if (anImage)
						{
						NSString *path=[anImage uniqueFileNameForFolder: basePath];ASSERTNOTNIL(path);
						NSData *data=[anImage valueForKeyPath: @"imageData"];ASSERTNOTNIL(data);
						NSData *rawData=[NSUnarchiver unarchiveObjectWithData: data];ASSERTNOTNIL(rawData);
						[rawData writeToFile: path atomically: YES];
						}
					}
				}

			NSSet *movies=[aStudy valueForKeyPath: @"files"];
			if (movies)
				{
				NSArray *moviesArray=[NSArray arrayWithArray: [movies allObjects]];ASSERTNOTNIL(moviesArray);
				NSSortDescriptor *movieSortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"order" ascending: YES] autorelease];ASSERTNOTNIL(movieSortDescriptor);
				moviesArray=[moviesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: movieSortDescriptor]];

// Loop through the movies.

				NSUInteger movieIndex;
				for (movieIndex=0; movieIndex<[moviesArray count]; movieIndex++)
					{
					FileEntity *aMovie=[moviesArray objectAtIndex: movieIndex];ASSERTNOTNIL(aMovie);
					[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];

					if (aMovie)
						{
						NSString *path=[aMovie uniqueFileNameForFolder: basePath];ASSERTNOTNIL(path);
						NSData *data=[aMovie valueForKeyPath: @"fileData"];ASSERTNOTNIL(data);
						if (![data writeToFile: path atomically: YES])
							allSucceeded=NO;
						}
					}
				}
			}

		if (conditionIndex % 10 == 9)
			{
			[pool release];
			pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);	
			}
		}
	
	[pool release];
	[waitController release];
	return (allSucceeded);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) exportToKeynoteFile: (NSString*) basePath
	{
	BOOL								allSucceeded				=	YES;
	

	if ([[self selectedObjects] count]==0)
		return (NO);

// Create XML document and set up a simple theme.

	NSXMLElement *root=[NSXMLElement elementWithName: @"presentation"];ASSERTNOTNIL(root);
	[root setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"36" forKey: @"version"]];

	KeynoteXMLDocument *xmlDoc = [[KeynoteXMLDocument alloc] initWithRootElement: root];ASSERTNOTNIL(xmlDoc);
	[xmlDoc setVersion: @"1.0"];
	[xmlDoc setCharacterEncoding: @"UTF-8"];
	[xmlDoc addDefaultThemeTo: root];
	
	NSXMLElement *slideListElement=[NSXMLElement elementWithName: @"slide-list"];ASSERTNOTNIL(slideListElement);
	[root addChild: slideListElement];

	NSXMLElement *uiStateElement=[NSXMLElement elementWithName: @"ui-state"];ASSERTNOTNIL(uiStateElement);
	[root addChild: uiStateElement];

	WaitController *waitController=[[WaitController alloc] initWithStatusString: ExportingToKeynoteString];ASSERTNOTNIL(waitController);
	[waitController showWindow: self];			
	[waitController setProgressBarMax: [[self selectedObjects] count]];

// Loop through the conditions and their studies.

	NSUInteger conditionIndex;
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);	

	for (conditionIndex=0; conditionIndex<[[self selectedObjects] count]; conditionIndex++)
		{
		[waitController setProgressBarValue: conditionIndex];
		[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];	

		ConditionEntity *condition=[[self selectedObjects] objectAtIndex: conditionIndex];ASSERTNOTNIL(condition);
		if ([[[NSUserDefaultsController sharedUserDefaultsController] defaults] boolForKey: IncludeConditionSlidesKey])
			[xmlDoc addNewConditionSlideToList: slideListElement forCondition: condition];
		
		NSSet *studies=[condition valueForKeyPath: @"studies"];
		if (studies)
			{
			NSArray *studiesArray=[NSArray arrayWithArray: [studies allObjects]];ASSERTNOTNIL(studiesArray);
			NSSortDescriptor *studySortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"dateOfStudy" ascending: YES] autorelease];ASSERTNOTNIL(studySortDescriptor);
			studiesArray=[studiesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: studySortDescriptor]];

			NSUInteger studyIndex;
			for (studyIndex=0; studyIndex<[studiesArray count]; studyIndex++)
				{
				StudyEntity *aStudy=[studiesArray objectAtIndex: studyIndex];ASSERTNOTNIL(aStudy);
				if ([[[NSUserDefaultsController sharedUserDefaultsController] defaults] boolForKey: IncludeStudySlidesKey])
					[xmlDoc addNewStudySlideToList: slideListElement forStudy: aStudy];
				
				NSSet *images=[aStudy valueForKeyPath: @"images"];
				if (images)
					{
					NSArray *imagesArray=[NSArray arrayWithArray: [images allObjects]];ASSERTNOTNIL(imagesArray);
					NSSortDescriptor *imageSortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"order" ascending: YES] autorelease];ASSERTNOTNIL(imageSortDescriptor);
					imagesArray=[imagesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: imageSortDescriptor]];

// Loop through the images.

					NSUInteger imageIndex;
					for (imageIndex=0; imageIndex<[imagesArray count]; imageIndex++)
						{
						ImageEntity *anImage=[imagesArray objectAtIndex: imageIndex];ASSERTNOTNIL(anImage);
						[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];

						if (anImage)
							{
							NSString *path=[anImage uniqueFileNameForFolder: basePath];ASSERTNOTNIL(path);
							NSData *data=[anImage valueForKeyPath: @"imageData"];ASSERTNOTNIL(data);
							NSData *rawData=[NSUnarchiver unarchiveObjectWithData: data];ASSERTNOTNIL(rawData);
							[rawData writeToFile: path atomically: YES];
							
							NSImage *im=[[[NSImage alloc] initWithData: rawData] autorelease];ASSERTNOTNIL(im);
							[xmlDoc addNewImageSlideToList: slideListElement withName: [path lastPathComponent] nativeSize: [im size]];
							}
						}
					}

				NSSet *movies=[aStudy valueForKeyPath: @"files"];
				if (movies)
					{
					NSArray *moviesArray=[NSArray arrayWithArray: [movies allObjects]];ASSERTNOTNIL(moviesArray);
					NSSortDescriptor *movieSortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"order" ascending: YES] autorelease];ASSERTNOTNIL(movieSortDescriptor);
					moviesArray=[moviesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: movieSortDescriptor]];

// Loop through the movies.

					NSUInteger movieIndex;
					for (movieIndex=0; movieIndex<[moviesArray count]; movieIndex++)
						{
						FileEntity *aMovie=[moviesArray objectAtIndex: movieIndex];ASSERTNOTNIL(aMovie);
						[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];

						if (aMovie)
							{
							NSString *path=[aMovie uniqueFileNameForFolder: basePath];ASSERTNOTNIL(path);
							FileEntity *file=[moviesArray objectAtIndex: movieIndex];ASSERTNOTNIL(file);
							QTMovie *qtMovie=[file qtMovie];ASSERTNOTNIL(qtMovie);
							NSValue *value=[qtMovie attributeForKey: QTMovieNaturalSizeAttribute];ASSERTNOTNIL(value);
							if ([qtMovie writeToFile: path withAttributes: nil])
								[xmlDoc addNewMovieSlideToList: slideListElement withName: [path lastPathComponent] nativeSize: [value sizeValue]];
							else
								allSucceeded=NO;
							}
						}
					}
				}
			}

		if (conditionIndex % 10 == 9)
			{
			[pool release];
			pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);
			}
		}

	[pool release];

// Write the XML document.
	
	[[NSFileManager defaultManager] createFileAtPath: [basePath stringByAppendingPathComponent: @"presentation.apxl"]
	  contents: [xmlDoc XMLDataWithOptions: NSXMLNodePrettyPrint] attributes: nil];
	[xmlDoc release];
	
	[waitController release];
	return (allSucceeded);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) exportToMIRCArchiveAtPath: (NSString*) basePath
	{
	BOOL								success			=	YES;
	MIRCXMLDocument						*xmlDoc			=	nil;
	WaitController						*waitController	=	nil;


	NSDate *startDate=[NSDate date];ASSERTNOTNIL(startDate);

	@try
		{
		ConditionEntity *condition=[[self selectedObjects] objectAtIndex: 0];ASSERTNOTNIL(condition);

// Create XML document and set up basic authoring info.

		NSXMLElement *root=[NSXMLElement elementWithName: @"MIRCdocument"];ASSERTNOTNIL(root);
		[root setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"mstf", nil]
		  forKeys: [NSArray arrayWithObjects: @"display", nil]]];

		MIRCXMLDocument *xmlDoc=[[MIRCXMLDocument alloc] initWithRootElement: root];ASSERTNOTNIL(xmlDoc);
		[xmlDoc setVersion: @"1.0"];
		[xmlDoc setCharacterEncoding: @"UTF-8"];

		[xmlDoc addStandardInformationTo: root];
		[xmlDoc addSystemTo: root withDetail: [condition valueForKeyPath: @"systemText"]];
		[xmlDoc addPathologyTo: root withDetail: [condition valueForKeyPath: @"pathologyText"]];
		[xmlDoc addAnatomyTo: root withDetail: [condition valueForKeyPath: @"conditionRegionText"]];
		[xmlDoc addCategoryTo: root withCondition: condition];

		[xmlDoc addTitleInformationTo: root withTitle: [condition valueForKeyPath: @"conditionDescription"]];
		[xmlDoc addAuthorInformationTo: root];

// Add the current condition information.
		
		[xmlDoc addAbstractTo: root withDetail: [condition valueForKeyPath: @"conditionDescription"]];
		[xmlDoc addHistoryTo: root withCondition: condition];

// Add the collated study descriptions into findings and then the discussion.

		NSSet *studies=[condition valueForKeyPath: @"studies"];
		NSString *findingsString=@"";
		if (studies)
			{
			NSArray *studiesArray=[NSArray arrayWithArray: [studies allObjects]];ASSERTNOTNIL(studiesArray);
			NSSortDescriptor *studySortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"dateOfStudy" ascending: YES] autorelease];ASSERTNOTNIL(studySortDescriptor);
			studiesArray=[studiesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: studySortDescriptor]];

			NSUInteger studyIndex;
			for (studyIndex=0; studyIndex<[studiesArray count]; studyIndex++)
				{
				StudyEntity *aStudy=[studiesArray objectAtIndex: studyIndex];ASSERTNOTNIL(aStudy);
				NSString *studyDescription=[aStudy valueForKeyPath: @"studyDescription"];
				if (studyDescription)
					{
					findingsString=[findingsString stringByAppendingFormat: @"%@\r", studyDescription];
					}
				}
			}
		if ([findingsString length] > 0)
			{
			[xmlDoc addFindingsTo: root withDetail: findingsString];
			}
		[xmlDoc addDiscussionTo: root withDetail: [condition valueForKeyPath: @"conditionDescription"]];
	
// Create the image section.

		NSXMLElement *imageSectionElement=[NSXMLElement elementWithName: @"image-section"];ASSERTNOTNIL(imageSectionElement);
		NSInteger mircMaxImageWidth=[[NSUserDefaults standardUserDefaults] integerForKey: MIRCImageWidthKey];
		NSString *mircImagePaneWidthStr=[NSString stringWithFormat: @"%ld", (long) mircMaxImageWidth];ASSERTNOTNIL(mircImagePaneWidthStr);
		[imageSectionElement setAttributesAsDictionary:
		  [NSDictionary dictionaryWithObjectsAndKeys: @"Images", @"heading", @"no", @"icons", mircImagePaneWidthStr, @"image-pane-width", nil]];
		[root addChild: imageSectionElement];

// Loop through its studies, adding each of the images.
	
		if (studies)
			{
			NSArray *studiesArray=[NSArray arrayWithArray: [studies allObjects]];ASSERTNOTNIL(studiesArray);
			NSSortDescriptor *studySortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"dateOfStudy" ascending: YES] autorelease];ASSERTNOTNIL(studySortDescriptor);
			studiesArray=[studiesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: studySortDescriptor]];

			NSUInteger studyIndex;
			for (studyIndex=0; studyIndex<[studiesArray count]; studyIndex++)
				{
				StudyEntity *aStudy=[studiesArray objectAtIndex: studyIndex];ASSERTNOTNIL(aStudy);
				NSSet *images=[aStudy valueForKeyPath: @"images"];
				if (images)
					{
					NSArray *imagesArray=[NSArray arrayWithArray: [images allObjects]];ASSERTNOTNIL(imagesArray);
					NSSortDescriptor *imageSortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"order" ascending: YES] autorelease];ASSERTNOTNIL(imageSortDescriptor);
					imagesArray=[imagesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: imageSortDescriptor]];

					NSUInteger imageIndex;
					for (imageIndex=0; imageIndex<[imagesArray count]; imageIndex++)
						{
						if (!waitController && [[NSDate date] timeIntervalSinceDate: startDate] > AllowedTimeBeforeShowingWaitDialog)
							{
							waitController=[[WaitController alloc] initWithStatusString: ExportingAsMIRCArchiveString];ASSERTNOTNIL(waitController);
							[waitController setProgressBarMax: [imagesArray count]];
							[waitController showWindow: self];			
							}
							
						ImageEntity *anImage=[imagesArray objectAtIndex: imageIndex];ASSERTNOTNIL(anImage);
						[waitController setProgressBarValue: imageIndex];
						[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];

						if (anImage)
							{
							NSString *path=[NSString stringWithFormat: @"%@/Image %ld-%ld.%@", basePath, (long) studyIndex+1, (long) imageIndex+1, [anImage suffixString]];
							ASSERTNOTNIL(path);
							NSData *imageData=[anImage imageDataForMIRC];ASSERTNOTNIL(imageData);
							[imageData writeToFile: path atomically: YES];
							[xmlDoc addImageTo: imageSectionElement withName: [path lastPathComponent] withModality: [aStudy valueForKeyPath: @"modalityText"]];
							}
						}
					}
				}
			}

// Write the XML document.
		
		[[NSFileManager defaultManager] createFileAtPath: [basePath stringByAppendingPathComponent: @"MIRCdocument.xml"]
		  contents: [xmlDoc XMLDataWithOptions: NSXMLNodePrettyPrint] attributes: nil];

// Convert the folder to a zip archive.

		ZipManager *zipper=[[[ZipManager alloc] init] autorelease];ASSERTNOTNIL(zipper);
		if (![zipper createZipArchiveFromPath: basePath])
			{
			[[NSException exceptionWithName: @"UnableToCreateZip" reason: nil userInfo: nil] raise];
			}
		
// Delete the folder.

		NSError *error=nil;
		if (![[NSFileManager defaultManager] removeItemAtPath: basePath error: &error])
			{
			NSLog(@"Error: %@", [error description]);
			}
		}
	@catch (NSException *exception)
		{
		success=NO;
		}
	@finally
		{
		[xmlDoc release];
		[waitController release];
		return (success);
		}
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) exportToMIRCServerUsingPath: (NSString*) basePath
	{
	
// Create archive.
	
	if (![self exportToMIRCArchiveAtPath: basePath])
		{
		return NO;
		}

// Submit to MIRC.

	NSString *zipFilePath=[basePath stringByAppendingString: @".zip"];ASSERTNOTNIL(zipFilePath);
	ZipManager *zipper=[[[ZipManager alloc] init] autorelease];ASSERTNOTNIL(zipper);	// Released on completion.
	[zipper setOwner: self];
	return ([zipper sendZipToMIRCServer: zipFilePath]);
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) indicateSuccess
	{
	[[NSNotificationCenter defaultCenter] postNotificationName: ExportToMIRCCompletedSuccessfullyNotification object: self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) indicateFailure
	{
	[[NSNotificationCenter defaultCenter] postNotificationName: ExportToMIRCFailedNotification object: self];
	}



@end
