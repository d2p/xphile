/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "AgeTransformer.h"
#import "KeynoteXMLDocument.h"


@implementation KeynoteXMLDocument


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Theme Setup

- (void) addDefaultThemeTo: (NSXMLElement*) root
	{
	NSXMLElement *themeElement=[NSXMLElement elementWithName: @"theme"];ASSERTNOTNIL(themeElement);
	[themeElement setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"1024 768" forKey: @"slide-size"]];
	[root addChild: themeElement];

// Master slides.
	
	NSXMLElement *masterSlidesElement=[NSXMLElement elementWithName: @"master-slides"];ASSERTNOTNIL(masterSlidesElement);
	[themeElement addChild: masterSlidesElement];

// Master slide 1 (Text).

	NSXMLElement *masterSlide1Element=[NSXMLElement elementWithName: @"master-slide"];ASSERTNOTNIL(masterSlide1Element);
	[masterSlide1Element setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"master-slide-1", @"Text", nil]
	  forKeys: [NSArray arrayWithObjects: @"id", @"name", nil]]];
	[masterSlidesElement addChild: masterSlide1Element];

	NSXMLElement *drawables1Element=[NSXMLElement elementWithName: @"drawables"];ASSERTNOTNIL(drawables1Element);
	[masterSlide1Element addChild: drawables1Element];

	[self addTitleTo: drawables1Element withVisibility: YES];
	[self addBodyTo: drawables1Element withVisibility: YES];
	[self addPageNumberTo: drawables1Element];

	[self addBulletsTo: masterSlide1Element];
	[self addBackgroundFillTo: masterSlide1Element];
	[self addTransitionStyleTo: masterSlide1Element];

// Master slide 2 (Image).

	NSXMLElement *masterSlide2Element=[NSXMLElement elementWithName: @"master-slide"];ASSERTNOTNIL(masterSlide2Element);
	[masterSlide2Element setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"master-slide-2", @"Image", nil]
	  forKeys: [NSArray arrayWithObjects: @"id", @"name", nil]]];
	[masterSlidesElement addChild: masterSlide2Element];

	NSXMLElement *drawables2Element=[NSXMLElement elementWithName: @"drawables"];ASSERTNOTNIL(drawables2Element);
	[masterSlide2Element addChild: drawables2Element];

	[self addTitleTo: drawables2Element withVisibility: NO];
	[self addBodyTo: drawables2Element withVisibility: NO];
	[self addPageNumberTo: drawables2Element];

	[self addBulletsTo: masterSlide2Element];
	[self addBackgroundFillTo: masterSlide2Element];
	[self addTransitionStyleTo: masterSlide2Element];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) addTitleTo: (NSXMLElement*) element withVisibility: (BOOL) visible
	{
	NSXMLElement *titleElement=[NSXMLElement elementWithName: @"title"];ASSERTNOTNIL(titleElement);
	[titleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects:
	  [NSArray arrayWithObjects: @"78.0 16.0", @"1", @"868.0 150.0", @"g0", @"1", @"middle", visible ? @"visible": @"hidden", nil]
	  forKeys: [NSArray arrayWithObjects: @"location", @"opacity", @"size", @"stroke-color", @"stroke-width", @"vertical-alignment", @"visibility", nil]]];
	[element addChild: titleElement];

	NSXMLElement *titleStylesElement=[NSXMLElement elementWithName: @"styles"];ASSERTNOTNIL(titleStylesElement);
	[self addStyleElementsTo: titleStylesElement];
	[titleElement addChild: titleStylesElement];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) addBodyTo: (NSXMLElement*) element withVisibility: (BOOL) visible
	{
	NSXMLElement *bodyElement=[NSXMLElement elementWithName: @"body"];ASSERTNOTNIL(bodyElement);
	[bodyElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects:
	  [NSArray arrayWithObjects: @"0 20 48 75 102 130", @"78 170", @"868 520", @"1", @"g0", @"1", @"middle", visible ? @"visible" : @"hidden", nil]
	  forKeys: [NSArray arrayWithObjects: @"bullet-indentation", @"location", @"size", @"opacity", @"stroke-color", @"stroke-width", @"vertical-alignment", @"visibility", nil]]];
	[element addChild: bodyElement];

	NSXMLElement *bodyStylesElement=[NSXMLElement elementWithName: @"styles"];ASSERTNOTNIL(bodyStylesElement);
	[self addStyleElementsTo: bodyStylesElement];
	[bodyElement addChild: bodyStylesElement];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) addPageNumberTo: (NSXMLElement*) element
	{
	NSXMLElement *pageNumberElement=[NSXMLElement elementWithName: @"page-number"];ASSERTNOTNIL(pageNumberElement);
	[pageNumberElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects:
	  [NSArray arrayWithObjects: @"380 570", @"1", @"39 23", @"g1", @"1", @"middle", @"hidden", nil]
	  forKeys: [NSArray arrayWithObjects: @"location", @"opacity", @"size", @"stroke-color", @"stroke-width", @"vertical-alignment", @"visibility", nil]]];
	[element addChild: pageNumberElement];

	NSXMLElement *pageNumberStylesElement=[NSXMLElement elementWithName: @"styles"];ASSERTNOTNIL(pageNumberStylesElement);
	[self addStyleElementsTo: pageNumberStylesElement];
	[pageNumberElement addChild: pageNumberStylesElement];

	NSXMLElement *textAttributesElement=[NSXMLElement elementWithName: @"text-attributes"];ASSERTNOTNIL(textAttributesElement);
	[pageNumberElement addChild: textAttributesElement];
	}
	
	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) addBulletsTo: (NSXMLElement*) element
	{
	NSXMLElement *prototypeBulletsElement=[NSXMLElement elementWithName: @"prototype-bullets"];ASSERTNOTNIL(prototypeBulletsElement);
	[element addChild: prototypeBulletsElement];
	
	NSInteger index;
	for (index=0; index<=5; index++)
		{
		NSXMLElement *bulletElement=[NSXMLElement elementWithName: @"bullet"];ASSERTNOTNIL(bulletElement);
		[bulletElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects:
		  [NSArray arrayWithObjects: [NSString stringWithFormat: @"%ld", (long) index], @"none",
		  [NSString stringWithFormat: @"%ld", (long) index*10], nil]
		  forKeys: [NSArray arrayWithObjects: @"level", @"marker-type", @"spacing", nil]]];
		[prototypeBulletsElement addChild: bulletElement];

		NSXMLElement *bulletContentElement=[NSXMLElement elementWithName: @"content"];ASSERTNOTNIL(bulletContentElement);
		[bulletContentElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects:
		  [NSArray arrayWithObjects: @"g1", nil]
		  forKeys: [NSArray arrayWithObjects: @"font-color", nil]]];
		[bulletElement addChild: bulletContentElement];
		}
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) addBackgroundFillTo: (NSXMLElement*) element
	{
	NSXMLElement *backgroundFillStyleElement=[NSXMLElement elementWithName: @"background-fill-style"];ASSERTNOTNIL(backgroundFillStyleElement);
	[backgroundFillStyleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects:
	  [NSArray arrayWithObjects: @"g0", @"color", nil] forKeys: [NSArray arrayWithObjects: @"fill-color", @"fill-type", nil]]];
	[element addChild: backgroundFillStyleElement];
	}
	
	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) addTransitionStyleTo: (NSXMLElement*) element
	{
	NSXMLElement *transitionStyleElement=[NSXMLElement elementWithName: @"transition-style"];ASSERTNOTNIL(transitionStyleElement);
	[transitionStyleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects:
	  [NSArray arrayWithObjects: @"0", @"none", nil] forKeys: [NSArray arrayWithObjects: @"duration", @"type", nil]]];
	[element addChild: transitionStyleElement];	
	}
		
		
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) addStyleElementsTo: (NSXMLElement*) element
	{
	NSXMLElement *fillStyleElement=[NSXMLElement elementWithName: @"fill-style"];ASSERTNOTNIL(fillStyleElement);
	[fillStyleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"none" forKey: @"fill-type"]];
	[element addChild: fillStyleElement];

	NSXMLElement *dashStyleElement=[NSXMLElement elementWithName: @"dash-style"];ASSERTNOTNIL(dashStyleElement);
	[dashStyleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"none" forKey: @"pattern"]];
	[element addChild: dashStyleElement];

	NSXMLElement *shadowStyleElement=[NSXMLElement elementWithName: @"shadow-style"];ASSERTNOTNIL(shadowStyleElement);
	[shadowStyleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects:
	  [NSArray arrayWithObjects: @"0", @"0", nil] forKeys: [NSArray arrayWithObjects: @"opacity", @"radius", nil]]];
	[element addChild: shadowStyleElement];
	}
	
	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Bullets


- (void) addTitleBulletTo: (NSXMLElement*) element withString: (NSString*) bulletString
	{
	NSXMLElement *titleBulletElement=[NSXMLElement elementWithName: @"bullet"];ASSERTNOTNIL(titleBulletElement);
	[titleBulletElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"inherited", @"0", nil]
	  forKeys: [NSArray arrayWithObjects: @"marker-type", @"level", nil]]];
	[element addChild: titleBulletElement];

	NSXMLElement *titleBulletContentElement=[NSXMLElement elementWithName: @"content"];ASSERTNOTNIL(titleBulletContentElement);
	[titleBulletContentElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"L 96", @"60", @"GillSans", @"0 0.5 1", @"center", nil]
	  forKeys: [NSArray arrayWithObjects: @"tab-stops", @"font-size", @"font-name", @"font-color", @"paragraph-aligment", nil]]];

// Ensure string of reasonable size.
	
	NSArray *sentenceArray=[bulletString componentsSeparatedByString: @". "];ASSERTNOTNIL(sentenceArray);
	NSArray *wordArray=[bulletString componentsSeparatedByString: @" "];ASSERTNOTNIL(wordArray);
	NSString *abbreviatedStr=nil;
	if ([sentenceArray count] > 0)						// Sentence.
		{
		abbreviatedStr=[sentenceArray objectAtIndex: 0];ASSERTNOTNIL(abbreviatedStr);
		}
	else if ([wordArray count] > 4)						// 5 Words.
		{
		wordArray=[wordArray subarrayWithRange: NSMakeRange(0,4)];ASSERTNOTNIL(wordArray);
		abbreviatedStr=[wordArray componentsJoinedByString: @" "];ASSERTNOTNIL(abbreviatedStr);
		}
	else if ([wordArray count] > 0)						// n Words.
		{
		abbreviatedStr=[wordArray componentsJoinedByString: @" "];ASSERTNOTNIL(abbreviatedStr);
		}
	else												// Failsafe.
		{
		abbreviatedStr=bulletString;
		}

	[titleBulletContentElement setStringValue: abbreviatedStr];
	[titleBulletElement addChild: titleBulletContentElement];
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) addBodyBulletTo: (NSXMLElement*) element withString: (NSString*) bulletString
	{
	NSXMLElement *bodyBulletElement=[NSXMLElement elementWithName: @"bullet"];ASSERTNOTNIL(bodyBulletElement);
	[bodyBulletElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"inherited", @"1", nil]
	  forKeys: [NSArray arrayWithObjects: @"marker-type", @"level", nil]]];
	[element addChild: bodyBulletElement];

	NSXMLElement *bodyBulletContentElement=[NSXMLElement elementWithName: @"content"];ASSERTNOTNIL(bodyBulletContentElement);
	[bodyBulletContentElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"L 96", @"42", @"GillSans", @"1 1 1", @"center", nil]
	  forKeys: [NSArray arrayWithObjects: @"tab-stops", @"font-size", @"font-name", @"font-color", @"paragraph-aligment", nil]]];

// Strip spaces.

	NSMutableArray *removedSpacesArray=[NSMutableArray arrayWithCapacity: 0];ASSERTNOTNIL(removedSpacesArray);
	NSArray *array=[bulletString componentsSeparatedByString: @" "];ASSERTNOTNIL(array);
	NSUInteger index;
	for (index=0; index<[array count]; index++)
		{
		NSString *str=[array objectAtIndex: index];ASSERTNOTNIL(str);
		if ([str length] > 0)
			[removedSpacesArray addObject: str];
		}
	NSString *strippedString=[removedSpacesArray componentsJoinedByString: @" "];

	[bodyBulletContentElement setStringValue: strippedString];
	[bodyBulletElement addChild: bodyBulletContentElement];
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Slide Types

- (void) addNewConditionSlideToList: (NSXMLElement*) listElement forCondition: (ConditionEntity*) aCondition
	{
	
// Create slide.
	
	NSXMLElement *slideElement=[NSXMLElement elementWithName: @"slide"];ASSERTNOTNIL(slideElement);
	[slideElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjectsAndKeys: @"0", @"depth", @"master-slide-1", @"master-slide-id", nil]];
	[listElement addChild: slideElement];

// Drawables.

	NSXMLElement *drawablesElement=[NSXMLElement elementWithName: @"drawables"];ASSERTNOTNIL(drawablesElement);
	[slideElement addChild: drawablesElement];

// Body.

	NSXMLElement *bodyElement=[NSXMLElement elementWithName: @"body"];ASSERTNOTNIL(bodyElement);
	[bodyElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"tracks-master", @"tracks-master", nil]
	  forKeys: [NSArray arrayWithObjects: @"visibility", @"vertical-alignment", nil]]];
	[drawablesElement addChild: bodyElement];

// Title.

	NSXMLElement *titleElement=[NSXMLElement elementWithName: @"title"];ASSERTNOTNIL(titleElement);
	[titleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"tracks-master", @"tracks-master", nil]
	  forKeys: [NSArray arrayWithObjects: @"visibility", @"vertical-alignment", nil]]];
	[drawablesElement addChild: titleElement];

// Transition style.
	
	NSXMLElement *transitionStyleElement=[NSXMLElement elementWithName: @"transition-style"];ASSERTNOTNIL(transitionStyleElement);
	[transitionStyleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"inherited" forKey: @"type"]];
	[slideElement addChild: transitionStyleElement];

// Bullet list.

	NSXMLElement *bulletsElement=[NSXMLElement elementWithName: @"bullets"];ASSERTNOTNIL(bulletsElement);
	[slideElement addChild: bulletsElement];

	NSString* conditionDescription=[aCondition valueForKeyPath: @"conditionDescription"];
	if (conditionDescription==nil)
		conditionDescription=@"";

// Title bullet.

	[self addTitleBulletTo: bulletsElement withString: conditionDescription];

// Body bullet.

	NSString* demographics=[NSString stringWithFormat: @"%@ %@", [aCondition valueForKeyPath: @"abbreviatedAgeAtOnset"],
	  [aCondition valueForKeyPath: @"patient.genderText"]];ASSERTNOTNIL(demographics);
	[self addBodyBulletTo: bulletsElement withString: demographics];

	NSArray *sentenceArray=[conditionDescription componentsSeparatedByString: @". "];ASSERTNOTNIL(sentenceArray);
	NSUInteger index;
	for (index=0; index<[sentenceArray count]; index++)
		{
		[self addBodyBulletTo: bulletsElement withString: [sentenceArray objectAtIndex: index]];
		}
	}
	

//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

- (void) addNewStudySlideToList: (NSXMLElement*) listElement forStudy: (StudyEntity*) aStudy
	{
// Work out depth depending on whether we are showing condition and study slides also.

	NSInteger depth=0;
	if ([[[NSUserDefaultsController sharedUserDefaultsController] defaults] boolForKey: IncludeConditionSlidesKey])
		depth++;
	NSString *depthString=[NSString stringWithFormat: @"%ld", (long) depth];ASSERTNOTNIL(depthString);

// Create slide.
	
	NSXMLElement *slideElement=[NSXMLElement elementWithName: @"slide"];ASSERTNOTNIL(slideElement);
	[slideElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjectsAndKeys: depthString, @"depth", @"master-slide-1", @"master-slide-id", nil]];
	[listElement addChild: slideElement];

// Drawables.

	NSXMLElement *drawablesElement=[NSXMLElement elementWithName: @"drawables"];ASSERTNOTNIL(drawablesElement);
	[slideElement addChild: drawablesElement];

// Body.

	NSXMLElement *bodyElement=[NSXMLElement elementWithName: @"body"];ASSERTNOTNIL(bodyElement);
	[bodyElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"tracks-master", @"tracks-master", nil]
	  forKeys: [NSArray arrayWithObjects: @"visibility", @"vertical-alignment", nil]]];
	[drawablesElement addChild: bodyElement];

// Title.

	NSXMLElement *titleElement=[NSXMLElement elementWithName: @"title"];ASSERTNOTNIL(titleElement);
	[titleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"tracks-master", @"tracks-master", nil]
	  forKeys: [NSArray arrayWithObjects: @"visibility", @"vertical-alignment", nil]]];
	[drawablesElement addChild: titleElement];

// Transition style.
	
	NSXMLElement *transitionStyleElement=[NSXMLElement elementWithName: @"transition-style"];ASSERTNOTNIL(transitionStyleElement);
	[transitionStyleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"inherited" forKey: @"type"]];
	[slideElement addChild: transitionStyleElement];

// Bullet list.

	NSXMLElement *bulletsElement=[NSXMLElement elementWithName: @"bullets"];ASSERTNOTNIL(bulletsElement);
	[slideElement addChild: bulletsElement];

// Title bullet (time course : modality : region).

	TimeCourseTransformer *timeCourseTransformer=[[[TimeCourseTransformer alloc] init] autorelease];ASSERTNOTNIL(timeCourseTransformer);
	NSString *timeFromDiagnosisStr=[timeCourseTransformer transformedValue: [aStudy valueForKeyPath: @"daysSinceOnset"]];ASSERTNOTNIL(timeFromDiagnosisStr);

	NSString *modalityText=[aStudy valueForKeyPath: @"modalityText"];ASSERTNOTNIL(modalityText);
	if ([modalityText isEqualToString: @"-"])
		{
		modalityText=@"";ASSERTNOTNIL(modalityText);
		}

	NSString *studyRegionText=[aStudy valueForKeyPath: @"studyRegionText"];ASSERTNOTNIL(studyRegionText);
	if ([studyRegionText isEqualToString: @"-"])
		{
		studyRegionText=@"";ASSERTNOTNIL(studyRegionText);
		}

	NSString *studyTitle=[NSString stringWithFormat: @"%@ %@ %@", timeFromDiagnosisStr, modalityText, studyRegionText];ASSERTNOTNIL(studyTitle);
	[self addTitleBulletTo: bulletsElement withString: studyTitle];

// Body bullet.

	NSString *studyDescriptionText=[aStudy valueForKey: @"studyDescription"];
	if (studyDescriptionText==nil)
		studyDescriptionText=@"";
	[self addBodyBulletTo: bulletsElement withString: studyDescriptionText];
	}
	

//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

- (void) addNewImageSlideToList: (NSXMLElement*) listElement withName: (NSString*) fileName nativeSize: (NSSize) imageSize
	{
	
// Work out depth depending on whether we are showing condition and study slides also.

	NSInteger depth=0;
	if ([[[NSUserDefaultsController sharedUserDefaultsController] defaults] boolForKey: IncludeConditionSlidesKey])
		{
		depth++;
		}
	if ([[[NSUserDefaultsController sharedUserDefaultsController] defaults] boolForKey: IncludeStudySlidesKey])
		{
		depth++;
		}
	NSString *depthString=[NSString stringWithFormat: @"%ld", (long) depth];ASSERTNOTNIL(depthString);

// Create slide.
	
	NSXMLElement *slideElement=[NSXMLElement elementWithName: @"slide"];ASSERTNOTNIL(slideElement);
	[slideElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjectsAndKeys: depthString, @"depth", @"master-slide-2", @"master-slide-id", nil]];
	[listElement addChild: slideElement];

// Drawables.

	NSXMLElement *drawablesElement=[NSXMLElement elementWithName: @"drawables"];ASSERTNOTNIL(drawablesElement);
	[slideElement addChild: drawablesElement];

// Body.

	NSXMLElement *bodyElement=[NSXMLElement elementWithName: @"body"];ASSERTNOTNIL(bodyElement);
	[bodyElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"tracks-master", @"tracks-master", nil]
	  forKeys: [NSArray arrayWithObjects: @"visibility", @"vertical-alignment", nil]]];
	[drawablesElement addChild: bodyElement];

// Title.

	NSXMLElement *titleElement=[NSXMLElement elementWithName: @"title"];ASSERTNOTNIL(titleElement);
	[titleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"tracks-master", @"tracks-master", nil]
	  forKeys: [NSArray arrayWithObjects: @"visibility", @"vertical-alignment", nil]]];
	[drawablesElement addChild: titleElement];

// Work out image scaling.

	NSString *affineStr=[self affineStringForMediaSize: imageSize];ASSERTNOTNIL(affineStr);

// Image.

	NSXMLElement *imageElement=[NSXMLElement elementWithName: @"image"];ASSERTNOTNIL(imageElement);
	[imageElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: fileName,
	  [fileName stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding], @"true", affineStr, nil]
	  forKeys: [NSArray arrayWithObjects: @"display-name", @"image-data", @"lock-aspect-ratio", @"transformation", nil]]];
	[drawablesElement addChild: imageElement];

// Transition style.

	NSXMLElement *transitionStyleElement=[NSXMLElement elementWithName: @"transition-style"];ASSERTNOTNIL(transitionStyleElement);
	[transitionStyleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"inherited" forKey: @"type"]];
	[slideElement addChild: transitionStyleElement];
	}


//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

- (void) addNewMovieSlideToList: (NSXMLElement*) listElement withName: (NSString*) fileName nativeSize: (NSSize) movieSize
	{
	
// Work out depth depending on whether we are showing condition and study slides also.

	NSInteger depth=0;
	if ([[[NSUserDefaultsController sharedUserDefaultsController] defaults] boolForKey: IncludeConditionSlidesKey])
		depth++;
	if ([[[NSUserDefaultsController sharedUserDefaultsController] defaults] boolForKey: IncludeStudySlidesKey])
		depth++;
	NSString *depthString=[NSString stringWithFormat: @"%ld", (long) depth];ASSERTNOTNIL(depthString);

// Create slide.
	
	NSXMLElement *slideElement=[NSXMLElement elementWithName: @"slide"];ASSERTNOTNIL(slideElement);
	[slideElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjectsAndKeys: depthString, @"depth", @"master-slide-2", @"master-slide-id", nil]];
	[listElement addChild: slideElement];

// Drawables.

	NSXMLElement *drawablesElement=[NSXMLElement elementWithName: @"drawables"];ASSERTNOTNIL(drawablesElement);
	[slideElement addChild: drawablesElement];

// Body.

	NSXMLElement *bodyElement=[NSXMLElement elementWithName: @"body"];ASSERTNOTNIL(bodyElement);
	[bodyElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"tracks-master", @"tracks-master", nil]
	  forKeys: [NSArray arrayWithObjects: @"visibility", @"vertical-alignment", nil]]];
	[drawablesElement addChild: bodyElement];

// Title.

	NSXMLElement *titleElement=[NSXMLElement elementWithName: @"title"];ASSERTNOTNIL(titleElement);
	[titleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"tracks-master", @"tracks-master", nil]
	  forKeys: [NSArray arrayWithObjects: @"visibility", @"vertical-alignment", nil]]];
	[drawablesElement addChild: titleElement];

// Work out image scaling.

	NSString *affineStr=[self affineStringForMediaSize: movieSize];ASSERTNOTNIL(affineStr);

// Plugin.

	NSXMLElement *pluginElement=[NSXMLElement elementWithName: @"plugin"];ASSERTNOTNIL(pluginElement);
	[pluginElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"1",  affineStr, nil]
	  forKeys: [NSArray arrayWithObjects: @"opacity", @"transformation", nil]]];
	[drawablesElement addChild: pluginElement];

// Plug-in data.

	NSXMLElement *pluginDataElement=[NSXMLElement elementWithName: @"plugin-data"];ASSERTNOTNIL(pluginDataElement);
	[pluginElement addChild: pluginDataElement];

// Movie.

	NSXMLElement *movieElement=[NSXMLElement elementWithName: @"plugin:movie"];ASSERTNOTNIL(movieElement);
	[movieElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: 
	  [fileName stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding], @"root", [NSString stringWithFormat: @"%f", movieSize.width], [NSString stringWithFormat: @"%f", movieSize.height], nil]
	  forKeys: [NSArray arrayWithObjects: @"src", @"key", @"width", @"height", nil]]];
	[pluginDataElement addChild: movieElement];

// CP Version.

	NSXMLElement *cpVersionElement=[NSXMLElement elementWithName: @"string"];ASSERTNOTNIL(cpVersionElement);
	[cpVersionElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"CPVersion", nil]
	  forKeys: [NSArray arrayWithObjects: @"key", nil]]];
	[cpVersionElement setStringValue: @"1.0"];
	[pluginDataElement addChild: cpVersionElement];

// Mime type.

	NSXMLElement *mimeTypeElement=[NSXMLElement elementWithName: @"string"];ASSERTNOTNIL(mimeTypeElement);
	[mimeTypeElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: @"MIMEType", nil]
	  forKeys: [NSArray arrayWithObjects: @"key", nil]]];
	[mimeTypeElement setStringValue: @"video/quicktime"];
	[pluginDataElement addChild: mimeTypeElement];

// Transition style.

	NSXMLElement *transitionStyleElement=[NSXMLElement elementWithName: @"transition-style"];ASSERTNOTNIL(transitionStyleElement);
	[transitionStyleElement setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"inherited" forKey: @"type"]];
	[slideElement addChild: transitionStyleElement];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Utilities

- (NSString*) affineStringForMediaSize: (NSSize) itsSize
	{
	CGFloat								scale;
	
	
	if (itsSize.width<=800 && itsSize.height <=600)
		{
		scale=1.0;
		}
	else
		{
		if (itsSize.width > itsSize.height && itsSize.height<=600)
			scale=800.0/itsSize.width;
		else
			scale=600.0/itsSize.height;
		}
		
	NSInteger newWidth=(itsSize.width)*scale;
	NSInteger newHeight=(itsSize.height)*scale;
	
	NSInteger hOffset=512-(newWidth/2);
	NSInteger vOffset=384-(newHeight/2);
		
	NSString *affineStr=[NSString stringWithFormat: @"%f 0 0 %f %ld %ld", scale, scale, (long) hOffset, (long) vOffset];ASSERTNOTNIL(affineStr);
	return (affineStr);
	}

@end
