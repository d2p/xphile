/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "ApplicationController.h"
#import "FileTableView.h"
#import "ImageTableView.h"
#import "MovieView.h"


@implementation MovieView



- (void) drawRect: (NSRect) rect
	{
	if ([self movie]!=nil)
		{
		[[NSColor blackColor] set];
		[NSBezierPath fillRect: rect];
		}

	[super drawRect: rect];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) isOpaque
	{
	return ([self movie]!=nil);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) setMovie: (id) movie
	{
	[super setMovie: movie];

	[movie setAttribute: [NSNumber numberWithBool: YES] forKey: QTMovieLoopsAttribute];
	[movie setAttribute: [NSNumber numberWithBool: NO] forKey: QTMovieEditableAttribute];
	
	[self setEditable: NO];
	[self setNeedsDisplay: YES];
	[self checkForAutomaticPlay];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) checkForAutomaticPlay
	{
	if ([[NSUserDefaults standardUserDefaults] boolForKey: AutomaticallyPlayMoviesKey])
		{
		[self play: self];
		}
	}
	

@end



//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

@implementation EditableMovieView

- (id) initWithCoder: (NSCoder*) decoder
	{
	if ((self=[super initWithCoder: decoder]))
		{
		[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(fileTableViewWasSelected) name: FTVBecameFirstResponderNotification object: [self window]];
		[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(imageTableViewWasSelected) name: ITVBecameFirstResponderNotification object: [self window]];
		}
		
	return (self);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setMovie: (id) movie
	{
	[super setMovie: movie];

	[movie setAttribute: [NSNumber numberWithBool: YES] forKey: QTMovieEditableAttribute];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) dealloc
	{
	[[NSNotificationCenter defaultCenter] removeObserver: self name: FTVBecameFirstResponderNotification object: [self window]];
	[[NSNotificationCenter defaultCenter] removeObserver: self name: ITVBecameFirstResponderNotification object: [self window]];

	[super dealloc];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) fileTableViewWasSelected
	{
	[self setHidden: NO];
	[self checkForAutomaticPlay];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
	
- (void) imageTableViewWasSelected
	{
	[self pause: self];
	[self setHidden: YES];
	}


@end

