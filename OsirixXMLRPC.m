/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/



#import "OsirixXMLRPC.h"
#import "VersionManager.h"


@implementation OsirixXMLRPC



- (id) initWithAddress: (NSString*) ipAddress andPort: (NSString*) thePort
	{
	if ((self=[super init]))
		{
		theConnection=nil;
		address=[ipAddress retain];ASSERTNOTNIL(address);
		port=[thePort retain];ASSERTNOTNIL(port);
		displayErrors=YES;
		}

	return (self);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) dealloc
	{
	[theConnection release];
	[address release];
	[port release];

	[super dealloc];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setDisplayErrors: (BOOL) b
	{
	displayErrors=b;
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) sendXMLData: (NSData*) xmlData
	{

// HTTP post the xml data.

	NSURL *url=[NSURL URLWithString: [NSString stringWithFormat: @"http://%@:%@", address, port]];ASSERTNOTNIL(url);
	NSMutableURLRequest *request=[[[NSMutableURLRequest alloc] initWithURL: url
	  cachePolicy: NSURLRequestReloadIgnoringCacheData timeoutInterval: 60.0] autorelease];ASSERTNOTNIL(request);
	[request setHTTPMethod: @"POST"]; 
	[request setHTTPBody: xmlData]; 
	[request setValue: [NSString stringWithFormat: @"%ld", (long) [xmlData length]] forHTTPHeaderField: @"Content-Length"]; 
	
	theConnection=[[NSURLConnection connectionWithRequest: request delegate: self] retain];
	if (theConnection)
		{														// Create the NSMutableData that will hold the received data.
		receivedData=[[NSMutableData data] retain];ASSERTNOTNIL(receivedData);
		}
	else														// Failed to make connection.
		{
		[self autorelease];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connection: (NSURLConnection* __unused) connection didReceiveResponse: (NSURLResponse* __unused) response
	{

// This method is called when the server has determined that it has enough information to create the NSURLResponse
// it can be called multiple times, for example in the case of a redirect, so each time we reset the data.
	
	ASSERTNOTNIL(receivedData);
	
	[receivedData setLength: 0];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connection: (NSURLConnection* __unused) connection didReceiveData: (NSData*) data
	{
	ASSERTNOTNIL(data);
	ASSERTNOTNIL(receivedData);

// Append the new data to the receivedData.

    [receivedData appendData: data];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connection: (NSURLConnection* __unused) connection didFailWithError: (NSError*) error
	{
		ASSERTNOTNIL(receivedData);

// Indicate failure to communicate with OsiriX.

	if (displayErrors)
		{
		[[NSAlert alertWithMessageText: UnableToCommunicateWithOsirixString defaultButton: OKString alternateButton: @"" otherButton: @""
		  informativeTextWithFormat: ReasonString, [error localizedDescription]] runModal];
		}
		
// Release the data object.

    [receivedData release];
	receivedData=nil;
	[self autorelease];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connectionDidFinishLoading: (NSURLConnection* __unused) connection
	{
	ASSERTNOTNIL(receivedData);

// Convert response to XML and walk down the tree to the error member.

	@try
		{
		NSError *error=nil;
		NSXMLDocument *responseXMLDoc=[[[NSXMLDocument alloc] initWithData: receivedData options: NSXMLDocumentKind error: &error] autorelease];
		if (responseXMLDoc==nil)
			{
			[NSException raise: @"XML-Exception" format: @"No response received"];
			}

		NSArray *nodesArray=[responseXMLDoc nodesForXPath: @".//member[name='error']/value" error: &error];
		NSXMLElement *resultElement=[nodesArray objectAtIndex: 0];ASSERTNOTNIL(resultElement);
		NSInteger result=[[resultElement stringValue] integerValue];

// If we are communicating with the local machine, consider bringing OsiriX to the front or advising that study was not found.
	
		if ([address isEqualToString: @"127.0.0.1"])
			{
			if (result==0)				// OsiriX found the patientID.
				{
				if ([[NSUserDefaults standardUserDefaults] boolForKey: SetOsirixToFrontOnOpenKey])
					{
					NSArray *osirixAppsArray=[NSRunningApplication runningApplicationsWithBundleIdentifier: OsirixBundleIdentifier];
					ASSERTNOTNIL(osirixAppsArray);
					for (NSRunningApplication *osirixApp in osirixAppsArray)
						{
						[osirixApp activateWithOptions: NSApplicationActivateAllWindows | NSApplicationActivateIgnoringOtherApps];
						}
					}
				}
			else
				{
				[[NSAlert alertWithMessageText: PatientNotFoundString defaultButton: OKString alternateButton: @"" otherButton: @""
				  informativeTextWithFormat: NoMatchInOsirixDatabaseString] runModal];
				}
			}
		}
	@catch (NSException *exception)
		{
// No-op.
		}
	
// Release the data object.

	@finally
		{
		[receivedData release];
		receivedData = nil;
		[self autorelease];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connection: (NSURLConnection* __unused) connection didReceiveAuthenticationChallenge: (NSURLAuthenticationChallenge*) challenge
	{
		ASSERTNOTNIL(challenge);

	[[challenge sender] cancelAuthenticationChallenge: challenge];
	}


@end
