/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "FileTableView.h"
#import "MediaArrayController.h"
#import "XphileDocument.h"


@implementation FileTableView


- (void) awakeFromNib
	{
	[super awakeFromNib];
	
	[self registerForDraggedTypes: [NSArray arrayWithObjects: NSFilenamesPboardType, MediaOrderDragType, nil]];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) becomeFirstResponder
	{
	[[NSNotificationCenter defaultCenter] postNotificationName: FTVBecameFirstResponderNotification object: [self window]];
	
	return [super becomeFirstResponder];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
/*
- (BOOL) resignFirstResponder
	{
	[[NSNotificationCenter defaultCenter] postNotificationName: FTVResignedFirstResponderNotification object: [self window]];

	return [super resignFirstResponder];
	}
*/

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (IBAction) doEdit: (id __unused) sender
	{
	NSWindow *window=[self window];ASSERTNOTNIL(window);
	NSWindowController *controller=[window windowController];ASSERTNOTNIL(controller);
	XphileDocument *document=[controller document];ASSERTNOTNIL(document);
	[document performSelector: @selector(editMovie:) withObject: self];	
	}


@end
