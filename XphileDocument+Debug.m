/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#ifdef Debug

#import "FileEntity.h"
#import "XphileDocument+Debug.h"
#import "XphileDocument+ImportMIRC.h"





@implementation XphileDocument (XphileDocument_Debug)


- (IBAction) doImportFromFileMaker: (id __unused) sender
	{

// This is specific output from a particular FM Pro database.  It demonstrates how to import from an arbitrary text file.
// Limitations are no images imbedded in text file type output from FM Pro.
// Expects [PatientID, doB, gender], [dateOfOnset, system, pathology, region, condition description], dateOfStudy, modality, study region, study description.

	NSError						*anErr;
	NSArray						*returnArray,
								*tabArray;
	NSUInteger					index;
	PatientEntity				*pat		=	nil;
	ConditionEntity				*cond		=	nil;
	StudyEntity					*study		=	nil;
	
	
// Choose file.

	NSOpenPanel *openPanel=[NSOpenPanel openPanel];
	[openPanel setCanChooseDirectories: NO];
	[openPanel setAllowedFileTypes: [NSArray arrayWithObject: @"txt"]];
	if ([openPanel runModal]==NSCancelButton)
		return;

// Read contents into string.

	NSString *textStr=[NSString stringWithContentsOfFile: [[openPanel URL] path] encoding: NSUnicodeStringEncoding error: &anErr];

// Divide into return-delimited array.

	returnArray=[textStr componentsSeparatedByString: @"\r"];

// Divide each into tab-delimted ararys and import their components.
	
	for (index=0; index<[returnArray count]; index++)
		{
		tabArray=[[returnArray objectAtIndex: index] componentsSeparatedByString: @"\t"];
		if ([tabArray count]==12)
			{
			NSDateFormatter *dateFormater=[[[NSDateFormatter alloc] initWithDateFormat: @"%d/%m/%Y" allowNaturalLanguage: NO] autorelease];
			NSString *aStr=[tabArray objectAtIndex: 0];ASSERTNOTNIL(aStr);
			if ([aStr length]>0)
				{
				pat=[NSEntityDescription insertNewObjectForEntityForName: @"Patient" inManagedObjectContext: [self managedObjectContext]];
				[pat setValue: [tabArray objectAtIndex: 0] forKeyPath: @"identifier"];				
				[pat setValue: [dateFormater dateFromString: [tabArray objectAtIndex: 1]] forKeyPath: @"dateOfBirth"];
				NSUInteger genderIndex=0, genderLoop;
				for (genderLoop=0;genderLoop<[[[NSUserDefaults standardUserDefaults] objectForKey: GenderArrayKey] count];genderLoop++)
					{
					if ([[tabArray objectAtIndex: 2] isEqualToString: [[[NSUserDefaults standardUserDefaults] objectForKey: GenderArrayKey] objectAtIndex: genderLoop]])
						genderIndex=genderLoop;
					}
				[pat setValue: [NSNumber numberWithInteger:genderIndex] forKeyPath: @"gender"];
				}

			aStr=[tabArray objectAtIndex: 3];ASSERTNOTNIL(aStr);
			if ([aStr length]>0)
				{
				cond=[NSEntityDescription insertNewObjectForEntityForName: @"Condition" inManagedObjectContext: [self managedObjectContext]];
				[cond setValue: pat forKeyPath: @"patient"];
				[cond setValue: [dateFormater dateFromString: [tabArray objectAtIndex: 3]] forKeyPath: @"dateOfOnset"];
				NSUInteger systemIndex=0, systemLoop;
				for (systemLoop=0;systemLoop<[[[NSUserDefaults standardUserDefaults] objectForKey: SystemArrayKey] count];systemLoop++)
					{
					if ([[tabArray objectAtIndex: 4] isEqualToString: [[[NSUserDefaults standardUserDefaults] objectForKey: SystemArrayKey] objectAtIndex: systemLoop]])
						systemIndex=systemLoop;
					}
				[cond setValue: [NSNumber numberWithInteger:systemIndex] forKeyPath: @"system"];
				NSUInteger pathologyIndex=0, pathologyLoop;
				for (pathologyLoop=0;pathologyLoop<[[[NSUserDefaults standardUserDefaults] objectForKey: PathologyArrayKey] count];pathologyLoop++)
					{
					if ([[tabArray objectAtIndex: 5] isEqualToString: [[[NSUserDefaults standardUserDefaults] objectForKey: PathologyArrayKey] objectAtIndex: pathologyLoop]])
						pathologyIndex=pathologyLoop;
					}
				[cond setValue: [NSNumber numberWithInteger:pathologyIndex] forKeyPath: @"pathology"];
				NSUInteger conditionRegionIndex=0, conditionRegionLoop;
				for (conditionRegionLoop=0;conditionRegionLoop<[[[NSUserDefaults standardUserDefaults] objectForKey: RegionArrayKey] count];conditionRegionLoop++)
					{
					if ([[tabArray objectAtIndex: 6] isEqualToString: [[[NSUserDefaults standardUserDefaults] objectForKey: RegionArrayKey] objectAtIndex: conditionRegionLoop]])
						conditionRegionIndex=conditionRegionLoop;
					}
				[cond setValue: [NSNumber numberWithInteger:conditionRegionIndex] forKeyPath: @"conditionRegion"];
				[cond setValue: [tabArray objectAtIndex: 7] forKeyPath: @"conditionDescription"];
				}
			
			aStr=[tabArray objectAtIndex: 8];ASSERTNOTNIL(aStr);
			if ([aStr length]>0)
				{
				study=[NSEntityDescription insertNewObjectForEntityForName: @"Study" inManagedObjectContext: [self managedObjectContext]];
				[study setValue: cond forKeyPath: @"condition"];
				[study setValue: [dateFormater dateFromString: [tabArray objectAtIndex: 8]] forKeyPath: @"dateOfStudy"];
				NSUInteger modalityIndex=0, modalityLoop;
				for (modalityLoop=0; modalityLoop<[[[NSUserDefaults standardUserDefaults] objectForKey: ModalityArrayKey] count]; modalityLoop++)
					{
					if ([[tabArray objectAtIndex: 9] isEqualToString: [[[NSUserDefaults standardUserDefaults] objectForKey: ModalityArrayKey] objectAtIndex: modalityLoop]])
						modalityIndex=modalityLoop;
					}
				[study setValue: [NSNumber numberWithInteger:modalityIndex] forKeyPath: @"modality"];
				NSUInteger studyRegionIndex=0, studyRegionLoop;
				for (studyRegionLoop=0;studyRegionLoop<[[[NSUserDefaults standardUserDefaults] objectForKey: RegionArrayKey] count];studyRegionLoop++)
					{
					if ([[tabArray objectAtIndex: 10] isEqualToString: [[[NSUserDefaults standardUserDefaults] objectForKey: RegionArrayKey] objectAtIndex: studyRegionLoop]])
						studyRegionIndex=studyRegionLoop;
					}
				[study setValue: [NSNumber numberWithInteger:studyRegionIndex] forKeyPath: @"studyRegion"];
				[study setValue: [tabArray objectAtIndex: 11] forKeyPath: @"studyDescription"];
				}
			}
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (IBAction) debugNewPatient: (id __unused) sender
	{
	NSInteger							itsMax;
	
	
	NSString *dateString=[NSString stringWithFormat: @"%ld-0%ld-24 10:45:32 +1300", 1990+[self randomInt: 16], [self randomInt: 10]-1];
	
	PatientEntity* pat=[NSEntityDescription insertNewObjectForEntityForName: @"Patient" inManagedObjectContext: [self managedObjectContext]];
	[pat setValue: [NSString stringWithFormat: @"%ld", [self randomInt: 99999999]] forKeyPath: @"identifier"];
	[pat setValue: [NSDate dateWithString: dateString] forKeyPath: @"dateOfBirth"];
	[pat setValue: [NSNumber numberWithInteger:[self randomInt: 2]] forKeyPath: @"gender"];

	ConditionEntity* cond=[NSEntityDescription insertNewObjectForEntityForName: @"Condition" inManagedObjectContext: [self managedObjectContext]];
	[cond setValue: pat forKeyPath: @"patient"];
	[cond setValue: [NSDate date] forKeyPath: @"dateOfOnset"];
	itsMax=[[[NSUserDefaults standardUserDefaults] objectForKey: SystemArrayKey] count];
	[cond setValue: [NSNumber numberWithInteger:[self randomInt: itsMax-1]] forKeyPath: @"system"];
	itsMax=[[[NSUserDefaults standardUserDefaults] objectForKey: PathologyArrayKey] count];
	[cond setValue: [NSNumber numberWithInteger:[self randomInt: itsMax-1]] forKeyPath: @"pathology"];
	itsMax=[[[NSUserDefaults standardUserDefaults] objectForKey: RegionArrayKey] count];
	[cond setValue: [NSNumber numberWithInteger:[self randomInt: itsMax-1]] forKeyPath: @"conditionRegion"];
	[cond setValue: @"Condition detail" forKeyPath: @"conditionDescription"];

	StudyEntity* study=[NSEntityDescription insertNewObjectForEntityForName: @"Study" inManagedObjectContext: [self managedObjectContext]];
	[study setValue: cond forKeyPath: @"condition"];
	[study setValue: [NSDate date] forKeyPath: @"dateOfStudy"];
	itsMax=[[[NSUserDefaults standardUserDefaults] objectForKey: ModalityArrayKey] count];
	[study setValue: [NSNumber numberWithInteger:[self randomInt: itsMax-1]] forKeyPath: @"modality"];
	[study setValue: [cond valueForKeyPath: @"conditionRegion"] forKeyPath: @"studyRegion"];
	[study setValue: @"Study detail" forKeyPath: @"studyDescription"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) debugBatchNewPatient: (id __unused) sender
	{
	NSInteger							i;
	
	for (i=0;i<5000;i++)
		[self debugNewPatient: self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -


- (IBAction) debugResponderChain: (id __unused) sender
	{
	RESPONDERCHAIN([imageWindow firstResponder]);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) debugRemoveOrphans: (id __unused) sender
	{
	NSManagedObjectContext *moc=[self managedObjectContext];ASSERTNOTNIL(moc);
	NSFetchRequest *request=[[[NSFetchRequest alloc] init] autorelease];ASSERTNOTNIL(request);
	[request setReturnsObjectsAsFaults: NO];

// Files.

	fprintf(stderr, "Evaluating for orphan files... ");
	NSEntityDescription *entity=[NSEntityDescription entityForName: @"File" inManagedObjectContext: moc];ASSERTNOTNIL(entity);
	[request setEntity: entity];
	NSPredicate *predicate=[NSPredicate predicateWithFormat: @"study == nil"];ASSERTNOTNIL(predicate);
	[request setPredicate: predicate];
	NSError *error=nil;
	NSArray *orphansArray=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(orphansArray);
	fprintf(stderr, "%ld\n", (long) [orphansArray count]);
	NSEnumerator *enumerator=[orphansArray objectEnumerator];ASSERTNOTNIL(enumerator);
	id currentObject;
	while ((currentObject=[enumerator nextObject]))
		{
		fprintf(stderr, "Deleting\n%s\n", [[currentObject description] UTF8String]);
		[moc deleteObject: currentObject];
		}

// Images.

	fprintf(stderr, "Evaluating for orphan images... ");
	entity=[NSEntityDescription entityForName: @"Image" inManagedObjectContext: moc];ASSERTNOTNIL(entity);
	[request setEntity: entity];
	predicate=[NSPredicate predicateWithFormat: @"study == nil"];ASSERTNOTNIL(predicate);
	[request setPredicate: predicate];
	orphansArray=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(orphansArray);
	fprintf(stderr, "%ld\n", (long) [orphansArray count]);
	enumerator=[orphansArray objectEnumerator];ASSERTNOTNIL(enumerator);
	while ((currentObject=[enumerator nextObject]))
		{
		fprintf(stderr, "Deleting\n%s\n", [[currentObject description] UTF8String]);
		[moc deleteObject: currentObject];
		}

// Studies.

	fprintf(stderr, "Evaluating for orphan studies... ");
	entity=[NSEntityDescription entityForName: @"Study" inManagedObjectContext: moc];ASSERTNOTNIL(entity);
	[request setEntity: entity];
	predicate=[NSPredicate predicateWithFormat: @"condition == nil"];ASSERTNOTNIL(predicate);
	[request setPredicate: predicate];
	orphansArray=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(orphansArray);
	fprintf(stderr, "%ld\n", (long) [orphansArray count]);
	enumerator=[orphansArray objectEnumerator];ASSERTNOTNIL(enumerator);
	while ((currentObject=[enumerator nextObject]))
		{
		fprintf(stderr, "Deleting\n%s\n", [[currentObject description] UTF8String]);
		[moc deleteObject: currentObject];
		}

// Conditions.

	fprintf(stderr, "Evaluating for orphan conditions... ");
	entity=[NSEntityDescription entityForName: @"Condition" inManagedObjectContext: moc];ASSERTNOTNIL(entity);
	[request setEntity: entity];
	predicate=[NSPredicate predicateWithFormat: @"patient == nil"];ASSERTNOTNIL(predicate);
	[request setPredicate: predicate];
	orphansArray=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(orphansArray);
	fprintf(stderr, "%ld\n", (long) [orphansArray count]);
	enumerator=[orphansArray objectEnumerator];ASSERTNOTNIL(enumerator);
	while ((currentObject=[enumerator nextObject]))
		{
		fprintf(stderr, "Deleting\n%s\n", [[currentObject description] UTF8String]);
		[moc deleteObject: currentObject];
		}

// Patients.

	fprintf(stderr, "Evaluating for orphan patients...");
	entity=[NSEntityDescription entityForName: @"Patient" inManagedObjectContext: moc];ASSERTNOTNIL(entity);
	[request setEntity: entity];
	[request setPredicate: nil];
	orphansArray=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(orphansArray);
	NSMutableSet *orphansSet=[NSMutableSet set];ASSERTNOTNIL(orphansSet);
	for (PatientEntity *patient in orphansArray)
		{
		NSMutableSet *conditionSet=[patient mutableSetValueForKey: @"conditions"];ASSERTNOTNIL(conditionSet);
		if (conditionSet.count == 0)
			{
			[orphansSet addObject: patient];
			}
		}
	fprintf(stderr, "%ld\n", (long) [orphansSet count]);
	enumerator=[orphansSet objectEnumerator];ASSERTNOTNIL(enumerator);
	while ((currentObject=[enumerator nextObject]))
		{
		fprintf(stderr, "Deleting\n%s\n", [[currentObject description] UTF8String]);
		[moc deleteObject: currentObject];
		}
	
	fprintf(stderr, "Done.\n");
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) debugShowLargeBlobs: (id __unused) sender
	{
	NSManagedObjectContext *moc=[self managedObjectContext];ASSERTNOTNIL(moc);
	NSFetchRequest *request=[[[NSFetchRequest alloc] init] autorelease];ASSERTNOTNIL(request);
	[request setReturnsObjectsAsFaults: NO];

// Files.

	NSLog(@"%@", @"Evaluating for large files... ");
	NSEntityDescription *entity=[NSEntityDescription entityForName: @"File" inManagedObjectContext: moc];ASSERTNOTNIL(entity);
	[request setEntity: entity];
	NSError *error=nil;
	NSArray *largeFilesArray=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(largeFilesArray);

	NSUInteger largeTotal=0L;
	NSUInteger total=0L;
	for (FileEntity *fileEntity in largeFilesArray)
		{
		NSData *data=[fileEntity rawData];ASSERTNOTNIL(data);
		NSUInteger size=[data length];
		total+=size;
		
		if (size > 1000000)
			{
			largeTotal+=size;
			
			StudyEntity *study=[fileEntity valueForKey: @"study"];ASSERTNOTNIL(study);
			ConditionEntity *condition=[study valueForKey: @"condition"];ASSERTNOTNIL(condition);
			PatientEntity *patient=[condition valueForKey: @"patient"];ASSERTNOTNIL(patient);
			NSLog(@"Large file for patient %@: %ld KB", [patient valueForKey: @"identifier"], size/1024);
			}
		}
		
	NSLog(@"Large files represent %ld / %ld KB", largeTotal/1024, total/1024);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) debugRedrawAlbumTable: (id __unused) sender
	{
	[albumTableView setNeedsDisplay: YES];
	}


@end


#endif