/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "AlbumTextFieldCell.h"


@implementation AlbumTextFieldCell


- (id) initWithCoder: (NSCoder*) coder
	{

// Ensure truncated to middle.

	if ((self=[super initWithCoder: coder]))
		{
		[self setLineBreakMode: NSLineBreakByTruncatingMiddle];		
		}
	return self;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
 
- (void) drawInteriorWithFrame: (NSRect) cellFrame inView: (NSView*) controlView
	{
 
// Fill with gradient and display the selected row in white text.
 
	NSMutableDictionary *attrDict=[[[[self attributedStringValue] attributesAtIndex: 0 effectiveRange: nil] mutableCopy] autorelease];
	ASSERTNOTNIL(attrDict);
 
	if ([self isHighlighted])
		{
		NSGradient *gradient;
		
		if ([[controlView window] isMainWindow] && [[controlView window] isKeyWindow] && ([[controlView window] firstResponder]==controlView))
			{
			NSColor *startColor=[NSColor colorWithCalibratedRed: 0.06 green: 0.37 blue: 0.85 alpha: 1.0];ASSERTNOTNIL(startColor);
			NSColor *endColor=[NSColor colorWithCalibratedRed: 0.30 green: 0.60 blue: 0.92 alpha: 1.0];ASSERTNOTNIL(endColor);
			gradient=[[[NSGradient alloc] initWithStartingColor: startColor endingColor: endColor] autorelease];ASSERTNOTNIL(gradient);
			}
		else
			{
			NSColor *startColor=[NSColor colorWithCalibratedRed: 0.43 green: 0.43 blue: 0.43 alpha: 1.0];ASSERTNOTNIL(startColor);
			NSColor *endColor=[NSColor colorWithCalibratedRed: 0.60 green: 0.60 blue: 0.60 alpha: 1.0];ASSERTNOTNIL(endColor);
			gradient=[[[NSGradient alloc] initWithStartingColor: startColor endingColor: endColor] autorelease];ASSERTNOTNIL(gradient);
			}
		ASSERTNOTNIL(gradient);
		[gradient drawInRect: cellFrame angle: 270];

		[attrDict setValue: [NSColor whiteColor] forKey: @"NSColor"];
		}
		
	NSRect drawRect=[self drawingRectForBounds: cellFrame];
	drawRect.origin.x+=2;
	[[self stringValue] drawWithRect: drawRect options: NSStringDrawingUsesLineFragmentOrigin attributes: attrDict];
	}
 


@end
