/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "AlbumEntity.h"
#import "StudyEntity.h"

#define SystemArrayKey									@"SystemArray"
#define PathologyArrayKey								@"PathologyArray"




@interface ConditionEntity : NSManagedObject
	{
	NSUInteger				previewImageIndex;
	}
	
@property (nonatomic, readonly) NSInteger daysOldAtOnset;
@property (nonatomic, readonly, copy) NSString *abbreviatedAgeAtOnset;

@property (nonatomic, readonly, copy) NSString *systemText;
@property (nonatomic, readonly, copy) NSString *pathologyText;
@property (nonatomic, readonly, copy) NSString *conditionRegionText;

@property (nonatomic, readonly) NSUInteger numberOfImages;
@property (nonatomic, readonly) NSUInteger numberOfMovies;
@property (nonatomic, readonly, copy) NSArray *mediaArray;
@property (nonatomic, readonly, copy) NSImage *previewImage;
- (void) incrementPreviewImageIndex;
- (void) decrementPreviewImageIndex;

@property (nonatomic, readonly, copy) NSString *allStudyDescriptions;
//- (NSString*) allMediaDescriptions;
@property (nonatomic, readonly, copy) NSString *allStudyModalities;
@property (nonatomic, readonly, copy) NSDate *oldestStudyDate;
@property (nonatomic, readonly, copy) NSDate *newestStudyDate;

- (NSDictionary*) dictionaryRepresentation;

@end
