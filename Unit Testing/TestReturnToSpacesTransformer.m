/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/



#import <XCTest/XCTest.h>
#import "ReturnToSpacesTransformer.h"




@interface TestReturnToSpacesTransformer : XCTestCase
	{
	ReturnToSpacesTransformer						*transformer;
	}

@end




@implementation TestReturnToSpacesTransformer

- (void) setUp
	{
	[super setUp];
	
	transformer=[[ReturnToSpacesTransformer alloc] init];XCTAssertNotNil(transformer, @"");
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) tearDown
	{
	[transformer release];
	
	[super tearDown];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testRespondsToTransformedValue
	{
	XCTAssertTrue([transformer respondsToSelector: @selector(transformedValue:)], @"");
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testNoReturn
	{
	NSString *inStr=@"No return.";XCTAssertNotNil(inStr, @"");
	NSString *outStr=[transformer transformedValue: inStr];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"No return."], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testWithReturn
	{
	NSString *inStr=@"With\nreturn.";XCTAssertNotNil(inStr, @"");
	NSString *outStr=[transformer transformedValue: inStr];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"With return."], @"%@", outStr);
	}



@end
	