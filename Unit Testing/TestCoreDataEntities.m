/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import <XCTest/XCTest.h>

#import "AgeTransformer.h"
#import "ConditionEntity.h"
#import "ImageEntity.h"
#import "PatientEntity.h"
#import "StudyEntity.h"



@interface TestCoreDataEntities : XCTestCase
	{
	NSManagedObjectModel						*model;
	NSPersistentStoreCoordinator				*coordinator;
	NSManagedObjectContext						*context;
	PatientEntity								*patient;
	}

@end




@implementation TestCoreDataEntities

- (void) setUp
	{
	[super setUp];
	
	model=[NSManagedObjectModel mergedModelFromBundles: @[[NSBundle bundleForClass: [self class]]]];XCTAssertNotNil(model);
	coordinator=[[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: model];XCTAssertNotNil(coordinator);
	context=[[NSManagedObjectContext alloc] init];XCTAssertNotNil(context);
	[context setPersistentStoreCoordinator: coordinator];

	patient=[NSEntityDescription insertNewObjectForEntityForName: @"Patient" inManagedObjectContext: context];XCTAssertNotNil(patient);
	[patient setValue: @"Test Patient" forKey: @"identifier"];
	[patient setValue: @1 forKey: @"gender"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) tearDown
	{
	[context deleteObject: patient];
	[context release];
	[coordinator release];
	
	[super tearDown];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) testPatientExists
	{
	XCTAssertEqual([patient valueForKey: @"identifier"], @"Test Patient");
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) testPatientAgeAtDiagnosis
	{
	[patient setValue: [NSDate dateWithString: @"1969-10-18 00:00:00 +1200"] forKey: @"dateOfBirth"];
	ConditionEntity *condition=[NSEntityDescription insertNewObjectForEntityForName: @"Condition" inManagedObjectContext: context];XCTAssertNotNil(condition);
	[condition setValue: patient forKey: @"patient"];

// Test -1 days old.
	
	[condition setValue: [NSDate dateWithString: @"1969-10-17 00:00:00 +1200"] forKey: @"dateOfOnset"];
	XCTAssertTrue([[condition valueForKeyPath: @"abbreviatedAgeAtOnset"] isEqualToString: @"?"], @"%@", [condition valueForKeyPath: @"abbreviatedAgeAtOnset"]);
	XCTAssertTrue([[condition valueForKeyPath: @"daysOldAtOnset"] intValue] == -1, @"%d", [[condition valueForKeyPath: @"daysOldAtOnset"] intValue]);

// Test 0 days old.
	
	[condition setValue: [NSDate dateWithString: @"1969-10-18 00:00:00 +1200"] forKey: @"dateOfOnset"];
	XCTAssertTrue([[condition valueForKeyPath: @"abbreviatedAgeAtOnset"] isEqualToString: @"0d"], @"%@", [condition valueForKeyPath: @"abbreviatedAgeAtOnset"]);
	XCTAssertTrue([[condition valueForKeyPath: @"daysOldAtOnset"] intValue] == 0, @"%d", [[condition valueForKeyPath: @"daysOldAtOnset"] intValue]);

// Test 1 day old.
	
	[condition setValue: [NSDate dateWithString: @"1969-10-19 00:00:00 +1200"] forKey: @"dateOfOnset"];
	XCTAssertTrue([[condition valueForKeyPath: @"abbreviatedAgeAtOnset"] isEqualToString: @"1d"], @"%@", [condition valueForKeyPath: @"abbreviatedAgeAtOnset"]);
	XCTAssertTrue([[condition valueForKeyPath: @"daysOldAtOnset"] intValue] == 1, @"%d", [[condition valueForKeyPath: @"daysOldAtOnset"] intValue]);

// Test 10 days old.
	
	[condition setValue: [NSDate dateWithString: @"1969-10-28 00:00:00 +1200"] forKey: @"dateOfOnset"];
	XCTAssertTrue([[condition valueForKeyPath: @"abbreviatedAgeAtOnset"] isEqualToString: @"10d"], @"%@", [condition valueForKeyPath: @"abbreviatedAgeAtOnset"]);
	XCTAssertTrue([[condition valueForKeyPath: @"daysOldAtOnset"] intValue] == 10, @"%d", [[condition valueForKeyPath: @"daysOldAtOnset"] intValue]);

// Test 2 weeks old.
	
	[condition setValue: [NSDate dateWithString: @"1969-11-2 00:00:00 +1200"] forKey: @"dateOfOnset"];
	XCTAssertTrue([[condition valueForKeyPath: @"abbreviatedAgeAtOnset"] isEqualToString: @"2w"], @"%@", [condition valueForKeyPath: @"abbreviatedAgeAtOnset"]);
	XCTAssertTrue([[condition valueForKeyPath: @"daysOldAtOnset"] intValue] == 15, @"%d", [[condition valueForKeyPath: @"daysOldAtOnset"] intValue]);

// Test 4 weeks old.
	
	[condition setValue: [NSDate dateWithString: @"1969-11-16 00:00:00 +1200"] forKey: @"dateOfOnset"];
	XCTAssertTrue([[condition valueForKeyPath: @"abbreviatedAgeAtOnset"] isEqualToString: @"4w"], @"%@", [condition valueForKeyPath: @"abbreviatedAgeAtOnset"]);
	XCTAssertTrue([[condition valueForKeyPath: @"daysOldAtOnset"] intValue] == 29, @"%d", [[condition valueForKeyPath: @"daysOldAtOnset"] intValue]);

// Test 4 months old.
	
	[condition setValue: [NSDate dateWithString: @"1970-2-18 00:00:00 +1200"] forKey: @"dateOfOnset"];
	XCTAssertTrue([[condition valueForKeyPath: @"abbreviatedAgeAtOnset"] isEqualToString: @"4m"], @"%@", [condition valueForKeyPath: @"abbreviatedAgeAtOnset"]);
	XCTAssertTrue([[condition valueForKeyPath: @"daysOldAtOnset"] intValue] == 123, @"%d", [[condition valueForKeyPath: @"daysOldAtOnset"] intValue]);

// Test 1 year old.
	
	[condition setValue: [NSDate dateWithString: @"1970-10-18 00:00:00 +1200"] forKey: @"dateOfOnset"];
	XCTAssertTrue([[condition valueForKeyPath: @"abbreviatedAgeAtOnset"] isEqualToString: @"1y"], @"%@", [condition valueForKeyPath: @"abbreviatedAgeAtOnset"]);
	XCTAssertTrue([[condition valueForKeyPath: @"daysOldAtOnset"] intValue] == 365, @"%d", [[condition valueForKeyPath: @"daysOldAtOnset"] intValue]);

// Test 10 years old.
	
	[condition setValue: [NSDate dateWithString: @"1979-10-18 00:00:00 +1200"] forKey: @"dateOfOnset"];
	XCTAssertTrue([[condition valueForKeyPath: @"abbreviatedAgeAtOnset"] isEqualToString: @"10y"], @"%@", [condition valueForKeyPath: @"abbreviatedAgeAtOnset"]);
	XCTAssertTrue([[condition valueForKeyPath: @"daysOldAtOnset"] intValue] == 365*10+10/4, @"%d", [[condition valueForKeyPath: @"daysOldAtOnset"] intValue]);

// Test 30 years old.
	
	[condition setValue: [NSDate dateWithString: @"1999-10-18 00:00:00 +1200"] forKey: @"dateOfOnset"];
	XCTAssertTrue([[condition valueForKeyPath: @"abbreviatedAgeAtOnset"] isEqualToString: @"30y"], @"%@", [condition valueForKeyPath: @"abbreviatedAgeAtOnset"]);
	XCTAssertTrue([[condition valueForKeyPath: @"daysOldAtOnset"] intValue] == 365*30+30/4, @"%d", [[condition valueForKeyPath: @"daysOldAtOnset"] intValue]);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) testAbbreviatedAgeAtStudy
	{
	[patient setValue: [NSDate dateWithString: @"2000-1-1 00:00:00 +1200"] forKey: @"dateOfBirth"];
	ConditionEntity *condition=[NSEntityDescription insertNewObjectForEntityForName: @"Condition" inManagedObjectContext: context];XCTAssertNotNil(condition);
	[condition setValue: patient forKey: @"patient"];
	[condition setValue: [NSDate dateWithString: @"2005-1-1 00:00:00 +1200"] forKey: @"dateOfOnset"];
	StudyEntity *study=[NSEntityDescription insertNewObjectForEntityForName: @"Study" inManagedObjectContext: context];XCTAssertNotNil(study);
	[study setValue: condition forKey: @"condition"];

// Test 0 days old.
	
	[study setValue: [NSDate dateWithString: @"2000-1-1 00:00:00 +1200"] forKey: @"dateOfStudy"];
	XCTAssertTrue([[study valueForKeyPath: @"abbreviatedAgeAtStudy"] isEqualToString: @"0d"], @"%@", [study valueForKeyPath: @"abbreviatedAgeAtStudy"]);
	XCTAssertTrue([[study valueForKeyPath: @"daysOldAtStudy"] intValue] == 0, @"%d", [[study valueForKeyPath: @"daysOldAtStudy"] intValue]);

// Test 5 weeks old.
	
	[study setValue: [NSDate dateWithString: @"2000-2-5 00:00:00 +1200"] forKey: @"dateOfStudy"];
	XCTAssertTrue([[study valueForKeyPath: @"abbreviatedAgeAtStudy"] isEqualToString: @"5w"], @"%@", [study valueForKeyPath: @"abbreviatedAgeAtStudy"]);
	XCTAssertTrue([[study valueForKeyPath: @"daysOldAtStudy"] intValue] == 5*7, @"%d", [[study valueForKeyPath: @"daysOldAtStudy"] intValue]);

// Test 5 years old.
	
	[study setValue: [NSDate dateWithString: @"2005-1-1 00:00:00 +1200"] forKey: @"dateOfStudy"];
	XCTAssertTrue([[study valueForKeyPath: @"abbreviatedAgeAtStudy"] isEqualToString: @"5y"], @"%@", [study valueForKeyPath: @"abbreviatedAgeAtStudy"]);
	XCTAssertTrue([[study valueForKeyPath: @"daysOldAtStudy"] intValue] == 365*5+2, @"%d", [[study valueForKeyPath: @"daysOldAtStudy"] intValue]);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testStudyTimeFromDiagnosis
	{
	TimeSinceTransformer *timeSinceTransformer=[[[TimeSinceTransformer alloc] init] autorelease];XCTAssertNotNil(timeSinceTransformer, @"");

	[patient setValue: [NSDate dateWithString: @"2000-1-1 00:00:00 +1200"] forKey: @"dateOfBirth"];
	ConditionEntity *condition=[NSEntityDescription insertNewObjectForEntityForName: @"Condition" inManagedObjectContext: context];XCTAssertNotNil(condition);
	[condition setValue: patient forKey: @"patient"];
	[condition setValue: [NSDate dateWithString: @"2005-1-1 00:00:00 +1200"] forKey: @"dateOfOnset"];
	StudyEntity *study=[NSEntityDescription insertNewObjectForEntityForName: @"Study" inManagedObjectContext: context];XCTAssertNotNil(study);
	[study setValue: condition forKey: @"condition"];
	
	
// Test -3 days since onset.
	
	[study setValue: [NSDate dateWithString: @"2004-12-29 00:00:00 +1200"] forKey: @"dateOfStudy"];
	XCTAssertTrue([[timeSinceTransformer transformedValue: [study valueForKeyPath: @"daysSinceOnset"]] isEqualToString: @"-3 days"]);
	XCTAssertTrue([[study valueForKeyPath: @"daysSinceOnset"] intValue] == -3, @"%d", [[study valueForKeyPath: @"daysSinceOnset"] intValue]);

// Test -1 days since onset.
	
	[study setValue: [NSDate dateWithString: @"2004-12-31 00:00:00 +1200"] forKey: @"dateOfStudy"];
	XCTAssertTrue([[timeSinceTransformer transformedValue: [study valueForKeyPath: @"daysSinceOnset"]] isEqualToString: @"-1 day"]);
	XCTAssertTrue([[study valueForKeyPath: @"daysSinceOnset"] intValue] == -1, @"%d", [[study valueForKeyPath: @"daysSinceOnset"] intValue]);

// Test 0 days since onset.
	
	[study setValue: [NSDate dateWithString: @"2005-1-1 00:00:00 +1200"] forKey: @"dateOfStudy"];
	XCTAssertTrue([[timeSinceTransformer transformedValue: [study valueForKeyPath: @"daysSinceOnset"]] isEqualToString: @"0 days"]);
	XCTAssertTrue([[study valueForKeyPath: @"daysSinceOnset"] intValue] == 0, @"%d", [[study valueForKeyPath: @"daysSinceOnset"] intValue]);

// Test 1 day since onset.
	
	[study setValue: [NSDate dateWithString: @"2005-1-2 00:00:00 +1200"] forKey: @"dateOfStudy"];
	XCTAssertTrue([[timeSinceTransformer transformedValue: [study valueForKeyPath: @"daysSinceOnset"]] isEqualToString: @"1 day"]);
	XCTAssertTrue([[study valueForKeyPath: @"daysSinceOnset"] intValue] == 1, @"%d", [[study valueForKeyPath: @"daysSinceOnset"] intValue]);

// Test 5 days since onset.
	
	[study setValue: [NSDate dateWithString: @"2005-1-6 00:00:00 +1200"] forKey: @"dateOfStudy"];
	XCTAssertTrue([[timeSinceTransformer transformedValue: [study valueForKeyPath: @"daysSinceOnset"]] isEqualToString: @"5 days"]);
	XCTAssertTrue([[study valueForKeyPath: @"daysSinceOnset"] intValue] == 5, @"%d", [[study valueForKeyPath: @"daysSinceOnset"] intValue]);

// Test 10 days since onset.
	
	[study setValue: [NSDate dateWithString: @"2005-1-11 00:00:00 +1200"] forKey: @"dateOfStudy"];
	XCTAssertTrue([[timeSinceTransformer transformedValue: [study valueForKeyPath: @"daysSinceOnset"]] isEqualToString: @"10 days"]);
	XCTAssertTrue([[study valueForKeyPath: @"daysSinceOnset"] intValue] == 10, @"%d", [[study valueForKeyPath: @"daysSinceOnset"] intValue]);

// Test 12 weeks since onset.
	
	[study setValue: [NSDate dateWithString: @"2005-4-1 00:00:00 +1200"] forKey: @"dateOfStudy"];
	XCTAssertTrue([[timeSinceTransformer transformedValue: [study valueForKeyPath: @"daysSinceOnset"]] isEqualToString: @"12 weeks"], @"%@", [timeSinceTransformer transformedValue: [study valueForKeyPath: @"daysSinceOnset"]]);
	XCTAssertTrue([[study valueForKeyPath: @"daysSinceOnset"] intValue] == 90, @"%d", [[study valueForKeyPath: @"daysSinceOnset"] intValue]);

// Test 6 months since onset.
	
	[study setValue: [NSDate dateWithString: @"2005-7-1 00:00:00 +1200"] forKey: @"dateOfStudy"];
	XCTAssertTrue([[timeSinceTransformer transformedValue: [study valueForKeyPath: @"daysSinceOnset"]] isEqualToString: @"6 months"], @"%@", [timeSinceTransformer transformedValue: [study valueForKeyPath: @"daysSinceOnset"]]);
	XCTAssertTrue([[study valueForKeyPath: @"daysSinceOnset"] intValue] == 181, @"%d", [[study valueForKeyPath: @"daysSinceOnset"] intValue]);

	}

@end
