/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import <XCTest/XCTest.h>
#import "AgeTransformer.h"




@interface TestAgeTransformer : XCTestCase
	{
	AgeTransformer						*transformer;
	}

@end



@interface TestTimeSinceTransformer : XCTestCase
	{
	TimeSinceTransformer				*transformer;
	}

@end


@interface TestTimeCourseTransformer : XCTestCase
	{
	TimeCourseTransformer				*transformer;
	}

@end


@implementation TestAgeTransformer

- (void) setUp
	{
	[super setUp];
	
	transformer=[[AgeTransformer alloc] init];XCTAssertNotNil(transformer, @"");
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) tearDown
	{
	[transformer release];
	
	[super tearDown];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testRespondsToTransformedValue
	{
	XCTAssertTrue([transformer respondsToSelector: @selector(transformedValue:)], @"");
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testZero
	{
	NSNumber *inNum=@0;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"0w 0d"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testNegativeAge
	{
	NSNumber *inNum=@-2000;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"-"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testOneDay
	{
	NSNumber *inNum=@1;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"0w 1d"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testThreeWeeksThreeDays
	{
	NSNumber *inNum=@(3*7+3);XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"3w 3d"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testFourWeeks
	{
	NSNumber *inNum=@28;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"4w 0d"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testThreeMonths
	{
	NSNumber *inNum=@(365/4+1);XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"3m 0d"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testHalfYear
	{
	NSNumber *inNum=@(365/2+1);XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"6m 0d"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testSixMonthsTenDays
	{
	NSNumber *inNum=@(365/2+1+10);XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"6m 10d"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testThreeYearsEightMonths
	{
	NSNumber *inNum=@(3*365.25 + 365.25*2/3 + 1);XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"3y 8m"], @"%@", outStr);
	}


@end


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

@implementation TestTimeSinceTransformer

- (void) setUp
	{
	transformer=[[TimeSinceTransformer alloc] init];XCTAssertNotNil(transformer, @"");
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) tearDown
	{
	[transformer release];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testRespondsToTransformedValue
	{
	XCTAssertTrue([transformer respondsToSelector: @selector(transformedValue:)], @"");
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testZero
	{
	NSNumber *inNum=@0;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"0 days"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testOneDay
	{
	NSNumber *inNum=@1;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"1 day"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testThreeWeeks
	{
	NSNumber *inNum=@21;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"3 weeks"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testSixWeeks
	{
	NSNumber *inNum=@(7*6);XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"6 weeks"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testEightWeeks
	{
	NSNumber *inNum=@(8*7);XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"8 weeks"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testThreeMonths
	{
	NSNumber *inNum=@(31*3);XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"3 months"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testEighteenMonths
	{
	NSNumber *inNum=@(365 * 1.5 + 1);XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"18 months"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testSmallNegative
	{
	NSNumber *inNum=@-2;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"-2 days"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testLargeNegative
	{
	NSNumber *inNum=@-4000;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"-11 years"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testSmallPositive
	{
	NSNumber *inNum=@3;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"3 days"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testLargePositive
	{
	NSNumber *inNum=@2000;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"5 years"], @"%@", outStr);
	}


@end


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

@implementation TestTimeCourseTransformer

- (void) setUp
	{
	transformer=[[TimeCourseTransformer alloc] init];XCTAssertNotNil(transformer, @"");
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) tearDown
	{
	[transformer release];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testRespondsToTransformedValue
	{
	XCTAssertTrue([transformer respondsToSelector: @selector(transformedValue:)], @"");
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testZero
	{
	NSNumber *inNum=@0;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"Day 1"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testOneDay
	{
	NSNumber *inNum=@1;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"Day 2"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testThreeWeeks
	{
	NSNumber *inNum=@21;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"Week 3"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testSixWeeks
	{
	NSNumber *inNum=@(7*6);XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"Week 6"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testEightWeeks
	{
	NSNumber *inNum=@(8*7);XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"Week 8"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testThreeMonths
	{
	NSNumber *inNum=@(31*3);XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"Month 3"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testEighteenMonths
	{
	NSNumber *inNum=@(365 * 1.5 + 1);XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"Month 18"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testSmallNegative
	{
	NSNumber *inNum=@-2;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"Day -1"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testLargeNegative
	{
	NSNumber *inNum=@-4000;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"Year -11"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testSmallPositive
	{
	NSNumber *inNum=@3;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"Day 4"], @"%@", outStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) testLargePositive
	{
	NSNumber *inNum=@2000;XCTAssertNotNil(inNum, @"");
	NSString *outStr=[transformer transformedValue: inNum];XCTAssertNotNil(outStr, @"");
	XCTAssertTrue([outStr isEqualToString: @"Year 5"], @"%@", outStr);
	}


@end
