/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "AgeTransformer.h"
#import "StudyEntity.h"



const CGFloat										avDaysPerMonth	=	(365.25/12);

 
@implementation AgeTransformer
 

+ (Class) transformedValueClass
	{
	return [NSString self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (BOOL) allowsReverseTransformation
	{
	return (NO);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (id) transformedValue: (id) beforeObject
	{
	if (beforeObject == nil)
		return (@"-");
	
	NSInteger totalDays=[beforeObject integerValue];
	if (totalDays == UnspecifiedAge)
		return (@"-");

	if (totalDays < 0)
		return (@"-");
	
	if (totalDays > 200*365)
		return (@"-");
	
	if (totalDays > 365.25)
		{
		NSInteger years=floor(totalDays / 365.25);
		NSInteger months=(totalDays - years * 365.25) / avDaysPerMonth;
		return [NSString stringWithFormat: YearsMonthsString, years, months];
		}
	else if (totalDays < 60)
		{
		NSInteger weeks=floor(totalDays / 7);
		NSInteger days=(totalDays - weeks * 7);
		return [NSString stringWithFormat: WeeksDaysString, weeks, days];
		}
	else
		{
		NSInteger months=floor(totalDays / avDaysPerMonth);
		NSInteger days=totalDays - (months * avDaysPerMonth);
		return [NSString stringWithFormat: MonthsDaysString, months, days];
		}
	}


@end


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
 
@implementation TimeSinceTransformer
 

+ (Class) transformedValueClass
	{
	return [NSString self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (BOOL) allowsReverseTransformation
	{
	return (NO);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (id) transformedValue: (id) beforeObject
	{
	NSInteger						years, months, weeks, days;


	if (beforeObject == nil)
		return (@"-");
	
	days=[beforeObject integerValue];
	if (days==UnspecifiedAge)
		return (@"-");

	if (labs(days)>740)
		{
		years=round(days / 365.25);	
		return [NSString stringWithFormat: @"%ld %@", (long) years, labs(years)==1 ? SingularYearString : PluralYearsString];
		}
	else if (labs(days)>90)
		{
		months=round(days / avDaysPerMonth);
		return [NSString stringWithFormat: @"%ld %@", (long) months, labs(months)==1 ? SingularMonthString : PluralMonthsString];
		}
	else if (labs(days)>14)
		{
		weeks=round(days / 7);
		return [NSString stringWithFormat: @"%ld %@", (long) weeks, PluralWeeksString];
		}

	return [NSString stringWithFormat: @"%ld %@", (long) days, labs(days)==1 ? SingularDayString : PluralDaysString];
	}


@end


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
 
@implementation TimeCourseTransformer
 

+ (Class) transformedValueClass
	{
	return [NSString self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (BOOL) allowsReverseTransformation
	{
	return (NO);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————


- (id) transformedValue: (id) beforeObject
	{
	NSInteger						years, months, weeks, days;
	NSString				*returnStr;
	

	if (beforeObject == nil)
		return (@"-");
	
	days=[beforeObject integerValue]+1;				// Day 0 -> Day 1
	if (days==UnspecifiedAge)
		return (@"-");

	if (labs(days)>740)
		{
		years=round(days / 365.25);	
		returnStr=[NSString stringWithFormat: @"%@ %ld", SingularYearString, (long) years];ASSERTNOTNIL(returnStr);
		}
	else if (labs(days)>90)
		{
		months=round(days / avDaysPerMonth);
		returnStr=[NSString stringWithFormat: @"%@ %ld", SingularMonthString, (long) months];ASSERTNOTNIL(returnStr);
		}
	else if (labs(days)>14)
		{
		weeks=round(days / 7);
		returnStr=[NSString stringWithFormat: @"%@ %ld", SingularWeekString, (long) weeks];ASSERTNOTNIL(returnStr);
		}
	else
		{
		returnStr=[NSString stringWithFormat: @"%@ %ld", SingularDayString, (long) days];ASSERTNOTNIL(returnStr);
		}
		
	return [returnStr capitalizedString];
	}

@end