/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "NSImageView+Additions.h"


@implementation NSImageView (NSImageView_Additions)


- (CGFloat) scalingFactor
	{
	NSSize size = [[self image] size];
	NSRect itsFrame = [self bounds]; 

	if (NSWidth(itsFrame) > size.width && NSHeight(itsFrame) > size.height)
		{					// Both dimensions are larger than the image.
		return 1.0;
		}
	else					// One or zero dimensions of the image is greater.  Record the smallest ratio.
		{
		CGFloat xRatio = NSWidth(itsFrame) / size.width;
		CGFloat yRatio = NSHeight(itsFrame) / size.height;
		return MIN(xRatio, yRatio);
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSRect) actualRectForImageView
	{
	NSSize size = [[self image] size];
	NSRect itsBounds = [self bounds];
	CGFloat scalingFactor = [self scalingFactor];
	
	NSRect actualRect;
	actualRect.size.width = floor(size.width * scalingFactor + 0.5);
	actualRect.size.height = floor(size.height * scalingFactor + 0.5);
	actualRect.origin.x = floor((itsBounds.size.width - actualRect.size.width) / 2.0 + 0.5);
	actualRect.origin.y = floor((itsBounds.size.height - actualRect.size.height) / 2.0 + 0.5);
	return (actualRect);
	}
	

@end
