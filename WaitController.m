/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "WaitController.h"


@implementation WaitController


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (id) initWithStatusString: (NSString*) s
	{
	self=[self initWithWindowNibName: @"Wait"];
	if (self != nil)
		{
		initialStatusString=s;
		}
	return self;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) dealloc
	{
	[self dismiss];

	[super dealloc];
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) windowDidLoad
	{
	[[self window] setLevel: NSModalPanelWindowLevel];
	[[self window] center];
	[statusText setStringValue: initialStatusString];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) showWindow: (id) sender
	{
	[super showWindow: sender];
	
	[[self window] makeKeyAndOrderFront: sender];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) dismiss
	{
	[[self window] orderOut: self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) setIndeterminate
	{
	[progressBar setIndeterminate: YES];
	[progressBar startAnimation: self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setDeterminate
	{
	[progressBar setIndeterminate: NO];
	[progressBar startAnimation: self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setProgressBarValue: (NSInteger) i
	{
	[progressBar setDoubleValue: (double) i];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setProgressBarMax: (NSInteger) i
	{
	[progressBar setMaxValue: (double) i];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setStatusString: (NSString*) s
	{
	ASSERTNOTNIL(s);
	[statusText setStringValue: s];
	}


@end
