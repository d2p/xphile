/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "FileEntity.h"
#import "ImageEntity.h"
#import "StudyEntity.h"


@implementation StudyEntity



//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingDaysOldAtStudy
	{
	return [NSSet setWithObjects: @"condition.patient.dateOfBirth", @"dateOfStudy", nil];
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingAbbreviatedAgeAtStudy
	{
	return [NSSet setWithObjects: @"condition.patient.dateOfBirth", @"dateOfStudy", nil];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingDaysSinceOnset
	{
	return [NSSet setWithObjects: @"condition.dateOfOnset", @"dateOfStudy", nil];
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingStudyRegionText
	{
	return [NSSet setWithObjects: @"studyRegion", nil];
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingModalityText
	{
	return [NSSet setWithObjects: @"modality", nil];
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingStudyInfo
	{
	return [NSSet setWithObjects: @"modality", @"studyRegion", nil];
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingImageWindowTitle
	{
	return [NSSet setWithObjects: @"condition.patient.identifier", @"condition.patient.dateOfBirth", @"condition.patient.gender",
	  @"modality", @"studyRegion", @"dateOfStudy", nil];
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingConditionAndStudyDescription
	{
	return [NSSet setWithObjects: @"condition.conditionDescription", @"studyDescription", nil];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (id) valueForUndefinedKey: (NSString*) key
	{
	NSLog(@"StudyEntity: no such key - %@", key);
	return (nil);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (NSInteger) daysOldAtStudy
	{
	NSDate *birthDate=[self valueForKeyPath: @"condition.patient.dateOfBirth"];
	if (!birthDate) return (UnspecifiedAge);
	NSDate *studyDate=[self valueForKeyPath: @"dateOfStudy"];
	if (!studyDate) return (UnspecifiedAge);
	
	CGFloat days=[studyDate timeIntervalSinceDate: birthDate] / (60*60*24);
	return (ceil(days));
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) abbreviatedAgeAtStudy
	{
	NSDate *birthDate=[self valueForKeyPath: @"condition.patient.dateOfBirth"];
	if (!birthDate) return (@"?");
	NSDate *studyDate=[self valueForKeyPath: @"dateOfStudy"];
	if (!studyDate) return (@"?");
		
	NSInteger days=ceil([studyDate timeIntervalSinceDate: birthDate] / (60*60*24));
	NSInteger years=floor(days/365.25);

	if (years==0 && days<14)
		return [NSString stringWithFormat: AbbreviatedDaysString, days];
	else if (years==0 && days<90)
		{
		NSInteger weeks=round(days/7);
		return [NSString stringWithFormat: AbbreviatedWeeksString, weeks];
		}
	else if (years==0)
		{
		NSInteger months=round(days/(365/12));
		return [NSString stringWithFormat: AbbreviatedMonthsString, months];
		}
	else
		return [NSString stringWithFormat: AbbreviatedYearsString, years];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSInteger) daysSinceOnset
	{
	NSDate *conditionDate=[self valueForKeyPath: @"condition.dateOfOnset"];
	if (!conditionDate) return (UnspecifiedAge);
	NSDate *studyDate=[self valueForKeyPath: @"dateOfStudy"];
	if (!studyDate) return (UnspecifiedAge);

	CGFloat daysFromOnset=[studyDate timeIntervalSinceDate: conditionDate] / (60*60*24);
	return (ceil(daysFromOnset));
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Full Screen HUD

- (NSString*) conditionAndStudyDescription
	{
	NSString *condDesc=[self valueForKeyPath: @"condition.conditionDescription"];
	if (condDesc==nil)
		condDesc=@"";
	
	NSString *studyDesc=[self valueForKey: @"studyDescription"];
	if (studyDesc==nil)
		studyDesc=@"";
	
	NSString *combinedDesc=[NSString stringWithFormat: @"%@\n\n%@", condDesc, studyDesc];ASSERTNOTNIL(combinedDesc);
	return combinedDesc;
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Text Labels

- (NSString*) studyRegionText
	{
	NSInteger regionTag=[[self valueForKeyPath: @"studyRegion"] integerValue];
	return [[NSUserDefaults standardUserDefaults] valueForKeyPath: RegionArrayKey][regionTag];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) modalityText
	{
	NSInteger modalityTag=[[self valueForKeyPath: @"modality"] integerValue];
	return [[NSUserDefaults standardUserDefaults] valueForKeyPath: ModalityArrayKey][modalityTag];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Image Labels

- (NSInteger) numberOfImages
	{
	NSSet *set=[self valueForKey: @"images"];ASSERTNOTNIL(set);
	NSInteger count=[set count];
	
	return (count);
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSInteger) numberOfMovies
	{
	NSSet *set=[self valueForKey: @"files"];ASSERTNOTNIL(set);
	NSInteger count=[set count];
	
	return (count);
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) imageWindowTitle
	{
	NSString					*studyDateStr	=	nil;
	
	
	NSDate *studyDate=[self valueForKeyPath: @"dateOfStudy"];
	if (studyDate)
		{
		NSDateFormatter *dateFormatter=[[[NSDateFormatter alloc] init] autorelease];ASSERTNOTNIL(dateFormatter);
		[dateFormatter setDateStyle: NSDateFormatterShortStyle];
		studyDateStr=[dateFormatter stringFromDate: studyDate];
		}
	else
		studyDateStr=@"-";
	ASSERTNOTNIL(studyDateStr);
		
	return ([NSString stringWithFormat: @"%@ (%@ %@) | %@ %@ (%@)",
	  [self valueForKeyPath: @"condition.patient.identifier"],
	  [self abbreviatedAgeAtStudy],
	  [self valueForKeyPath: @"condition.patient.abbreviatedGenderText"],
	  [self modalityText],
	  [self studyRegionText],
	  studyDateStr
	  ]);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) studyInfo
	{
	return ([NSString stringWithFormat: @"%@ %@", [self modalityText], [self studyRegionText]]);
	}


@end
