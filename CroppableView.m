/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "CropPath.h"
#import "CroppableView.h"
#import "ImageArrayController.h"


@implementation CroppableView

- (id) initWithCoder: (NSCoder*) decoder
	{
	if ((self=[super initWithCoder: decoder]))
		{
		[self setSelectionMarker: [CropPath markerForView: self]];
		[self performSelector: @selector(delayedSetNotification) withObject: nil afterDelay: 0.0];
		}
	return self;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) delayedSetNotification
	{
	NSDictionary *bindingDict=[self infoForBinding: NSValueBinding];ASSERTNOTNIL(bindingDict);
	NSArrayController *ac=[bindingDict valueForKey: NSObservedObjectKey];ASSERTNOTNIL(ac);
	[[NSNotificationCenter defaultCenter] addObserver: ac selector: @selector(notifyCropFromView:) name: CropNotification object: self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) dealloc
	{
	NSDictionary *bindingDict=[self infoForBinding: NSValueBinding];ASSERTNOTNIL(bindingDict);
	NSArrayController *ac=[bindingDict valueForKey: NSObservedObjectKey];ASSERTNOTNIL(ac);
	[[NSNotificationCenter defaultCenter] removeObserver: ac name: CropNotification object: self];
		
	[super dealloc];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) mouseUp: (NSEvent*) theEvent 
	{ 
	if ([theEvent clickCount] > 1)
		{
		NSPoint pt=[theEvent locationInWindow];
		pt=[self convertPoint: pt fromView: nil];
		if (NSPointInRect(pt, [selectionMarker selectedRect]))
			[[NSNotificationCenter defaultCenter] postNotificationName: CropNotification object: self];
		return;
		}
	
	[super mouseUp: theEvent];
	}



@end
