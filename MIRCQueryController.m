/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/




#import "ApplicationController.h"
#import "MIRCQueryController.h"
#import "VersionManager.h"
#import "XphileDocument+ImportMIRC.h"



@implementation MIRCQueryController


- (id) init
	{
	self=[super initWithWindowNibName: @"MIRCQuery"];
	if (self != nil)
		{
		queryResultsArray=[[NSMutableArray array] retain];
		queryData=nil;
		fetchData=nil;
		}
		
	return self;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) windowDidLoad
	{
	ASSERTNOTNIL([self window]);
	ASSERTNOTNIL(statusField);
	
	[[self window] center];
	[statusField setStringValue: @""];
	[self setFetchInProgress: NO];
	[self setSearchText: @""];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) windowFrameAutosaveName
	{
	return @"SearchMIRC";
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (NSMutableArray*) queryResultsArray
	{
	return (queryResultsArray);
	}
	


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) fetchInProgress
	{
	return (fetchInProgress);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setFetchInProgress: (BOOL) b
	{
	fetchInProgress=b;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) searchText
	{
	return (searchText);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setSearchText: (NSString*) s
	{
	[s retain];
	[searchText release];
	searchText=s;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) connection: (NSURLConnection* __unused) connection didReceiveResponse: (NSURLResponse* __unused) response
	{

// This method is called when the server has determined that it has enough information to create the NSURLResponse
// it can be called multiple times, for example in the case of a redirect, so each time we reset the data.

// Often the response over a local network is too fast to warrant flashing up this message.  If the completed message
// is about to be displayed this delayed message is cancelled first.

	[statusField performSelector: @selector(setStringValue:) withObject: ReceivingResponseString afterDelay: 0.5];

	if (queryData)
		{
		[queryData setLength: 0];
		}
	if (fetchData)
		{
		[fetchData setLength: 0];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connection: (NSURLConnection* __unused) connection didReceiveData: (NSData*) data
	{
	
// Append the new data to the appropriate ivar.
	
	if (queryData)
		{
		[queryData appendData: data];
		}
	if (fetchData)
		{
		[fetchData appendData: data];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connection: (NSURLConnection* __unused) connection didFailWithError: (NSError* __unused) error
	{
	
// Cancel any pending statusField updates.

	[NSObject cancelPreviousPerformRequestsWithTarget: statusField];

// Release the connection, and the data object.

	[connection release];
	
	if (queryData)
		{
		[queryData release];
		queryData=nil;
		[statusField setStringValue: SearchFailedString];
		}
	if (fetchData)
		{
		[fetchData release];
		fetchData=nil;
		[statusField setStringValue: ImportFailedString];
		[self setFetchInProgress: NO];
		}
		
	[statusField performSelector: @selector(setStringValue:) withObject: @"" afterDelay: 3.0];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connectionDidFinishLoading: (NSURLConnection*) connection
	{
	if (queryData)
		[self queryConnectionDidFinishLoading: connection];
		
	if (fetchData)
		[self fetchConnectionDidFinishLoading: connection];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (IBAction) doQuery: (id __unused) sender
	{
	
// Set up the query URL.

	NSString *mircURLString=[NSString stringWithFormat: @"http://%@:%@/storage/%@",
	  [[NSUserDefaults standardUserDefaults] valueForKey: MIRCIPAddressKey],
	  [[NSUserDefaults standardUserDefaults] valueForKey: MIRCPortNumberKey],
	  [[NSUserDefaults standardUserDefaults] valueForKey: MIRCStorageServiceKey]];
	ASSERTNOTNIL(mircURLString);
	
	mircURLString=[mircURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];ASSERTNOTNIL(mircURLString);
	NSURL *url=[NSURL URLWithString: mircURLString];ASSERTNOTNIL(url);
	NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL: url cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval: DefaultHTTPTimeout];ASSERTNOTNIL(theRequest);
	
// Reset queryResultsArray.

	[resultsArrayController removeObjects: [resultsArrayController arrangedObjects]];
				
// Create the connection with the request.

	[theRequest setHTTPMethod: @"POST"];
	[theRequest setValue: @"text/xml" forHTTPHeaderField: @"Content-Type"];

// Set up the XML query.

	NSXMLElement *queryRoot=[NSXMLElement elementWithName: @"MIRCquery"];ASSERTNOTNIL(queryRoot);
	NSInteger maxResults=[[[NSUserDefaults standardUserDefaults] objectForKey: MIRCQueryMaximumResultsKey] integerValue];
	NSString *maxResultsStr=[NSString stringWithFormat: @"%ld", (long) maxResults];
	[queryRoot setAttributesAsDictionary: [NSDictionary dictionaryWithObject: maxResultsStr forKey: @"maxresults"]];
	[queryRoot setObjectValue: searchText];
	[theRequest setHTTPBody: [[queryRoot XMLString] dataUsingEncoding: NSUTF8StringEncoding]];

	queryConnection=[[NSURLConnection alloc] initWithRequest: theRequest delegate: self];
	if (queryConnection)
		{
		queryData=[[NSMutableData data] retain];ASSERTNOTNIL(queryData);
		}
	else
		{
		[statusField setStringValue: SearchFailedString];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) queryConnectionDidFinishLoading: (NSURLConnection*) connection
	{
	NSError							*error;

	
// Cancel any pending statusField updates.

	[NSObject cancelPreviousPerformRequestsWithTarget: statusField];

// Convert XML response to an array of dictionaries.

	ASSERTNOTNIL(queryData);
	@try
		{
		NSXMLDocument *response=[[NSXMLDocument alloc] initWithData: queryData options: NSXMLDocumentKind error: &error];
		if (response==nil)
			{
			[[NSException exceptionWithName: @"ResponseFailed" reason: nil userInfo: nil] raise];
			}
		[response autorelease];
		
		NSMutableDictionary *dictionary=nil;
		NSXMLElement *aNode=[response rootElement];ASSERTNOTNIL(aNode);
		while ((aNode=(NSXMLElement*) [aNode nextNode]))
			{
			NSString *nodeName=[aNode name];
			NSString *nodeValue=[aNode stringValue];

			if ([nodeName isEqualToString: @"MIRCdocument"])
				{
				NSArray *attributesArray=[aNode attributes];ASSERTNOTNIL(attributesArray);
				ASSERTNOTNIL([attributesArray objectAtIndex: 0]);
				dictionary=[NSMutableDictionary dictionaryWithObject: [[attributesArray objectAtIndex: 0] stringValue] forKey: QueryDocumentAddressKey];
				ASSERTNOTNIL(dictionary);
				[resultsArrayController addObject: dictionary];
				}
			if ([nodeName isEqualToString: @"title"])
				{
				[dictionary setValue: nodeValue forKey: QueryTitleKey];
				}
			if ([nodeName isEqualToString: @"access"])
				{
				[dictionary setValue: nodeValue forKey: QueryAccessKey];
				}
			}
		[statusField setStringValue: SearchCompleteString];
		[searchButton setKeyEquivalent: @""];
		[fetchButton setKeyEquivalent: @"\r"];
		[resultsArrayController setSelectionIndex: 0];
		}
		
	@catch (NSException *exception)
		{
		[statusField setStringValue: SearchFailedString];
		}
		
	@finally
		{
		[connection release];
		[queryData release];
		queryData=nil;
		[statusField performSelector: @selector(setStringValue:) withObject: @"" afterDelay: 3.0];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (IBAction) doFetch: (id __unused) sender
	{
	
// Get selection document address.
	
	NSDictionary *dict=[[resultsArrayController selectedObjects] objectAtIndex: 0];ASSERTNOTNIL(dict);
	NSString *docAddressStr=[dict valueForKey: QueryDocumentAddressKey];ASSERTNOTNIL(docAddressStr);
	docAddressStr=[docAddressStr stringByAppendingString: @"?zip"];

	NSURL *url=[NSURL URLWithString: docAddressStr];ASSERTNOTNIL(url);
	NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL: url cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval: DefaultHTTPTimeout];ASSERTNOTNIL(theRequest);
				
// Create the connection with the request.

	[theRequest setHTTPMethod: @"GET"];

	fetchConnection=[[NSURLConnection alloc] initWithRequest: theRequest delegate: self];
	if (fetchConnection)
		{
		fetchData=[[NSMutableData data] retain];ASSERTNOTNIL(fetchData);
	
		[self setFetchInProgress: YES];
		[statusField setStringValue: ImportingArchiveString];
		}
	else
		[statusField setStringValue: ImportFailedString];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) fetchConnectionDidFinishLoading: (NSURLConnection*) connection
	{

// Cancel any pending statusField updates.

	[NSObject cancelPreviousPerformRequestsWithTarget: statusField];

	ASSERTNOTNIL(fetchData);

// Import the zip archive.

	@try
		{
		NSString *response=[NSString stringWithUTF8String: [fetchData bytes]];
		if ([response rangeOfString: @"login?path"].location != NSNotFound)
			{
			[[NSException exceptionWithName: @"AccessDenied" reason: nil userInfo: nil] raise];
			}
			
		NSString *path=[NSTemporaryDirectory() stringByAppendingPathComponent: @"Xphile MIRC Archive.zip"];ASSERTNOTNIL(path);
		NSError *error=nil;
		if ([[NSFileManager defaultManager] fileExistsAtPath: path])
			{
			if (![[NSFileManager defaultManager] removeItemAtPath: path error: &error])
				{
				NSLog(@"Error: %@", [error description]);
				}
			}
		[fetchData writeToFile: path atomically: YES];

		XphileDocument *myDoc=[[NSDocumentController sharedDocumentController] currentDocument];
		if (myDoc==nil)
			[[NSException exceptionWithName: @"NoDocumentForImport" reason: nil userInfo: nil] raise];
		[myDoc importMIRCFileAtPath: path];
		[statusField setStringValue: ImportCompleteString];

// Force update of browser display.

		[[NSNotificationCenter defaultCenter] postNotificationName: ResetBrowserNotification object: self];
		[[myDoc managedObjectContext] processPendingChanges];
		}
	@catch (NSException *exception)
		{
		[statusField setStringValue: ImportFailedString];
		}
	@finally
		{

// Release the connection, and the data object

		[connection release];
		[fetchData release];
		fetchData=nil;

		[self setFetchInProgress: NO];
		[statusField performSelector: @selector(setStringValue:) withObject: @"" afterDelay: 3.0];

// Remove first item from selection and call Fetch again.
		
		NSArray *selectionArray=[resultsArrayController selectedObjects];ASSERTNOTNIL(selectionArray);
		if ([selectionArray count] > 0)
			{
			NSDictionary *selectedDict=[selectionArray objectAtIndex: 0];ASSERTNOTNIL(selectedDict);
			[resultsArrayController removeSelectedObjects: [NSArray arrayWithObject: selectedDict]];
			if ([[resultsArrayController selectedObjects] count] > 0)
				[self doFetch: self];
			}
		}

	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -


- (IBAction) doBrowse: (id __unused) sender
	{
		
// Get first selection document address.
	
	ASSERTNOTNIL(resultsArrayController);
	NSArray *selectionArray=[resultsArrayController selectedObjects];ASSERTNOTNIL(selectionArray);
	
	if ([selectionArray count] == 0)
		return;

	NSDictionary *selectedDict=[selectionArray objectAtIndex: 0];ASSERTNOTNIL(selectedDict);
	NSString *docAddressStr=[selectedDict valueForKey: QueryDocumentAddressKey];ASSERTNOTNIL(docAddressStr);

	NSURL *url=[NSURL URLWithString: docAddressStr];ASSERTNOTNIL(url);
	
	[[NSWorkspace sharedWorkspace] openURL: url];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) controlTextDidBeginEditing: (NSNotification * __unused) aNotification
	{
	
	[searchButton setKeyEquivalent: @"\r"];
	[fetchButton setKeyEquivalent: @""];
	}
	

@end
