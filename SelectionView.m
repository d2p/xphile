/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/

// Modified from JCR's sample code at http://developer.apple.com/samplecode/Cropped_Image/index.html
// actualRectForImageView modified from http://www.stone.com/The_Cocoa_Files/Thanks_A_Bundle_.html


#import "SelectionView.h"


@implementation SelectionView
	

- (id) initWithCoder: (NSCoder*) decoder
	{
	if ((self=[super initWithCoder: decoder]))
		{
		selectionMarker=nil;
		[self setSelectionMarker: [SelectionPath markerForView: self]];
		}
	return self;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) dealloc
	{
	[self setSelectionMarker: nil];
	
	[super dealloc];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) drawRect: (NSRect) rect 
	{
	[super drawRect: rect];
	[selectionMarker drawSelection];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) mouseDown: (NSEvent*) theEvent
	{
	[selectionMarker mouseDown: theEvent];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) mouseUp: (NSEvent*) theEvent 
	{ 
	[selectionMarker mouseUp: theEvent]; 
	[self selectionChanged];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) mouseDragged: (NSEvent*) theEvent 
	{ 
	[selectionMarker mouseDragged: theEvent]; 
	[self selectionChanged];	
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) selectionChanged
	{
	[self setNeedsDisplay: YES];
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setSelectionMarker: (SelectionPath*) selectionPath
	{
	[selectionPath retain];
	[selectionMarker release];
	selectionMarker=selectionPath;
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (SelectionPath*) selectionMarker
	{
	return selectionMarker;
	}


@end

