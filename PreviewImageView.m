/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "ConditionEntity.h"
#import "PreviewImageView.h"
#import "XphileDocument.h"
#import "XphileDocument+Toolbar.h"


@implementation PreviewImageView


- (void) mouseDown: (NSEvent*) theEvent
	{
	if ([self image]==nil)
		return;
	
	if ([theEvent clickCount] > 1)
		{
		NSWindow *window=[self window];ASSERTNOTNIL(window);
		NSWindowController *controller=[window windowController];ASSERTNOTNIL(controller);
		XphileDocument *document=[controller document];ASSERTNOTNIL(document);
		[document performSelector: @selector(showImages:) withObject: self];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Scroll support

- (void) scrollUp
	{
	NSWindow *window=[self window];ASSERTNOTNIL(window);
	NSWindowController *controller=[window windowController];ASSERTNOTNIL(controller);
	XphileDocument *document=[controller document];ASSERTNOTNIL(document);

	[document performSelector: @selector(incrementPreviewImage) withObject: nil];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) scrollDown
	{
	NSWindow *window=[self window];ASSERTNOTNIL(window);
	NSWindowController *controller=[window windowController];ASSERTNOTNIL(controller);
	XphileDocument *document=[controller document];ASSERTNOTNIL(document);

	[document performSelector: @selector(decrementPreviewImage) withObject: nil];
	}


@end


