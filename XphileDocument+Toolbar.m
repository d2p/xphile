/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "ApplicationController.h"
#import "OsirixXMLDocument.h"
#import "OsirixXMLRPC.h"
#import "XphileDocument+Albums.h"
#import "XphileDocument+Toolbar.h"


@implementation XphileDocument (XphileDocument_Toolbar)


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Toolbar Setup

- (void) setUpToolbar
	{
	NSToolbar *toolbar = [[[NSToolbar alloc] initWithIdentifier: @"ToolbarIdentifier"] autorelease];ASSERTNOTNIL(toolbar);
    
// Set up toolbar properties: Allow customization, give a default display mode, and remember state in user defaults.

	[toolbar setAllowsUserCustomization: YES];
	[toolbar setAutosavesConfiguration: YES];
	[toolbar setDisplayMode: NSToolbarDisplayModeIconAndLabel];

// Set the search mode to the current default.
	
// Set the toolbar delegate as this document and attach to the main window.

	[toolbar setDelegate: (id) self];
	[[self windowForSheet] setToolbar: toolbar];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSToolbarItem*) toolbar: (NSToolbar* __unused) toolbar itemForItemIdentifier: (NSString*) itemIdent willBeInsertedIntoToolbar: (BOOL __unused) willBeInserted
	{	
	NSToolbarItem *toolbarItem=[[[NSToolbarItem alloc] initWithItemIdentifier: itemIdent] autorelease];ASSERTNOTNIL(toolbarItem);

	if ([itemIdent isEqual: AddCaseKey])
		{
		[toolbarItem setLabel: AddCaseString];
		[toolbarItem setImage: [NSImage imageNamed: @"Add Patient.tif"]];
		[toolbarItem setAction: @selector(addCase:)];
		}
	else if ([itemIdent isEqual: RemoveCaseKey])
		{
		[toolbarItem setLabel: RemoveCaseString];
		[toolbarItem setImage: [NSImage imageNamed: @"Remove Patient.tif"]];
		[toolbarItem setAction: @selector(removeCase:)];
		}
	else if ([itemIdent isEqual: ResetBrowserKey])
		{
		[toolbarItem setLabel: ResetBrowserString];
		[toolbarItem setImage: [NSImage imageNamed: @"Revert Arrow.tif"]];
		[toolbarItem setAction: @selector(resetBrowser:)];
		}
	else if ([itemIdent isEqual: EditCaseKey])
		{
		[toolbarItem setLabel: EditCaseString];
		[toolbarItem setImage: [NSImage imageNamed: @"Edit Patient.tif"]];
		[toolbarItem setAction: @selector(editCase:)];
		}	
	else if ([itemIdent isEqual: PatientListKey])
		{
		[toolbarItem setLabel: PatientListString];
		[toolbarItem setImage: [NSImage imageNamed: @"Patient List.tif"]];
		[toolbarItem setAction: @selector(showPatientList:)];
		}
	else if ([itemIdent isEqual: ShowImagesKey])
		{
		[toolbarItem setLabel: DisplayImagesString];
		NSImage *im=[[NSWorkspace sharedWorkspace] iconForFile: @"/System/Library/PreferencePanes/Displays.prefPane"];ASSERTNOTNIL(im);
		[toolbarItem setImage: im];
		[toolbarItem setAction: @selector(showImages:)];
		}	
	else if ([itemIdent isEqual: FullScreenKey])
		{
		[toolbarItem setLabel: FullScreenString];
		[toolbarItem setImage: [NSImage imageNamed: @"FullScreen"]];
		[toolbarItem setAction: @selector(enterFullScreen:)];
		}	
	else if ([itemIdent isEqual: RandomCasesKey])
		{
		[toolbarItem setLabel: RandomCasesString];
		[toolbarItem setImage: [NSImage imageNamed: @"Random Cases.tif"]];
		[toolbarItem setAction: @selector(createRandomAlbum:)];
		}
	else if ([itemIdent isEqual: ExportImagesKey])
		{
		[toolbarItem setLabel: ExportImagesString];
		NSImage *im=[[NSWorkspace sharedWorkspace] iconForFile: @"/"];ASSERTNOTNIL(im);
		[toolbarItem setImage: im];
		[toolbarItem setAction: @selector(exportImages:)];
		}
	else if ([itemIdent isEqual: ExportToKeynoteKey])
		{
		[toolbarItem setLabel: ExportToKeynoteString];
		NSString *keynotePath=[[NSWorkspace sharedWorkspace] absolutePathForAppBundleWithIdentifier: KeynoteBundleIdentifier];
		if (keynotePath!=nil)
			{
			NSImage *im=[[NSWorkspace sharedWorkspace] iconForFile: keynotePath];ASSERTNOTNIL(im);
			[toolbarItem setImage: im];
			[toolbarItem setAction: @selector(exportToKeynote:)];
			}
		else									// Keynote not installed on this system, so use a generic app icon.
			{
			NSImage *im=[[NSWorkspace sharedWorkspace] iconForFile: @"/System/Library/CoreServices/loginwindow.app"];ASSERTNOTNIL(im);
			[toolbarItem setImage: im];
			}
		}
	else if ([itemIdent isEqual: OpenInOsirixKey])
		{
		[toolbarItem setLabel: OpenInOsirixString];
		NSString *osirixPath=[[NSWorkspace sharedWorkspace] absolutePathForAppBundleWithIdentifier: OsirixBundleIdentifier];
		if (osirixPath!=nil)
			{
			NSImage *im=[[NSWorkspace sharedWorkspace] iconForFile: osirixPath];ASSERTNOTNIL(im);
			[toolbarItem setImage: im];
			[toolbarItem setAction: @selector(openInOsirix:)];
			}
		else									// Osirix not installed on this system, so use a generic app icon.
			{
			NSImage *im=[[NSWorkspace sharedWorkspace] iconForFile: @"/System/Library/CoreServices/loginwindow.app"];ASSERTNOTNIL(im);
			[toolbarItem setImage: im];
			}
		}
	else if ([itemIdent isEqual: ExportToMIRCKey])
		{
		[toolbarItem setLabel: ExportToMIRCString];
		NSImage *im=[[NSWorkspace sharedWorkspace] iconForFile: @"/System/Library/PreferencePanes/Network.prefPane"];ASSERTNOTNIL(im);
		[toolbarItem setImage: im];
		[toolbarItem setAction: @selector(exportToMIRCServer:)];
		}
	else if ([itemIdent isEqual: ToolbarSearchKey])
		{
		[toolbarItem setLabel: SearchString];
		[toolbarItem setView: toolbarSearchView];
		[toolbarItem setMinSize: NSMakeSize(NSWidth([toolbarSearchView frame]), NSHeight([toolbarSearchView frame]))];
		[toolbarItem setMaxSize: NSMakeSize(NSWidth([toolbarSearchView frame]), NSHeight([toolbarSearchView frame]))];
		}
	else
		toolbarItem=nil;
	
	if (toolbarItem!=nil)
		{
		[toolbarItem setTarget: self];
		[toolbarItem setPaletteLabel: [toolbarItem label]];
		}

    return toolbarItem;
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSArray*) toolbarDefaultItemIdentifiers: (NSToolbar* __unused) toolbar
	{
		
	return [NSArray arrayWithObjects: ResetBrowserKey, NSToolbarSpaceItemIdentifier, PatientListKey, AddCaseKey, EditCaseKey,
	  RemoveCaseKey, NSToolbarFlexibleSpaceItemIdentifier, ExportImagesKey, ExportToMIRCKey, NSToolbarFlexibleSpaceItemIdentifier,
	  ShowImagesKey, FullScreenKey, ToolbarSearchKey, nil];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSArray*) toolbarAllowedItemIdentifiers: (NSToolbar* __unused) toolbar
	{
		
    return [NSArray arrayWithObjects: AddCaseKey, RemoveCaseKey, EditCaseKey, PatientListKey, ShowImagesKey, FullScreenKey, ResetBrowserKey,
	  ExportImagesKey, ExportToKeynoteKey, ExportToMIRCKey, OpenInOsirixKey, RandomCasesKey, NSToolbarSeparatorItemIdentifier, NSToolbarSpaceItemIdentifier,
	  NSToolbarFlexibleSpaceItemIdentifier, ToolbarSearchKey, nil];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Toolbar Routines

- (IBAction) resetBrowser: (id __unused) sender
	{
	
	[(NSArrayController*) albumArrayController setSelectionIndexes: [NSIndexSet indexSet]];
	[(NSArrayController*) stage1BrowserArrayController setSelectionIndexes: [NSIndexSet indexSet]];
	[(NSArrayController*) browserArrayController setSelectionIndexes: [NSIndexSet indexSet]];

	[toolbarSearchField setStringValue: @""];
	[browserArrayController setFilterPredicate: nil];
	[browserArrayController setFetchPredicate: nil];
	
	[(NSArrayController*) systemListController setSelectionIndexes: [NSIndexSet indexSet]];
	[(NSArrayController*) pathologyListController setSelectionIndexes: [NSIndexSet indexSet]];
	[(NSArrayController*) regionListController setSelectionIndexes: [NSIndexSet indexSet]];

	[[self managedObjectContext] processPendingChanges];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) addCase: (id) sender
	{

// Reset the browser, so that the new case can be added.

	[self resetBrowser: sender];

// Add new patient.

	PatientEntity *pat=[NSEntityDescription insertNewObjectForEntityForName: @"Patient" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(pat);

// Add new condition.

	ConditionEntity *cond=[NSEntityDescription insertNewObjectForEntityForName: @"Condition" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(cond);
	[cond setValue: pat forKeyPath: @"patient"];
	[cond setValue: [NSDate date] forKeyPath: @"dateOfOnset"];

	[browserArrayController performSelector: @selector(setSelectedObjects:) withObject: [NSArray arrayWithObject: cond] afterDelay: 0.5];

	[editorWindow makeKeyAndOrderFront: sender];	
	[identifierTextField selectText: self];
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) removeCase: (id __unused) sender
	{
	BOOL							shouldDeleteFromAlbum	=	NO;
	AlbumEntity						*album					=	nil;
	
	
	
// First check if a simple album is selected, and if so we will remove the case from the album.

	ASSERTNOTNIL(albumArrayController);
	NSArray *albums=[albumArrayController selectedObjects];ASSERTNOTNIL(albums);
	if ([albums count]==1)
		{
		album=[albums objectAtIndex: 0];ASSERTNOTNIL(album);
		if (![album isSmartAlbum])
			shouldDeleteFromAlbum=YES;
		}

	ASSERTNOTNIL(browserArrayController);
	NSArray *selectionArray=[browserArrayController selectedObjects];ASSERTNOTNIL(selectionArray);
	if ([selectionArray count]==0)
		return;
		
	if (shouldDeleteFromAlbum)
		{
		NSUInteger index;
		for (index=0; index<[selectionArray count]; index++)
			{
			ConditionEntity *cond=[selectionArray objectAtIndex: index];ASSERTNOTNIL(cond);
			NSMutableSet *albumsSet=[cond mutableSetValueForKeyPath: @"albums"];ASSERTNOTNIL(albumsSet);
			[albumsSet removeObject: album];
			}
		return;
		}

// Check we really do want to remove from the database.

	NSString *removeWarningString = ([selectionArray count]==1) ? RemoveCaseFromLibraryString : RemoveCasesFromLibraryString;
	ASSERTNOTNIL(removeWarningString);

	NSAlert *alert=[NSAlert alertWithMessageText: removeWarningString defaultButton: RemoveString alternateButton: CancelString otherButton: @"" informativeTextWithFormat: @""];
	ASSERTNOTNIL(alert);
	[alert beginSheetModalForWindow: [self windowForSheet] modalDelegate: self didEndSelector: @selector(removeCaseAlertDidEnd: returnCode: contextInfo:) contextInfo: selectionArray];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) removeCaseAlertDidEnd: (NSAlert*) alert returnCode: (NSInteger) returnCode contextInfo: (NSArray*) selectionArray
	{
	[[alert window] close];
	if (returnCode==NSCancelButton)
		return;
		
	[statusField setStringValue: RemovingString];
	[progressIndicator startAnimation: self];

	NSUInteger index;
	for (index=0; index<[selectionArray count]; index++)
		{
		NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);
		[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];

		ConditionEntity *cond=[selectionArray objectAtIndex: index];ASSERTNOTNIL(cond);
		PatientEntity *pat=[cond valueForKeyPath: @"patient"];ASSERTNOTNIL(pat);
		NSSet *conditions=[pat mutableSetValueForKeyPath: @"conditions"];ASSERTNOTNIL(conditions);

// Check if there is more than one condition for this particular patient.
// If so, just delete this particular condition.  Otherwise delete the whole patient, so as not to leave a childless patient.

		if ([conditions count] > 1)
			{
			[[self managedObjectContext] deleteObject: cond];
			}
		else
			{
			[[self managedObjectContext] deleteObject: pat];
			}
		
		[pool release];
		}

	[statusField setStringValue: SuccessString];
	[progressIndicator stopAnimation: self];
	[statusField performSelector: @selector(setStringValue:) withObject: ReadyString afterDelay: 3.0];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) editCase: (id __unused) sender
	{
		ASSERTNOTNIL(browserArrayController);
	ASSERTNOTNIL(editorWindow);

	NSArray *conditionsArray=[browserArrayController selectedObjects];ASSERTNOTNIL(conditionsArray);
	if ([conditionsArray count]==0)
		return;

	[conditionArrayController setSelectedObjects: [NSArray arrayWithObject: [conditionsArray objectAtIndex: 0]]];
	[editorWindow makeKeyAndOrderFront: self];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) showPatientList: (id __unused) sender
	{

// Secondary windows are released in document dealloc.

	if (patientListWindowController == nil)
		{
		patientListWindowController=[[PatientListWindowController alloc] initWithWindowNibName: @"PatientList"];
		ASSERTNOTNIL(patientListWindowController);
		[self addWindowController: patientListWindowController];
		[patientListWindowController window];
		}
		
	[[NSNotificationCenter defaultCenter] postNotificationName: ShowSecondaryWindowNotification object: self];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) showImages: (id __unused) sender
	{
	
	NSArray *conditionsArray=[browserArrayController arrangedObjects];
	if ([conditionsArray count]==0)
		return;
	
	conditionsArray=[browserArrayController selectedObjects];ASSERTNOTNIL(conditionsArray);
	if ([conditionsArray count]==0)
		[browserArrayController setSelectionIndex: 0];

	ConditionEntity *currentCondition=[[browserArrayController selectedObjects] objectAtIndex: 0];ASSERTNOTNIL(currentCondition);
	[conditionArrayController setSelectedObjects: [NSArray arrayWithObject: currentCondition]];
	
	[self showImageWindow];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) showImageWindow
	{
	[imageWindow makeKeyAndOrderFront: self];
	
// Select the imageTableView (and its first item) unless there are no images, but there are movies.
	
	if ([[imageArrayController arrangedObjects] count] == 0 && [[fileArrayController arrangedObjects] count] > 0)
		{
		[imageWindow makeFirstResponder: fileTableView];
		[fileArrayController setSelectionIndex: 0];
		return;
		}
	
	[imageWindow makeFirstResponder: imageTableView];
	[imageArrayController setSelectionIndex: 0];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) openInOsirix: (id __unused) sender
	{	
		
// Get the browser selection patient ID.

	ASSERTNOTNIL(browserArrayController);
	NSArray *selectionArray=[browserArrayController selectedObjects];ASSERTNOTNIL(selectionArray);
	if ([selectionArray count]!=1)
		return;
		
	ConditionEntity *condition=[selectionArray objectAtIndex: 0];ASSERTNOTNIL(condition);
	NSString *patientID=[condition valueForKeyPath: @"patient.identifier"];ASSERTNOTNIL(patientID);

// Establish the range of study dates.  Offset by at least 24 hours on each side.

	NSDate *oldestDate=[condition oldestStudyDate];
	if (oldestDate==nil)
		oldestDate=[NSDate distantPast];
	else
		oldestDate=[oldestDate dateByAddingTimeInterval: (-60*60*24.0)];
	NSDate *newestDate=[condition newestStudyDate];
	if (newestDate==nil)
		newestDate=[NSDate distantFuture];
	else
		newestDate=[newestDate dateByAddingTimeInterval: (60*60*24.0)];

// Build the query string.

	NSString *requestString=[NSString stringWithFormat: @"((patientID == '%@') AND (date >= CAST(%f, 'NSDate')) AND (date < CAST(%f, 'NSDate')))",
	  patientID, [oldestDate timeIntervalSinceReferenceDate], [newestDate timeIntervalSinceReferenceDate]];
	ASSERTNOTNIL(requestString);
	  
// Set up methodCall xml document.

	NSXMLElement *root=[NSXMLElement elementWithName: @"methodCall"];ASSERTNOTNIL(root);
	OsirixXMLDocument *xmlDoc=[[[OsirixXMLDocument alloc] initWithRootElement: root] autorelease];ASSERTNOTNIL(xmlDoc);
	[xmlDoc setVersion: @"1.0"];
	[xmlDoc setCharacterEncoding: @"UTF-8"];
	[xmlDoc addMethodName: @"DBWindowFind" toElement: root];
	
	NSXMLElement *paramsElement=[NSXMLElement elementWithName: @"params"];ASSERTNOTNIL(paramsElement);
	[root addChild: paramsElement];
	NSDictionary *parameterDictionary=[NSDictionary dictionaryWithObjects:
	  [NSArray arrayWithObjects: requestString, @"Study", @"Open", nil]
	  forKeys: [NSArray arrayWithObjects: @"request", @"table", @"execute", nil]];ASSERTNOTNIL(parameterDictionary);
	[xmlDoc addParameterDictionary: parameterDictionary toElement: paramsElement];

// HTTP post the xml data to each registered OsiriX station for the network.

	NSArray *osirixXMLArray=[[NSUserDefaults standardUserDefaults] valueForKey: @"OsirixXMLArray"];ASSERTNOTNIL(osirixXMLArray);
	NSUInteger index;
	for (index=0; index<[osirixXMLArray count]; index++)
		{
		NSData *xmlData=[xmlDoc XMLData];ASSERTNOTNIL(xmlData);
		
		NSDictionary *osirixXMLDict=[osirixXMLArray objectAtIndex: index];ASSERTNOTNIL(osirixXMLDict);
		NSString *address=[osirixXMLDict valueForKey: OsirixXMLRPCAddressKey];ASSERTNOTNIL(address);
		NSString *port=[osirixXMLDict valueForKey: OsirixXMLRPCPortKey];ASSERTNOTNIL(port);
		BOOL active=[[osirixXMLDict valueForKey: OsirixXMLRPCActiveKey] boolValue];
		if (active)
			{
			osirixXMLRPC=[[OsirixXMLRPC alloc] initWithAddress: address andPort: port];ASSERTNOTNIL(osirixXMLRPC);
			[osirixXMLRPC setDisplayErrors: ([osirixXMLArray count]==1)];
			[osirixXMLRPC sendXMLData: xmlData];
			}
		}
	}


@end