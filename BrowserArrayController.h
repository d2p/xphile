/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "DragAndDropArrayController.h"


#define BrowserRowIndexesDragType								@"BrowserRowIndexesDrag"
#define InDocumentBrowserDragType								@"InDocumentBrowserDrag"
#define BetweenDocumentBrowserDragType							@"BetweenDocumentBrowserDrag"
#define OsiriXDragType											@"OsiriXPboardType"

#define ResetBrowserNotification								@"ResetBrowserNotification"


// Localised strings.

#define BrowserArrayControllerLocalizedString(str)				NSLocalizedStringFromTable((str), @"BrowserArrayController", (str))

#define OsirixStudyDescriptionString							BrowserArrayControllerLocalizedString(@"Referred by %@ to %@.  %@ performed by %@.")



@interface BrowserArrayController : DragAndDropArrayController
	{
	NSArray														*storedSortArray;
	}

- (BOOL) tableView: (NSTableView*) aTableView acceptOsiriXData: (NSArray*) osirixDataArray;
- (BOOL) tableView: (NSTableView*) aTableView acceptBetweenDocumentData: (NSArray*) dataArray;

- (BOOL) exportImagesTo: (NSString*) basePath;
- (BOOL) exportToKeynoteFile: (NSString*) basePath;
- (BOOL) exportToMIRCArchiveAtPath: (NSString*) basePath;
- (BOOL) exportToMIRCServerUsingPath: (NSString*) basePath;

- (void) indicateSuccess;
- (void) indicateFailure;

@end
