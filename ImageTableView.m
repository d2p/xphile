/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "ImageEntity.h"
#import "ImageTableView.h"
#import "XphileDocument.h"
#import "XphileDocument+Movies.h"


@implementation ImageTableView

- (void) awakeFromNib
	{
	[super awakeFromNib];
	
	[self registerForDraggedTypes: [NSArray arrayWithObjects: NSFilenamesPboardType, NSTIFFPboardType, MediaOrderDragType, nil]];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) becomeFirstResponder
	{
	[[NSNotificationCenter defaultCenter] postNotificationName: ITVBecameFirstResponderNotification object: [self window]];
	
	return [super becomeFirstResponder];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (IBAction) doEdit: (id __unused) sender
	{
	NSWindow *window=[self window];ASSERTNOTNIL(window);
	NSWindowController *controller=[window windowController];ASSERTNOTNIL(controller);
	XphileDocument *document=[controller document];ASSERTNOTNIL(document);
	[document performSelector: @selector(editImage:) withObject: self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doCombineImages: (id __unused) sender
	{
	
	NSAlert *alert=[NSAlert alertWithMessageText: RemoveOriginalImagesString defaultButton: RemoveString alternateButton: KeepString otherButton: @"" informativeTextWithFormat: @""];
	ASSERTNOTNIL(alert);
	[alert beginSheetModalForWindow: [self window] modalDelegate: self didEndSelector: @selector(combineImagesAlertDidEnd: returnCode: contextInfo:) contextInfo: nil];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) combineImagesAlertDidEnd: (NSAlert*) alert returnCode: (NSInteger) returnCode contextInfo: (void* __unused) context
	{
	
	[[alert window] close];
	BOOL deleteOriginalImages=(returnCode==NSOKButton);
	ImageArrayController *iac=(ImageArrayController*) [self delegate];ASSERTNOTNIL(iac);
	[iac combineImages: deleteOriginalImages];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doCrop: (id __unused) sender
	{
	
	NSWindow *window=[self window];ASSERTNOTNIL(window);
	NSWindowController *controller=[window windowController];ASSERTNOTNIL(controller);
	XphileDocument *document=[controller document];ASSERTNOTNIL(document);

	ImageArrayController *iac=(ImageArrayController*) [self delegate];ASSERTNOTNIL(iac);
	[iac cropFromView: [document imageView]];
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doFlipHorizontal: (id __unused) sender
	{
	
	ImageArrayController *iac=(ImageArrayController*) [self delegate];ASSERTNOTNIL(iac);
	[iac doGeometricTransition: @selector(flipHorizontal)];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doFlipVertical: (id __unused) sender
	{
	
	ImageArrayController *iac=(ImageArrayController*) [self delegate];ASSERTNOTNIL(iac);
	[iac doGeometricTransition: @selector(flipVertical)];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doRotate90: (id __unused) sender
	{
	
	ImageArrayController *iac=(ImageArrayController*) [self delegate];ASSERTNOTNIL(iac);
	[iac doGeometricTransition: @selector(rotate90)];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doRotate180: (id __unused) sender
	{
	
	ImageArrayController *iac=(ImageArrayController*) [self delegate];ASSERTNOTNIL(iac);
	[iac doGeometricTransition: @selector(rotate180)];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doRotate270: (id __unused) sender
	{
	
	ImageArrayController *iac=(ImageArrayController*) [self delegate];ASSERTNOTNIL(iac);
	[iac doGeometricTransition: @selector(rotate270)];
	}

	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doCreateMovie: (id __unused) sender
	{
	
	NSWindow *window=[self window];ASSERTNOTNIL(window);
	NSWindowController *controller=[window windowController];ASSERTNOTNIL(controller);
	XphileDocument *document=[controller document];ASSERTNOTNIL(document);

	[document createMovie];
	}

	
@end
