/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/



#define SetOsirixToFrontOnOpenKey								@"setOsirixToFrontOnOpen"
#define OsirixXMLRPCAddressKey									@"osirixXMLRPCAddress"
#define OsirixXMLRPCPortKey										@"osirixXMLRPCPort"
#define OsirixXMLRPCActiveKey									@"osirixXMLRPCActive"

#define OsirixBundleIdentifier									@"com.rossetantoine.osirix"


// Localised strings.

#define OsirixXMLRPCLocalizedString(str)			NSLocalizedStringFromTable((str), @"OsirixXMLRPC", (str))

#define UnableToCommunicateWithOsirixString			OsirixXMLRPCLocalizedString(@"Unable to Communicate with OsiriX")
#define PatientNotFoundString						OsirixXMLRPCLocalizedString(@"Patient Not Found")
#define NoMatchInOsirixDatabaseString				OsirixXMLRPCLocalizedString(@"There was no match in the currently open OsiriX database")


@interface OsirixXMLRPC : NSObject
	{
	NSString								*address;
	NSString								*port;
	NSURLConnection							*theConnection;
	NSMutableData							*receivedData;
	BOOL									displayErrors;
	}

- (instancetype) initWithAddress: (NSString*) ipAddress andPort: (NSString*) thePort;
- (void) setDisplayErrors: (BOOL) b;
- (void) sendXMLData: (NSData*) xmlData;

@end
