/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "BlackImageView.h"


@implementation BlackImageView


- (void) drawRect: (NSRect) rect
	{
	if ([self image]!=nil)
		{
		[[NSColor blackColor] set];
		[NSBezierPath fillRect: rect];
		}
	else if ([NSMenu menuBarVisible])
		{
		[[NSColor grayColor] set];
		NSRect r=[self bounds];
		[NSBezierPath strokeRect: r];
		}
		
	[super drawRect: rect];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) isOpaque
	{
	return ([self image]!=nil);
	}


@end


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————


@implementation DismissingBlackImageView


- (void) mouseDown: (NSEvent* __unused) theEvent
	{
	[NSAnimationContext runAnimationGroup: ^(NSAnimationContext *context)
		{
		context.duration = 1;
		self.animator.alphaValue = 0;
		}
	  completionHandler: ^
	  	{
		self.hidden = YES;
		self.alphaValue = 1;
		}];
	}


@end