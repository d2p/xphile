/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "BrowserArrayController.h"
#import "BrowserTableView.h"
#import "ExtendedTableView.h"
#import "PreviewImageView.h"
#import "XphileDocument.h"
#import "XphileDocument+Toolbar.h"


@implementation BrowserTableView

- (void) awakeFromNib
	{
	[self setTarget: self];
	[self setDoubleAction: @selector(doDoubleClick)];

	[self registerForDraggedTypes: @[BetweenDocumentBrowserDragType, OsiriXDragType]];

// Set our drag mask to allow between application copy drags.

	[self setDraggingSourceOperationMask: NSDragOperationAll forLocal: YES];
	[self setDraggingSourceOperationMask: NSDragOperationCopy forLocal: NO];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) doDoubleClick
	{

// If double clicking in header, exit.

	if ([self isClickInHeader])
       return;

	NSWindow *window=[self window];ASSERTNOTNIL(window);
	NSWindowController *controller=[window windowController];ASSERTNOTNIL(controller);
	XphileDocument *document=[controller document];ASSERTNOTNIL(document);
	[document performSelector: @selector(editCase:) withObject: self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) keyDown: (NSEvent*) theEvent
	{
	NSString *theChar=[theEvent characters];ASSERTNOTNIL(theChar);
	NSDocument *doc=[[[self window] windowController] document];ASSERTNOTNIL(doc);

	if ([theChar isEqualToString: @"\003"])													// Enter.
		{
		[doc performSelector: @selector(editCase:) withObject: self];
		}
	else if ([theChar isEqualToString: @"\r"])												// Return.
		{
		[doc performSelector: @selector(showImages:) withObject: self];
		}
	else if ([theChar isEqualToString: @"\177"])											// Delete.
		{
		[doc performSelector: @selector(removeCase:) withObject: self];
		}
	else
		{
		[super keyDown: theEvent];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) selectRowIndexes: (NSIndexSet*) indexes byExtendingSelection: (BOOL) extend
	{
	
// Maximum browser selection should be 100 rows.
	
	if ([indexes count] <= 100)
		[super selectRowIndexes: indexes byExtendingSelection: extend];
	}



@end
