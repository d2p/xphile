/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "ApplicationController.h"
#import "BrowserSelectionTableHeaderView.h"
#import "CroppableView.h"
#import "EditorWindow.h"
#import "FileEntity.h"
#import "FileTableView.h"
#import "FullScreenWindow.h"
#import "ImageArrayController.h"
#import "ImageEntity.h"
#import "ImageTableView.h"
#import "ImageView.h"
#import "MovieView.h"
#import "NSImage+Additions.h"
#import "NSManagedObject+Additions.h"
#import "PatientEntity.h"
#import "PreviewImageView.h"
#import "XphileDocument.h"
#import "XphileDocument+Albums.h"
#import "XphileDocument+Toolbar.h"
#import "XphileDocument+Versioning.h"



@implementation XphileDocument


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Initialisation

- (id) init 
	{
    self=[super init];
    if (self != nil)
		{
		patientListWindowController=nil;
		}
    return self;
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) dealloc
	{

// Individually remove notifications, rather than calling [... removeObserver self].

	[[NSNotificationCenter defaultCenter] removeObserver: self name: FTVBecameFirstResponderNotification object: imageWindow];
	[[NSNotificationCenter defaultCenter] removeObserver: self name: ITVBecameFirstResponderNotification object: imageWindow];

	[[NSNotificationCenter defaultCenter] removeObserver: self name: ExportToMIRCCompletedSuccessfullyNotification object: browserArrayController];
	[[NSNotificationCenter defaultCenter] removeObserver: self name: ExportToMIRCFailedNotification object: browserArrayController];

	[[NSNotificationCenter defaultCenter] removeObserver: self name: ResetBrowserNotification object: browserArrayController];

	[[NSNotificationCenter defaultCenter] removeObserver: self name: OptionKeyPressedNotification object: editorWindow];
	[[NSNotificationCenter defaultCenter] removeObserver: self name: OptionKeyReleasedNotification object: editorWindow];

	[[NSNotificationCenter defaultCenter] removeObserver: self name: NSManagedObjectContextObjectsDidChangeNotification object: [self managedObjectContext]];

	[[NSDistributedNotificationCenter defaultCenter] removeObserver: self name: @"ExportOsiriXToXphile" object: @"nz.org.ddp.xphile"];

// Release secondary windows.

	[patientListWindowController release];

	[super dealloc];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) close
	{

// Error: 'Cannot perform operation without a managed object context' when closing.
// Workaround is to unselect all albums prior to closing the document.

	[(NSArrayController*) albumArrayController setSelectionIndexes: [NSIndexSet indexSet]];
	[[NSRunLoop currentRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow: 0.1]];

// Cancel any delayed performs invoked from windowControllerDidLoadNib.

	[[self class] cancelPreviousPerformRequestsWithTarget: self];

	[super close];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (BOOL) configurePersistentStoreCoordinatorForURL: (NSURL *) url ofType: (NSString*) fileType modelConfiguration: (NSString*) configuration storeOptions: (NSDictionary*) storeOptions error: (NSError**) error
	{
	if (storeOptions == nil)
		{
		storeOptions=[NSMutableDictionary dictionary];
		}
	ASSERTNOTNIL(storeOptions);
	[storeOptions setValue: @{@"journal_mode": @"DELETE"} forKey: NSSQLitePragmasOption];
	
	return [super configurePersistentStoreCoordinatorForURL: url ofType: fileType modelConfiguration: configuration
	  storeOptions: storeOptions error: error];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) makeWindowControllers
	{
	NSWindowController *wc=[[[NSWindowController alloc] initWithWindowNibName: @"XphileDocument" owner: self] autorelease];ASSERTNOTNIL(wc);
	[self addWindowController: wc];
	[wc setDocument: self];
	[wc setShouldCloseDocument: YES];
	[wc setShouldCascadeWindows: NO];
	
	NSURL *url=[self fileURL];
	if ([url path] != nil)
		[wc setWindowFrameAutosaveName: [url path]];
	else
		[wc setWindowFrameAutosaveName: @"Browser"];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) windowControllerDidLoadNib: (NSWindowController*) windowController 
	{
    [super windowControllerDidLoadNib: windowController];

	[statusField setStringValue: InitialisingString];

//  Add most secondary windows into the document controller hierarchy, so that the document object can be retrieved by the window later.

	ASSERTNOTNIL(editorWindow);
	NSWindowController *wc=[[[NSWindowController alloc] initWithWindow: editorWindow] autorelease];ASSERTNOTNIL(wc);
	[self addWindowController: wc];
	[wc setWindowFrameAutosaveName: @"PatientEditor"];
	
	ASSERTNOTNIL(imageWindow);
	wc=[[[NSWindowController alloc] initWithWindow: imageWindow] autorelease];ASSERTNOTNIL(wc);
	[self addWindowController: wc];
	[wc setWindowFrameAutosaveName: @"Images"];

// Set the table last columns to resize to window appropriately.

	[albumTableView sizeLastColumnToFit];
	[browserTableView sizeLastColumnToFit];
	[conditionListTableView sizeLastColumnToFit];
	[studyListTableView sizeLastColumnToFit];

// Override the browser selection table views' header views.

	NSRect headerFrame=[[systemTableView headerView] frame];
	BrowserSelectionTableHeaderView *systemHeaderView=[[[BrowserSelectionTableHeaderView alloc] initWithFrame: headerFrame] autorelease];ASSERTNOTNIL(systemHeaderView);
	[systemTableView setHeaderView: systemHeaderView];

	headerFrame=[[pathologyTableView headerView] frame];
	BrowserSelectionTableHeaderView *pathologyHeaderView=[[[BrowserSelectionTableHeaderView alloc] initWithFrame: headerFrame] autorelease];ASSERTNOTNIL(pathologyHeaderView);
	[pathologyTableView setHeaderView: pathologyHeaderView];

	headerFrame=[[regionTableView headerView] frame];
	BrowserSelectionTableHeaderView *regionHeaderView=[[[BrowserSelectionTableHeaderView alloc] initWithFrame: headerFrame] autorelease];ASSERTNOTNIL(regionHeaderView);
	[regionTableView setHeaderView: regionHeaderView];

	headerFrame=[[albumTableView headerView] frame];
	BrowserSelectionTableHeaderView *albumHeaderView=[[[BrowserSelectionTableHeaderView alloc] initWithFrame: headerFrame] autorelease];ASSERTNOTNIL(albumHeaderView);
	[albumTableView setHeaderView: albumHeaderView];

// Set the initial browser selections to nil.
	
	[(NSArrayController*) systemListController setSelectionIndexes: [NSIndexSet indexSet]];
	[(NSArrayController*) pathologyListController setSelectionIndexes: [NSIndexSet indexSet]];
	[(NSArrayController*) regionListController setSelectionIndexes: [NSIndexSet indexSet]];

// Formatters.  (Use short date style, but force a 4 digit year, by converting yy to yyyy).

	[NSDateFormatter setDefaultFormatterBehavior: NSDateFormatterBehavior10_4];								// We always run on at least 10.4.
	
	NSDateFormatter *dateFormatter=[[[NSDateFormatter alloc] init] autorelease];ASSERTNOTNIL(dateFormatter);
	[dateFormatter setDateStyle: NSDateFormatterShortStyle];
	NSMutableString *formatString=[[[dateFormatter dateFormat] mutableCopy] autorelease];ASSERTNOTNIL(formatString);
	NSInteger numReplacements=[formatString replaceOccurrencesOfString: @"y" withString: @"*" options: NSLiteralSearch range: NSMakeRange(0, [formatString length])];
	NSString *yearRepeatsString=[@"" stringByPaddingToLength: numReplacements withString: @"*" startingAtIndex: 0];ASSERTNOTNIL(yearRepeatsString);
	[formatString replaceOccurrencesOfString: yearRepeatsString withString: @"yyyy" options: NSLiteralSearch range: NSMakeRange(0, [formatString length])];
	[dateFormatter setDateFormat: formatString];

	[dateOfBirthTextField setFormatter: dateFormatter];
	NSTableColumn *conditionDateColumn=[conditionListTableView tableColumnWithIdentifier: @"Condition Date"];ASSERTNOTNIL(conditionDateColumn);
	[[conditionDateColumn dataCell] setFormatter: dateFormatter];
	NSTableColumn *studyDateColumn=[studyListTableView tableColumnWithIdentifier: @"Study Date"];ASSERTNOTNIL(studyDateColumn);
	[[studyDateColumn dataCell] setFormatter: dateFormatter];

// Manually respecify these bindings to allow for the null placeholder to reflect the localised format string.

	NSDictionary *fieldBindingOptionsDict=[NSDictionary dictionaryWithObjectsAndKeys:
	  [NSNumber numberWithBool: NO], NSAllowsEditingMultipleValuesSelectionBindingOption,
	  [NSNumber numberWithBool: YES], NSConditionallySetsEditableBindingOption,
	  [NSNumber numberWithBool: NO], NSContinuouslyUpdatesValueBindingOption,
	  [NSNumber numberWithBool: YES], NSRaisesForNotApplicableKeysBindingOption,
	  [NSNumber numberWithBool: NO], NSValidatesImmediatelyBindingOption,
	  [formatString lowercaseString], NSNullPlaceholderBindingOption,
	  nil];ASSERTNOTNIL(fieldBindingOptionsDict);
	[dateOfBirthTextField bind: NSValueBinding toObject: conditionArrayController withKeyPath: @"selection.patient.dateOfBirth" options: fieldBindingOptionsDict];

	NSDictionary *columnBindingOptionsDict=[NSDictionary dictionaryWithObjectsAndKeys:
	  [NSNumber numberWithBool: YES], NSAllowsEditingMultipleValuesSelectionBindingOption,
	  [NSNumber numberWithBool: YES], NSConditionallySetsEditableBindingOption,
	  [NSNumber numberWithBool: YES], NSCreatesSortDescriptorBindingOption,
	  [NSNumber numberWithBool: YES], NSRaisesForNotApplicableKeysBindingOption,
	  [formatString lowercaseString], NSNullPlaceholderBindingOption,
	  nil];ASSERTNOTNIL(columnBindingOptionsDict);
	[conditionDateColumn bind: NSValueBinding toObject: conditionArrayController withKeyPath: @"arrangedObjects.dateOfOnset" options: columnBindingOptionsDict];
	[studyDateColumn bind: NSValueBinding toObject: studyArrayController withKeyPath: @"arrangedObjects.dateOfStudy" options: columnBindingOptionsDict];

// Set up the toolbar.

	[self setUpToolbar];

// Key equivalents for image window and fullscreen.

	unichar leftArrowKey=NSLeftArrowFunctionKey;
	unichar rightArrowKey=NSRightArrowFunctionKey;
	unichar upArrowKey=NSUpArrowFunctionKey;
	unichar downArrowKey=NSDownArrowFunctionKey;

	[imageWindowPreviousStudyButton setKeyEquivalent: [NSString stringWithCharacters: &leftArrowKey length: 1]];
	[imageWindowNextStudyButton setKeyEquivalent: [NSString stringWithCharacters: &rightArrowKey length: 1]];

	[fullScreenWindowPreviousCaseButton setKeyEquivalent: [NSString stringWithCharacters: &leftArrowKey length: 1]];
	[fullScreenWindowNextCaseButton setKeyEquivalent: [NSString stringWithCharacters: &rightArrowKey length: 1]];
	[fullScreenWindowPreviousStudyButton setKeyEquivalent: [NSString stringWithCharacters: &leftArrowKey length: 1]];
	[fullScreenWindowNextStudyButton setKeyEquivalent: [NSString stringWithCharacters: &rightArrowKey length: 1]];
	[fullScreenWindowPreviousImageButton setKeyEquivalent: [NSString stringWithCharacters: &upArrowKey length: 1]];
	[fullScreenWindowNextImageButton setKeyEquivalent: [NSString stringWithCharacters: &downArrowKey length: 1]];

// Set up full screen tab view.

	[fullScreenTabSelector setSelectedSegment: 0];
	
// Set mediaWindow's movieView to receive key presses directed through fileTableView.

	[movieView setNextResponder: [fileTableView nextResponder]];
	[fileTableView setNextResponder: movieView];

// Set fullScreen movieView to receive key presses directed through fullScreenPanel.

//	[fullScreenMovieView setNextResponder: [fullScreenPanel nextResponder]];
//	[fullScreenPanel setNextResponder: fullScreenMovieView];

// Ensure default bindings for choose and edit buttons in image window.

	[self imageTableViewWasSelected];

// Manually prepare the content for browser and album arrays.  Automatically Prepare Content is off in the Nib.
// This allows for the overridden fetch and the transient extendedLabel property.
// See http://developer.apple.com/DOCUMENTATION/Cocoa/Conceptual/CoreData/Articles/cdBindings.html#//apple_ref/doc/uid/TP40004194-SW1

	NSError *error=nil;
	[browserArrayController fetchWithRequest: nil merge: NO error: &error];
	[albumArrayController fetchWithRequest: nil merge: NO error: &error];
	
// Set the default table sortings (browser is deferred to allow for overridden fetch)

	NSSortDescriptor *sortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"dateOfOnset" ascending: NO] autorelease];ASSERTNOTNIL(sortDescriptor);
	[browserTableView setSortDescriptors: @[sortDescriptor]];
	[browserArrayController setSelectionIndex: 0];
	
	sortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"extendedLabel" ascending: YES] autorelease];ASSERTNOTNIL(sortDescriptor);
	[albumTableView setSortDescriptors: @[sortDescriptor]];

	sortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"dateOfOnset" ascending: YES] autorelease];ASSERTNOTNIL(sortDescriptor);
	[conditionListTableView setSortDescriptors: @[sortDescriptor]];

	sortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"dateOfStudy" ascending: YES] autorelease];ASSERTNOTNIL(sortDescriptor);
	[studyListTableView setSortDescriptors: @[sortDescriptor]];
	[browserStudyController setSortDescriptors: @[sortDescriptor]];

	sortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"order" ascending: YES] autorelease];ASSERTNOTNIL(sortDescriptor);
	[imageTableView setSortDescriptors: @[sortDescriptor]];
	[fileTableView setSortDescriptors: @[sortDescriptor]];
	[browserImageController setSortDescriptors: @[sortDescriptor]];
	[browserFileController setSortDescriptors: @[sortDescriptor]];

// Notifications.

	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(fileTableViewWasSelected) name: FTVBecameFirstResponderNotification object: imageWindow];
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(imageTableViewWasSelected) name: ITVBecameFirstResponderNotification object: imageWindow];

	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(exportToMIRCCompletedSuccessfully) name: ExportToMIRCCompletedSuccessfullyNotification object: browserArrayController];
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(exportToMIRCFailed) name: ExportToMIRCFailedNotification object: browserArrayController];

	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(resetBrowser:) name: ResetBrowserNotification object: browserArrayController];

	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(setLinkLabelToUnlink) name: OptionKeyPressedNotification object: editorWindow];
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(setLinkLabelToLink) name: OptionKeyReleasedNotification object: editorWindow];

	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(objectsDidChange:) name: NSManagedObjectContextObjectsDidChangeNotification object: [self managedObjectContext]];

	[[NSDistributedNotificationCenter defaultCenter] addObserver: self selector: @selector(exportedFromOsirixWithNotification:)
	  name: @"ExportOsiriXToXphile" object: @"nz.org.ddp.xphile" suspensionBehavior: NSNotificationSuspensionBehaviorDeliverImmediately];

	[statusField setStringValue: ReadyString];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Window Delegates

- (void) windowDidBecomeKey: (NSNotification*) aNotification
	{
	
// If image window has become key, check to reload its image.
	
	if ([aNotification object]==imageWindow)
		{
		if ([imageWindow firstResponder]==imageTableView)
			{
			[[imageTableView delegate] performSelector: @selector(reloadImageAfterEditing) withObject: nil];
			}
		if ([imageWindow firstResponder]==fileTableView)
			{
			[[fileTableView delegate] performSelector: @selector(reloadImageAfterEditing) withObject: nil];
			}
		}
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) windowDidResignKey: (NSNotification*) aNotification
	{

// If the editor window is closed, processPendingChanges in MOC.

	if ([aNotification object] == editorWindow)
		{
		[editorWindow makeFirstResponder: nil];
		}

// If the fullscreen panel is clicked out (multiple monitors), formally exit full screen.

	if ([aNotification object] == fullScreenWindow)
		{
		[self exitFullScreen: self];
		}
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (BOOL) windowShouldClose: (id) sender
	{

// Just ordering out locks the movieView image in place.  The following prevents this from occuring.

	if (sender==imageWindow)
		{
		[movieView setMovie: nil];
		}

// If one of the secondary windows, dont close (resulting in a release of the relevant window controller), instead just hide the window.

	if (sender==imageWindow || sender==editorWindow)
		{
		[sender orderOut: self];
		return (NO);
		}

	return YES;
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSUndoManager*) windowWillReturnUndoManager: (NSWindow* __unused) sender
	{
	return [[self managedObjectContext] undoManager];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Control Delegates

- (BOOL) control: (NSControl* __unused) control didFailToFormatString: (NSString*) str errorDescription: (NSString* __unused) errDescription
	{
		
	if (str==nil)
		{
		return YES;
		}
	
	if ([str length]==0)
		{
		return YES;
		}

	return NO;
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Validation

- (BOOL) validateUserInterfaceItem: (id <NSValidatedUserInterfaceItem>) anItem
	{
    SEL theAction=[anItem action];

	BOOL dependsOnNonZeroSelection=NO;
	BOOL dependsOnSingleSelection=NO;
	
	if (theAction==@selector(openInOsirix:) || theAction==@selector(editCase:) || theAction==@selector(showImages:) || theAction==@selector(removeCase:))
		{
		dependsOnNonZeroSelection=YES;
		}
	if (theAction==@selector(openInOsirix:) || theAction==@selector(editCase:) || theAction==@selector(showImages:))
		{
		dependsOnSingleSelection=YES;
		}

	NSIndexSet *indexes=[browserArrayController selectionIndexes];ASSERTNOTNIL(indexes);
	if (dependsOnNonZeroSelection)
		{
		if ([indexes count]==0)
			{
			return NO;
			}
		}
	if (dependsOnSingleSelection)
		{
		if ([indexes count]!=1)
			{
			return NO;
			}
		}

	return ([super validateUserInterfaceItem: anItem]);
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Album and Browser Table Delegates

- (void) tableViewSelectionDidChange: (NSNotification*) aNotification
	{
	ASSERTNOTNIL([aNotification object]);
	
	if ([aNotification object]==albumTableView)
		{
		[self updateForAlbumSelection];									// If changed selection of album: update binding of browserArrayController and reset the browser selections.
		}
	else
		{
		[self updateForBrowserSelection];								// Alternatively, update for current browser (system/path/region) selection.
		}
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) updateForAlbumSelection
	{
	if ([[albumTableView selectedRowIndexes] count] == 0)				// no selection, remove bindings.
		{
		[stage1BrowserArrayController unbind: NSContentSetBinding];
		[stage1BrowserArrayController unbind: NSContentArrayForMultipleSelectionBinding];
		[stage1BrowserArrayController setFetchPredicate: nil];
		}
	else																// selection, bind so that either stage1BrowserArrayController shows subset from selected album.
		{																// or the fetch predicate reflects the smart album.
		AlbumEntity *album=[[albumArrayController selectedObjects] objectAtIndex: 0];ASSERTNOTNIL(album);
		if ([album isSmartAlbum])										// smart album.
			{
			[stage1BrowserArrayController unbind: NSContentSetBinding];
			[stage1BrowserArrayController unbind: NSContentArrayForMultipleSelectionBinding];
			NSData *predicateData=[album valueForKeyPath: @"itsPredicate"];
			if (predicateData!=nil)
				{
				NSPredicate *smartAlbumPredicate=[NSKeyedUnarchiver unarchiveObjectWithData: predicateData];ASSERTNOTNIL(smartAlbumPredicate);
				[stage1BrowserArrayController setFetchPredicate: smartAlbumPredicate];
				}
			}
		else															// simple album.
			{
			NSDictionary *bindingOptionsDict=[NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects: [NSNumber numberWithBool: YES], [NSNumber numberWithBool: YES], nil]
			  forKeys: [NSArray arrayWithObjects: NSConditionallySetsEditableBindingOption, NSRaisesForNotApplicableKeysBindingOption, nil]];ASSERTNOTNIL(bindingOptionsDict);

			[stage1BrowserArrayController bind: NSContentSetBinding toObject: albumArrayController withKeyPath: @"selection.self.conditions" options: bindingOptionsDict];
			[stage1BrowserArrayController bind: NSContentArrayForMultipleSelectionBinding toObject: albumArrayController withKeyPath: @"@distinctUnionOfSets.conditions" options: bindingOptionsDict];
			[stage1BrowserArrayController setFetchPredicate: nil];
			}
		}
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) updateForBrowserSelection
	{
	
// Create an array of sub-predicates, from smart album, system, pathology and region.  Only add if not TRUEPREDICATE.
// Must be at least TRUEPREDICATE.  (Efficiency and Leopard compatibility).

	NSMutableArray *subPredicateArray=[NSMutableArray arrayWithCapacity: 0];ASSERTNOTNIL(subPredicateArray);
	if (![[self systemPredicateString] isEqualToString: @"TRUEPREDICATE"])
		{
		[subPredicateArray addObject: [NSPredicate predicateWithFormat: [self systemPredicateString]]];
		}
	if (![[self pathologyPredicateString] isEqualToString: @"TRUEPREDICATE"])
		{
		[subPredicateArray addObject: [NSPredicate predicateWithFormat: [self pathologyPredicateString]]];
		}
	if (![[self regionPredicateString] isEqualToString: @"TRUEPREDICATE"])
		{
		[subPredicateArray addObject: [NSPredicate predicateWithFormat: [self regionPredicateString]]];
		}
	if ([subPredicateArray count]==0)
		{
		[subPredicateArray addObject: [NSPredicate predicateWithFormat: @"TRUEPREDICATE"]];
		}
	NSPredicate *compoundPredicate=[NSCompoundPredicate andPredicateWithSubpredicates: subPredicateArray];ASSERTNOTNIL(compoundPredicate);

	[stage1BrowserArrayController setFilterPredicate: compoundPredicate];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Filing Routines

- (void) saveDocumentWithDelegate: (id) delegate didSaveSelector: (SEL) didSaveSelector contextInfo: (void*) contextInfo
	{

// Before calling save, ensure that the currently edited patient has been finalised.  Then ensure that the managed object context is up to date.

	[[self windowForSheet] makeFirstResponder: nil];
	[[self managedObjectContext] processPendingChanges];

	[super saveDocumentWithDelegate: delegate didSaveSelector: didSaveSelector contextInfo: contextInfo];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Export Routines

- (IBAction) exportImages: (id) sender
	{
	BOOL							success;
	
	
// Called from various views, eg album, browser or image table.
// Distributes export job from the tableView to its appropriate controller after choosing the export directory.

// First check that there is a valid selection if the toolbar button was used, and exit if not (no toolbar in image viewer).

	if ([[self windowForSheet] firstResponder]==albumTableView)
		{
		if ([[albumTableView selectedRowIndexes] count] == 0)
			{
			return;
			}
		}
	else if ([[self windowForSheet] firstResponder]==browserTableView)
		{
		if ([[browserTableView selectedRowIndexes] count] == 0)
			{
			return;
			}
		}

	ASSERTNOTNIL(sender);
	
	NSOpenPanel *panel=[NSOpenPanel openPanel];ASSERTNOTNIL(panel);
	[panel setPrompt: ExportString];
	[panel setTitle: ExportImagesString];
	[panel setCanChooseDirectories: YES];
	[panel setCanChooseFiles: NO];
	[panel setCanCreateDirectories: YES];
	if ([panel runModal]==NSCancelButton)
		{
		return;
		}

	[statusField setStringValue: ExportingImagesString];
	[progressIndicator startAnimation: self];

	if (sender==albumTableView)
		{
		success=[albumArrayController exportImagesTo: [[panel URL] path]];
		}
	else if (sender==browserTableView)
		{
		success=[browserArrayController exportImagesTo: [[panel URL] path]];
		}
	else if (sender==imageTableView)
		{
		success=[imageArrayController exportImagesTo: [[panel URL] path]];
		}
	else if (sender==fileTableView)
		{
		success=[fileArrayController exportImagesTo: [[panel URL] path]];
		}
	else if ([[self windowForSheet] firstResponder]==albumTableView)
		{
		success=[albumArrayController exportImagesTo: [[panel URL] path]];
		}
	else if ([[self windowForSheet] firstResponder]==browserTableView)
		{
		success=[browserArrayController exportImagesTo: [[panel URL] path]];
		}
	else
		{
		success=NO;
		}

	if (success)
		{
		[statusField setStringValue: SuccessfullyExportedImagesString];
		}
	else
		{
		[[NSAlert alertWithMessageText: UnableToExportImageString defaultButton: OKString alternateButton: @"" otherButton: @"" informativeTextWithFormat: UnsupportedFormatString] runModal];
		}
	[statusField performSelector: @selector(setStringValue:) withObject: ReadyString afterDelay: 3.0];
	[progressIndicator stopAnimation: self];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) exportToKeynote: (id) sender
	{
	
// Called from various views, eg album, browser or image table.
// Distributes export job from the tableView to its appropriate controller after choosing save as location.

	ASSERTNOTNIL(sender);

// First check that there is a valid selection if the toolbar button was used, and exit if not.

	if ([[self windowForSheet] firstResponder]==albumTableView)
		{
		if ([[albumTableView selectedRowIndexes] count] == 0)
			return;
		}
	else if ([[self windowForSheet] firstResponder]==browserTableView)
		{
		if ([[browserTableView selectedRowIndexes] count] == 0)
			return;
		}

// Get save location.

	NSSavePanel *panel=[NSSavePanel savePanel];ASSERTNOTNIL(panel);
	[panel setTitle: SaveKeynoteString];
	[panel setAllowedFileTypes: [NSArray arrayWithObject: @"key"]];
	[panel setCanSelectHiddenExtension: YES];
	ASSERTNOTNIL(savePanelAccessoryView);
	[panel setAccessoryView: savePanelAccessoryView];
	if ([panel runModal]==NSCancelButton)
		{
		return;
		}

	NSError *error=nil;
	if ([[NSFileManager defaultManager] fileExistsAtPath: [[panel URL] path]])
		{
		if (![[NSFileManager defaultManager] removeItemAtPath: [[panel URL] path] error: &error])
			{
			NSLog(@"Error: %@", [error description]);
			}
		}
	BOOL success=[[NSFileManager defaultManager] createDirectoryAtPath: [[panel URL] path] withIntermediateDirectories: YES  attributes: nil error: &error];
	if (!success)
		{
		NSLog(@"Error: %@", [error description]);
		return;
		}
	
	[statusField setStringValue: ExportingToKeynoteString];
	[progressIndicator startAnimation: self];
	
	@try
		{
		if (sender==albumTableView || [[self windowForSheet] firstResponder]==albumTableView)
			{
			NSIndexSet *storedIndex=[browserArrayController selectionIndexes];ASSERTNOTNIL(storedIndex);
			[browserArrayController selectAll];
			success=[browserArrayController exportToKeynoteFile: [[panel URL] path]];
			[browserArrayController setSelectionIndexes: storedIndex];
			}
		else if (sender==browserTableView)
			{
			success=[browserArrayController exportToKeynoteFile: [[panel URL] path]];
			}
		else if (sender==imageTableView)
			{
			success=[imageArrayController exportToKeynoteFile: [[panel URL] path]];
			}
		else if (sender==fileTableView)
			{
			success=[fileArrayController exportToKeynoteFile: [[panel URL] path]];
			}
		else if ([[self windowForSheet] firstResponder]==albumTableView)
			{
			success=[albumArrayController exportToKeynoteFile: [[panel URL] path]];
			}
		else if ([[self windowForSheet] firstResponder]==browserTableView)
			{
			success=[browserArrayController exportToKeynoteFile: [[panel URL] path]];
			}
		else
			{
			success=NO;
			}
			
		if (!success)
			{
			[[NSException exceptionWithName: @"KeynoteExportFailed" reason: nil userInfo: nil] raise];
			}

		[statusField setStringValue: SuccessfullyExportedToKeynoteString];
		[statusField performSelector: @selector(setStringValue:) withObject: ReadyString afterDelay: 3.0];
		}
	@catch (NSException *exception)
		{
		[statusField setStringValue: UnableToExportToKeynoteString];
		[statusField performSelector: @selector(setStringValue:) withObject: ReadyString afterDelay: 3.0];
		[[NSAlert alertWithMessageText: UnableToExportToKeynoteString defaultButton: OKString alternateButton: @"" otherButton: @"" informativeTextWithFormat: @"%@", [exception description]] runModal];
		}
		
	[progressIndicator stopAnimation: self];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) exportAsMIRCArchive: (id) sender
	{
	
// First check that there is a valid selection if the toolbar button was used, and exit if not.

	if ([[self windowForSheet] firstResponder]==browserTableView)
		{
		if ([[browserTableView selectedRowIndexes] count] != 1)
			{
			[[NSAlert alertWithMessageText: UnableToExportAsMIRCArchiveString defaultButton: OKString alternateButton: @"" otherButton: @""
			  informativeTextWithFormat: UnableToExportAlbumDetailString] runModal];
			return;
			}
		}

	ASSERTNOTNIL(sender);
	
// Get save location.

	NSSavePanel *panel=[NSSavePanel savePanel];ASSERTNOTNIL(panel);
	[panel setAllowedFileTypes: [NSArray arrayWithObject: @"zip"]];
	[panel setCanSelectHiddenExtension: YES];
	if ([panel runModal]==NSCancelButton)
		{
		return;
		}
	
	[statusField setStringValue: ExportingAsMIRCArchiveString];
	[progressIndicator startAnimation: self];

// Set up the temporary archive folder.
	
	@try
		{
		NSString *temporaryPath=[[[panel URL] path] stringByDeletingPathExtension];ASSERTNOTNIL(temporaryPath);
		NSError *error=nil;
		if ([[NSFileManager defaultManager] fileExistsAtPath: temporaryPath])
			{
			if (![[NSFileManager defaultManager] removeItemAtPath: temporaryPath error: &error])
				{
				NSLog(@"Error: %@", [error description]);
				[[NSException exceptionWithName: @"FailedArchiveCreation" reason: @"" userInfo: nil] raise];
				}
			}
		
		if (![[NSFileManager defaultManager] createDirectoryAtPath: temporaryPath withIntermediateDirectories: YES  attributes: nil error: &error])
			{
			NSLog(@"Error: %@", [error description]);
			[[NSException exceptionWithName: @"FailedArchiveCreation" reason: @"" userInfo: nil] raise];
			}

		BOOL success=YES;
		if (sender==browserTableView)
			{
			success=[browserArrayController exportToMIRCArchiveAtPath: temporaryPath];
			}
		else if ([[self windowForSheet] firstResponder]==browserTableView)
			{
			success=[browserArrayController exportToMIRCArchiveAtPath: temporaryPath];
			}
		else
			{
			[[NSAlert alertWithMessageText:UnableToExportAlbumString defaultButton: OKString alternateButton: @"" otherButton: @""
			  informativeTextWithFormat: UnableToExportAlbumDetailString] runModal];
			[statusField setStringValue: ReadyString];
			}
			
		if (!success)
			{
			[[NSException exceptionWithName: @"MIRCExportFailed" reason: nil userInfo: nil] raise];
			}
		[statusField setStringValue: SuccessfullyExportedAsMIRCArchiveString];
		[statusField performSelector: @selector(setStringValue:) withObject: ReadyString afterDelay: 3.0];
		}
	@catch (NSException *exception)
		{
		[statusField setStringValue: UnableToExportAsMIRCArchiveString];
		[[NSAlert alertWithMessageText: UnableToExportAsMIRCArchiveString defaultButton: OKString alternateButton: @"" otherButton: @"" informativeTextWithFormat: @""] runModal];
		}
	[progressIndicator stopAnimation: self];
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) exportToMIRCServer: (id) sender
	{
	ASSERTNOTNIL(sender);

// First check that there is a valid selection if the toolbar button was used, and exit if not.

	if ([[self windowForSheet] firstResponder]==browserTableView)
		{
		if ([[browserTableView selectedRowIndexes] count] != 1)
			{
			[[NSAlert alertWithMessageText: UnableToExportAsMIRCArchiveString defaultButton: OKString alternateButton: @"" otherButton: @""
			  informativeTextWithFormat: UnableToExportAlbumDetailString] runModal];
			return;
			}
		}

	[statusField setStringValue: ExportingToMIRCString];
	[progressIndicator startAnimation: self];

	@try
		{

// Set up the temporary archive folder.
	
		NSString *temporaryPath=[NSTemporaryDirectory() stringByAppendingPathComponent: @"Xphile MIRC Archive"];ASSERTNOTNIL(temporaryPath);
		NSError *error=nil;

		if ([[NSFileManager defaultManager] fileExistsAtPath: temporaryPath])
			{
			if (![[NSFileManager defaultManager] removeItemAtPath: temporaryPath error: &error])
				{
				NSLog(@"Error: %@", [error description]);
				[[NSException exceptionWithName: @"FailedArchiveCreation" reason: @"" userInfo: nil] raise];
				}
			}
		if (![[NSFileManager defaultManager] createDirectoryAtPath: temporaryPath withIntermediateDirectories: YES attributes: nil error: &error])
			{
			NSLog(@"Error: %@", [error description]);
			[[NSException exceptionWithName: @"FailedArchiveCreation" reason: @"" userInfo: nil] raise];
			}

		BOOL success=YES;
		if (sender==browserTableView)
			{
			success=[browserArrayController exportToMIRCServerUsingPath: temporaryPath];
			}
		else if ([[self windowForSheet] firstResponder]==browserTableView)
			{
			success=[browserArrayController exportToMIRCServerUsingPath: temporaryPath];
			}
		else
			{
			[[NSAlert alertWithMessageText: UnableToExportAlbumString defaultButton: OKString alternateButton: @"" otherButton: @""
			  informativeTextWithFormat: UnableToExportAlbumDetailString] runModal];
			[statusField setStringValue: ReadyString];
			[progressIndicator stopAnimation: self];
			}
			
		if (!success)
			{
			[[NSException exceptionWithName: @"MIRCExportFailed" reason: nil userInfo: nil] raise];
			}
		}
	@catch (NSException *exception)
		{
		[statusField setStringValue: UnableToExportToMIRCString];
		[[NSAlert alertWithMessageText: UnableToExportToMIRCString defaultButton: OKString alternateButton: @"" otherButton: @"" informativeTextWithFormat: @""] runModal];
		[progressIndicator stopAnimation: self];
		}
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) exportToMIRCCompletedSuccessfully
	{
	[statusField setStringValue: SuccessfullyExportedToMIRCString];
	[statusField performSelector: @selector(setStringValue:) withObject: ReadyString afterDelay: 3.0];
	[progressIndicator stopAnimation: self];
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) exportToMIRCFailed
	{
	[[NSAlert alertWithMessageText: UnableToExportToMIRCString defaultButton: OKString alternateButton: @"" otherButton: @"" informativeTextWithFormat: @""] runModal];
	[progressIndicator stopAnimation: self];
	[statusField setStringValue: UnableToExportToMIRCString];
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Full Screen

- (IBAction) enterFullScreen: (id __unused) sender
	{
	if (![NSMenu menuBarVisible])
		{
		return;
		}

// Make sure we have at least one screen before trying to find the main screen.

	NSArray *screensArray=[NSScreen screens];ASSERTNOTNIL(screensArray);
	if ([screensArray count] == 0)
		{
		return;
		}
	
// If selection is not 1, set it to the first item.

	NSArray *conditionsArray=[browserArrayController selectedObjects];ASSERTNOTNIL(conditionsArray);
	if ([conditionsArray count]!=1)
		{
		[browserArrayController setSelectedObjects: @[conditionsArray.firstObject]];
		}

	NSScreen *chosenScreen=[screensArray objectAtIndex: 0];ASSERTNOTNIL(chosenScreen);			// ? Need to allow for a secondary screen.
	NSRect screenRect=[chosenScreen frame];
	[NSMenu setMenuBarVisible: NO];

	fullScreenWindow = [[FullScreenWindow alloc] initWithContentRect: screenRect styleMask: NSBorderlessWindowMask
	  backing: NSBackingStoreBuffered defer: NO screen: chosenScreen];ASSERTNOTNIL(fullScreenWindow);
	[fullScreenWindow setBackgroundColor: [NSColor blackColor]];
	[fullScreenPanel setFrame: screenRect display: YES];
	[fullScreenWindow setContentView: [fullScreenPanel contentView]];
	[fullScreenWindow setDelegate: (id) self];
	[fullScreenWindow makeKeyAndOrderFront: self];
	[fullScreenHUDWindow setFloatingPanel: YES];

// Show blank image?
// Reverse preference setting if option key is down.
	
	BOOL shouldShowBlankImage=[[NSUserDefaults standardUserDefaults] boolForKey: FullScreenBlankImageKey];
	BOOL isOptionKey=(([[NSApp currentEvent] modifierFlags] & NSAlternateKeyMask) != 0);
	if (isOptionKey)
		{
		shouldShowBlankImage=!shouldShowBlankImage;
		}
	[fullscreenBlankView setHidden: !shouldShowBlankImage];

// Select the image tab unless there are no images, but there are movies.
	
	if ([[imageArrayController arrangedObjects] count] == 0 && [[fileArrayController arrangedObjects] count] > 0)
		{
		[fullScreenTabSelector setSelectedSegment: 1];
		[fileArrayController setSelectionIndex: 0];
		[imageWindow makeFirstResponder: fileTableView];
		}
	else
		{
		[fullScreenTabSelector setSelectedSegment: 0];
		[imageArrayController setSelectionIndex: 0];
		[imageWindow makeFirstResponder: imageTableView];
		}
	[fullScreenTabSelector performClick: self];
	}

	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) exitFullScreen: (id __unused) sender
	{
	
	if ([NSMenu menuBarVisible])
		return;
		
	[NSMenu setMenuBarVisible: YES];

	[fullScreenPanel setContentView: [fullScreenWindow contentView]];
	[fullScreenWindow orderOut: self];

	[fullScreenHUDWindow orderOut: self];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) toggleHUDWindow: (id) sender
	{
	if ([fullScreenHUDWindow isVisible])
		[fullScreenHUDWindow orderOut: sender];
	else
		[fullScreenHUDWindow makeKeyAndOrderFront: sender]; 
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) selectFullScreenTab: (id) sender
	{
	[fullScreenTabView selectTabViewItemAtIndex: [sender selectedSegment]];

	BOOL toggledAsMovie=([sender selectedSegment]==1);
	if (toggledAsMovie)
		{
		[fullScreenWindowNextImageButton setTarget: browserFileController];
		[fullScreenWindowNextImageButton unbind: NSEnabledBinding];
		[fullScreenWindowNextImageButton bind: NSEnabledBinding toObject: browserFileController withKeyPath: @"canSelectNext" options: nil];

		[fullScreenWindowPreviousImageButton setTarget: browserFileController];
		[fullScreenWindowPreviousImageButton unbind: NSEnabledBinding];
		[fullScreenWindowPreviousImageButton bind: NSEnabledBinding toObject: browserFileController withKeyPath: @"canSelectPrevious" options: nil];
		}
	else
		{
		[fullScreenWindowNextImageButton setTarget: browserImageController];
		[fullScreenWindowNextImageButton unbind: NSEnabledBinding];
		[fullScreenWindowNextImageButton bind: NSEnabledBinding toObject: browserImageController withKeyPath: @"canSelectNext" options: nil];

		[fullScreenWindowPreviousImageButton setTarget: browserImageController];
		[fullScreenWindowPreviousImageButton unbind: NSEnabledBinding];
		[fullScreenWindowPreviousImageButton bind: NSEnabledBinding toObject: browserImageController withKeyPath: @"canSelectPrevious" options: nil];
		}
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Editor Window


- (void) editPatient: (PatientEntity*) aPatientEntity
	{
	ASSERTNOTNIL(aPatientEntity);
	
	NSMutableSet *conditionSet=[aPatientEntity mutableSetValueForKey: @"conditions"];ASSERTNOTNIL(conditionSet);
	if ([conditionSet count] == 0)
		{
		return;
		}

	[self resetBrowser: self];

	[browserArrayController performSelector: @selector(setSelectedObjects:) withObject: [conditionSet allObjects] afterDelay: 0.5];
	[editorWindow makeKeyAndOrderFront: self];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) setLinkLabelToUnlink
	{
	[editorLinkedToButton setTitle: UnlinkedToString];
	[editorLinkedFromButton setTitle: UnlinkedFromString];
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) setLinkLabelToLink
	{
	[editorLinkedToButton setTitle: LinkedToString];
	[editorLinkedFromButton setTitle: LinkedFromString];
	}
	
	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Image Window

- (void) fileTableViewWasSelected
	{
	NSDictionary *buttonBindingOptionsDict=[NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool: YES], NSRaisesForNotApplicableKeysBindingOption,
	  NSIsNotNilTransformerName, NSValueTransformerNameBindingOption, nil];
	ASSERTNOTNIL(buttonBindingOptionsDict);
	[imageWindowEditButton unbind: NSEnabledBinding];
	[imageWindowEditButton bind: NSEnabledBinding toObject: fileArrayController withKeyPath: @"selection.fileData" options: buttonBindingOptionsDict];

	buttonBindingOptionsDict=[NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool: YES], NSRaisesForNotApplicableKeysBindingOption,
	  @"IsSingleTransformer", NSValueTransformerNameBindingOption, nil];
	ASSERTNOTNIL(buttonBindingOptionsDict);
	[imageWindowChooseButton unbind: NSEnabledBinding];
	[imageWindowChooseButton bind: NSEnabledBinding toObject: fileArrayController withKeyPath: @"selection.@count" options: buttonBindingOptionsDict];
	}
	
	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) imageTableViewWasSelected
	{
	NSDictionary *buttonBindingOptionsDict=[NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects:
		[NSNumber numberWithBool: YES], NSIsNotNilTransformerName, nil]
	  forKeys: [NSArray arrayWithObjects:
		NSRaisesForNotApplicableKeysBindingOption, NSValueTransformerNameBindingOption, nil]];
	ASSERTNOTNIL(buttonBindingOptionsDict);
	[imageWindowEditButton unbind: NSEnabledBinding];
	[imageWindowEditButton bind: NSEnabledBinding toObject: imageArrayController withKeyPath: @"selection.imageData" options: buttonBindingOptionsDict];

	buttonBindingOptionsDict=[NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects:
		[NSNumber numberWithBool: YES], @"IsSingleTransformer", nil]
	  forKeys: [NSArray arrayWithObjects: NSRaisesForNotApplicableKeysBindingOption, NSValueTransformerNameBindingOption, nil]];
	ASSERTNOTNIL(buttonBindingOptionsDict);
	[imageWindowChooseButton unbind: NSEnabledBinding];
	[imageWindowChooseButton bind: NSEnabledBinding toObject: imageArrayController withKeyPath: @"selection.@count" options: buttonBindingOptionsDict];
	}
	
	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) editImage: (id __unused) sender
	{
		ASSERTNOTNIL(imageArrayController);
	
	NSArray *imageSelectionArray=[imageArrayController selectedObjects];ASSERTNOTNIL(imageSelectionArray);
	if ([imageSelectionArray count]==0)
		return;

// Name unique temporary file.

	ImageEntity *anImage=[imageSelectionArray objectAtIndex: 0];ASSERTNOTNIL(anImage);
	NSString *tempImagePath=[NSTemporaryDirectory() stringByAppendingPathComponent: [anImage defaultFileName]];
	ASSERTNOTNIL(tempImagePath);

// Write image data to temporary file.

	NSData *data=[[imageSelectionArray objectAtIndex: 0] valueForKeyPath: @"imageData"];ASSERTNOTNIL(data);
	NSData *rawData=[NSUnarchiver unarchiveObjectWithData: data];ASSERTNOTNIL(rawData);
	[rawData writeToFile: tempImagePath atomically: YES];

// Open temp file in editor.

	NSString *editorPath=[[NSUserDefaults standardUserDefaults] valueForKey: ImageEditorPathKey];ASSERTNOTNIL(editorPath);
	BOOL success=[[NSWorkspace sharedWorkspace] openFile: tempImagePath withApplication: editorPath];

// Alert if failed.

	if (!success)
		{
		[[NSAlert alertWithMessageText: FailureToLaunchString defaultButton: OKString alternateButton: @"" otherButton: @"" informativeTextWithFormat: @"%@", editorPath] runModal];
		}
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) editMovie: (id __unused) sender
	{
		ASSERTNOTNIL(fileArrayController);
	
	NSArray *fileSelectionArray=[fileArrayController selectedObjects];ASSERTNOTNIL(fileSelectionArray);
	if ([fileSelectionArray count]==0)
		return;

// Name unique temporary file.

	FileEntity *aMovie=[fileSelectionArray objectAtIndex: 0];ASSERTNOTNIL(aMovie);
	NSString *tempMoviePath=[NSTemporaryDirectory() stringByAppendingPathComponent: [aMovie valueForKey: @"fileName"]];
	ASSERTNOTNIL(tempMoviePath);

// Write movie data to temporary file.

	NSData *data=[[fileSelectionArray objectAtIndex: 0] valueForKeyPath: @"fileData"];ASSERTNOTNIL(data);
	[data writeToFile: tempMoviePath atomically: YES];

// Open temp file in editor.

	NSString *editorPath=[[NSUserDefaults standardUserDefaults] valueForKey: MovieEditorPathKey];ASSERTNOTNIL(editorPath);
	BOOL success=[[NSWorkspace sharedWorkspace] openFile: tempMoviePath withApplication: editorPath];

// Alert if failed.

	if (!success)
		{
		[[NSAlert alertWithMessageText: FailureToLaunchString defaultButton: OKString alternateButton: @"" otherButton: @"" informativeTextWithFormat: @"%@", editorPath] runModal];
		}
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Distributed Notifications

- (void) exportedFromOsirixWithNotification: (NSNotification*) notification
	{

// If this document's browserWindow is not frontmost, exit.

	NSArray *appWindows=[NSApp orderedWindows];ASSERTNOTNIL(appWindows);
	if (appWindows.count == 0)
		{
		return;
		}

	NSWindow *frontmostWindow=[appWindows objectAtIndex: 0];ASSERTNOTNIL(frontmostWindow);
	if (frontmostWindow != browserWindow)
		{
		return;
		}
		
// Get the managed object context.

	NSManagedObjectContext *moc=[self managedObjectContext];ASSERTNOTNIL(moc);
	NSDictionary *osirixDictionary=[notification userInfo];ASSERTNOTNIL(osirixDictionary);

// Check for an existing patient with this id, otherwise create a new patient record.

	PatientEntity *patient = nil;
	NSString *idString=[osirixDictionary valueForKey: @"patientID"];
	if (idString!=nil)
		{
		NSString *predicateString=[NSString stringWithFormat: @"identifier like[c] '%@'", idString];ASSERTNOTNIL(predicateString);
		NSPredicate *predicate=[NSPredicate predicateWithFormat: predicateString];ASSERTNOTNIL(predicate);
		NSFetchRequest *request=[[[NSFetchRequest alloc] init] autorelease];ASSERTNOTNIL(request);
		NSEntityDescription *entity=[NSEntityDescription entityForName: @"Patient" inManagedObjectContext: moc];ASSERTNOTNIL(entity);
		[request setEntity: entity];
		[request setPredicate: predicate];
		NSError *error=nil;
		NSArray *array=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(array);
		if (array == nil)
			{
			NSLog(@"Error: %@", [error description]);
			}

		if ([array count]>0)
			{
			patient=[array objectAtIndex: 0];
			}
		else
			{
			patient=[NSEntityDescription insertNewObjectForEntityForName: @"Patient" inManagedObjectContext: moc];
			}
		ASSERTNOTNIL(patient);
		}

// Create new condition and study.

	ConditionEntity *condition=[NSEntityDescription insertNewObjectForEntityForName: @"Condition" inManagedObjectContext: moc];ASSERTNOTNIL(condition);
	[condition setValue: patient forKey: @"patient"];
	StudyEntity *study=[NSEntityDescription insertNewObjectForEntityForName: @"Study" inManagedObjectContext: moc];ASSERTNOTNIL(study);
	[study setValue: condition forKey: @"condition"];

// Populate patient data.
	
	if (idString!=nil)
		{
		[patient setValue: idString forKeyPath: @"identifier"];
		}
	
	NSDate *dob=[osirixDictionary valueForKey: @"dateOfBirth"];
	if (dob!=nil)
		{
		[patient setValue: dob forKeyPath: @"dateOfBirth"];
		}

	NSString *genderString=[osirixDictionary valueForKey: @"patientSex"];
	if (genderString!=nil)
		{
		if ([genderString isEqualToString: @"M"])
			{
			[patient setValue: [NSNumber numberWithInteger: 1] forKeyPath: @"gender"];
			}
		else if ([genderString isEqualToString: @"F"])
			{
			[patient setValue: [NSNumber numberWithInteger: 2] forKeyPath: @"gender"];
			}
		else
			{
			[patient setValue: [NSNumber numberWithInteger: 0] forKeyPath: @"gender"];
			}
		}

// Populate condition data.

	NSDate *conditionDate=[osirixDictionary valueForKey: @"date"];
	if (conditionDate!=nil)
		{
		[condition setValue: conditionDate forKey: @"dateOfOnset"];
		}
	NSString *commentString=[osirixDictionary valueForKey: @"comment"];
	if (commentString!=nil)
		{
		[condition setValue: commentString forKey: @"conditionDescription"];
		}
		
// Populate study data.

	NSDate *studyDate=[osirixDictionary valueForKey: @"date"];
	if (studyDate!=nil)
		{
		[study setValue: studyDate forKey: @"dateOfStudy"];
		}
	
	NSString *modalityString=[osirixDictionary valueForKey: @"modality"];
	if ([modalityString isEqualToString: @"DX"])								// Temporary conversion until DX is directly supported.
		modalityString=@"CR";
	NSArray *modalityStringsArray=[[NSUserDefaults standardUserDefaults] valueForKey: ModalityArrayKey];ASSERTNOTNIL(modalityStringsArray);
	NSInteger modalityIndex=[modalityStringsArray indexOfObject: modalityString];
	if (modalityIndex==NSNotFound)
		modalityIndex=0;
	[study setValue: [NSNumber numberWithInteger: modalityIndex] forKey: @"modality"];

// Add images.

	NSArray *imagesArray=[osirixDictionary objectForKey: @"images"];

	if (imagesArray != nil)
		{
		for (NSData *imageData in imagesArray)
			{
			ImageEntity *newImage=[NSEntityDescription insertNewObjectForEntityForName: @"Image" inManagedObjectContext: moc];ASSERTNOTNIL(newImage);
			[newImage setValue: study forKey: @"study"];
			[newImage setImageFromData: imageData];
			}
		}

// Force update of browser display.

	[moc processPendingChanges];
	[[NSNotificationCenter defaultCenter] postNotificationName: ResetBrowserNotification object: self];
	[browserArrayController setSelectedObjects: [NSArray arrayWithObject: condition]];
	[browserTableView scrollRowToVisible: [browserArrayController selectionIndex]];

// Force Xphile to the front.

	[NSApp activateIgnoringOtherApps: YES];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Duplicate Patient Handling

- (IBAction) checkUniqueID: (id) sender
	{
	NSString *idStr=[sender stringValue];ASSERTNOTNIL(idStr);
	if ([idStr length]==0)
		return;
	
	NSString *predicateString=[NSString stringWithFormat: @"identifier like[c] '%@'", idStr];ASSERTNOTNIL(predicateString);
	NSPredicate *predicate=[NSPredicate predicateWithFormat: predicateString];ASSERTNOTNIL(predicate);
	
	NSFetchRequest *request=[[[NSFetchRequest alloc] init] autorelease];ASSERTNOTNIL(request);
	NSEntityDescription *entity=[NSEntityDescription entityForName: @"Patient" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(entity);
	[request setEntity: entity];
	[request setPredicate: predicate];
	NSError *error=nil;
	NSArray *fetchedPatientsArray=[[self managedObjectContext] executeFetchRequest: request error: &error];ASSERTNOTNIL(fetchedPatientsArray);
	if (error!=nil)
		{
		NSRunAlertPanel(ErrorAttemptingLookupString, @"", OKString, nil, nil);
		return;
		}

	if ([fetchedPatientsArray count]>1)
		{

// Alert the user that this identifier is already in use.

		[[NSAlert alertWithMessageText: PatientExistsString defaultButton: OKString alternateButton: @"" otherButton: @""
		  informativeTextWithFormat: PatientExistsExplanationString] runModal];

// Ensure that the copied patient is NOT the one we're currently editing.

		ConditionEntity *currentCondition=[[browserArrayController selectedObjects] objectAtIndex: 0];ASSERTNOTNIL(currentCondition);
		PatientEntity *currentPatient=[currentCondition valueForKey: @"patient"];ASSERTNOTNIL(currentPatient);
		if ([fetchedPatientsArray objectAtIndex: 0] != currentPatient)
			currentPatient=[fetchedPatientsArray objectAtIndex: 0];
		else
			currentPatient=[fetchedPatientsArray objectAtIndex: 1];

// Create new patient and copy its attributes.

		PatientEntity *newPatient=[NSEntityDescription insertNewObjectForEntityForName: @"Patient" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(newPatient);
		[currentPatient duplicateTo: newPatient];
		NSMutableSet *newLinkedToPatients=[newPatient mutableSetValueForKeyPath: @"linkedToPatients"];ASSERTNOTNIL(newLinkedToPatients);
		NSMutableSet *newLinkedFromPatients=[newPatient mutableSetValueForKeyPath: @"linkedFromPatients"];ASSERTNOTNIL(newLinkedFromPatients);
		
// Loop through the matching patients, adding all the albums, conditions, studies and images / files.

		NSUInteger patientIndex;		
		for (patientIndex=0; patientIndex<[fetchedPatientsArray count]; patientIndex++)
			{
			PatientEntity *aPatient=[fetchedPatientsArray objectAtIndex: patientIndex];ASSERTNOTNIL(aPatient);
			
// Linked to.
			
			NSSet *linkedToPatients=[aPatient mutableSetValueForKey: @"linkedToPatients"];ASSERTNOTNIL(linkedToPatients);
			NSEnumerator *linkedToEnumerator=[linkedToPatients objectEnumerator];ASSERTNOTNIL(linkedToEnumerator);
			PatientEntity *aLinkedPatient;
			while ((aLinkedPatient=[linkedToEnumerator nextObject]))
				{
				[newLinkedToPatients addObject: aLinkedPatient];
				}

// Linked from.
			
			NSSet *linkedFromPatients=[aPatient mutableSetValueForKey: @"linkedFromPatients"];ASSERTNOTNIL(linkedFromPatients);
			NSEnumerator *linkedFromEnumerator=[linkedFromPatients objectEnumerator];ASSERTNOTNIL(linkedFromEnumerator);
			while ((aLinkedPatient=[linkedFromEnumerator nextObject]))
				{
				[newLinkedFromPatients addObject: aLinkedPatient];
				}

// Conditions.
			
			NSMutableSet *conditions=[aPatient mutableSetValueForKey: @"conditions"];ASSERTNOTNIL(conditions);
			ConditionEntity *aCondition, *newCondition;
			NSEnumerator *conditionEnumerator=[conditions objectEnumerator];ASSERTNOTNIL(conditionEnumerator);
			while ((aCondition=[conditionEnumerator nextObject]))
				{
				newCondition=[NSEntityDescription insertNewObjectForEntityForName: @"Condition" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(newCondition);
				[aCondition duplicateTo: newCondition];
				[newCondition setValue: newPatient forKey: @"patient"];

// Albums.

				NSSet *currentAlbumSet=[aCondition mutableSetValueForKeyPath: @"albums"];ASSERTNOTNIL(currentAlbumSet);
				NSEnumerator *currentAlbumEnumerator=[currentAlbumSet objectEnumerator];ASSERTNOTNIL(currentAlbumEnumerator);
				AlbumEntity *aCurrentAlbum;
				while ((aCurrentAlbum=[currentAlbumEnumerator nextObject]))
					{
					NSMutableSet *conditionSet=[aCurrentAlbum mutableSetValueForKeyPath: @"conditions"];ASSERTNOTNIL(conditionSet);
					[conditionSet addObject: newCondition];
					}

// Studies.
				
				NSMutableSet *studies=[aCondition mutableSetValueForKey: @"studies"];ASSERTNOTNIL(studies);
				StudyEntity *aStudy;
				NSEnumerator *studyEnumerator=[studies objectEnumerator];ASSERTNOTNIL(studyEnumerator);
				while ((aStudy=[studyEnumerator nextObject]))
					{
					StudyEntity *newStudy=[NSEntityDescription insertNewObjectForEntityForName: @"Study" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(newStudy);
					[aStudy duplicateTo: newStudy];
					[newStudy setValue: newCondition forKey: @"condition"];

// Images.

					NSMutableSet *images=[aStudy mutableSetValueForKey: @"images"];ASSERTNOTNIL(images);
					ImageEntity *anImage;
					NSEnumerator *imageEnumerator=[images objectEnumerator];ASSERTNOTNIL(imageEnumerator);
					while ((anImage=[imageEnumerator nextObject]))
						{
						ImageEntity *newImage=[NSEntityDescription insertNewObjectForEntityForName: @"Image" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(newImage);
						[anImage duplicateTo: newImage];
						[newImage setValue: newStudy forKey: @"study"];
						}

// Files.

					NSMutableSet *files=[aStudy mutableSetValueForKey: @"files"];ASSERTNOTNIL(files);
					FileEntity *aFile;
					NSEnumerator *fileEnumerator=[files objectEnumerator];ASSERTNOTNIL(fileEnumerator);
					while ((aFile=[fileEnumerator nextObject]))
						{
						FileEntity *newFile=[NSEntityDescription insertNewObjectForEntityForName: @"File" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(newFile);
						[aFile duplicateTo: newFile];
						[newFile setValue: newStudy forKey: @"study"];
						}
					}
				}
			
			[[self managedObjectContext] deleteObject: aPatient];
			}
		}
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Linked Patients

- (IBAction) linkTo: (id) sender
	{
	NSInteger linkedToPatientIndex=[sender indexOfSelectedItem]-1;
	ConditionEntity *currentCondition=[[conditionArrayController selectedObjects] objectAtIndex: 0];ASSERTNOTNIL(currentCondition);
	PatientEntity *currentPatient=[currentCondition valueForKeyPath: @"patient"];ASSERTNOTNIL(currentPatient);
	NSMutableSet *linkSet=[currentPatient mutableSetValueForKeyPath: @"linkedToPatients"];ASSERTNOTNIL(linkSet);
	NSArray *linkArray=[linkSet allObjects];
	if ((NSInteger) [linkArray count] < linkedToPatientIndex + 1)
		{
		return;
		}
	
	PatientEntity *linkedPatient=[linkArray objectAtIndex: linkedToPatientIndex];
	if (([[NSApp currentEvent] modifierFlags] & NSAlternateKeyMask) != 0)
		{
		[linkSet removeObject: linkedPatient];
		return;
		}

	NSMutableSet *conditionSet=[linkedPatient mutableSetValueForKey: @"conditions"];ASSERTNOTNIL(conditionSet);
	[browserArrayController performSelector: @selector(setSelectedObjects:) withObject: [conditionSet allObjects] afterDelay: 0.0];
	[editorWindow makeKeyAndOrderFront: self];
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) linkFrom: (id) sender
	{
	NSInteger linkedFromPatientIndex=[sender indexOfSelectedItem]-1;
	
	ConditionEntity *currentCondition=[[conditionArrayController selectedObjects] objectAtIndex: 0];ASSERTNOTNIL(currentCondition);
	PatientEntity *currentPatient=[currentCondition valueForKeyPath: @"patient"];ASSERTNOTNIL(currentPatient);
	NSMutableSet *linkSet=[currentPatient mutableSetValueForKeyPath: @"linkedFromPatients"];ASSERTNOTNIL(linkSet);
	NSArray *linkArray=[linkSet allObjects];
	if ((NSInteger) [linkArray count] < linkedFromPatientIndex + 1)
		{
		return;
		}
	
	PatientEntity *linkedPatient=[linkArray objectAtIndex: linkedFromPatientIndex];
	if (([[NSApp currentEvent] modifierFlags] & NSAlternateKeyMask) != 0)
		{
		[linkSet removeObject: linkedPatient];
		return;
		}

	NSMutableSet *conditionSet=[linkedPatient mutableSetValueForKey: @"conditions"];ASSERTNOTNIL(conditionSet);
	[browserArrayController performSelector: @selector(setSelectedObjects:) withObject: [conditionSet allObjects] afterDelay: 0.0];
	[editorWindow makeKeyAndOrderFront: self];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Predicates

- (NSString*) systemPredicateString
	{
	NSString* predicateString=@"";
	NSIndexSet *indexSet=[systemListController selectionIndexes];
	NSUInteger current = [indexSet firstIndex];
	while (current != NSNotFound)
		{
		predicateString=[predicateString stringByAppendingFormat: @"system == %ld || ", (long) current];
		current = [indexSet indexGreaterThanIndex: current];
		}

// Trim extra || or set to true.
	
	NSInteger stringLength=[predicateString length];
	if (stringLength > 0)
		predicateString=[predicateString substringToIndex: stringLength-4];
	else
		predicateString=@"TRUEPREDICATE";

// If first selection set to true.

	if ([predicateString isEqualToString: @"system == 0"])
		predicateString=@"TRUEPREDICATE";

	return (predicateString);
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSString*) pathologyPredicateString
	{
	NSString* predicateString=@"";
	NSIndexSet *indexSet=[pathologyListController selectionIndexes];
	NSUInteger current = [indexSet firstIndex];
	while (current != NSNotFound)
		{
		predicateString=[predicateString stringByAppendingFormat: @"pathology == %ld || ", (long) current];
		current = [indexSet indexGreaterThanIndex: current];
		}

// Trim extra || or set to true.
	
	NSInteger stringLength=[predicateString length];
	if (stringLength > 0)
		predicateString=[predicateString substringToIndex: stringLength-4];
	else
		predicateString=@"TRUEPREDICATE";

// If first selection set to true.

	if ([predicateString isEqualToString: @"pathology == 0"])
		predicateString=@"TRUEPREDICATE";

	return (predicateString);
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSString*) regionPredicateString
	{
	NSString* predicateString=@"";
	NSIndexSet *indexSet=[regionListController selectionIndexes];
	NSUInteger current = [indexSet firstIndex];
	while (current != NSNotFound)
		{
		predicateString=[predicateString stringByAppendingFormat: @"conditionRegion == %ld || ", (long) current];
		current = [indexSet indexGreaterThanIndex: current];
		}

// Trim extra || or set to true.
	
	NSInteger stringLength=[predicateString length];
	if (stringLength > 0)
		predicateString=[predicateString substringToIndex: stringLength-4];
	else
		predicateString=@"TRUEPREDICATE";

// If first selection set to true.

	if ([predicateString isEqualToString: @"conditionRegion == 0"])
		predicateString=@"TRUEPREDICATE";

	return (predicateString);
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Advanced Library Housekeeping

- (IBAction) anonymiseAll: (id __unused) sender
	{
	
	NSAlert *alert=[NSAlert alertWithMessageText: AnonymiseAllCasesString defaultButton: AnonymiseString alternateButton: CancelString otherButton: @""
	  informativeTextWithFormat: AnonymiseAllCasesExplanationString];
	ASSERTNOTNIL(alert);
	[alert beginSheetModalForWindow: [self windowForSheet] modalDelegate: self didEndSelector: @selector(anonymiseAllAlertDidEnd: returnCode: contextInfo:) contextInfo: nil];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) anonymiseAllAlertDidEnd: (NSAlert*) alert returnCode: (NSInteger) returnCode contextInfo: (void* __unused) contextInfo
	{
	[[alert window] close];
	if (returnCode==NSCancelButton)
		{
		return;
		}
	
	[statusField setStringValue: AnonymisingAllCasesString];
	[progressIndicator startAnimation: self];
	[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];

	NSManagedObjectContext *moc=[self managedObjectContext];ASSERTNOTNIL(moc);
	NSFetchRequest *request=[[[NSFetchRequest alloc] init] autorelease];ASSERTNOTNIL(request);
	NSEntityDescription *entity=[NSEntityDescription entityForName: @"Patient" inManagedObjectContext: moc];ASSERTNOTNIL(entity);
	[request setEntity: entity];
	[request setPredicate: nil];
	NSError *error=nil;
	NSArray *patArray=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(patArray);
	[patArray makeObjectsPerformSelector: @selector(anonymise)];

	[progressIndicator stopAnimation: self];
	[statusField setStringValue: AnonymisingAllCasesCompletedString];
	[statusField performSelector: @selector(setStringValue:) withObject: ReadyString afterDelay: 3.0];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) convertAllToJPEG: (id __unused) sender
	{
	
	NSAlert *alert=[NSAlert alertWithMessageText: ConvertAllToJPEGString defaultButton: ConvertString alternateButton: CancelString otherButton: @""
	  informativeTextWithFormat: ConvertAllToJPEGExplanationString, JPEGCompressionFactor];
	ASSERTNOTNIL(alert);
	[alert beginSheetModalForWindow: [self windowForSheet] modalDelegate: self didEndSelector: @selector(convertAllToJPEGAlertDidEnd: returnCode: contextInfo:) contextInfo: nil];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) convertAllToJPEGAlertDidEnd: (NSAlert*) alert returnCode: (NSInteger) returnCode contextInfo: (NSArray* __unused) selectionArray
	{
	
	[[alert window] close];
	if (returnCode==NSCancelButton)
		return;
	
	[statusField setStringValue: ConvertingAllToJPEGString];
	[progressIndicator startAnimation: self];
	
	NSManagedObjectContext *moc=[self managedObjectContext];ASSERTNOTNIL(moc);
	NSFetchRequest *request=[[[NSFetchRequest alloc] init] autorelease];ASSERTNOTNIL(request);
	NSEntityDescription *entity=[NSEntityDescription entityForName: @"Patient" inManagedObjectContext: moc];ASSERTNOTNIL(entity);
	[request setEntity: entity];
	[request setPredicate: nil];
	NSError *error=nil;
	NSArray *patArray=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(patArray);
	if (patArray == nil)
		{
		NSLog(@"Error: %@", [error description]);
		}
	
	NSUInteger index;
	for (index=0; index<[patArray count]; index++)
		{
		NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);

		PatientEntity *aPatient=[patArray objectAtIndex: index];ASSERTNOTNIL(aPatient);
		NSString *statusString=[NSString stringWithFormat: ConvertingPatientToJPEGString,
		  index+1, [patArray count], [aPatient valueForKey: @"identifier"]];
		[statusField setStringValue: statusString];
		
		NSSet *conditions=[aPatient mutableSetValueForKey: @"conditions"];ASSERTNOTNIL(conditions);
		ConditionEntity *aCondition;
		NSEnumerator *conditionEnumerator=[conditions objectEnumerator];ASSERTNOTNIL(conditionEnumerator);
		while ((aCondition=[conditionEnumerator nextObject]))
			{
			NSSet *studies=[aCondition valueForKeyPath: @"studies"];
			StudyEntity *aStudy;
			NSEnumerator *studyEnumerator = [studies objectEnumerator];ASSERTNOTNIL(studyEnumerator);
			while ((aStudy=[studyEnumerator nextObject]))
				{				
				NSSet *images=[aStudy valueForKeyPath: @"images"];
				ImageEntity *anImage;
				NSEnumerator *imageEnumerator = [images objectEnumerator];ASSERTNOTNIL(imageEnumerator);
				while ((anImage=[imageEnumerator nextObject]))
					{
					[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];

					[anImage reStoreImageAsJPEG];
					}
				}
			}
		[pool release];
		}
	
	[progressIndicator stopAnimation: self];
	[statusField setStringValue: ConvertingToJPEGCompletedString];
	[statusField performSelector: @selector(setStringValue:) withObject: ReadyString afterDelay: 3.0];
	}

	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - Utility

- (id) imageView
	{
	return imageView;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) incrementPreviewImage
	{
	if ([[browserArrayController selectedObjects] count] != 1)
		return;

// Force update of the preview image.
	
	ConditionEntity *condition=[[browserArrayController selectedObjects] objectAtIndex: 0];ASSERTNOTNIL(condition);
	[condition incrementPreviewImageIndex];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) decrementPreviewImage
	{
	if ([[browserArrayController selectedObjects] count] != 1)
		return;

// Force update of the preview image.
	
	ConditionEntity *condition=[[browserArrayController selectedObjects] objectAtIndex: 0];ASSERTNOTNIL(condition);
	[condition decrementPreviewImageIndex];
	}

	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) objectsDidChange: (NSNotification* __unused) notification
	{
	
// Workaround for subsequently losing focus with editor's text fields (patientID, dateOfBirth).
	
	if ([editorWindow isMainWindow])
		{
		[[self managedObjectContext] commitEditing];
		}
		
// Data in the MOC has changed, update various table views to reflect these changes.
	
	[browserArrayController rearrangeObjects];
	[conditionArrayController rearrangeObjects];
	[studyArrayController rearrangeObjects];
	[albumArrayController rearrangeObjects];
	}
	
	
@end


