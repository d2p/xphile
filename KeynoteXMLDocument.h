/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "ImageEntity.h"


@interface KeynoteXMLDocument : NSXMLDocument

- (void) addDefaultThemeTo: (NSXMLElement*) root;
- (void) addStyleElementsTo: (NSXMLElement*) element;
- (void) addTitleTo: (NSXMLElement*) element withVisibility: (BOOL) visible;
- (void) addBodyTo: (NSXMLElement*) element withVisibility: (BOOL) visible;
- (void) addPageNumberTo: (NSXMLElement*) element;
- (void) addBulletsTo: (NSXMLElement*) element;
- (void) addBackgroundFillTo: (NSXMLElement*) element;
- (void) addTransitionStyleTo: (NSXMLElement*) element;

- (void) addTitleBulletTo: (NSXMLElement*) element withString: (NSString*) bulletString;
- (void) addBodyBulletTo: (NSXMLElement*) element withString: (NSString*) bulletString;
- (void) addNewConditionSlideToList: (NSXMLElement*) listElement forCondition: (ConditionEntity*) aCondition;
- (void) addNewStudySlideToList: (NSXMLElement*) listElement forStudy: (StudyEntity*) aStudy;
- (void) addNewImageSlideToList: (NSXMLElement*) listElement withName: (NSString*) fileName nativeSize: (NSSize) imageSize;
- (void) addNewMovieSlideToList: (NSXMLElement*) listElement withName: (NSString*) fileName nativeSize: (NSSize) movieSize;

- (NSString*) affineStringForMediaSize: (NSSize) itsSize;

@end
