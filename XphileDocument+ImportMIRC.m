/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "ImageEntity.h"
#import "XphileDocument+ImportMIRC.h"


@implementation XphileDocument (XphileDocument_ImportMIRC)

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) doImportFromMIRC: (id __unused) sender
	{

// Choose file.

	NSOpenPanel *openPanel=[NSOpenPanel openPanel];ASSERTNOTNIL(openPanel);
	[openPanel setTitle: ChooseMIRCArchiveString];
	[openPanel setCanChooseDirectories: NO];
	[openPanel setAllowedFileTypes: [NSArray arrayWithObjects: @"zip", @"xml", nil]];
	if ([openPanel runModal]==NSCancelButton)
		return;

	[self importMIRCFileAtPath: [[openPanel URL] path]];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) importMIRCFileAtPath: (NSString*) fileName
	{
	NSError								*error=nil;


// If zip expand to temporary archive, otherwise just open the xml file.

	NSString *tempPathName=fileName;ASSERTNOTNIL(tempPathName);
	if ([[tempPathName pathExtension] isEqualToString: @"zip"])
		{

// Delete temporary folder and expand zip archive to it.

		tempPathName=[NSTemporaryDirectory() stringByAppendingPathComponent: @"Xphile MIRC Archive/"];ASSERTNOTNIL(tempPathName);
		if ([[NSFileManager defaultManager] fileExistsAtPath: tempPathName])
			{
			if (![[NSFileManager defaultManager] removeItemAtPath: tempPathName error: &error])
				{
				NSLog(@"Error deleting: %@", [error description]);
				}
			}
		[self unzipTask: [NSArray arrayWithObjects: @"-qq", @"-d", tempPathName, fileName, nil]];

// Find the xml file (usually MIRCDocument.xml).

		NSEnumerator *enumerator=[[NSFileManager defaultManager] enumeratorAtPath: tempPathName];ASSERTNOTNIL(enumerator);
		NSString *file;
		while ((file = [enumerator nextObject]))
			{
			if ([[file pathExtension] isEqualToString: @"xml"])
				{
				tempPathName=[tempPathName stringByAppendingPathComponent: file];ASSERTNOTNIL(tempPathName);
				}
			}
		}
		
// Open xml document.

	NSURL *url=[NSURL fileURLWithPath: tempPathName];ASSERTNOTNIL(url);
	NSXMLDocument *doc=[[[NSXMLDocument alloc] initWithContentsOfURL: url options: NSXMLDocumentTidyXML error: &error] autorelease];
	if (doc == nil)
		{
		NSLog(@"Unable to open MIRC XML document: %@", [error description]);
		}
	ASSERTNOTNIL(doc);
		
// Add new patient, condition and study.

	PatientEntity *patient=[NSEntityDescription insertNewObjectForEntityForName: @"Patient" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(patient);
	[patient setValue: [NSString stringWithFormat: @"%ld", (long) [self randomInt: 99999999]] forKeyPath: @"identifier"];
	ConditionEntity *condition=[NSEntityDescription insertNewObjectForEntityForName: @"Condition" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(condition);
	[condition setValue: patient forKeyPath: @"patient"];
	StudyEntity *study=[NSEntityDescription insertNewObjectForEntityForName: @"Study" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(study);
	[study setValue: condition forKeyPath: @"condition"];

// Set dateOfOnset and dateOfStudy to current date as this info is not preserved in MIRC.

	[condition setValue: [NSDate date] forKeyPath: @"dateOfOnset"];
	[study setValue: [NSDate date] forKeyPath: @"dateOfStudy"];

// Walk through XML document.

	NSXMLNode *aNode=[doc rootElement];ASSERTNOTNIL(aNode);
	while ((aNode=[aNode nextNode]))
		{
		NSString *nodeName=[aNode name];

// Gender.

		if ([nodeName isEqualToString: @"pt-sex"])
			{
			if ([[aNode stringValue] isEqualToString: @"male"])
				[patient setValue: [NSNumber numberWithInteger:1] forKey: @"gender"];
			if ([[aNode stringValue] isEqualToString: @"female"])
				[patient setValue: [NSNumber numberWithInteger:2] forKey: @"gender"];
			}

// Set date of birth as offset from now.
			
		if ([nodeName isEqualToString: @"years"])
			{
			NSInteger age=[[aNode stringValue] integerValue];
			NSDate *dob=[NSDate date];
			dob=[dob dateByAddingTimeInterval: -(age*365*24*60*50)];
			[patient setValue: dob forKeyPath: @"dateOfBirth"];
			}
			
		if ([nodeName isEqualToString: @"months"])
			{
			NSInteger age=[[aNode stringValue] integerValue];
			NSDate *dob=[NSDate date];
			dob=[dob dateByAddingTimeInterval: -(age*12*24*60*50)];
			[patient setValue: dob forKeyPath: @"dateOfBirth"];
			}
			
		if ([nodeName isEqualToString: @"days"])
			{
			NSInteger age=[[aNode stringValue] integerValue];
			NSDate *dob=[NSDate date];
			dob=[dob dateByAddingTimeInterval: -(age*24*60*50)];
			[patient setValue: dob forKeyPath: @"dateOfBirth"];
			}

// System, pathology, region.

		if ([nodeName isEqualToString: @"organ-system"])
			{
			NSArray *systemArray=[[NSUserDefaults standardUserDefaults] objectForKey: SystemArrayKey];ASSERTNOTNIL(systemArray);
			NSInteger index=[systemArray indexOfObject: [aNode stringValue]];
			if (index!=NSNotFound)
				[condition setValue: [NSNumber numberWithInteger:index] forKey: @"system"];
			}

		if ([nodeName isEqualToString: @"pathology"])
			{
			NSArray *pathologyArray=[[NSUserDefaults standardUserDefaults] objectForKey: PathologyArrayKey];ASSERTNOTNIL(pathologyArray);
			NSInteger index=[pathologyArray indexOfObject: [aNode stringValue]];
			if (index!=NSNotFound)
				[condition setValue: [NSNumber numberWithInteger:index] forKey: @"pathology"];
			}

		if ([nodeName isEqualToString: @"anatomy"])
			{
			NSArray *regionArray=[[NSUserDefaults standardUserDefaults] objectForKey: RegionArrayKey];ASSERTNOTNIL(regionArray);
			NSInteger index=[regionArray indexOfObject: [aNode stringValue]];
			if (index!=NSNotFound)
				{
				[condition setValue: [NSNumber numberWithInteger:index] forKey: @"conditionRegion"];
				[study setValue: [NSNumber numberWithInteger:index] forKey: @"studyRegion"];
				}
			}

// Condition description.

		if ([nodeName isEqualToString: @"section"])
			{
			NSArray *attributesArray=[(NSXMLElement*) aNode attributes];ASSERTNOTNIL(attributesArray);
			NSUInteger i;
			for (i=0; i<[attributesArray count]; i++)
				{
				NSXMLNode *attributeNode=[attributesArray objectAtIndex: i];ASSERTNOTNIL(attributeNode);
				if ([[attributeNode stringValue] isEqualToString: @"Discussion"])
					{
					[condition setValue: [aNode stringValue] forKey: @"conditionDescription"];
					}
				}
			}

// Modality.

		if ([nodeName isEqualToString: @"modality"])
			{
			NSArray *modalityArray=[[NSUserDefaults standardUserDefaults] objectForKey: ModalityArrayKey];ASSERTNOTNIL(modalityArray);
			NSInteger index=[modalityArray indexOfObject: [aNode stringValue]];
			if (index!=NSNotFound)
				[study setValue: [NSNumber numberWithInteger:index] forKey: @"modality"];
			}

// Images.

		if ([nodeName isEqualToString: @"image"])
			{
			NSArray *attributesArray=[(NSXMLElement*) aNode attributes];ASSERTNOTNIL(attributesArray);
			NSUInteger i;
			for (i=0; i<[attributesArray count]; i++)
				{
				NSXMLNode *attributeNode=[attributesArray objectAtIndex: i];ASSERTNOTNIL(attributeNode);
				if ([[attributeNode name] isEqualToString: @"src"])		// also href for some MIRC documents??
					{
					NSString *path=[tempPathName stringByDeletingLastPathComponent];ASSERTNOTNIL(path);
					path=[path stringByAppendingPathComponent: [attributeNode stringValue]];
					if ([[NSFileManager defaultManager] fileExistsAtPath: path])
						{
						ImageEntity *image=[NSEntityDescription insertNewObjectForEntityForName: @"Image" inManagedObjectContext: [self managedObjectContext]];ASSERTNOTNIL(image);
						[image setValue: study forKeyPath: @"study"];
						[image setMediaFromPath: path];
						}
					}
				}
			}
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) unzipTask: (NSArray*) args
	{
	NSTask						*zipTask;
	BOOL						success			=	YES;


	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
	zipTask=[[NSTask alloc] init];
	[zipTask setLaunchPath: @"/usr/bin/unzip"];
	[zipTask setArguments: args];
	
	[zipTask launch];
	while ([zipTask isRunning]) [NSThread sleepForTimeInterval: 0.01];
	[zipTask interrupt];

	if ([zipTask terminationStatus] != 0L)
		success=NO;
		
	[zipTask release];
	[pool release];
	
	return (success);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSInteger) randomInt: (NSInteger) i
	{
	return (rand() % i) + 1;
	}




@end
