/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/




#import "OsiriXAPI/browserController.h"
#import "OsiriXAPI/DicomStudy.h"

#import "ExportToXphileFromDatabase.h"



@implementation ExportToXphileFromDatabase


- (long) filterImage: (NSString*) menuName
	{
	if (! [self checkIfXphileIsRunning])
		{
		return -1;
		}
	
	BrowserController *browser=[BrowserController currentBrowser];ASSERTNOTNIL(browser);
	NSArray *browserSelectionArray=[browser databaseSelection];
	if (browserSelectionArray == nil)
		{
		return -1;
		}
	
	for (DicomStudy* study in browserSelectionArray)
		{
		NSMutableDictionary *dict=[NSMutableDictionary dictionary];ASSERTNOTNIL(dict);
		NSArray *validKeysArray=@[@"patientID", @"patientSex", @"dateOfBirth", @"date", @"modality", @"comment"];ASSERTNOTNIL(validKeysArray);
		NSUInteger index;
		for (index=0; index<[validKeysArray count]; index++)
			{
			NSString *validKey=[validKeysArray objectAtIndex: index];ASSERTNOTNIL(validKey);
			if ([study valueForKey: validKey] != nil)
				{
				NSString *idValueStr=[study valueForKey: validKey];ASSERTNOTNIL(idValueStr);
				[dict setObject: idValueStr forKey: validKey];
				}
			}

		[[NSDistributedNotificationCenter defaultCenter] postNotificationName: @"ExportOsiriXToXphile" object: XphileBundleID userInfo: dict];
		}

	return 0;
	}

@end
