/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/




#import "XphilePluginFilter.h"


@implementation XphilePluginFilter


- (void) initPlugin
	{
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) checkIfXphileIsRunning
	{
	NSArray *runningApps=[[NSWorkspace sharedWorkspace] runningApplications];
	for (NSRunningApplication *aRunningApp in runningApps)
		{
		NSString *aRunningAppsBundleID=aRunningApp.bundleIdentifier;
		if ([aRunningAppsBundleID isEqualToString: XphileBundleID])
			{
			return YES;
			}
		}
	
	NSUInteger result=[[NSAlert alertWithMessageText: @"Xphile is not running" defaultButton: @"Launch" alternateButton: @"Cancel" otherButton: nil
	  informativeTextWithFormat: @"Please launch Xphile and try again."] runModal];

	if (result == NSAlertDefaultReturn)
		{
		[[NSWorkspace sharedWorkspace] launchAppWithBundleIdentifier: XphileBundleID options: NSWorkspaceLaunchDefault
		  additionalEventParamDescriptor: nil launchIdentifier: nil];
		}

	return NO;
	}


@end
