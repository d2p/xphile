/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/



#import "OsiriXAPI/browserController.h"
#import "OsiriXAPI/DicomStudy.h"
#import "OsiriXAPI/DicomSeries.h"
#import "OsiriXAPI/DicomImage.h"
#import "OsiriXAPI/DicomFile.h"

#import "ExportToXphileFromStudy.h"



@implementation ExportToXphileFromStudy


- (long) filterImage: (NSString*) menuName
	{
	if (! [self checkIfXphileIsRunning])
		{
		return -1;
		}
	
	DCMView* dcmView=[viewerController imageView];ASSERTNOTNIL(dcmView);
	NSArray* fileArray=[viewerController fileList];ASSERTNOTNIL(fileArray);
	DicomFile* dcmFile=[fileArray objectAtIndex: [dcmView curImage]];ASSERTNOTNIL(dcmFile);

	DicomStudy* dicomStudy=[dcmFile valueForKeyPath: @"series.study"];ASSERTNOTNIL(dicomStudy);

// Loop through all series and add key images as NSImages.

	NSMutableArray* imageArray=[[NSMutableArray array] retain];ASSERTNOTNIL(imageArray);
	for (DicomSeries* aSeries in dicomStudy.series)
		{
		[[BrowserController currentBrowser] loadSeries: aSeries :viewerController : YES keyImagesOnly: YES];
		
		NSSet* keyImagesInSeries=aSeries.keyImages;
		for (DicomImage* keyImage in keyImagesInSeries)
			{
			[viewerController setImage: keyImage];
			[[NSRunLoop currentRunLoop] runUntilDate: [[NSDate date] dateByAddingTimeInterval: 0.1]];
			
			NSImage* im=[dcmView nsimage];ASSERTNOTNIL(im);
			NSData *imageData=[im TIFFRepresentationUsingCompression: NSTIFFCompressionJPEG factor: 1.0];ASSERTNOTNIL(imageData);
			[imageArray addObject: imageData];
			}
		}

// Return to random (last?) series.

	if (dicomStudy.series.count > 0)
		{
		DicomSeries *firstSeries=[[dicomStudy.series allObjects] firstObject];ASSERTNOTNIL(firstSeries);
		[[BrowserController currentBrowser] loadSeries: firstSeries : viewerController : YES keyImagesOnly: NO];
		}

// Create dictionary of NSImages to send to Xphile.

	NSMutableDictionary *dict=[[NSMutableDictionary dictionary] retain];ASSERTNOTNIL(dict);
	[dict setObject: imageArray forKey: @"images"];

	NSArray *validKeysArray=@[@"patientID", @"patientSex", @"dateOfBirth", @"date", @"modality", @"comment"];ASSERTNOTNIL(validKeysArray);
	NSUInteger index;
	for (index=0; index<[validKeysArray count]; index++)
		{
		NSString *validKey=[validKeysArray objectAtIndex: index];ASSERTNOTNIL(validKey);
		if ([dicomStudy valueForKey: validKey] != nil)
			{
			NSString *idValueStr=[dicomStudy valueForKey: validKey];ASSERTNOTNIL(idValueStr);
			[dict setObject: idValueStr forKey: validKey];
			}
		}

	[[NSDistributedNotificationCenter defaultCenter] postNotificationName: @"ExportOsiriXToXphile" object: XphileBundleID userInfo: dict];

// Release retained objects ([NSRunLoop runUntilDate:] may release).

	[imageArray release];
	[dict release];

	return 0;
	}


@end
