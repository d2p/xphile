/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "GreyBox.h"


@implementation GreyBox

- (void) drawRect: (NSRect) rect
	{
	if (rect.size.height > rect.size.width)
		{
		CGFloat currentWidth=rect.size.width;
		rect.size.width=1.0;
		rect.origin.x+=(currentWidth-1.0)/2;
		}
		
	[[NSColor lightGrayColor] set];
	NSFrameRect(rect);
	}

@end
