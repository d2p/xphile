/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "ApplicationController.h"
#import "MIRCXMLDocument.h"
#import "NSString+Additions.h"


@implementation MIRCXMLDocument


- (void) addStandardInformationTo: (NSXMLElement*) root
	{
	NSXMLElement *docTypeElement=[NSXMLElement elementWithName: @"document-type"];ASSERTNOTNIL(docTypeElement);
	[docTypeElement setObjectValue: @"radiologic teaching file"];
	[root addChild: docTypeElement];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) addTitleInformationTo: (NSXMLElement*) root withTitle: (NSString*) title
	{
		
// Include first sentence, tab or up to 50 characters.
	
	NSXMLElement *titleElement=[NSXMLElement elementWithName: @"title"];ASSERTNOTNIL(titleElement);
	[root addChild: titleElement];

	NSArray *paragraphArray=[title componentsSeparatedByString: @"\n"];ASSERTNOTNIL(paragraphArray);
	NSArray *sentenceArray=[title componentsSeparatedByString: @". "];ASSERTNOTNIL(sentenceArray);
	NSArray *wordArray=[title componentsSeparatedByString: @" "];ASSERTNOTNIL(wordArray);
	NSString *abbreviatedTitleStr=nil;

	if ([paragraphArray count] > 0)						// Paragraph by return.
		{
		abbreviatedTitleStr=[paragraphArray objectAtIndex: 0];ASSERTNOTNIL(abbreviatedTitleStr);
		}
	else if ([sentenceArray count] > 0)					// Sentence by .
		{
		abbreviatedTitleStr=[sentenceArray objectAtIndex: 0];ASSERTNOTNIL(abbreviatedTitleStr);
		}
	else if ([wordArray count] > 4)						// 5 Words.
		{
		wordArray=[wordArray subarrayWithRange: NSMakeRange(0,4)];ASSERTNOTNIL(wordArray);
		abbreviatedTitleStr=[wordArray componentsJoinedByString: @" "];ASSERTNOTNIL(abbreviatedTitleStr);
		}
	else if ([wordArray count] > 0)						// n Words.
		{
		abbreviatedTitleStr=[wordArray componentsJoinedByString: @" "];ASSERTNOTNIL(abbreviatedTitleStr);
		}
	else												// Failsafe.
		{
		abbreviatedTitleStr=title;
		}

	[titleElement setObjectValue: [abbreviatedTitleStr mircSafeString]];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) addAuthorInformationTo: (NSXMLElement*) root
	{
	NSXMLElement *authorElement=[NSXMLElement elementWithName: @"author"];ASSERTNOTNIL(authorElement);
	[root addChild: authorElement];
	
	NSXMLElement *nameElement=[NSXMLElement elementWithName: @"name"];ASSERTNOTNIL(nameElement);
	[nameElement setObjectValue: [[[NSUserDefaults standardUserDefaults] valueForKey: MIRCAuthorKey] mircSafeString]];
	[authorElement addChild: nameElement];
	
	NSXMLElement *affiliationElement=[NSXMLElement elementWithName: @"affiliation"];ASSERTNOTNIL(affiliationElement);
	[affiliationElement setObjectValue: [[[NSUserDefaults standardUserDefaults] valueForKey: MIRCAffiliationKey] mircSafeString]];
	[authorElement addChild: affiliationElement];
	
	NSXMLElement *contactElement=[NSXMLElement elementWithName: @"contact"];ASSERTNOTNIL(contactElement);
	[contactElement setObjectValue: [[[NSUserDefaults standardUserDefaults] valueForKey: MIRCContactKey] mircSafeString]];
	[authorElement addChild: contactElement];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) addAbstractTo: (NSXMLElement*) root withDetail: (NSString*) abstractDetail
	{

// First try splitting by paragraph, tab and then by sentence.

	NSString								*abstractString;
	
	
	NSXMLElement *abstractElement=[NSXMLElement elementWithName: @"abstract"];ASSERTNOTNIL(abstractElement);
	[root addChild: abstractElement];

	NSArray *paragraphArray=[abstractDetail componentsSeparatedByString: @"\n"];ASSERTNOTNIL(paragraphArray);
	NSArray *tabArray=[abstractDetail componentsSeparatedByString: @"\t"];ASSERTNOTNIL(tabArray);
	NSArray *sentenceArray=[abstractDetail componentsSeparatedByString: @"."];ASSERTNOTNIL(sentenceArray);
	
	if ([paragraphArray count] > 1)						// First paragraph.
		{
		abstractString=[paragraphArray objectAtIndex: 0];ASSERTNOTNIL(abstractString);
		}
	else if ([tabArray count] > 1)						// Then tab.
		{
		abstractString=[tabArray objectAtIndex: 0];ASSERTNOTNIL(abstractString);
		}
	else if ([sentenceArray count] > 1)					// Then sentence.
		{
		abstractString=[sentenceArray objectAtIndex: 0];ASSERTNOTNIL(abstractString);
		}
	else
		abstractString=abstractDetail;
	
	NSXMLElement *paragraphElement=[NSXMLElement elementWithName: @"p"];ASSERTNOTNIL(paragraphElement);
	[paragraphElement setObjectValue: [abstractString mircSafeString]];
	[abstractElement addChild: paragraphElement];
	}

	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) addHistoryTo: (NSXMLElement*) root withCondition: (ConditionEntity*) condition
	{
	NSXMLElement *historyElement=[NSXMLElement elementWithName: @"section"];ASSERTNOTNIL(historyElement);
	[historyElement setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"History" forKey: @"heading"]];
	[root addChild: historyElement];

	NSXMLElement *patientElement=[NSXMLElement elementWithName: @"patient"];ASSERTNOTNIL(patientElement);
	[historyElement addChild: patientElement];

// Age.

	NSXMLElement *ageElement=[NSXMLElement elementWithName: @"pt-age"];ASSERTNOTNIL(ageElement);
	[patientElement addChild: ageElement];

	NSInteger daysOld=[[condition valueForKeyPath: @"daysOldAtOnset"] integerValue];
	
	if (daysOld < 14)
		{
		NSXMLElement *daysElement=[NSXMLElement elementWithName: @"days"];ASSERTNOTNIL(daysElement);
		[ageElement addChild: daysElement];
		[daysElement setObjectValue: [NSString stringWithFormat: @"%ld", (long) daysOld]];
		}
	else if (daysOld < (6*7))
		{
		NSXMLElement *weeksElement=[NSXMLElement elementWithName: @"weeks"];ASSERTNOTNIL(weeksElement);
		[ageElement addChild: weeksElement];
		[weeksElement setObjectValue: [NSString stringWithFormat: @"%ld", (long) daysOld/7]];
		}
	else if (daysOld < (24*31))
		{
		NSXMLElement *monthsElement=[NSXMLElement elementWithName: @"months"];ASSERTNOTNIL(monthsElement);
		[ageElement addChild: monthsElement];
		[monthsElement setObjectValue: [NSString stringWithFormat: @"%ld", (long) daysOld/31]];
		}
	else
		{
		NSXMLElement *yearsElement=[NSXMLElement elementWithName: @"years"];ASSERTNOTNIL(yearsElement);
		[ageElement addChild: yearsElement];
		[yearsElement setObjectValue: [NSString stringWithFormat: @"%ld", (long) daysOld/365]];
		}

// Sex.

	NSXMLElement *sexElement=[NSXMLElement elementWithName: @"pt-sex"];ASSERTNOTNIL(sexElement);
	if ([[condition valueForKeyPath: @"patient.gender"] integerValue]==1)
		{
		[sexElement setObjectValue: @"male"];
		[patientElement addChild: sexElement];
		}
	if ([[condition valueForKeyPath: @"patient.gender"] integerValue]==2)
		{
		[sexElement setObjectValue: @"female"];
		[patientElement addChild: sexElement];
		}
	}

	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) addDiscussionTo: (NSXMLElement*) root withDetail: (NSString*) discussionDetail
	{
	NSXMLElement *discussionElement=[NSXMLElement elementWithName: @"section"];ASSERTNOTNIL(discussionElement);
	[discussionElement setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"Discussion" forKey: @"heading"]];
	[root addChild: discussionElement];

	NSArray *paragraphArray=[discussionDetail componentsSeparatedByString: @"\n"];ASSERTNOTNIL(paragraphArray);
	NSUInteger index;
	
	for (index=0; index<[paragraphArray count]; index++)
		{
		NSXMLElement *paragraphElement=[NSXMLElement elementWithName: @"p"];ASSERTNOTNIL(paragraphElement);
		[paragraphElement setObjectValue: [[paragraphArray objectAtIndex: index] mircSafeString]];
		[discussionElement addChild: paragraphElement];
		}
	}

	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) addFindingsTo: (NSXMLElement*) root withDetail: (NSString*) findingsDetail
	{
	NSXMLElement *findingsElement=[NSXMLElement elementWithName: @"section"];ASSERTNOTNIL(findingsElement);
	[findingsElement setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"Findings" forKey: @"heading"]];
	[root addChild: findingsElement];

	NSArray *paragraphArray=[findingsDetail componentsSeparatedByString: @"\n"];ASSERTNOTNIL(paragraphArray);
	NSUInteger index;
	
	for (index=0; index<[paragraphArray count]; index++)
		{
		NSXMLElement *paragraphElement=[NSXMLElement elementWithName: @"p"];ASSERTNOTNIL(paragraphElement);
		[paragraphElement setObjectValue: [[paragraphArray objectAtIndex: index] mircSafeString]];
		[findingsElement addChild: paragraphElement];
		}
	}

	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) addNotesTo: (NSXMLElement*) root withDetail: (NSString*) notesDetail
	{
	NSXMLElement *notesElement=[NSXMLElement elementWithName: @"section"];ASSERTNOTNIL(notesElement);
	[notesElement setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"Notes" forKey: @"heading"]];
	[root addChild: notesElement];

	NSXMLElement *paragraphElement=[NSXMLElement elementWithName: @"p"];ASSERTNOTNIL(paragraphElement);
	[paragraphElement setObjectValue: [notesDetail mircSafeString]];
	[notesElement addChild: paragraphElement];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) addSystemTo: (NSXMLElement*) root withDetail: (NSString*) systemString
	{
	NSXMLElement *systemElement=[NSXMLElement elementWithName: @"organ-system"];ASSERTNOTNIL(systemElement);
	[systemElement setObjectValue: [systemString mircSafeString]];
	[root addChild: systemElement];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) addPathologyTo: (NSXMLElement*) root withDetail: (NSString*) pathologyString
	{
	NSXMLElement *pathologyElement=[NSXMLElement elementWithName: @"pathology"];ASSERTNOTNIL(pathologyElement);
	[pathologyElement setObjectValue: [pathologyString mircSafeString]];
	[root addChild: pathologyElement];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) addAnatomyTo: (NSXMLElement*) root withDetail: (NSString*) anatomyString
	{
	NSXMLElement *anatomyElement=[NSXMLElement elementWithName: @"anatomy"];ASSERTNOTNIL(anatomyElement);
	[anatomyElement setObjectValue: [anatomyString mircSafeString]];
	[root addChild: anatomyElement];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) addCategoryTo: (NSXMLElement*) root withCondition: (ConditionEntity*) condition
	{
	NSXMLElement *categoryElement=[NSXMLElement elementWithName: @"category"];ASSERTNOTNIL(categoryElement);
	[root addChild: categoryElement];

	NSArray *systemArray=[[NSUserDefaults standardUserDefaults] valueForKey: SystemArrayKey];ASSERTNOTNIL(systemArray);
	NSUInteger conditionIndex=[[condition valueForKeyPath: @"system"] integerValue];
	NSArray *regionArray=[[NSUserDefaults standardUserDefaults] valueForKey: RegionArrayKey];ASSERTNOTNIL(regionArray);
	NSUInteger regionIndex=[[condition valueForKeyPath: @"conditionRegion"] integerValue];
	NSInteger daysOld=[[condition valueForKeyPath: @"daysOldAtOnset"] integerValue];
	
	if (conditionIndex==[systemArray indexOfObject: MusculoskeletalString])
		[categoryElement setObjectValue: @"Musculoskeletal"];
	else if (conditionIndex==[systemArray indexOfObject: RespiratoryString])
		[categoryElement setObjectValue: @"Pulmonary"];
	else if (conditionIndex==[systemArray indexOfObject: CardiacString])
		[categoryElement setObjectValue: @"Cardiovascular"];
	else if (conditionIndex==[systemArray indexOfObject: VascularString] && regionIndex==[regionArray indexOfObject: ChestString])
		[categoryElement setObjectValue: @"Cardiovascular"];
	else if (conditionIndex==[systemArray indexOfObject: GastrointestinalString])
		[categoryElement setObjectValue: @"Gastrointestinal"];
	else if (conditionIndex==[systemArray indexOfObject: HepatobiliaryString])
		[categoryElement setObjectValue: @"Gastrointestinal"];
	else if (conditionIndex==[systemArray indexOfObject: GenitalTractFemaleString])
		[categoryElement setObjectValue: @"Genitourinary"];
	else if (conditionIndex==[systemArray indexOfObject: GenitalTractMaleString])
		[categoryElement setObjectValue: @"Genitourinary"];
	else if (conditionIndex==[systemArray indexOfObject: UrinaryTractString])
		[categoryElement setObjectValue: @"Genitourinary"];
	else if (conditionIndex==[systemArray indexOfObject: NeurologicalString])
		[categoryElement setObjectValue: @"Neuro"];
	else if (conditionIndex==[systemArray indexOfObject: HeadAndNeckString])
		[categoryElement setObjectValue: @"Neuro"];
	else if (conditionIndex==[systemArray indexOfObject: VascularString])
		[categoryElement setObjectValue: @"Vascular and Interventional"];
	else if (conditionIndex==[systemArray indexOfObject: BreastString])
		[categoryElement setObjectValue: @"Breast"];
	else if (daysOld < (15*365))
		[categoryElement setObjectValue: @"Pediatric"];
	else
		[categoryElement setObjectValue: @"Unknown"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) addImageTo: (NSXMLElement*) imageRoot withName: (NSString*) imageName withModality: (NSString*) modalityString
	{
	NSXMLElement *imageElement=[NSXMLElement elementWithName: @"image"];ASSERTNOTNIL(imageElement);
	[imageElement setAttributesAsDictionary: [NSDictionary dictionaryWithObjects:
	  [NSArray arrayWithObjects: imageName, nil]
	  forKeys: [NSArray arrayWithObjects: @"src", nil]]];
	[imageRoot addChild: imageElement];
	
	NSXMLElement *modalityElement=[NSXMLElement elementWithName: @"modality"];ASSERTNOTNIL(modalityElement);
	[modalityElement setObjectValue: modalityString];
	[imageElement addChild: modalityElement];
	}

	
@end

