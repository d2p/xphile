/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "OsirixXMLDocument.h"


@implementation OsirixXMLDocument

- (void) addMethodName: (NSString*) methodName toElement: (NSXMLElement*) element
	{
	NSXMLElement *methodNameElement=[NSXMLElement elementWithName: @"methodName"];ASSERTNOTNIL(methodNameElement);
	[methodNameElement setStringValue: methodName];
	[element addChild: methodNameElement];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) addParameterDictionary: (NSDictionary*) paramDict toElement: (NSXMLElement*) element
	{
	NSXMLElement *paramElement=[NSXMLElement elementWithName: @"param"];ASSERTNOTNIL(paramElement);
	[element addChild: paramElement];
	
	NSXMLElement *valueElement=[NSXMLElement elementWithName: @"value"];ASSERTNOTNIL(valueElement);
	[paramElement addChild: valueElement];
	
	NSXMLElement *structElement=[NSXMLElement elementWithName: @"struct"];ASSERTNOTNIL(structElement);
	[valueElement addChild: structElement];
	
	NSEnumerator *keyEnumerator=[paramDict keyEnumerator];ASSERTNOTNIL(keyEnumerator);
	NSString *param;
	while ((param=[keyEnumerator nextObject]))
		{
		NSString *value=[paramDict valueForKey: param];ASSERTNOTNIL(value);
		[self addParameter: param andValue: value toElement: structElement];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) addParameter: (NSString*) parameter andValue: (NSString*) value toElement: (NSXMLElement*) element
	{
	NSXMLElement *memberElement=[NSXMLElement elementWithName: @"member"];ASSERTNOTNIL(memberElement);
	[element addChild: memberElement];
	
	NSXMLElement *nameElement=[NSXMLElement elementWithName: @"name"];ASSERTNOTNIL(nameElement);
	[nameElement setStringValue: parameter];
	[memberElement addChild: nameElement];
	
	NSXMLElement *valueElement=[NSXMLElement elementWithName: @"value"];ASSERTNOTNIL(valueElement);
	[memberElement addChild: valueElement];

	NSXMLElement *stringElement=[NSXMLElement elementWithName: @"string"];ASSERTNOTNIL(stringElement);
	[stringElement setStringValue: value];
	[valueElement addChild: stringElement];
	}
	

@end
