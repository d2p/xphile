/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "AboutController.h"
#import "NSDate+Additions.h"
#import "VersionManager.h"


@implementation AboutController


- (id) init
	{
	self=[super initWithWindowNibName: @"About"];
	if (self!=nil)
		{
		}
	return self;
	}


//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

- (void) dealloc
	{

// Won't usually be called.

	[super dealloc];
	}
	
	
//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

- (void) windowDidLoad
	{
	[self setBackgroundGradient];
	
	NSString *str=[NSString stringWithFormat: @"%@ %@",  [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleName"],
	  [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"]];
	ASSERTNOTNIL(str);
	[nameField setStringValue: str];

	NSDateFormatter *dateFormatter=[[[NSDateFormatter alloc] init] autorelease];ASSERTNOTNIL(dateFormatter);
	[dateFormatter setDateStyle: NSDateFormatterMediumStyle];
	NSString *buildDateString=[dateFormatter stringFromDate: [NSDate compiledDate]];ASSERTNOTNIL(buildDateString);
	str=[NSString stringWithFormat: BuildString, [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"], buildDateString];
	ASSERTNOTNIL(str);
	[versionField setStringValue: str];
	
	[[self window] setDelegate: (id) self];
	[[self window] center];
	}


//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

- (void) setBackgroundGradient
	{

// Modified from Dave Barton's example on window backgrounds.
// http://www.mere-mortal-software.com/blog/details.php?d=2007-01-08

	NSColor *color1=[NSColor colorWithCalibratedRed: 0.4 green: 0.4 blue: 0.4 alpha: 1.0];ASSERTNOTNIL(color1);
	NSColor *color2=[NSColor colorWithCalibratedRed: 0.2 green: 0.2 blue: 0.2 alpha: 1.0];ASSERTNOTNIL(color2);

	NSGradient *gradient=[[[NSGradient alloc] initWithStartingColor: color1 endingColor: color2] autorelease];ASSERTNOTNIL(gradient);	
	
	NSRect gradientRect=NSMakeRect(0, 0, [[self window] frame].size.width, [[self window] frame].size.height);
	NSImage *gradientImage=[[[NSImage alloc] initWithSize: gradientRect.size] autorelease];ASSERTNOTNIL(gradientImage);

	[gradientImage lockFocus];
	[gradient drawInBezierPath: [NSBezierPath bezierPathWithRect: gradientRect] angle: 270];
	[gradientImage unlockFocus];

	[[self window] setBackgroundColor: [NSColor colorWithPatternImage: gradientImage]];
	}


//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

- (void) mouseUp: (NSEvent* __unused) theEvent
	{
	[[self window] close];
	}


//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

- (void) windowDidBecomeKey: (NSNotification* __unused) aNotification
	{
		
	[self showStatusString: @""];

	if ([[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"] rangeOfString: @"a"].length>0)
		{
		[self showStatusString: @"Alpha version"];
		}
	if ([[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"] rangeOfString: @"b"].length>0)
		{
		[self showStatusString: @"Beta version"];
		}
	}


//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

- (void) windowDidResignKey: (NSNotification* __unused) aNotification
	{
		
	[[self window] close];
	}


//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

- (void) fadeIn: (NSTimer *) timer
	{
	CGFloat alpha=[[self window] alphaValue];

    if (alpha < 1.0)
		{
		alpha+=1.0;
		if (alpha > 1.0) alpha=1.0;
        [[self window] setAlphaValue: alpha];
		}
	else
		{
        [timer invalidate];
        timer=nil;
		}
	}


//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

- (void) fadeOut: (NSTimer *) timer
	{
	CGFloat alpha=[[self window] alphaValue];
		
    if (alpha > 0.0)
		{
		alpha-=0.1;
		if (alpha < 0.0) alpha=0.0;
        [[self window] setAlphaValue: alpha];
		}
	else
		{
        [timer invalidate];
        timer=nil;
		
        [[self window] close];
		[[self window] setAlphaValue: 1.0];
		}
	}


//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

- (void) showStatusString: (NSString*) aString
	{
	[statusField setStringValue: aString];
	[[NSRunLoop currentRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow: 0.1]];
	}


	
	
@end


		
