/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "DataToImageTransformer.h"
#import "NSImage+Additions.h"


@implementation DataToImageTransformer

+ (Class) transformedValueClass
	{
    return [NSImage class];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (BOOL) allowsReverseTransformation
	{
    return YES;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (id) transformedValue: (id) value
	{
    if (value == nil || [value length] < 1)
		{
		return nil;
		}
		
    NSImage *image=nil;
    if ([value isKindOfClass: [NSData class]])
		{
 		NSData *rawData=[NSUnarchiver unarchiveObjectWithData: value];ASSERTNOTNIL(rawData);
		image=[[[NSImage alloc] initWithData: rawData] autorelease];ASSERTNOTNIL(image);
		}
		
	return image;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (id) reverseTransformedValue: (id) value
	{
    if (value == nil)
		{
		return nil;
		}
		
    NSData *data=nil;
    if ([value isKindOfClass: [NSImage class]])
		{
		NSData *rawData=[value JPEGRepresentationWithCompressionFactor: JPEGCompressionFactor];ASSERTNOTNIL(rawData);
		data=[NSArchiver archivedDataWithRootObject: rawData];ASSERTNOTNIL(data);
		}
		
	return data;
	}


@end