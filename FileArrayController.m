/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "FileArrayController.h"
#import "FileEntity.h"
#import "KeynoteXMLDocument.h"
#import "NSString+Additions.h"
#import "WaitController.h"
#import "XphileDocument.h"


@implementation FileArrayController


- (NSArray*) acceptableFileTypes
	{
	NSArray *fileTypesArray=[NSArray arrayWithObjects: @"avi", @"mov", @"mpg", @"m4v", nil];ASSERTNOTNIL(fileTypesArray);
	return (fileTypesArray);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark-
#pragma mark Dropping

// Promote in class hierarchy?

- (NSDragOperation) tableView: (NSTableView*) tv validateDrop: (id <NSDraggingInfo>) info proposedRow: (NSInteger) row proposedDropOperation: (NSTableViewDropOperation) op 
	{
	NSUInteger						index;


// Asserts.

	ASSERTNOTNIL(info);

// Check there is a study open.

	NSDictionary *bindingsDict=[self infoForBinding: @"contentArrayForMultipleSelection"];ASSERTNOTNIL(bindingsDict);
	NSArrayController *sac=[bindingsDict objectForKey: @"NSObservedObject"];ASSERTNOTNIL(sac);
	if ([[sac arrangedObjects] count]==0)
		return NSDragOperationNone;
	
// See if we have a file list.

	if ([[[info draggingPasteboard] types] containsObject: NSFilenamesPboardType])
		{
		NSArray *draggedFileNames=[NSArray arrayWithArray: [[info draggingPasteboard] propertyListForType: NSFilenamesPboardType]];ASSERTNOTNIL(draggedFileNames);

// Check that we are either adding >= 1 or replacing only 1.

		if (op==NSTableViewDropOn && [draggedFileNames count]>1)
			return NSDragOperationNone;

// Check that the file type is appropriate.

		NSArray *acceptableFileTypesArray=[self acceptableFileTypes];ASSERTNOTNIL(acceptableFileTypesArray);
		for (index=0; index < [draggedFileNames count]; index++)
			{
			NSString *extensionString=[[draggedFileNames objectAtIndex: index] pathExtension];ASSERTNOTNIL(extensionString);
			if (![acceptableFileTypesArray containsObject: [extensionString lowercaseString]])
				return NSDragOperationNone;
			}
		return NSDragOperationEvery;
		}
	
	return [super tableView: tv validateDrop: info proposedRow: row proposedDropOperation: op];    
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) tableView: (NSTableView*) aTableView acceptDrop: (id <NSDraggingInfo>) info row: (NSInteger) rowIndex dropOperation: (NSTableViewDropOperation) operation
	{
	NSUInteger							index;
	NSData						*imageData;
	NSData						*imageArchive;


// Work out order index.

	NSInteger orderIndexOffset;
	FileEntity *fileEntity=[[self arrangedObjects] lastObject];
	if (fileEntity==nil)
		orderIndexOffset=-1;
	else
		orderIndexOffset=[[fileEntity valueForKey: @"order"] integerValue];
	orderIndexOffset++;

	if ([[[info draggingPasteboard] types] containsObject: NSFilenamesPboardType])
		{

// Get the file list.

		NSArray *draggedFileNames=[NSArray arrayWithArray: [[info draggingPasteboard] propertyListForType: NSFilenamesPboardType]];ASSERTNOTNIL(draggedFileNames);

// Re-order the array to alphabetical.

		NSMutableArray *orderedFileNames=[[draggedFileNames mutableCopy] autorelease];ASSERTNOTNIL(orderedFileNames);
		[orderedFileNames sortUsingSelector: @selector(orderedFileNameCompare:)];
		
// Replacing one?

		if (operation==NSTableViewDropOn && [[self arrangedObjects] count]>0)
			{
			imageData=[NSData dataWithContentsOfFile: [orderedFileNames objectAtIndex: 0]];ASSERTNOTNIL(imageData);
			imageArchive=[NSArchiver archivedDataWithRootObject: imageData];ASSERTNOTNIL(imageArchive);
			[[[self arrangedObjects] objectAtIndex: rowIndex] setValue: imageArchive forKeyPath: @"fileData"];
			return (YES);
			}
		
// Inserting one or more?
		
		WaitController *waitController=nil;		
		NSDate *startDate=[NSDate date];ASSERTNOTNIL(startDate);

		NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);

		for (index=0; index<[orderedFileNames count]; index++)
			{
			if (!waitController && [[NSDate date] timeIntervalSinceDate: startDate] > AllowedTimeBeforeShowingWaitDialog)
				{
				waitController=[[WaitController alloc] initWithStatusString: ImportingMoviesString];ASSERTNOTNIL(waitController);
				[waitController showWindow: self];			
				[waitController setProgressBarMax: [orderedFileNames count]];
				}
			if (index % 10 == 9)
				{
				[pool release];
				pool=[[NSAutoreleasePool alloc] init];ASSERTNOTNIL(pool);	
				}
			if (waitController)
				{
				[waitController setProgressBarValue: index+1];
				[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];	
				}

			FileEntity *aNewMovie=[[self newObject] autorelease];ASSERTNOTNIL(aNewMovie);
			[aNewMovie setMediaFromPath: [orderedFileNames objectAtIndex: index]];
			[aNewMovie setValue: [NSNumber numberWithInteger:orderIndexOffset+index] forKey: @"order"];
			[self addObject: aNewMovie];
			}
		
		[pool release];
		[waitController dismiss];
		[waitController release];
		return (YES);
		}
		
	return [super tableView: aTableView acceptDrop: info row: rowIndex dropOperation: operation];
	}



//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Import / Export Routines


- (void) reloadImageAfterEditing
	{
	NSArray *fileSelectionArray=[self selectedObjects];ASSERTNOTNIL(fileSelectionArray);
	if ([fileSelectionArray count] != 1)
		return;

	FileEntity *file=[fileSelectionArray objectAtIndex: 0];ASSERTNOTNIL(file);

// Get temporary file path.

	NSString *tempFilePath=[NSTemporaryDirectory() stringByAppendingPathComponent: [file valueForKey: @"fileName"]];
	ASSERTNOTNIL(tempFilePath);

// If file exists replace the stored data with its contents.

	if (![[NSFileManager defaultManager] fileExistsAtPath: tempFilePath])
		return;

	[file setMediaFromPath: tempFilePath];
	
// Delete the temporary file.

	NSError *error=nil;
	if (![[NSFileManager defaultManager] removeItemAtPath: tempFilePath error: &error])
		{
		NSLog(@"Error: %@", [error description]);
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) exportImagesTo: (NSString*) basePath
	{
	NSUInteger									i;
	BOOL								allSucceeded				=	YES;


	NSAssert([[self selectedObjects] count]>0, @"No movie is selected");

	WaitController *waitController=[[WaitController alloc] initWithStatusString: ExportingImagesString];ASSERTNOTNIL(waitController);
	[waitController showWindow: self];			
	[waitController setProgressBarMax: [[self selectedObjects] count]];

	NSArray *filesArray=[self selectedObjects];ASSERTNOTNIL(filesArray);
	NSSortDescriptor *fileSortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"order" ascending: YES] autorelease];ASSERTNOTNIL(fileSortDescriptor);
	filesArray=[filesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: fileSortDescriptor]];

	for (i=0; i<[filesArray count]; i++)
		{
		[waitController setProgressBarValue: i];
		[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];
		
		FileEntity *aMovie=[filesArray objectAtIndex: i];
		if (aMovie != nil)
			{
			NSString *path=[aMovie uniqueFileNameForFolder: basePath];ASSERTNOTNIL(path);
			
			NSData *data=[[filesArray objectAtIndex: i] valueForKeyPath: @"fileData"];ASSERTNOTNIL(data);
			if (![data writeToFile: path atomically: YES])
				allSucceeded=NO;
			}
		}
	
	[waitController release];
	return (allSucceeded);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) exportToKeynoteFile: (NSString*) basePath
	{
	NSUInteger						i;
	BOOL								allSucceeded				=	YES;


	if ([[self selectedObjects] count]==0)
		return (NO);

// Create XML document and set up a simple theme.

	NSXMLElement *root=[NSXMLElement elementWithName: @"presentation"];ASSERTNOTNIL(root);
	[root setAttributesAsDictionary: [NSDictionary dictionaryWithObject: @"36" forKey: @"version"]];

	KeynoteXMLDocument *xmlDoc = [[KeynoteXMLDocument alloc] initWithRootElement: root];ASSERTNOTNIL(xmlDoc);
	[xmlDoc setVersion: @"1.0"];
	[xmlDoc setCharacterEncoding: @"UTF-8"];
	[xmlDoc addDefaultThemeTo: root];
	
	NSXMLElement *slideListElement=[NSXMLElement elementWithName: @"slide-list"];ASSERTNOTNIL(slideListElement);
	[root addChild: slideListElement];

	NSXMLElement *uiStateElement=[NSXMLElement elementWithName: @"ui-state"];ASSERTNOTNIL(uiStateElement);
	[root addChild: uiStateElement];

// Loop through selected movies.

	WaitController *waitController=[[WaitController alloc] initWithStatusString: ExportingToKeynoteString];ASSERTNOTNIL(waitController);
	[waitController showWindow: self];			
	[waitController setProgressBarMax: [[self selectedObjects] count]];

	NSArray *filesArray=[self selectedObjects];ASSERTNOTNIL(filesArray);
	NSSortDescriptor *fileSortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"order" ascending: YES] autorelease];ASSERTNOTNIL(fileSortDescriptor);
	filesArray=[filesArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: fileSortDescriptor]];

	for (i=0; i<[filesArray count]; i++)
		{
		[waitController setProgressBarValue: i];
		[[NSRunLoop currentRunLoop] runUntilDate: [NSDate date]];
		
		FileEntity *aMovie=[filesArray objectAtIndex: i];
		if (aMovie)
			{
			NSString *path=[aMovie uniqueFileNameForFolder: basePath];ASSERTNOTNIL(path);			
			QTMovie *qtMovie=[aMovie qtMovie];ASSERTNOTNIL(qtMovie);
			NSValue *value=[qtMovie attributeForKey: QTMovieNaturalSizeAttribute];ASSERTNOTNIL(value);
			if ([qtMovie writeToFile: path withAttributes: nil])
				[xmlDoc addNewMovieSlideToList: slideListElement withName: [path lastPathComponent] nativeSize: [value sizeValue]];
			else
				allSucceeded=NO;
			}
		}
	
// Write the XML document.
	
	[[NSFileManager defaultManager] createFileAtPath: [basePath stringByAppendingPathComponent: @"presentation.apxl"]
	  contents: [xmlDoc XMLDataWithOptions: NSXMLNodePrettyPrint] attributes: nil];
	[xmlDoc release];

	[waitController release];
	return (allSucceeded);
	}



@end
