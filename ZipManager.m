/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "ApplicationController.h"
#import "BrowserArrayController.h"
#import "ZipManager.h"


@implementation ZipManager

- (id) init
	{
	if ((self=[super init]))
		{
		passController=[[MIRCPasswordController alloc] init];ASSERTNOTNIL(passController);
		theConnection=nil;
		}

	return (self);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) dealloc
	{
	[passController release];
	if (theConnection)
		{
		[theConnection release];
		}

	[super dealloc];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setOwner: (id) i
	{
	owner=i;
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) zipTask: (NSArray*) args
	{
	NSTask						*zipTask;
	NSAutoreleasePool			*pool;
	BOOL						success			=	YES;


	pool=[[NSAutoreleasePool alloc] init];
	zipTask=[[NSTask alloc] init];
	[zipTask setLaunchPath: @"/usr/bin/zip"];
	[zipTask setArguments: args];
	
	[zipTask launch];
	while ([zipTask isRunning]) [NSThread sleepForTimeInterval: 0.01];
	[zipTask interrupt];

	if ([zipTask terminationStatus] != 0L)
		{
		success=NO;
		}
		
	[zipTask release];
	[pool release];
	
	return (success);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) createZipArchiveFromPath: (NSString*) sourcePath
 	{
	return [self zipTask: [NSArray arrayWithObjects: @"-q", @"-r", @"-j", sourcePath, sourcePath, nil]];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Zip Transmission

- (BOOL) sendZipToMIRCServer: (NSString*) pathToZip
	{
	ASSERTNOTNIL(pathToZip);

	NSString *mircURLString=[NSString stringWithFormat: @"http://%@:%@/submit/%@",
	  [[NSUserDefaults standardUserDefaults] valueForKey: MIRCIPAddressKey],
	  [[NSUserDefaults standardUserDefaults] valueForKey: MIRCPortNumberKey],
	  [[NSUserDefaults standardUserDefaults] valueForKey: MIRCStorageServiceKey]];
	ASSERTNOTNIL(mircURLString);
	
	mircURLString=[mircURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];ASSERTNOTNIL(mircURLString);
	NSURL *mircURL=[NSURL URLWithString: mircURLString];ASSERTNOTNIL(mircURL);
	return ([self sendZip: pathToZip toURL: mircURL]);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) sendZip: (NSString*) pathToZip toURL: (NSURL*) url
	{
	ASSERTNOTNIL(pathToZip);
	ASSERTNOTNIL(url);

// Create the request.

	NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL: url cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval: DefaultHTTPTimeout];
	ASSERTNOTNIL(theRequest);
	
// Create the connection with the request and start loading the data.

	[theRequest setHTTPMethod: @"POST"];
	[theRequest setValue: @"application/x-zip-compressed" forHTTPHeaderField: @"Content-Type"];
	[theRequest setHTTPBody: [NSData dataWithContentsOfFile: pathToZip]];

	theConnection=[[NSURLConnection connectionWithRequest: theRequest delegate: self] retain];
	if (theConnection)											// Create the NSMutableData that will hold the received data.
		{
		receivedData=[[NSMutableData data] retain];ASSERTNOTNIL(receivedData);
		[self retain];
		return YES;
		}
	else														// Failed to make connection.
		{
		ASSERTNOTNIL(owner);
		if ([owner respondsToSelector: @selector(indicateFailure)])
			{
			[owner performSelector: @selector(indicateFailure)];
			}

		return NO;
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connection: (NSURLConnection* __unused) connection didReceiveResponse: (NSURLResponse* __unused) response
	{

// This method is called when the server has determined that it has enough information to create the NSURLResponse
// it can be called multiple times, for example in the case of a redirect, so each time we reset the data.
	
	ASSERTNOTNIL(receivedData);

	[receivedData setLength: 0];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connection: (NSURLConnection* __unused) connection didReceiveData: (NSData*) data
	{
	ASSERTNOTNIL(data);
	ASSERTNOTNIL(receivedData);

// Append the new data to the receivedData.

    [receivedData appendData: data];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connection: (NSURLConnection* __unused) connection didFailWithError: (NSError* __unused) error
	{
	ASSERTNOTNIL(receivedData);
	ASSERTNOTNIL(owner);

// Inform the user of failure.

	if ([owner respondsToSelector: @selector(indicateFailure)])
		{
		[owner performSelector: @selector(indicateFailure)];
		}

// Release the data object.

    [receivedData release];
	receivedData=nil;

	[self autorelease];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connectionDidFinishLoading: (NSURLConnection* __unused) connection
	{
	ASSERTNOTNIL(owner);
	ASSERTNOTNIL(receivedData);

	NSString *response=[[[NSString alloc] initWithData: receivedData encoding: NSUTF8StringEncoding] autorelease];ASSERTNOTNIL(response);
	if ([response rangeOfString: @"The zip file was received and unpacked successfully"].location == NSNotFound)
		{
		if ([owner respondsToSelector: @selector(indicateFailure)])
			{
			[owner performSelector: @selector(indicateFailure)];
			}
		}
	else
		{
		if ([owner respondsToSelector: @selector(indicateSuccess)])
			{
			[owner performSelector: @selector(indicateSuccess)];
			}
		}

// Release the data object.

	[receivedData release];
	receivedData = nil;
	[self autorelease];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connection: (NSURLConnection* __unused) connection didReceiveAuthenticationChallenge: (NSURLAuthenticationChallenge*) challenge
	{
	ASSERTNOTNIL(challenge);
	ASSERTNOTNIL(passController);
	
    if ([challenge previousFailureCount] == 0)
		{
		NSString *userString=[[NSUserDefaults standardUserDefaults] valueForKey: MIRCUserNameKey];ASSERTNOTNIL(userString);
		NSString *passwordString=[[NSUserDefaults standardUserDefaults] valueForKey: MIRCPasswordKey];ASSERTNOTNIL(passwordString);

		if ([passwordString length]==0)						// Ask user to enter the password now.
			{
			[passController showWindow: self];
			[passController setUserName: [[NSUserDefaults standardUserDefaults] valueForKey: MIRCUserNameKey]];
			NSInteger result=[NSApp runModalForWindow: [passController window]];
			if (result==OKCode)
				{
				passwordString=[passController password];ASSERTNOTNIL(passwordString);
				}
			else
				{
				[[challenge sender] cancelAuthenticationChallenge: challenge];
				return;
				}
			}

		NSURLCredential *newCredential=[NSURLCredential credentialWithUser: userString password: passwordString persistence: NSURLCredentialPersistenceForSession];
		ASSERTNOTNIL(newCredential);
		[[challenge sender] useCredential: newCredential forAuthenticationChallenge: challenge];
		}
	else
		{
		[[challenge sender] cancelAuthenticationChallenge: challenge];
		}
	}


@end
