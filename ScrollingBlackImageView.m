/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/



#import "ScrollingBlackImageView.h"

@implementation ScrollingBlackImageView


- (void) awakeFromNib
	{
	cummulativeScrollY=0.0;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) scrollWheel: (NSEvent*) theEvent
	{	
	CGFloat deltaY=[theEvent scrollingDeltaY];
	CGFloat threshold=0.0;
	
	if ([theEvent hasPreciseScrollingDeltas])
		{
		cummulativeScrollY+=deltaY;
		deltaY=cummulativeScrollY;
		threshold=30.0;
		}
	
	if (deltaY < -threshold)
		{
		cummulativeScrollY=0;
		[self scrollUp];
		}
	else if (deltaY > threshold)
		{
		cummulativeScrollY=0;
		[self scrollDown];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) scrollUp
	{
// Override.
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) scrollDown
	{
// Override.
	}


@end
