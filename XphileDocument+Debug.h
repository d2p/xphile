/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/

#ifdef Debug

#import "XphileDocument.h"


@interface XphileDocument (XphileDocument_Debug)

- (IBAction) doImportFromFileMaker: (id) sender;
- (IBAction) debugNewPatient: (id) sender;
- (IBAction) debugBatchNewPatient: (id) sender;
- (IBAction) debugResponderChain: (id) sender;
- (IBAction) debugRemoveOrphans: (id) sender;
- (IBAction) debugShowLargeBlobs: (id) sender;
- (IBAction) debugRedrawAlbumTable: (id) sender;

@end

#endif