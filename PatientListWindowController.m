/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "PatientEntity.h"
#import "PatientListWindowController.h"
#import "XphileDocument.h"


@implementation PatientListWindowController

- (void) windowDidLoad
	{
	[super windowDidLoad];

// Set autosave name.

	[self setWindowFrameAutosaveName: @"PatientListWindow"];

// Observe changes to managed object context.

	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(objectsDidChange:) name: NSManagedObjectContextObjectsDidChangeNotification object: [self.document managedObjectContext]];

// Set default sorting of patient list table view.
	
	NSSortDescriptor *sortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"identifier" ascending: YES] autorelease];
	ASSERTNOTNIL(sortDescriptor);
	[patientListTableView setSortDescriptors: @[sortDescriptor]];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) dealloc
	{

// Remove observation of managed object context.

	[[NSNotificationCenter defaultCenter] removeObserver: self name: NSManagedObjectContextObjectsDidChangeNotification object: [self.document managedObjectContext]];

	[super dealloc];
	}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSString*) windowTitleForDocumentDisplayName: (NSString*) displayName
	{
	return [NSString stringWithFormat: @"Patient List (%@)", [displayName stringByDeletingPathExtension]];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Observation

/// No-op.

- (void) objectsDidChange: (NSNotification* __unused) notification
	{
	[patientArrayController rearrangeObjects];
	}
	

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) doEditPatient: (id __unused) sender
	{
	NSArray *patientsArray=[patientArrayController selectedObjects];ASSERTNOTNIL(patientsArray);
	if ([patientsArray count] != 1)
		{
		return;
		}
	PatientEntity *currentPat=patientsArray[0];ASSERTNOTNIL(currentPat);
	[self.document performSelector: @selector(editPatient:) withObject: currentPat];
	}


@end
