/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


// Modified from JCR's sample code at http://developer.apple.com/samplecode/Cropped_Image/index.html

#import "SelectionPath.h"




@implementation SelectionPath

+ (SelectionPath*) markerForView: (NSView*) aView
	{
	return [[[SelectionPath alloc] initWithView: aView] autorelease];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (id) initWithView: (NSView *) aView
	{
	if ((self=[super init]))
		{
		targetView=(NSImageView*) aView;
		
		[self setColor: [self selectionColor]];
		[self setSelectedRect: NSZeroRect];
		
		topLeftHandle=nil;
		topRightHandle=nil;
		bottomLeftHandle=nil;
		bottomRightHandle=nil;
		}
	return self;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) dealloc
	{
	[fillColor release];
	[strokeColor release];
		
	[bottomRightHandle release];
	[topRightHandle release];
	[bottomLeftHandle release];
	[topLeftHandle release];
				
	[super dealloc];
	}
  

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) drawSelection	
	{
	if (!NSIsEmptyRect(selectedRect))
		{

// Only dim outside the selection rect if large enough and an image is being displayed.		

		NSInteger minDimension=3;
		NSImage *im=[(NSImageView*) targetView image];
		if (selectedRect.size.width > minDimension && selectedRect.size.height > minDimension && im != nil)
			{
			NSBezierPath *beyondArea=[NSBezierPath bezierPathWithRect: [targetView bounds]];ASSERTNOTNIL(beyondArea);
			[beyondArea appendBezierPathWithRect: selectedRect];
			[beyondArea setWindingRule: NSEvenOddWindingRule];
			[fillColor set];
			[beyondArea fill];
			}

// Draw handles.		

		NSInteger tolerance=20;

		[strokeColor set]; 
		NSFrameRect(selectedRect);
		
		NSRect handleRect=NSMakeRect(NSMinX(selectedRect)-tolerance/2, NSMinY(selectedRect)-tolerance/2, tolerance, tolerance);
		[bottomLeftHandle release];
		bottomLeftHandle=[[NSBezierPath bezierPathWithOvalInRect: handleRect] retain];ASSERTNOTNIL(bottomLeftHandle);
		[bottomLeftHandle fill];
		
		handleRect=NSOffsetRect(handleRect, 0, selectedRect.size.height);
		[topLeftHandle release];
		topLeftHandle=[[NSBezierPath bezierPathWithOvalInRect: handleRect] retain];ASSERTNOTNIL(topLeftHandle);
		[topLeftHandle fill];				
		
		handleRect=NSOffsetRect(handleRect, selectedRect.size.width, 0);
		[topRightHandle release];
		topRightHandle=[[NSBezierPath bezierPathWithOvalInRect: handleRect] retain];ASSERTNOTNIL(topRightHandle);
		[topRightHandle fill];				
		
		handleRect=NSOffsetRect(handleRect, 0, -selectedRect.size.height);
		[bottomRightHandle release];
		bottomRightHandle=[[NSBezierPath bezierPathWithOvalInRect: handleRect] retain];ASSERTNOTNIL(bottomRightHandle);
		[bottomRightHandle fill];				
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark-

- (void) startSelectingAtPoint: (NSPoint) where
	{
	trackingMode=SelectionTrackingSelecting;
	pinLocation=where;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) continueSelectingAtPoint: (NSPoint) where
	{
	selectedRect=rectFromPoints(pinLocation, where);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) stopSelectingAtPoint: (NSPoint) where 
	{ 
	selectedRect=rectFromPoints(pinLocation, where);  
	trackingMode=SelectionTrackingNone;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) startMovingAtPoint: (NSPoint) where
	{
	trackingMode=SelectionTrackingMoving;
	pinLocation=where;
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) continueMovingAtPoint: (NSPoint) where
	{
	selectedRect.origin.x+=where.x-pinLocation.x;
	selectedRect.origin.y+=where.y-pinLocation.y;
	pinLocation=where;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) stopMovingAtPoint: (NSPoint) where
	{
	[self continueMovingAtPoint: where];
	trackingMode=SelectionTrackingNone;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) startResizingAtPoint: (NSPoint) where
	{
	trackingMode=SelectionTrackingResizing;
	
// Work out our pin Location, which should be opposite our current point.

	if ([bottomRightHandle containsPoint: where])
		{
		pinLocation.x=NSMinX(selectedRect);
		pinLocation.y=NSMaxY(selectedRect);
		return;
		}
		
	if ([topLeftHandle containsPoint: pinLocation])
		{
		pinLocation.x=NSMaxX(selectedRect);
		pinLocation.y=NSMinY(selectedRect);
		return;
		}
		
	if ([topRightHandle containsPoint: pinLocation])
		{
		pinLocation=selectedRect.origin;
		return;
		}

	if ([bottomLeftHandle containsPoint: pinLocation])
		{
		pinLocation.x=NSMaxX(selectedRect);
		pinLocation.y=NSMaxY(selectedRect);
		return;
		}
		
// We are not in a corner or a side handle, so change to dragging.

	[self startMovingAtPoint: where];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) continueResizingAtPoint: (NSPoint) where
	{
	selectedRect=rectFromPoints(pinLocation, where);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) stopResizingAtPoint: (NSPoint) where 
	{ 
	selectedRect=rectFromPoints(pinLocation, where);  
	trackingMode=SelectionTrackingNone;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark-

- (void) mouseDown: (NSEvent*) theEvent 
	{
	pinLocation=[targetView convertPoint: [theEvent locationInWindow] fromView: nil];

// Check if mouse down in each of the drag handles.
	
	if ([bottomRightHandle containsPoint: pinLocation])
		{
		[self startResizingAtPoint: pinLocation];
		return;
		}
	if ([topLeftHandle containsPoint: pinLocation])
		{
		[self startResizingAtPoint: pinLocation];
		return;
		}
	if ([bottomLeftHandle containsPoint: pinLocation])
		{
		[self startResizingAtPoint: pinLocation];
		return;
		}
	if ([topRightHandle containsPoint: pinLocation])
		{
		[self startResizingAtPoint: pinLocation];
		return;
		}

// Otherwise check if in rect, and move it.
	
	if (NSPointInRect(pinLocation, selectedRect))
		{
		[self startMovingAtPoint: pinLocation]; 
		return;
		}

// Or just start a new selection rect.
		
	[self startSelectingAtPoint: pinLocation];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) mouseUp: (NSEvent *) theEvent 
	{ 
	switch (trackingMode)
		{
		case SelectionTrackingSelecting:
			[self stopSelectingAtPoint: [targetView convertPoint: [theEvent locationInWindow] fromView: nil]];
			break;
			
		case SelectionTrackingMoving:
			[self stopMovingAtPoint: [targetView convertPoint: [theEvent locationInWindow] fromView: nil]];
			break;
		
		case SelectionTrackingResizing:
			[self stopResizingAtPoint: [targetView convertPoint: [theEvent locationInWindow] fromView: nil]];
			break;
		
		default:	
			break;
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) mouseDragged: (NSEvent*) theEvent 
	{ 
	switch (trackingMode)
		{
		case SelectionTrackingSelecting:
			[self continueSelectingAtPoint: [targetView convertPoint: [theEvent locationInWindow] fromView: nil]];
			break;
			
		case SelectionTrackingMoving:
			[self continueMovingAtPoint: [targetView convertPoint: [theEvent locationInWindow] fromView: nil]];
			break;
		
		case SelectionTrackingResizing:
			[self continueResizingAtPoint: [targetView convertPoint: [theEvent locationInWindow] fromView: nil]];
			break;
		
		default:	
			break;
		}
	}
  

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark-


- (void) setColor: (NSColor*) aColor
	{
	[self setStrokeColor: aColor];
	[self setFillColor: [[NSColor grayColor] colorWithAlphaComponent: 0.7]];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSColor*) selectionColor
	{
	return [NSColor lightGrayColor];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setFillColor: (NSColor*) color
	{
	[color retain];
	[fillColor release];
	fillColor=color;
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setStrokeColor: (NSColor*) color
	{
	[color retain];
	[strokeColor release];
	strokeColor=color;
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
	
- (NSBezierPath*) selectedPath
	{
	NSBezierPath *bezierPath=[NSBezierPath bezierPathWithRect: selectedRect];ASSERTNOTNIL(bezierPath);
	return bezierPath;
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
	
- (NSRect) selectedRect
	{
	return selectedRect;
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
	
- (void) setSelectedRect: (NSRect) rect
	{
	selectedRect=rect;
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setSelectedRectOrigin: (NSPoint) where
	{
	selectedRect.origin=where;
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setSelectedRectSize: (NSSize) size
	{
	selectedRect.size=size;
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) moveSelectedRectBy: (NSSize) delta
	{
	selectedRect.origin.x+=delta.width;
	selectedRect.origin.y+=delta.height;
	}


@end


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

NSRect rectFromPoints(NSPoint p1, NSPoint p2)
  {
  return NSMakeRect(MIN(p1.x, p2.x), MIN(p1.y, p2.y), fabs(p1.x - p2.x), fabs(p1.y - p2.y));
  }

