/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "XphileDocument.h"


// Localised strings.

#define XphileDocumentMoviesLocalizedString(str)		NSLocalizedStringFromTable((str), @"XphileDocument+Movies", (str))

#define UntitledMovieString								XphileDocumentMoviesLocalizedString(@"Untitled Movie")
#define CreatingMovieString								XphileDocumentMoviesLocalizedString(@"Creating movie")
#define StoringMovieString								XphileDocumentMoviesLocalizedString(@"Storing movie")



@interface XphileDocument (XphileDocument_Movies)

- (void) createMovie;
- (void) showMovieSheet: (NSWindow*) window;
- (IBAction) closeMovieSheet: (id) sender;
- (void) movieSheetDidEnd: (NSWindow*) sheet returnCode: (NSInteger) returnCode contextInfo: (void*) contextInfo;
- (void) createMovieWithLabel: (NSString*) label;

@end
