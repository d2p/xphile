/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "VersionManager.h"
#import "WaitController.h"



@interface VersionManager ()
	{
	BOOL						verbose;
	NSMutableData				*receivedVersionInfoData;
	NSString					*downloadDestinationPath;
	NSURLDownload				*versionDownload;
	NSURLConnection				*versionInfoConnection;
	NSInteger					expectedContentLength,
								receivedContentLength;
	WaitController				*waitController;
	}

@property (nonatomic, readonly) NSString *applicationPath;
@property (nonatomic, readonly) NSString *applicationName;

@end


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————


@implementation VersionManager


+ (VersionManager*) versionManager
	{

	static dispatch_once_t token;
	static VersionManager *sharedVersionManager;

	dispatch_once(&token, ^
		{
		sharedVersionManager=[[VersionManager alloc] init];
		});
	
	return (sharedVersionManager);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) checkVersionFromURL: (NSURL*) versionsURL verbose : (BOOL) beingVerbose
	{
	ASSERTNOTNIL(versionsURL);
	verbose=beingVerbose;

	NSURLRequest *versionInfoRequest=[NSURLRequest requestWithURL: versionsURL cachePolicy: NSURLRequestReloadIgnoringLocalCacheData
	  timeoutInterval: DefaultHTTPTimeout];
	ASSERTNOTNIL(versionInfoRequest);
	versionInfoConnection=[[NSURLConnection alloc] initWithRequest: versionInfoRequest delegate: self];
	if (versionInfoConnection)
		{
		receivedVersionInfoData=[[NSMutableData data] retain];
		ASSERTNOTNIL(receivedVersionInfoData);
		}
	else
		{
		[[NSNotificationCenter defaultCenter] postNotificationName: VersionCheckUnsuccessfulNotification object: nil];
		if (verbose)
			{
			[[NSAlert alertWithMessageText: UnableToContactServerString defaultButton: OKString alternateButton: @"" otherButton: @""
			  informativeTextWithFormat: @"%@", ReasonString] runModal];
			}
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (void) connection: (NSURLConnection* __unused) connection didReceiveResponse: (NSURLResponse* __unused) response
	{	
	[receivedVersionInfoData setLength: 0];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connection: (NSURLConnection* __unused) connection didReceiveData: (NSData*) data
	{
		
	[receivedVersionInfoData appendData: data];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connectionDidFinishLoading: (NSURLConnection*) theConnection
	{
	[receivedVersionInfoData autorelease];
	[theConnection release];

	NSString *versionInfoString=[[[NSString alloc] initWithData: receivedVersionInfoData encoding: NSUTF8StringEncoding] autorelease];ASSERTNOTNIL(versionInfoString);
	NSDictionary *versionInfoDictionary=[versionInfoString propertyList];

	if (versionInfoDictionary==nil)
		{
		[[NSNotificationCenter defaultCenter] postNotificationName: VersionCheckUnsuccessfulNotification object: nil];
		if (verbose)
			{
			[[NSAlert alertWithMessageText: UnableToContactServerString
			  defaultButton: OKString alternateButton: @"" otherButton: @""
			  informativeTextWithFormat: @"%@", PleaseTryAgainLaterString] runModal];
			}
		return;
		}

	NSString *currentVersionNumber=[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
	NSString *bundleIdentifier=[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleIdentifier"];

	if (currentVersionNumber==nil)
		{
		[[NSNotificationCenter defaultCenter] postNotificationName: VersionCheckUnsuccessfulNotification object: nil];
		if (verbose)
			{
			[[NSAlert alertWithMessageText: UnableToGetCurrentVersionString
			  defaultButton: OKString alternateButton: @"" otherButton: @""
			  informativeTextWithFormat: @""] runModal];
			}
		return;
		}
	
// Check if alpha or beta.
	
	if ([currentVersionNumber rangeOfString: @"a"].length > 0)
		{
		bundleIdentifier=[bundleIdentifier stringByAppendingString: @".alpha"];
		}
	if ([currentVersionNumber rangeOfString: @"b"].length > 0)
		{
		bundleIdentifier=[bundleIdentifier stringByAppendingString: @".beta"];
		}

// Get app's version dictionary.

	NSDictionary *appVersionDict=[versionInfoDictionary valueForKey: bundleIdentifier];
	if (appVersionDict==nil)
		{
		[[NSNotificationCenter defaultCenter] postNotificationName: VersionCheckUnsuccessfulNotification object: nil];
		if (verbose)
			{
			[[NSAlert alertWithMessageText: UnableToDownloadVersionInformationString
			  defaultButton: OKString alternateButton: @"" otherButton: @""
			  informativeTextWithFormat: @"%@", PleaseTryAgainLaterString] runModal];
			}
		return;
		}

// Successfully got server version details.

	[[NSNotificationCenter defaultCenter] postNotificationName: VersionCheckSuccessfulNotification object: nil];

	NSString *latestVersionNumber=[appVersionDict valueForKey: VersionKey];
	NSString *description=NewFeaturesDescriptionString;ASSERTNOTNIL(description);
	description=[description stringByAppendingString: [appVersionDict valueForKey: DescriptionKey]];

	if ([latestVersionNumber isEqualToString: currentVersionNumber])
		{
		if (verbose)
			{
			[[NSAlert alertWithMessageText:
			  [NSString stringWithFormat: ApplicationIsUpToDateString, [self applicationName]]
			  defaultButton: OKString alternateButton: @"" otherButton: @""
			  informativeTextWithFormat: CurrentlyInstalledVersionString, currentVersionNumber] runModal];
			}
		}
	else
		{
		NSAlert *newVersionAlert=[NSAlert alertWithMessageText:
		  [NSString stringWithFormat: NewVersionAvailableForDownloadString, [self applicationName], latestVersionNumber]
		  defaultButton: DownloadString
		  alternateButton: CancelString
		  otherButton: HomePageString
		  informativeTextWithFormat: @"%@", [description stringByAppendingString: @"\n\n"]];
		ASSERTNOTNIL(newVersionAlert);
		NSUInteger result=[newVersionAlert runModal];
		if (result == NSAlertDefaultReturn)
			{
			NSURL *downloadURL=[NSURL URLWithString: [appVersionDict valueForKey: DownloadKey]];ASSERTNOTNIL(downloadURL);
			NSURLRequest *downloadRequest=[NSURLRequest requestWithURL: downloadURL cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval: DefaultHTTPTimeout];
			ASSERTNOTNIL(downloadRequest);
			versionDownload=[[NSURLDownload alloc] initWithRequest: downloadRequest delegate: self];
			if (!versionDownload)
			   {
				[[NSAlert alertWithMessageText: UnableToDownloadUpdateString defaultButton: OKString alternateButton: @"" otherButton: @""
				  informativeTextWithFormat: @""] runModal];
				return;
				}
			}
		else if (result == NSAlertAlternateReturn)
			;
		else
			{
			[[NSWorkspace sharedWorkspace] openURL: [NSURL URLWithString: [appVersionDict valueForKey: HomePageKey]]];
			}
		}

	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) connection: (NSURLConnection*) theConnection didFailWithError: (NSError*) error
	{
	[[NSNotificationCenter defaultCenter] postNotificationName: VersionCheckUnsuccessfulNotification object: nil];
	if (verbose)
		{
		[[NSAlert alertWithMessageText: UnableToContactServerString defaultButton: OKString alternateButton: @"" otherButton: @""
		  informativeTextWithFormat: ReasonString, [error localizedDescription]] runModal];
		}
		
	[receivedVersionInfoData release];
	[theConnection release];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Download

- (void) downloadDidBegin: (NSURLDownload*) download
	{
	receivedContentLength=0;
	downloadDestinationPath=[[@"~/Downloads/Xphile.dmg" stringByExpandingTildeInPath] retain];ASSERTNOTNIL(downloadDestinationPath);
	[download setDestination: downloadDestinationPath allowOverwrite: YES];
	if (download)
		{
		waitController=[[WaitController alloc] initWithStatusString: DownloadingUpdateString];ASSERTNOTNIL(waitController);
		[waitController showWindow: self];
		}
	else
		{
		[[NSAlert alertWithMessageText: UnableToContactServerString defaultButton: OKString alternateButton: @"" otherButton: @""
			informativeTextWithFormat: @""] runModal];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) download: (NSURLDownload* __unused) theDownload didReceiveResponse: (NSURLResponse*) response
	{
		
    expectedContentLength=[response expectedContentLength];

    if (expectedContentLength > 0)
		[waitController setProgressBarMax: expectedContentLength];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) download: (NSURLDownload* __unused) theDownload didReceiveDataOfLength: (NSUInteger) length
	{
		
	if (expectedContentLength > 0)
		{
		receivedContentLength+=length;
		[waitController setProgressBarValue: receivedContentLength];
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) downloadDidFinish: (NSURLDownload*) theDownload
	{
	[theDownload release];
	[waitController release];
	[downloadDestinationPath autorelease];

	if (![[NSDocumentController sharedDocumentController] hasEditedDocuments])
		{ 
		if ([[NSAlert alertWithMessageText: NewVersionDownloadedString defaultButton: InstallString alternateButton: CancelString otherButton: @""
		  informativeTextWithFormat: NewVersionInstallInformationString] runModal] == NSCancelButton)
			{
			return;
			}

		NSArray *finderAppsArray=[NSRunningApplication runningApplicationsWithBundleIdentifier: @"com.apple.finder"];
		ASSERTNOTNIL(finderAppsArray);
		for (NSRunningApplication *finderApp in finderAppsArray)
			{
			[finderApp activateWithOptions: NSApplicationActivateAllWindows | NSApplicationActivateIgnoringOtherApps];
			}

 
		[[NSWorkspace sharedWorkspace] openURL: [NSURL fileURLWithPath: downloadDestinationPath]];
		[NSApp terminate: self];
		}
	else
		{
		[[NSAlert alertWithMessageText: NewVersionDownloadedString defaultButton: OKString alternateButton: @"" otherButton: @""
		  informativeTextWithFormat: NewVersionDownloadedInformationString] runModal];
		}
		
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) download: (NSURLDownload*) theDownload didFailWithError: (NSError*) error
	{
	[waitController release];
	[theDownload release];
	[downloadDestinationPath release];
	
    NSString *errorDescription=[error localizedDescription];ASSERTNOTNIL(errorDescription);
	[[NSAlert alertWithMessageText: UnableToDownloadUpdateString defaultButton: OKString alternateButton: @"" otherButton: @""
	  informativeTextWithFormat: ReasonString, errorDescription] runModal];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (NSString*) applicationPath
	{
	return ([[NSBundle mainBundle] bundlePath]);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) applicationName
	{
	return ([[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleName"]);
	}





@end
