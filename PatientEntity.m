/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "PatientEntity.h"


@implementation PatientEntity


+ (NSSet*) keyPathsForValuesAffectingGenderText
	{
	return [NSSet setWithObject: @"gender"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingAbbreviatedGenderText
	{
	return [NSSet setWithObject: @"gender"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingLinkedToPatientIDs
	{
	return [NSSet setWithObject: @"linkedToPatients"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingLinkedFromPatientIDs
	{
	return [NSSet setWithObject: @"linkedFromPatients"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingSomeLinkedToPatients
	{
	return [NSSet setWithObject: @"linkedToPatients"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingSomeLinkedFromPatients
	{
	return [NSSet setWithObject: @"linkedFromPatients"];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (id) valueForUndefinedKey: (NSString*) key
	{
	NSLog(@"PatientEntity: no such key - %@", key);
	return (nil);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (NSString*) genderText
	{
	NSInteger genderTag=[[self valueForKeyPath: @"gender"] integerValue];
	return [[NSUserDefaults standardUserDefaults] valueForKeyPath: GenderArrayKey][genderTag];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) abbreviatedGenderText
	{
	NSString *str=[self genderText];
	return [str substringToIndex: 1];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Anonymise

- (void) anonymise
	{

// ID.

	CFUUIDRef aUUID=CFUUIDCreate(nil);
	NSString *uString=(NSString*) CFUUIDCreateString(nil, aUUID);ASSERTNOTNIL(uString);
	CFRelease(aUUID);
	[uString autorelease];
	uString=[uString substringToIndex: 18];ASSERTNOTNIL(uString);

	[self setValue: uString forKeyPath: @"identifier"];

// DOB.

	NSInteger dateDelta=(rand() % 355) + 10;
	NSDate *date=[self valueForKeyPath: @"dateOfBirth"];
	if (date!=nil)
		{
		date=[date dateByAddingTimeInterval: -(dateDelta * 60*60*24)];
		[self setValue: date forKeyPath: @"dateOfBirth"];
		}
		
// Conditions: dateOfOnset.
	
	NSMutableSet *conditionSet=[self mutableSetValueForKeyPath: @"conditions"];ASSERTNOTNIL(conditionSet);
	NSEnumerator *conditionEnumerator=[conditionSet objectEnumerator];ASSERTNOTNIL(conditionEnumerator);
	ConditionEntity				*condition;
	while ((condition=[conditionEnumerator nextObject]))
		{
		date=[condition valueForKeyPath: @"dateOfOnset"];
		if (date!=nil)
			{
			date=[date dateByAddingTimeInterval: -(dateDelta * 60*60*24)];
			[condition setValue: date forKeyPath: @"dateOfOnset"];
			}

// Studies: dateOfStudy.

		NSMutableSet *studiesSet=[condition mutableSetValueForKeyPath: @"studies"];ASSERTNOTNIL(studiesSet);
		NSEnumerator *studyEnumerator=[studiesSet objectEnumerator];ASSERTNOTNIL(studyEnumerator);
		StudyEntity				*study;
		while ((study=[studyEnumerator nextObject]))
			{
			date=[study valueForKeyPath: @"dateOfStudy"];
			if (date!=nil)
				{
				date=[date dateByAddingTimeInterval: -(dateDelta * 60*60*24)];
				[study setValue: date forKeyPath: @"dateOfStudy"];
				}
			}
		}
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Links

- (BOOL) someLinkedToPatients
	{
	NSMutableSet *linkSet=[self mutableSetValueForKey: @"linkedToPatients"];
	if (linkSet==nil)
		{
		return (NO);
		}
		
	return ([linkSet count]>0);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) someLinkedFromPatients
	{
	NSMutableSet *linkSet=[self mutableSetValueForKey: @"linkedFromPatients"];
	if (linkSet==nil)
		{
		return (NO);
		}
		
	return ([linkSet count]>0);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) stringShowingWhetherHasLinkedPatients
	{
	BOOL hasLinkedToPatients=[self someLinkedToPatients];
	BOOL hasLinkedFromPatients=[self someLinkedFromPatients];

	if (hasLinkedToPatients && hasLinkedFromPatients)
		{
		return @"⇄";
		}
	else if (hasLinkedToPatients)
		{
		return @"←";
		}
	else if (hasLinkedFromPatients)
		{
		return @"→";
		}
		
	return @"";
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSArray*) linkedToPatientIDs
	{

// Return an array of identifier strings from the set of linkedToPatients.	
	
	NSArray *array=@[LinkedToString];ASSERTNOTNIL(array);
	
	NSMutableSet *linkSet=[self mutableSetValueForKey: @"linkedToPatients"];
	if (linkSet==nil)
		{
		return array;
		}
		
	NSEnumerator *enumerator=[linkSet objectEnumerator];ASSERTNOTNIL(enumerator);
	id object;
	while ((object=[enumerator nextObject]))
		{
		array=[array arrayByAddingObject: [object valueForKey: @"identifier"]];
		}

	return array;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSArray*) linkedFromPatientIDs
	{

// Return an array of identifier strings from the set of linkedFromPatients.	
	
	NSArray *array=@[LinkedFromString];ASSERTNOTNIL(array);
	
	NSMutableSet *linkSet=[self mutableSetValueForKey: @"linkedFromPatients"];
	if (linkSet==nil)
		{
		return array;
		}
		
	NSEnumerator *enumerator=[linkSet objectEnumerator];ASSERTNOTNIL(enumerator);
	id object;
	while ((object=[enumerator nextObject]))
		{
		array=[array arrayByAddingObject: [object valueForKey: @"identifier"]];
		}

	return array;
	}


@end
