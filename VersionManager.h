/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/



#define DefaultHTTPTimeout								5.0


#define VersionCheckUnsuccessfulNotification			@"VersionCheckUnsuccessful"
#define VersionCheckSuccessfulNotification				@"VersionCheckSuccessful"

#define VersionKey										@"version"
#define DescriptionKey									@"description"
#define DownloadKey										@"download"
#define HomePageKey										@"homepage"


// Localised strings.

#define VersionManagerLocalizedString(str)				NSLocalizedStringFromTable((str), @"VersionManager", (str))

#define UnableToContactServerString						VersionManagerLocalizedString(@"Unable to contact server")
#define PleaseTryAgainLaterString						VersionManagerLocalizedString(@"Please try again later.")
#define OKString										VersionManagerLocalizedString(@"OK")
#define UnableToGetCurrentVersionString					VersionManagerLocalizedString(@"Unable to get current version information")
#define UnableToDownloadVersionInformationString		VersionManagerLocalizedString(@"Unable to download version information")
#define NewFeaturesDescriptionString					VersionManagerLocalizedString(@"Features in the new version include:\n\n")
#define ApplicationIsUpToDateString						VersionManagerLocalizedString(@"%@ is up to date")
#define CurrentlyInstalledVersionString					VersionManagerLocalizedString(@"The currently installed version is %@")
#define NewVersionAvailableForDownloadString			VersionManagerLocalizedString(@"%@ %@ is available for download")
#define DownloadString									VersionManagerLocalizedString(@"Download")
#define CancelString									VersionManagerLocalizedString(@"Cancel")
#define HomePageString									VersionManagerLocalizedString(@"Home Page")
#define ReasonString									VersionManagerLocalizedString(@"Reason: %@")
#define NewVersionDownloadedString						VersionManagerLocalizedString(@"New version downloaded")
#define NewVersionInstallInformationString				VersionManagerLocalizedString(@"You can now quit and install the new version.")
#define NewVersionDownloadedInformationString			VersionManagerLocalizedString(@"The disk image has been downloaded to your Downloads folder.")
#define InstallString									VersionManagerLocalizedString(@"Install")
#define DownloadingUpdateString							VersionManagerLocalizedString(@"Downloading update...")
#define UnableToDownloadUpdateString					VersionManagerLocalizedString(@"Unable to download update")



@interface VersionManager: NSObject <NSURLDownloadDelegate>

+ (VersionManager*) versionManager;
- (void) checkVersionFromURL: (NSURL*) versionsURL verbose : (BOOL) beingVerbose;

@end
