/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "AlbumArrayController.h"
#import "BrowserArrayController.h"
#import "BrowserTableView.h"
#import "OsirixXMLRPC.h"
#import "PatientEntity.h"
#import "PatientListWindowController.h"
#import "ZipManager.h"


#define FullScreenBlankImageKey							@"FullScreenBlankImage"

#define ExportToMIRCCompletedSuccessfullyNotification	@"ExportToMIRCCompletedSuccessfully"
#define ExportToMIRCFailedNotification					@"ExportToMIRCFailed"

#define CurrentModelVersion								4


// Localised strings.

#define XphileDocumentLocalizedString(str)				NSLocalizedStringFromTable((str), @"XphileDocument", (str))

#define UpdatedFileNameString							XphileDocumentLocalizedString(@"%@ (Updated)")
#define ExportString									XphileDocumentLocalizedString(@"Export")
#define SuccessfullyExportedImagesString				XphileDocumentLocalizedString(@"All images successfully exported")
#define SuccessfullyExportedToKeynoteString				XphileDocumentLocalizedString(@"Successfully exported to Keynote")
#define UnableToExportImageString						XphileDocumentLocalizedString(@"Unable to Export Some Images")
#define UnableToExportToKeynoteString					XphileDocumentLocalizedString(@"Unable to Export To Keynote")
#define UnsupportedFormatString							XphileDocumentLocalizedString(@"These images may be an unsupported format")
#define SaveKeynoteString								XphileDocumentLocalizedString(@"Save Keynote Document")
#define SaveMIRCArchiveString							XphileDocumentLocalizedString(@"Save MIRC Archive")
#define InitialisingString								XphileDocumentLocalizedString(@"Initialising...")
#define ReadyString										XphileDocumentLocalizedString(@"Ready")
#define ExportingImagesString							XphileDocumentLocalizedString(@"Exporting images...")
#define ExportingToKeynoteString						XphileDocumentLocalizedString(@"Exporting to Keynote...")
#define ExportingToMIRCString							XphileDocumentLocalizedString(@"Exporting to MIRC...")
#define ExportingAsMIRCArchiveString					XphileDocumentLocalizedString(@"Exporting as MIRC Archive...")
#define SuccessfullyExportedToMIRCString				XphileDocumentLocalizedString(@"Successfully exported to MIRC")
#define SuccessfullyExportedAsMIRCArchiveString			XphileDocumentLocalizedString(@"Successfully exported as MIRC Archive")
#define UnableToExportAsMIRCArchiveString				XphileDocumentLocalizedString(@"Unable to export as MIRC Archive")
#define UnableToExportAlbumString						XphileDocumentLocalizedString(@"Unable to Export Album")
#define UnableToExportAlbumDetailString					XphileDocumentLocalizedString(@"Xphile can only export individual cases to MIRC.  Please select an individual case in the browser.")

#define ErrorAttemptingLookupString						XphileDocumentLocalizedString(@"Error attempting lookup")
#define FailureToLaunchString							XphileDocumentLocalizedString(@"The Editor Failed to Launch")

#define RemoveCaseFromLibraryString						XphileDocumentLocalizedString(@"Remove this Case from the Library?")
#define RemoveCasesFromLibraryString					XphileDocumentLocalizedString(@"Remove these Cases from the Library?")
#define RemovingString									XphileDocumentLocalizedString(@"Removing from library...")
#define RemoveString									XphileDocumentLocalizedString(@"Remove")

#define IncompatibleFormatString						XphileDocumentLocalizedString(@"Incompatible Document Version")
#define FutureVersionExplanationString					XphileDocumentLocalizedString(@"This document has been created with a later version of Xphile.  Please update Xphile before proceeding.")

#define PatientExistsString								XphileDocumentLocalizedString(@"Patient Exists")
#define PatientExistsExplanationString					XphileDocumentLocalizedString(@"A patient with this identifier already exists.  The information will be merged.")

#define AnonymiseAllCasesString							XphileDocumentLocalizedString(@"Anonymise all library cases?")
#define AnonymiseString									XphileDocumentLocalizedString(@"Anonymise")
#define AnonymiseAllCasesExplanationString				XphileDocumentLocalizedString(@"Each patient identifier and date of birth will be randomised.  Relative ages and dates will be maintained, however.\n\nDepending on the size of the library, this process may take some time.")
#define AnonymisingAllCasesString						XphileDocumentLocalizedString(@"Anonymising all cases...")
#define AnonymisingAllCasesCompletedString				XphileDocumentLocalizedString(@"All cases anonymised")

#define ConvertAllToJPEGString							XphileDocumentLocalizedString(@"Convert all library images to JPEG format?")
#define ConvertString									XphileDocumentLocalizedString(@"Convert")
#define ConvertAllToJPEGExplanationString				XphileDocumentLocalizedString(@"All images in the library will be converted to JPEG format, with a compression factor of %.1f.\n\nDepending on the size of the library, this process may take some time.")
#define ConvertingAllToJPEGString						XphileDocumentLocalizedString(@"Converting all images to JPEG...")
#define ConvertingPatientToJPEGString					XphileDocumentLocalizedString(@"Converting patient %ld of %ld (%@)")
#define ConvertingToJPEGCompletedString					XphileDocumentLocalizedString(@"All library images converted to JPEG format")

#define UnlinkedToString								XphileDocumentLocalizedString(@"Unlink To")
#define UnlinkedFromString								XphileDocumentLocalizedString(@"Unlink From")

#define ChooseMIRCArchiveString							XphileDocumentLocalizedString(@"Choose MIRC archive")



@interface XphileDocument : NSPersistentDocument
	{
	PatientListWindowController							*patientListWindowController;

	IBOutlet id											conditionArrayController,
														studyArrayController,
														browserStudyController,
														imageArrayController,
														browserImageController,
														browserFileController,
														fileArrayController;
	IBOutlet BrowserArrayController						*browserArrayController;
	IBOutlet id											stage1BrowserArrayController;
	IBOutlet AlbumArrayController						*albumArrayController;
										
	IBOutlet id											systemListController,
														pathologyListController,
														regionListController,
			
														conditionListTableView,
														studyListTableView,
														browserTableView,
														albumTableView,
														systemTableView,
														pathologyTableView,
														regionTableView,
														imageTableView,
														fileTableView,
														previewImageView,
														imageView,
														movieView,
														savePanelAccessoryView;
	IBOutlet NSView										*fullscreenBlankView;
												
	IBOutlet id											statusField,
														progressIndicator,
											
														browserWindow,
														editorWindow,
														imageWindow,
														fullScreenPanel,
														fullScreenHUDWindow,
														
														editorLinkedFromButton,
														editorLinkedToButton,
														
														imageWindowPreviousStudyButton,
														imageWindowNextStudyButton,
														imageWindowEditButton,
														imageWindowChooseButton,

														fullScreenWindowPreviousImageButton,
														fullScreenWindowNextImageButton,
														fullScreenWindowPreviousStudyButton,
														fullScreenWindowNextStudyButton,
														fullScreenWindowPreviousCaseButton,
														fullScreenWindowNextCaseButton,
														fullScreenTabSelector,
														fullScreenTabView,
														fullScreenMovieView,
										
														toolbarSearchView,
														toolbarSearchField,
										
														identifierTextField,
														dateOfBirthTextField,
										
														smartAlbumSheet,
														smartAlbumLabel,
														smartAlbumCompoundMenu,
														smartAlbumSearchTypeMenu1,
														smartAlbumOperatorMenu1,
														smartAlbumOptionMenu1,
														smartAlbumSearchTypeMenu2,
														smartAlbumOperatorMenu2,
														smartAlbumOptionMenu2,
														smartAlbumSearchTypeMenu3,
														smartAlbumOperatorMenu3,
														smartAlbumOptionMenu3,
														
														movieSheet,
														movieLabel;
	NSWindow											*fullScreenWindow;
	OsirixXMLRPC										*osirixXMLRPC;									// Reference held purely to satisfy CLANG static analyzer.
	}

- (IBAction) exportImages: (id) sender;
- (IBAction) exportToKeynote: (id) sender;
- (IBAction) exportAsMIRCArchive: (id) sender;
- (IBAction) exportToMIRCServer: (id) sender;

- (IBAction) editImage: (id) sender;
- (IBAction) editMovie: (id) sender;
- (void) fileTableViewWasSelected;
- (void) imageTableViewWasSelected;

- (IBAction) enterFullScreen: (id) sender;
- (IBAction) exitFullScreen: (id) sender;
- (IBAction) toggleHUDWindow: (id) sender;
- (IBAction) selectFullScreenTab: (id) sender;

- (void) editPatient: (PatientEntity*) aPatientEntity;

- (void) updateForAlbumSelection;
- (void) updateForBrowserSelection;

- (IBAction) checkUniqueID: (id) sender;

- (IBAction) linkTo: (id) sender;
- (IBAction) linkFrom: (id) sender;

@property (nonatomic, readonly, copy) NSString *systemPredicateString;
@property (nonatomic, readonly, copy) NSString *pathologyPredicateString;
@property (nonatomic, readonly, copy) NSString *regionPredicateString;

- (IBAction) anonymiseAll: (id) sender;
- (IBAction) convertAllToJPEG: (id) sender;

@property (nonatomic, readonly, strong) id imageView;
- (void) incrementPreviewImage;
- (void) decrementPreviewImage;

@end


