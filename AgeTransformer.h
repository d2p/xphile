/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/




// Localised strings.

#define AgeTransformerLocalizedString(str)								NSLocalizedStringFromTable((str), @"AgeTransformer", (str))

#define WeeksDaysString													AgeTransformerLocalizedString(@"%ldw %ldd")
#define MonthsDaysString												AgeTransformerLocalizedString(@"%ldm %ldd")
#define YearsMonthsString												AgeTransformerLocalizedString(@"%ldy %ldm")

#define SingularDayString												AgeTransformerLocalizedString(@"day")
#define SingularWeekString												AgeTransformerLocalizedString(@"week")
#define SingularMonthString												AgeTransformerLocalizedString(@"month")
#define SingularYearString												AgeTransformerLocalizedString(@"year")
#define PluralDaysString												AgeTransformerLocalizedString(@"days")
#define PluralWeeksString												AgeTransformerLocalizedString(@"weeks")
#define PluralMonthsString												AgeTransformerLocalizedString(@"months")
#define PluralYearsString												AgeTransformerLocalizedString(@"years")



@interface AgeTransformer : NSValueTransformer
@end


@interface TimeSinceTransformer : NSValueTransformer
@end


@interface TimeCourseTransformer : NSValueTransformer
@end
