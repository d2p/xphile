/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "PatientArrayController.h"
#import "PatientEntity.h"


@implementation PatientArrayController


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Required to Complete Data Source Delegate Informal Protocol

- (NSInteger) numberOfRowsInTableView: (NSTableView* __unused) aTableView
	{	
	return 0;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (id) tableView: (NSTableView* __unused) aTableView objectValueForTableColumn: (NSTableColumn* __unused) aTableColumn row: (NSInteger __unused) rowIndex
	{
	return nil;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark - Table Destination Drop Routines

- (NSDragOperation) tableView: (NSTableView*) tv validateDrop: (id <NSDraggingInfo>) info proposedRow: (NSInteger __unused) row proposedDropOperation: (NSTableViewDropOperation) op
	{
	
// Don't valiate if the originating document is different.

	if ([[[[info draggingSource] window] windowController] document] != [[[tv window] windowController] document])
		return (NSDragOperationNone);

	if (op==NSTableViewDropOn)
		return NSDragOperationEvery;
	
    return NSDragOperationNone;    
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) tableView: (NSTableView* __unused) aTableView acceptDrop: (id <NSDraggingInfo>) info row: (NSInteger) rowIndex
  dropOperation: (NSTableViewDropOperation __unused) operation
	{
	ASSERTNOTNIL(info);

// Get the managed object context.
	
	NSManagedObjectContext *moc=[self managedObjectContext];ASSERTNOTNIL(moc);

// Get the dropped on patient.

	PatientEntity *toPatient=[[self arrangedObjects] objectAtIndex: rowIndex];ASSERTNOTNIL(toPatient);

// Get the moURIArray from the pasteboard.

	NSPasteboard* pboard=[info draggingPasteboard];ASSERTNOTNIL(pboard);
	NSData* moURIArrayData=[pboard dataForType: InDocumentBrowserDragType];ASSERTNOTNIL(moURIArrayData);
	NSArray *moURIArray=[NSKeyedUnarchiver unarchiveObjectWithData: moURIArrayData];ASSERTNOTNIL(moURIArray);

// Loop through the array, converting the moURI to an moID and then to its respective managed object.
// Don't link identical patients.
	
	NSUInteger index;
	for (index=0; index<[moURIArray count]; index++)
		{
		NSURL *moURI=[moURIArray objectAtIndex: index];ASSERTNOTNIL(moURI);
		NSManagedObjectID *moID=[[moc persistentStoreCoordinator] managedObjectIDForURIRepresentation: moURI];
		NSManagedObject *condition=[moc objectRegisteredForID: moID];ASSERTNOTNIL(condition);
		PatientEntity *draggedPatient=[condition valueForKeyPath: @"patient"];ASSERTNOTNIL(draggedPatient);
		
		if (![toPatient isEqualTo: draggedPatient])
			{
			NSMutableSet *set=[draggedPatient mutableSetValueForKeyPath: @"linkedToPatients"];ASSERTNOTNIL(set);
			[set addObject: toPatient];			
			}
		}
	
	return (YES);
	}


@end
