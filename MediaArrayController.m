/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "MediaArrayController.h"
#import "MediaEntity.h"


@implementation MediaArrayController


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark-
#pragma mark Import Files


- (NSArray*) acceptableFileTypes
	{
	return (nil);
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (IBAction) chooseMedia: (id __unused) sender
	{

// Exit if no selection.

	NSArray *selectionArray=[self selectedObjects];ASSERTNOTNIL(selectionArray);
	if ([selectionArray count] == 0)
		return;

// Choose a file.

	NSOpenPanel *panel=[NSOpenPanel openPanel];ASSERTNOTNIL(panel);
	[panel setPrompt: ChooseString];
	[panel setCanChooseDirectories: NO];
	[panel setCanChooseFiles: YES];
	[panel setAllowedFileTypes: [self acceptableFileTypes]];
	if ([panel runModal]==NSCancelButton)
		return;
	
// Set the data for the currently selected entity.

	MediaEntity *mediaEntity=[selectionArray objectAtIndex: 0];
	[mediaEntity setMediaFromPath: [[panel URL] path]];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Drag

- (BOOL) tableView: (NSTableView* __unused) aTableView writeRowsWithIndexes: (NSIndexSet*) rowIndexes toPasteboard: (NSPasteboard*) pboard
	{
	ASSERTNOTNIL(rowIndexes);
	ASSERTNOTNIL(pboard);

	[self declareTypesForPasteboard: pboard];
	[self setDataForPasteboard: pboard withRowIndexes: rowIndexes];
	
    return YES;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) declareTypesForPasteboard: (NSPasteboard*) pboard
	{
	NSArray *typesArray=[NSArray arrayWithObjects: MediaOrderDragType, NSFilesPromisePboardType, nil];ASSERTNOTNIL(typesArray);
	[pboard declareTypes: typesArray owner: self];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setDataForPasteboard: (NSPasteboard*) pboard withRowIndexes: (NSIndexSet*) rowIndexes
	{
	[pboard setData: [NSKeyedArchiver archivedDataWithRootObject: rowIndexes] forType: MediaOrderDragType];
	[pboard setPropertyList: [NSArray arrayWithObject: @"jpg"] forType: NSFilesPromisePboardType];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSArray*) tableView: (NSTableView* __unused) aTableView namesOfPromisedFilesDroppedAtDestination: (NSURL*) dropDestination forDraggedRowsWithIndexes: (NSIndexSet*) indexSet
	{
	
// Set up the array file names written.

	NSMutableArray *fileNameArray=[NSMutableArray arrayWithCapacity: 0];ASSERTNOTNIL(fileNameArray);

// Get the base bath from the URL.

	NSString *basePath=[dropDestination path];ASSERTNOTNIL(basePath);

// Iterate the selected items and write them at base path.

	NSInteger index=[indexSet firstIndex];
	while (index!=NSNotFound)
		{
		MediaEntity *aMedium=[[self arrangedObjects] objectAtIndex: index];ASSERTNOTNIL(aMedium);
		NSString *path=[aMedium uniqueFileNameForFolder: basePath];ASSERTNOTNIL(path);
		[fileNameArray addObject: [path lastPathComponent]];
		[[aMedium rawData] writeToFile: path atomically: YES];

		index=[indexSet indexGreaterThanIndex: index];
		}

	return (fileNameArray);
	}
		
		
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark-
#pragma mark Drop

- (NSDragOperation) tableView: (NSTableView*) aTableView validateDrop: (id <NSDraggingInfo>) info proposedRow: (NSInteger __unused) row proposedDropOperation: (NSTableViewDropOperation) op 
	{
	
// See if we have MediaOrderDragType.

	if ([[[info draggingPasteboard] types] containsObject: MediaOrderDragType])
		{
		if (([info draggingSource] == aTableView) && (op==NSTableViewDropAbove))
			return NSDragOperationEvery;
		else
			return NSDragOperationNone;
		}
	
	return NSDragOperationNone;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) tableView: (NSTableView* __unused) aTableView acceptDrop: (id <NSDraggingInfo>) info row: (NSInteger) destinationRow
  dropOperation: (NSTableViewDropOperation __unused) operation
	{
		
// Moving image order?
		
	if ([[[info draggingPasteboard] types] containsObject: MediaOrderDragType])
		{
		NSData *draggedRowIndexesData=[[info draggingPasteboard] dataForType: MediaOrderDragType];
		if (!draggedRowIndexesData) return YES;
		NSIndexSet *draggedRowIndexes=[NSKeyedUnarchiver unarchiveObjectWithData: draggedRowIndexesData];
		if (!draggedRowIndexes) return YES;
		NSArray *images=[self arrangedObjects];ASSERTNOTNIL(images);
		
		NSUInteger numDraggedRows=[draggedRowIndexes count];
		NSInteger firstDraggedRow=[draggedRowIndexes firstIndex];
		if (destinationRow > firstDraggedRow)
			destinationRow-=numDraggedRows;

		NSMutableIndexSet *availableIndices=[NSMutableIndexSet indexSetWithIndexesInRange: NSMakeRange(0, [images count])];ASSERTNOTNIL(availableIndices);
		NSUInteger index;
		for (index=0; index<[images count]; index++)
			{
			if ([draggedRowIndexes containsIndex: index])
				{
				[[images objectAtIndex: index] setValue: [NSNumber numberWithInteger:destinationRow] forKey: @"order"];
				[availableIndices removeIndex: destinationRow];
				destinationRow++;
				}
			}

		for (index=0; index<[images count]; index++)
			{
			if (![draggedRowIndexes containsIndex: index])
				{
				[[images objectAtIndex: index] setValue: [NSNumber numberWithInteger:[availableIndices firstIndex]] forKey: @"order"];
				[availableIndices removeIndex: [availableIndices firstIndex]];
				}
			}
	
		return YES;
		}

	return NO;	
	}
	

@end
