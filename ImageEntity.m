/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/

#import <QuartzCore/QuartzCore.h>

#import "ConditionEntity.h"
#import "ImageEntity.h"
#import "NSImage+Additions.h"
#import "NSImageView+Additions.h"
#import "PatientEntity.h"
#import "StudyEntity.h"




@implementation ImageEntity


+ (NSSet*) keyPathsForValuesAffectingSuffixString
	{
	return [NSSet setWithObject: @"imageData"];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (NSString*) suffixString
	{
	NSString *suffixStr=@"";ASSERTNOTNIL(suffixStr);

	NSData *data=[self valueForKeyPath: @"imageData"];
	if (data==nil)
		return (suffixStr);
		
	NSData *rawData=[NSUnarchiver unarchiveObjectWithData: data];ASSERTNOTNIL(rawData);
	const char *buffer = [rawData bytes];

	if ( ((unsigned char) buffer[0]=='G') && ((unsigned char) buffer[1]=='I') && ((unsigned char) buffer[2]=='F') )
		suffixStr=@"gif";
	else if ( ((unsigned char) buffer[0]==0xFF) && ((unsigned char) buffer[1]==0xD8) && ((unsigned char) buffer[2]==0xFF) )
		suffixStr=@"jpg";
	else if ( ((unsigned char) buffer[0]==0x89) && ((unsigned char) buffer[1]=='P') && ((unsigned char) buffer[2]=='N') && ((unsigned char) buffer[3]=='G')  && ((unsigned char) buffer[4]==0x0D)  && ((unsigned char) buffer[5]==0x0A)  && ((unsigned char) buffer[6]==0x1A)  && ((unsigned char) buffer[7]==0x0A) )
		suffixStr=@"png";
	else if ( ((unsigned char) buffer[0]=='8') && ((unsigned char) buffer[1]=='B') && ((unsigned char) buffer[2]=='P')  && ((unsigned char) buffer[3]=='S') )
		suffixStr=@"psd";
	else if ( ((unsigned char) buffer[0]=='I') && ((unsigned char) buffer[1]=='I') )
		suffixStr=@"tif";
	else if ( ((unsigned char) buffer[0]=='M') && ((unsigned char) buffer[1]=='M') )
		suffixStr=@"tif";

	return (suffixStr);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) defaultFileName
	{
	NSString					*studyDateStr	=	nil;
	
	
	NSDate *studyDate=[self valueForKeyPath: @"study.dateOfStudy"];
	if (studyDate)
		studyDateStr=[studyDate descriptionWithCalendarFormat: @"%y%m%d" timeZone: nil locale: nil];
	else
		studyDateStr=@"-";

// Start with patient identifier, set to empty string if not yet set.
	
	NSString *idString=[self valueForKeyPath: @"study.condition.patient.identifier"];
	if (idString==nil)
		idString=@"";
	ASSERTNOTNIL(idString);

// Ensure no '/' in patient ID, converting to '-'.

	NSArray *idComponentArray=[idString pathComponents];ASSERTNOTNIL(idComponentArray);
	idString=[idComponentArray componentsJoinedByString: @"-"];ASSERTNOTNIL(idString);
	
	return [NSString stringWithFormat: @"%@ %@ %@ %@.%@",
	  idString,
	  [self valueForKeyPath: @"study.modalityText"],
	  [self valueForKeyPath: @"study.studyRegionText"],
	  studyDateStr,
	  [self suffixString]];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSData*) rawData
	{
	NSData *data=[self valueForKeyPath: @"imageData"];ASSERTNOTNIL(data);
	NSData *rawData=[NSUnarchiver unarchiveObjectWithData: data];ASSERTNOTNIL(rawData);
	return (rawData);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Media Handling

- (void) setMediaFromPath: (NSString*) path
	{
	
// Evaluate size of image file and if too large default to fail image with localised text.

	NSError *error=nil;
	NSDictionary *dict=[[NSFileManager defaultManager] attributesOfItemAtPath: path error: &error];ASSERTNOTNIL(dict);
	if (dict == nil)
		{
		NSLog(@"Error: %@", [error description]);
		}
	NSUInteger size=[[dict valueForKey: NSFileSize] unsignedIntegerValue];
	NSUInteger maxImageSize=[[NSUserDefaults standardUserDefaults] integerForKey: MaximumImageSizeKey];
	if (size > maxImageSize)
		{
		NSImage *failImage=[NSImage imageNamed: @"ImageTooLarge"];ASSERTNOTNIL(failImage);
		[failImage lockFocus]; 
				
		NSRect imRect=NSMakeRect(0, 0, [failImage size].width, [failImage size].height/2);
		[failImage drawAtPoint: NSMakePoint(0, 0) fromRect: imRect operation: NSCompositeCopy fraction: 1.0];
		
		NSMutableDictionary	*attributes=[[[NSMutableDictionary alloc] init] autorelease];ASSERTNOTNIL(attributes);
		[attributes setValue: [NSFont labelFontOfSize: 28] forKey: NSFontAttributeName];
		[attributes setValue: [NSColor colorWithCalibratedRed: 0.0 green: 0.5 blue: 1.0 alpha: 1.0] forKey: NSForegroundColorAttributeName];
		NSMutableParagraphStyle *paragraphStyle=[[[NSParagraphStyle defaultParagraphStyle] mutableCopy] autorelease];ASSERTNOTNIL(paragraphStyle);
		[paragraphStyle setAlignment: NSCenterTextAlignment];
		[attributes setValue: paragraphStyle forKey: NSParagraphStyleAttributeName];
		[ImageTooLargeString drawInRect: imRect withAttributes: attributes]; 

		[failImage unlockFocus];
		NSData *imData=[failImage JPEGRepresentationWithCompressionFactor: 0.8];ASSERTNOTNIL(imData);
		[self setImageFromData: imData];
		return;
		}
	
	[super setMediaFromPath: path];
	
	NSData *imageData=[NSData dataWithContentsOfFile: path];ASSERTNOTNIL(imageData);
	[self setImageFromData: imageData];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setImageFromData: (NSData*) imageData
	{
	ASSERTNOTNIL(imageData);

// Resize if not 1:1 size:pixel relationship (faster).

	NSImage *theImage=[[[NSImage alloc] initWithData: imageData] autorelease];ASSERTNOTNIL(theImage);
	NSBitmapImageRep *bestRep=[NSBitmapImageRep imageRepWithData: [theImage TIFFRepresentation]];ASSERTNOTNIL(bestRep);
	if ([bestRep pixelsHigh] != [theImage size].height || [bestRep pixelsWide] != [theImage size].width)
		{
		NSBitmapImageRep *bitmapImageRep=[[[NSBitmapImageRep alloc] initWithData: imageData] autorelease];ASSERTNOTNIL(bitmapImageRep);
		[bitmapImageRep setSize: NSMakeSize([bitmapImageRep pixelsWide], [bitmapImageRep pixelsHigh])];
		theImage=[[[NSImage alloc] initWithSize: [bitmapImageRep size]] autorelease];ASSERTNOTNIL(theImage);
		[theImage addRepresentation: bitmapImageRep];
		imageData=[theImage JPEGRepresentationWithCompressionFactor: JPEGCompressionFactor];ASSERTNOTNIL(imageData);
		}

	NSData *imageArchive=[NSArchiver archivedDataWithRootObject: imageData];ASSERTNOTNIL(imageArchive);
	[self setValue: imageArchive forKeyPath: @"imageData"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSImage*) image
	{
	[self willAccessValueForKey: @"image"];
	NSImage *anImage = [self primitiveValueForKey: @"image"];
	[self didAccessValueForKey: @"image"];
	
	if (anImage == nil)
		{
		NSData *imageArchive=[self valueForKey: @"imageData"];
		if (imageArchive != nil)
			{
			NSData *imageData=[NSUnarchiver unarchiveObjectWithData: imageArchive];ASSERTNOTNIL(imageData);
			anImage=[[[NSImage alloc] initWithData: imageData] autorelease];ASSERTNOTNIL(anImage);
			[self setPrimitiveValue: anImage forKey: @"image"];
			}
		}
		
	return anImage;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) setImage: (NSImage* __unused) anImage
	{
// NO-OP.
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) reStoreImageAsJPEG
	{
		
// Ensure image is available.

	NSData *currentImageArchive=[self valueForKey: @"imageData"];ASSERTNOTNIL(currentImageArchive);
	if (currentImageArchive == nil)
		return;
	NSData *currentImageData=[NSUnarchiver unarchiveObjectWithData: currentImageArchive];ASSERTNOTNIL(currentImageData);
	NSImage *initialImage=[[[NSImage alloc] initWithData: currentImageData] autorelease];ASSERTNOTNIL(initialImage);
	
// Update the bound image.

	NSValueTransformer *dataImageTransformer=[NSValueTransformer valueTransformerForName: @"DataToImageTransformer"];ASSERTNOTNIL(dataImageTransformer);
	NSData *newImageArchive=[dataImageTransformer reverseTransformedValue: initialImage];ASSERTNOTNIL(newImageArchive);
	[self setValue: newImageArchive forKeyPath: @"imageData"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSData*) imageDataForMIRC
	{
	NSData *currentImageArchive=[self valueForKey: @"imageData"];ASSERTNOTNIL(currentImageArchive);
	if (currentImageArchive == nil)
		return nil;
	NSData *currentImageData=[NSUnarchiver unarchiveObjectWithData: currentImageArchive];ASSERTNOTNIL(currentImageData);
	NSImage *originalImage=[[[NSImage alloc] initWithData: currentImageData] autorelease];ASSERTNOTNIL(originalImage);
	
// If the image size is within limits return its data.

	NSUInteger maxWidth=[[NSUserDefaults standardUserDefaults] integerForKey: MIRCImageWidthKey];
	NSUInteger maxHeight=[[NSUserDefaults standardUserDefaults] integerForKey: MIRCImageHeightKey];
	NSSize originalSize=[originalImage size];
	if (originalSize.width <= maxWidth && originalSize.height <= maxHeight)
		{
		return (currentImageData);
		}
	
// Otherwise calculate new size, scaling to for the larger dimension.
	
	CGFloat xScale=originalSize.width / maxWidth;
	CGFloat yScale=originalSize.height / maxHeight;
	CGFloat scaleFactor=xScale>yScale ? xScale : yScale;
	
	CGFloat resizeWidth=originalSize.width / scaleFactor;
	CGFloat resizeHeight=originalSize.height / scaleFactor;	

	NSImage *resizedImage=[[[NSImage alloc] initWithSize: NSMakeSize(resizeWidth, resizeHeight)] autorelease];ASSERTNOTNIL(resizedImage);
	[resizedImage lockFocus];
	[originalImage drawInRect: NSMakeRect(0, 0, resizeWidth, resizeHeight) fromRect: NSMakeRect(0, 0, originalSize.width, originalSize.height) operation: NSCompositeSourceOver fraction: 1.0];
	[resizedImage unlockFocus];
	
	NSData *resizedImageData=[resizedImage JPEGRepresentationWithCompressionFactor: JPEGCompressionFactor];ASSERTNOTNIL(resizedImageData);
	return resizedImageData;
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Geometric Transformations.


- (void) flipXBy: (CGFloat) x yBy: (CGFloat) y
	{
		
// Ensure image is available and convert to a CIImage.

	NSData *currentImageArchive=[self valueForKey: @"imageData"];ASSERTNOTNIL(currentImageArchive);
	if (currentImageArchive == nil)
		return;
	NSData *currentImageData=[NSUnarchiver unarchiveObjectWithData: currentImageArchive];ASSERTNOTNIL(currentImageData);
	CIImage *ciImage=[CIImage imageWithData: currentImageData];ASSERTNOTNIL(ciImage);

// Create the affine transform filter.

	CIFilter *atFilter=[CIFilter filterWithName: @"CIAffineTransform"];ASSERTNOTNIL(atFilter);
	NSAffineTransform *at=[NSAffineTransform transform];ASSERTNOTNIL(at); 
	[at scaleXBy: x yBy: y];
	[at concat];

	[atFilter setValue: ciImage forKey: @"inputImage"];
	[atFilter setValue: at forKey: @"inputTransform"];

// Apply the filter and convert to an image representation.
	
	ciImage=[atFilter valueForKey: @"outputImage"];ASSERTNOTNIL(ciImage);
	NSCIImageRep *ciImageRep=[NSCIImageRep imageRepWithCIImage: ciImage];ASSERTNOTNIL(ciImageRep);

// Create the new NSImage.
	
	CGRect imageExtent=[ciImage extent];
	NSSize newSize=NSMakeSize(imageExtent.size.width, imageExtent.size.height);
	NSImage *resultImage=[[[NSImage alloc] initWithSize: newSize] autorelease];ASSERTNOTNIL(resultImage);
	
	[resultImage lockFocus];
	NSRect rect=NSMakeRect(0, 0, newSize.width, newSize.height);
	[[NSColor blackColor] set];
	[NSBezierPath fillRect: rect];
	[ciImageRep draw];
	[resultImage unlockFocus];

// Update the bound image.

	NSValueTransformer *dataImageTransformer=[NSValueTransformer valueTransformerForName: @"DataToImageTransformer"];ASSERTNOTNIL(dataImageTransformer);
	NSData *newImageArchive=[dataImageTransformer reverseTransformedValue: resultImage];ASSERTNOTNIL(newImageArchive);
	[self setValue: newImageArchive forKeyPath: @"imageData"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) flipHorizontal
	{
	[self flipXBy: -1.0 yBy: 1.0];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) flipVertical
	{
	[self flipXBy: 1.0 yBy: -1.0];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) rotateBy: (CGFloat) degrees
	{
	NSAssert1((degrees==-90.0 || degrees==90.0), @"Degrees should be +/-90, %f", degrees);
	
// Ensure image is available and convert to a CIImage.

	NSData *currentImageArchive=[self valueForKey: @"imageData"];ASSERTNOTNIL(currentImageArchive);
	if (currentImageArchive == nil)
		return;
	NSData *currentImageData=[NSUnarchiver unarchiveObjectWithData: currentImageArchive];ASSERTNOTNIL(currentImageData);
	CIImage *ciImage=[CIImage imageWithData: currentImageData];ASSERTNOTNIL(ciImage);
	CGRect imageExtent=[ciImage extent];

// Create the affine transform filter, giving it a dummy image to base itself in.

	NSSize affineSize=NSMakeSize(imageExtent.size.height, imageExtent.size.width);
	NSImage *affineImage=[[[NSImage alloc] initWithSize: affineSize] autorelease];ASSERTNOTNIL(affineImage);
	[affineImage lockFocus];
	
	CIFilter *atFilter=[CIFilter filterWithName: @"CIAffineTransform"];ASSERTNOTNIL(atFilter);
	NSAffineTransform *at=[NSAffineTransform transform];ASSERTNOTNIL(at); 
	[at translateXBy: imageExtent.size.width/2.0 yBy: imageExtent.size.height/2.0];
	[at rotateByDegrees: degrees];
	[at translateXBy: -imageExtent.size.width/2.0 yBy: -imageExtent.size.height/2.0];
	[at concat];ASSERTNOTNIL(at); 
	
	[affineImage unlockFocus];

	[atFilter setValue: ciImage forKey: @"inputImage"];
	[atFilter setValue: at forKey: @"inputTransform"];

// Apply the filter.
	
	ciImage=[atFilter valueForKey: @"outputImage"];ASSERTNOTNIL(ciImage);

// Convert to an image representation.

	NSCIImageRep *ciImageRep=[NSCIImageRep imageRepWithCIImage: ciImage];ASSERTNOTNIL(ciImageRep);

// Create the new NSImage, reversing height and width for either -90 or 90 degrees.
	
	NSSize newSize=NSMakeSize(imageExtent.size.height, imageExtent.size.width);
	NSImage *resultImage=[[[NSImage alloc] initWithSize: newSize] autorelease];ASSERTNOTNIL(resultImage);
	
	[resultImage lockFocus];
	NSRect rect=NSMakeRect(0, 0, newSize.width, newSize.height);
	[[NSColor blackColor] set];
	[NSBezierPath fillRect: rect];
	[ciImageRep draw];
	[resultImage unlockFocus];

// Update the bound image.

	NSValueTransformer *dataImageTransformer=[NSValueTransformer valueTransformerForName: @"DataToImageTransformer"];ASSERTNOTNIL(dataImageTransformer);
	NSData *newImageArchive=[dataImageTransformer reverseTransformedValue: resultImage];ASSERTNOTNIL(newImageArchive);
	[self setValue: newImageArchive forKeyPath: @"imageData"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) rotate90
	{
	[self rotateBy: -90.0];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) rotate180
	{
	[self flipXBy: -1.0 yBy: -1.0];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) rotate270
	{
	[self rotateBy: 90.0];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Cropping

- (void) cropFromView: (CroppableView*) cropView
	{

// Ensure image is available and convert to a CIImage.

	NSData *currentImageArchive=[self valueForKey: @"imageData"];ASSERTNOTNIL(currentImageArchive);
	if (currentImageArchive == nil)
		return;
	NSData *currentImageData=[NSUnarchiver unarchiveObjectWithData: currentImageArchive];ASSERTNOTNIL(currentImageData);
	CIImage *ciImage=[CIImage imageWithData: currentImageData];ASSERTNOTNIL(ciImage);

// Get the original image rect and that scaled for the current view.

	NSRect originalRect=NSMakeRect(0, 0, [ciImage extent].size.width, [ciImage extent].size.height);
	NSRect scaledImageRect=[cropView actualRectForImageView];

// Work out factors for original to scaled images.

	CGFloat scaleX=originalRect.size.width / scaledImageRect.size.width;
	CGFloat scaleY=originalRect.size.height / scaledImageRect.size.height;

// Get the crop rect.  Scale and offset it in relation to the original image.
	
	SelectionPath *selectionPath=[cropView selectionMarker];ASSERTNOTNIL(selectionPath);
	NSRect cropRect=[selectionPath selectedRect];
	cropRect=NSIntersectionRect(cropRect, scaledImageRect);
	if (NSEqualSizes(cropRect.size, NSZeroSize))
		return;

// Scale and offset the crop rect in relation to the original image.

	cropRect=NSOffsetRect(cropRect, -NSMinX(scaledImageRect), -NSMinY(scaledImageRect));
	cropRect.origin.x *= scaleX;
	cropRect.origin.y *= scaleY;
	cropRect.size.width *= scaleX;
	cropRect.size.height *= scaleY;

// Create the crop filter.

	CIFilter *cropFilter=[CIFilter filterWithName: @"CICrop"];ASSERTNOTNIL(cropFilter);
	[cropFilter setValue: ciImage forKey: @"inputImage"];
	[cropFilter setValue: [CIVector vectorWithX: cropRect.origin.x Y: cropRect.origin.y Z: cropRect.size.width W: cropRect.size.height] forKey: @"inputRectangle"];

// Apply the filter and convert to an image representation.
	
	ciImage=[cropFilter valueForKey: @"outputImage"];ASSERTNOTNIL(ciImage);
	NSCIImageRep *ciImageRep=[NSCIImageRep imageRepWithCIImage: ciImage];ASSERTNOTNIL(ciImageRep);

// Create the new NSImage.
	
	CGRect imageExtent=[ciImage extent];
	NSSize newSize=NSMakeSize(imageExtent.size.width, imageExtent.size.height);
	NSImage *resultImage=[[[NSImage alloc] initWithSize: newSize] autorelease];ASSERTNOTNIL(resultImage);

	[resultImage lockFocus];
	NSRect rect=NSMakeRect(0, 0, newSize.width, newSize.height);
	[[NSColor blackColor] set];
	[NSBezierPath fillRect: rect];
	[ciImageRep draw];
	[resultImage unlockFocus];

// Update the bound image.

	NSValueTransformer *dataImageTransformer=[NSValueTransformer valueTransformerForName: @"DataToImageTransformer"];ASSERTNOTNIL(dataImageTransformer);
	NSData *newImageArchive=[dataImageTransformer reverseTransformedValue: resultImage];ASSERTNOTNIL(newImageArchive);
	[self setValue: newImageArchive forKeyPath: @"imageData"];
	}



@end
