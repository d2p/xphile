/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "ConditionEntity.h"
#import "FileEntity.h"
#import "ImageEntity.h"


@implementation ConditionEntity

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingDaysOldAtOnset
	{
	return [NSSet setWithObjects: @"patient.dateOfBirth", @"dateOfOnset", nil];
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingAbbreviatedAgeAtOnset
	{
	return [NSSet setWithObjects: @"patient.dateOfBirth", @"dateOfOnset", nil];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingSystemText
	{
	return [NSSet setWithObject: @"system"];
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingPathologyText
	{
	return [NSSet setWithObject: @"pathology"];
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

+ (NSSet*) keyPathsForValuesAffectingConditionRegionText
	{
	return [NSSet setWithObject: @"conditionRegion"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (id) valueForUndefinedKey: (NSString*) key
	{
	NSLog(@"ConditionEntity: no such key - %@", key);
	return (nil);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) awakeFromFetch
	{
	previewImageIndex=0;
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Age

- (NSInteger) daysOldAtOnset
	{
	NSDate *birthDate=[self valueForKeyPath: @"patient.dateOfBirth"];
	if (!birthDate) return (UnspecifiedAge);
	NSDate *conditionDate=[self valueForKeyPath: @"dateOfOnset"];
	if (!conditionDate) return (UnspecifiedAge);
	
	NSInteger days=ceil([conditionDate timeIntervalSinceDate: birthDate] / (60*60*24));
	return (days);
	}
	
	
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) abbreviatedAgeAtOnset
	{
	NSDate *birthDate=[self valueForKeyPath: @"patient.dateOfBirth"];
	if (!birthDate) return (@"?");
	NSDate *conditionDate=[self valueForKeyPath: @"dateOfOnset"];
	if (!conditionDate) return (@"?");
		
	NSInteger days=ceil([conditionDate timeIntervalSinceDate: birthDate] / (60*60*24));
	NSInteger months=floor(days/(365/12));
	NSInteger years=floor(days/365);

	if (days<0)
		return (@"?");

	if (years==0 && days<14)
		return [NSString stringWithFormat: AbbreviatedDaysString, days];
	else if (years==0 && days<90)
		{
		NSInteger weeks=round(days/7);
		return [NSString stringWithFormat: AbbreviatedWeeksString, weeks];
		}
	else if (years==0)
		return [NSString stringWithFormat: AbbreviatedMonthsString, months];
	else
		return [NSString stringWithFormat: AbbreviatedYearsString, years];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Text Labels

- (NSString*) systemText
	{
	NSInteger systemTag=[[self valueForKeyPath: @"system"] integerValue];
	return [[NSUserDefaults standardUserDefaults] valueForKeyPath: SystemArrayKey][systemTag];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) pathologyText
	{
	NSInteger pathologyTag=[[self valueForKeyPath: @"pathology"] integerValue];
	return [[NSUserDefaults standardUserDefaults] valueForKeyPath: PathologyArrayKey][pathologyTag];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) conditionRegionText
	{
	NSInteger regionTag=[[self valueForKeyPath: @"conditionRegion"] integerValue];
	return [[NSUserDefaults standardUserDefaults] valueForKeyPath: RegionArrayKey][regionTag];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Preview Image


- (NSUInteger) numberOfImages
	{
	NSInteger					totalImages	=	0;
	
	
	NSSet *studies=[self valueForKeyPath: @"studies"];
	if (studies==nil)
		return 0;

	NSEnumerator *studyEnumerator = [studies objectEnumerator];ASSERTNOTNIL(studyEnumerator);
	id aStudy;
	while ((aStudy=[studyEnumerator nextObject]))
		totalImages+=[aStudy numberOfImages];
	
	return (totalImages);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSUInteger) numberOfMovies
	{
	NSInteger					totalMovies	=	0;
	
	
	NSSet *studies=[self valueForKeyPath: @"studies"];
	if (studies==nil)
		return 0;

	NSEnumerator *studyEnumerator = [studies objectEnumerator];ASSERTNOTNIL(studyEnumerator);
	id aStudy;
	while ((aStudy=[studyEnumerator nextObject]))
		totalMovies+=[aStudy numberOfMovies];
	
	return (totalMovies);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSArray*) mediaArray
	{
	NSSet *studies=[self valueForKeyPath: @"studies"];
	if (studies==nil)
		return nil;

	NSMutableArray *studyArray=[[[studies allObjects] mutableCopy] autorelease];				ASSERTNOTNIL(studyArray);
	NSSortDescriptor *sortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"dateOfStudy" ascending: YES] autorelease];
	ASSERTNOTNIL(sortDescriptor);
	[studyArray sortUsingDescriptors:@[sortDescriptor]];
	
	NSMutableArray *mediaArray=[NSMutableArray arrayWithCapacity: 0];							ASSERTNOTNIL(mediaArray);
	sortDescriptor=[[[NSSortDescriptor alloc] initWithKey: @"order" ascending: YES] autorelease];ASSERTNOTNIL(sortDescriptor);
	
	NSUInteger index;
	for (index=0; index<[studyArray count]; index++)
		{
		StudyEntity *aStudy=studyArray[index];									ASSERTNOTNIL(aStudy);
		
// Images.

		NSSet *images=[aStudy valueForKeyPath: @"images"];										ASSERTNOTNIL(images);
		NSMutableArray *imageArray=[[[images allObjects] mutableCopy] autorelease];				ASSERTNOTNIL(imageArray);
		[mediaArray addObjectsFromArray: [imageArray sortedArrayUsingDescriptors: @[sortDescriptor]]];

// Movies.

		NSSet *movies=[aStudy valueForKeyPath: @"files"];										ASSERTNOTNIL(movies);
		NSMutableArray *movieArray=[[[movies allObjects] mutableCopy] autorelease];				ASSERTNOTNIL(movieArray);
		[mediaArray addObjectsFromArray: [movieArray sortedArrayUsingDescriptors: @[sortDescriptor]]];
		}
	
	return (mediaArray);
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSImage*) previewImage
	{
	NSArray* mediaArray=[self mediaArray];
	
	if (mediaArray==nil)
		return (nil);
	if ([mediaArray count] == 0)
		return (nil);
	
	id imageObject=mediaArray[previewImageIndex];								ASSERTNOTNIL(imageObject);

	if ([imageObject isMemberOfClass: [ImageEntity class]])
		{
		NSData *data=[imageObject valueForKeyPath: @"imageData"];
		if (data)
			{
			NSData *imageData=[NSUnarchiver unarchiveObjectWithData: data];						ASSERTNOTNIL(imageData);
			NSImage *im=[[[NSImage alloc] initWithData: imageData] autorelease];				ASSERTNOTNIL(im);
			return (im);
			}
		}
		
	if ([imageObject isMemberOfClass: [FileEntity class]])
		{
		QTMovie *qtMovie=[imageObject qtMovie];
		if (qtMovie)
			{
			NSImage *im=[qtMovie posterImage];													ASSERTNOTNIL(im);						
			return (im);
			}
		}

	return (nil);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) incrementPreviewImageIndex
	{

// These routines access non-declared (in the model) transient attributes: previewIamge.  previewImageIndex is an instance variable.
// Process any valid UI changes then temporarily disable undo recording when changing the previewImage attribute.

	NSUInteger totalConditionImages=[self numberOfImages] + [self numberOfMovies];

	if (previewImageIndex == totalConditionImages-1)
		return;
	
	[[self managedObjectContext] processPendingChanges];
	[[[self managedObjectContext] undoManager] disableUndoRegistration];

	[self willChangeValueForKey: @"previewImage"];
	previewImageIndex++;
	[self didChangeValueForKey: @"previewImage"];

	[[self managedObjectContext] processPendingChanges];
	[[[self managedObjectContext] undoManager] enableUndoRegistration];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) decrementPreviewImageIndex
	{
	if (previewImageIndex == 0)
		return;
		
	[[self managedObjectContext] processPendingChanges];
	[[[self managedObjectContext] undoManager] disableUndoRegistration];

	[self willChangeValueForKey: @"previewImage"];
	previewImageIndex--;
	[self didChangeValueForKey: @"previewImage"];

	[[self managedObjectContext] processPendingChanges];
	[[[self managedObjectContext] undoManager] enableUndoRegistration];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -

- (NSString*) allStudyDescriptions
	{
	
// Return an array of all study descriptions in this condition.

	NSMutableArray *stringArray=[NSMutableArray arrayWithCapacity: 0];			ASSERTNOTNIL(stringArray);
	
	NSSet *studies=[self valueForKeyPath: @"studies"];
	if (studies==nil)
		return nil;

	NSEnumerator *studyEnumerator = [studies objectEnumerator];
	id aStudy;
	while ((aStudy=[studyEnumerator nextObject]))
		{
		NSString *str=[aStudy valueForKeyPath: @"studyDescription"];
		if (str)
			[stringArray addObject: str];
		}

// Concatenate array of strings.

	NSString *concatenatedString=[stringArray componentsJoinedByString: @" "];ASSERTNOTNIL(concatenatedString);
	return (concatenatedString);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
// Defer until available in the interface.
/*
- (NSString*) allMediaDescriptions
	{
	
// Return an array of all descriptions from all images and files of this condition.

	NSMutableArray *stringArray=[NSMutableArray arrayWithCapacity: 0];							ASSERTNOTNIL(stringArray);
	
	NSSet *studies=[self valueForKeyPath: @"studies"];
	if (studies==nil)
		return nil;

	NSEnumerator *studyEnumerator = [studies objectEnumerator];
	id aStudy;
	while ((aStudy=[studyEnumerator nextObject]))
		{

// Images.

		NSSet *images=[aStudy valueForKeyPath: @"images"];
		if (images)
			{
			NSEnumerator *imageEnumerator=[images objectEnumerator];							ASSERTNOTNIL(imageEnumerator);
			id anImage;

			while ((anImage=[imageEnumerator nextObject]))
				{
				if (anImage)
					{
					NSString *str=[anImage valueForKeyPath: @"imageDescription"];
					if (str)
						[stringArray addObject: str];
					}
				}
			}

// Movies.

		NSSet *movies=[aStudy valueForKeyPath: @"files"];
		if (movies)
			{
			NSEnumerator *movieEnumerator=[movies objectEnumerator];							ASSERTNOTNIL(movieEnumerator);
			FileEntity *aMovie;

			while ((aMovie=[movieEnumerator nextObject]))
				{
				if (aMovie)
					{
					NSString *str=[aMovie valueForKeyPath: @"fileDescription"];
					if (str)
						[stringArray addObject: str];
					}
				}
			}
		}

// Concatenate array of strings.

	NSString *concatenatedString=[stringArray componentsJoinedByString: @" "];ASSERTNOTNIL(concatenatedString);
	return (concatenatedString);
	}
*/

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) allStudyModalities
	{
	
// Return a set of all study modalities used by this condition.

	NSMutableSet *modalitySet=[NSMutableSet set];ASSERTNOTNIL(modalitySet);
	
	NSSet *studies=[self valueForKeyPath: @"studies"];
	if (studies==nil)
		return nil;

	NSEnumerator *studyEnumerator = [studies objectEnumerator];
	id aStudy;
	while ((aStudy=[studyEnumerator nextObject]))
		{
		NSString *modalityString=[aStudy valueForKeyPath: @"modalityText"];ASSERTNOTNIL(modalityString);
		[modalitySet addObject: modalityString];
		}
	
	NSArray *modalityArray=[modalitySet allObjects];ASSERTNOTNIL(modalityArray);
	NSArray *sortedModalityArray=[modalityArray sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];ASSERTNOTNIL(sortedModalityArray);
	NSString *concatenatedString=[sortedModalityArray componentsJoinedByString: @" "];ASSERTNOTNIL(concatenatedString);
	return (concatenatedString);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSDate*) oldestStudyDate
	{
	
// Return the oldest study in this condition.

	NSMutableArray *studyDatesArray=[NSMutableArray arrayWithCapacity: 0];ASSERTNOTNIL(studyDatesArray);
	
	NSSet *studies=[self valueForKeyPath: @"studies"];
	if (studies==nil)
		return nil;
	if ([studies count]==0)
		return nil;
	
	NSEnumerator *studyEnumerator = [studies objectEnumerator];
	id aStudy;
	while ((aStudy=[studyEnumerator nextObject]))
		{
		NSDate *aDate=[aStudy valueForKeyPath: @"dateOfStudy"];ASSERTNOTNIL(aDate);
		[studyDatesArray addObject: aDate];
		}
	
	NSArray *sortedDatesArray=[studyDatesArray sortedArrayUsingSelector: @selector(compare:)];ASSERTNOTNIL(sortedDatesArray);
	return sortedDatesArray[0];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSDate*) newestStudyDate
	{
	
// Return the newest study in this condition.

	NSMutableArray *studyDatesArray=[NSMutableArray arrayWithCapacity: 0];ASSERTNOTNIL(studyDatesArray);
	
	NSSet *studies=[self valueForKeyPath: @"studies"];
	if (studies==nil)
		return nil;
	if ([studies count]==0)
		return nil;

	NSEnumerator *studyEnumerator = [studies objectEnumerator];
	id aStudy;
	while ((aStudy=[studyEnumerator nextObject]))
		{
		NSDate *aDate=[aStudy valueForKeyPath: @"dateOfStudy"];ASSERTNOTNIL(aDate);
		[studyDatesArray addObject: aDate];
		}
	
	NSArray *sortedDatesArray=[studyDatesArray sortedArrayUsingSelector: @selector(compare:)];ASSERTNOTNIL(sortedDatesArray);
	return [sortedDatesArray lastObject];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#pragma mark -
#pragma mark Drag and Drop Support

- (NSDictionary*) dictionaryRepresentation
	{
	
// Set up mutable dictionary.

	NSMutableDictionary *conditionDict=[NSMutableDictionary dictionary];					ASSERTNOTNIL(conditionDict);

// Patient details.

	[conditionDict setValue: [self valueForKeyPath: @"patient.identifier"] forKey: @"identifier"];
	[conditionDict setValue: [self valueForKeyPath: @"patient.dateOfBirth"] forKey: @"dateOfBirth"];
	[conditionDict setValue: [self valueForKeyPath: @"patient.gender"] forKey: @"gender"];

// Condition details.

	[conditionDict setValue: [self valueForKey: @"system"] forKey: @"system"];
	[conditionDict setValue: [self valueForKey: @"pathology"] forKey: @"pathology"];
	[conditionDict setValue: [self valueForKey: @"conditionRegion"] forKey: @"conditionRegion"];
	[conditionDict setValue: [self valueForKey: @"conditionDescription"] forKey: @"conditionDescription"];
	[conditionDict setValue: [self valueForKey: @"dateOfOnset"] forKey: @"dateOfOnset"];

// Study details.
	
	NSMutableArray *studiesArray=[NSMutableArray arrayWithCapacity: 0];						ASSERTNOTNIL(studiesArray);
	[conditionDict setValue: studiesArray forKey: @"studiesArray"];

// Loop through studies.
	
	NSSet *studies=[self valueForKeyPath: @"studies"];										ASSERTNOTNIL(studies);
	NSEnumerator *studyEnumerator = [studies objectEnumerator];								ASSERTNOTNIL(studyEnumerator);
	id aStudy;
	while ((aStudy=[studyEnumerator nextObject]))
		{
		NSMutableDictionary *studyDict=[NSMutableDictionary dictionary];					ASSERTNOTNIL(studyDict);
		[studiesArray addObject: studyDict];
		[studyDict setValue: [aStudy valueForKey: @"dateOfStudy"] forKey: @"dateOfStudy"];
		[studyDict setValue: [aStudy valueForKey: @"modality"] forKey: @"modality"];
		[studyDict setValue: [aStudy valueForKey: @"studyRegion"] forKey: @"studyRegion"];
		[studyDict setValue: [aStudy valueForKey: @"studyDescription"] forKey: @"studyDescription"];

// Image details.

		NSMutableArray *imagesArray=[NSMutableArray arrayWithCapacity: 0];					ASSERTNOTNIL(imagesArray);
		[studyDict setValue: imagesArray forKey: @"imagesArray"];

// Loop through images.

		NSSet *images=[aStudy valueForKeyPath: @"images"];
		if (images)
			{
			NSEnumerator *imageEnumerator=[images objectEnumerator];						ASSERTNOTNIL(imageEnumerator);
			id anImage;
			while ((anImage=[imageEnumerator nextObject]))
				{
				if (anImage)
					{
					NSMutableDictionary *imageDict=[NSMutableDictionary dictionary];		ASSERTNOTNIL(imageDict);
					[imagesArray addObject: imageDict];
					[imageDict setValue: [anImage valueForKey: @"imageData"] forKey: @"imageData"];
					[imageDict setValue: [anImage valueForKey: @"order"] forKey: @"order"];
					[imageDict setValue: [anImage valueForKey: @"imageDescription"] forKey: @"imageDescription"];
					[imageDict setValue: [anImage valueForKey: @"fileName"] forKey: @"fileName"];
					}
				}
			}

// File details.

		NSMutableArray *filesArray=[NSMutableArray arrayWithCapacity: 0];					ASSERTNOTNIL(filesArray);
		[studyDict setValue: filesArray forKey: @"filesArray"];

// Loop through files.

		NSSet *files=[aStudy valueForKeyPath: @"files"];
		if (files)
			{
			NSEnumerator *fileEnumerator=[files objectEnumerator];							ASSERTNOTNIL(fileEnumerator);
			id aFile;

			while ((aFile=[fileEnumerator nextObject]))
				{
				if (aFile)
					{
					NSMutableDictionary *fileDict=[NSMutableDictionary dictionary];			ASSERTNOTNIL(fileDict);
					[filesArray addObject: fileDict];
					[fileDict setValue: [aFile valueForKey: @"fileData"] forKey: @"fileData"];
					[fileDict setValue: [aFile valueForKey: @"order"] forKey: @"order"];
					[fileDict setValue: [aFile valueForKey: @"fileDescription"] forKey: @"fileDescription"];
					[fileDict setValue: [aFile valueForKey: @"fileName"] forKey: @"fileName"];
					}
				}
			}
		}
		
	return (conditionDict);
	}
	
	
@end
