/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "FileEntity.h"
#import "ImageEntity.h"
#import "StudyArrayController.h"
#import "StudyEntity.h"


@implementation StudyArrayController


- (void) add: (id __unused) sender
	{

// Override to allow for default modality and date specification.

	id aNewObject=[[self newObject] autorelease];
	[self addObject: aNewObject];	

	[aNewObject setValue: [aNewObject valueForKeyPath: @"condition.dateOfOnset"] forKeyPath: @"dateOfStudy"];
	[aNewObject setValue: [aNewObject valueForKeyPath: @"condition.conditionRegion"] forKeyPath: @"studyRegion"];
	}


//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
#pragma mark -
#pragma mark Required to Complete Data Source Delegate Informal Protocol

- (NSInteger) numberOfRowsInTableView: (NSTableView* __unused) aTableView
	{
	return 0;
	}


//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ

- (id) tableView: (NSTableView* __unused) aTableView objectValueForTableColumn: (NSTableColumn* __unused) aTableColumn row: (NSInteger __unused) rowIndex
	{			
	return nil;
	}


//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
//ΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡΡ
#pragma mark - Combine Studies

- (void) combineStudies
	{
	NSArray *studyArray=[self selectedObjects];ASSERTNOTNIL(studyArray);
	if ([studyArray count] < 2)
		return;
		
	StudyEntity *masterStudy=[studyArray objectAtIndex: 0];ASSERTNOTNIL(masterStudy);
	NSUInteger index;
	for (index=1; index<[studyArray count]; index++)
		{
		StudyEntity *aStudy=[studyArray objectAtIndex: index];ASSERTNOTNIL(aStudy);
		
		NSArray *imagesArray=[[aStudy valueForKey: @"images"] allObjects];ASSERTNOTNIL(imagesArray);
		for (ImageEntity *image in imagesArray)
			{
			[image setValue: masterStudy forKey: @"study"];
			}
			
		NSArray *filesArray=[[aStudy valueForKey: @"files"] allObjects];ASSERTNOTNIL(filesArray);
		for (FileEntity *file in filesArray)
			{
			[file setValue: masterStudy forKey: @"study"];
			}		
		}
	}
	

@end
