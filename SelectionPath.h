/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/



typedef enum : NSUInteger
	{
	SelectionTrackingNone		=	0,
	SelectionTrackingSelecting,
	SelectionTrackingMoving,
	SelectionTrackingResizing
	} SelectionTrackingMode;

	
@interface SelectionPath : NSObject
	{
	NSImageView						*targetView;
	NSRect							selectedRect;
	NSPoint							pinLocation;
	NSColor							*fillColor,
									*strokeColor;
	SelectionTrackingMode			trackingMode;
	NSBezierPath					*bottomLeftHandle,
									*topLeftHandle,
									*topRightHandle,
									*bottomRightHandle;
	}


+ (SelectionPath*) markerForView: (NSView*) aView;

- (instancetype) initWithView: (NSView*) aView;

- (void) drawSelection;

- (void) startSelectingAtPoint: (NSPoint) where;
- (void) continueSelectingAtPoint: (NSPoint) where;
- (void) stopSelectingAtPoint: (NSPoint) where;
- (void) startMovingAtPoint: (NSPoint) where;
- (void) continueMovingAtPoint: (NSPoint) where;
- (void) stopMovingAtPoint: (NSPoint) where;
- (void) startResizingAtPoint: (NSPoint) where;
- (void) continueResizingAtPoint: (NSPoint) where;
- (void) stopResizingAtPoint: (NSPoint) where;

- (void) mouseDown: (NSEvent*) theEvent;
- (void) mouseUp: (NSEvent*) theEvent;
- (void) mouseDragged: (NSEvent*) theEvent;

- (void) setColor: (NSColor*) color;
@property (nonatomic, readonly, copy) NSColor *selectionColor;
- (void) setFillColor: (NSColor*) color;
- (void) setStrokeColor: (NSColor*) color;
@property (nonatomic, readonly, copy) NSBezierPath *selectedPath;
@property (nonatomic) NSRect selectedRect;
- (void) setSelectedRectOrigin: (NSPoint) where;
- (void) setSelectedRectSize: (NSSize) size;
- (void) moveSelectedRectBy: (NSSize) delta;

@end

NSRect rectFromPoints(NSPoint p1, NSPoint p2);


