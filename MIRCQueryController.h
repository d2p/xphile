/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/




#define QueryDocumentAddressKey							@"QueryDocumentAddress"
#define QueryTitleKey									@"QueryTitle"
#define QueryAccessKey									@"QueryAccess"


#define MIRCQueryControllerLocalizedString(str)			NSLocalizedStringFromTable((str), @"MIRCQueryController", (str))

#define RequestingDataString							MIRCQueryControllerLocalizedString(@"Requesting data...")
#define ReceivingResponseString							MIRCQueryControllerLocalizedString(@"Receiving response...")
#define SearchFailedString								MIRCQueryControllerLocalizedString(@"Search failed")
#define SearchCompleteString							MIRCQueryControllerLocalizedString(@"Search complete")
#define ImportingArchiveString							MIRCQueryControllerLocalizedString(@"Importing archive...")
#define ImportCompleteString							MIRCQueryControllerLocalizedString(@"Import complete")
#define ImportFailedString								MIRCQueryControllerLocalizedString(@"Import failed")





@interface MIRCQueryController : NSWindowController
	{
	NSMutableData						*queryData;
	NSMutableData						*fetchData;
	NSMutableArray						*queryResultsArray;
	BOOL								fetchInProgress;
	NSString							*searchText;
	NSURLConnection						*queryConnection;
	NSURLConnection						*fetchConnection;
	IBOutlet id							resultsArrayController,
										statusField,
										searchButton,
										fetchButton;
	}

@property (nonatomic, readonly, copy) NSMutableArray *queryResultsArray;
@property (nonatomic) BOOL fetchInProgress;
@property (nonatomic, copy) NSString *searchText;

- (IBAction) doQuery: (id) sender;
- (IBAction) doFetch: (id) sender;

- (void) queryConnectionDidFinishLoading: (NSURLConnection*) connection;
- (void) fetchConnectionDidFinishLoading: (NSURLConnection*) connection;

- (IBAction) doBrowse: (id) sender;

@end
