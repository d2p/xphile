/*===============================================================================================================================================
	
	Program:   Xphile

	Copyright (C) 2007-2016 Xphile Team

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	The source code is available from <http://xphile.ddp.org.nz>


================================================================================================================================================*/


#import "AlbumEntity.h"


@implementation AlbumEntity


+ (NSSet*) keyPathsForValuesAffectingExtendedLabel
	{
	return [NSSet setWithObjects: @"label", @"conditions", @"itsPredicate", @"conditions.@count", nil];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (id) valueForUndefinedKey: (NSString*) key
	{
	NSLog(@"AlbumEntity: no such key - %@", key);
	return (nil);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (void) awakeFromInsert
	{
	[super awakeFromInsert];

// Default to non-smart album.
	
	[self setValue: nil forKeyPath: @"itsPredicate"];
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (BOOL) isSmartAlbum
	{
	NSData *data=[self valueForKeyPath: @"itsPredicate"];
	return (data!=nil);
	}


//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSUInteger) numberOfConditionsInSmartAlbum
	{
	NSData *predicateData=[self valueForKeyPath: @"itsPredicate"];
	if (predicateData == nil)
		return 0;
	
	NSPredicate *smartAlbumPredicate=[NSKeyedUnarchiver unarchiveObjectWithData: predicateData];ASSERTNOTNIL(smartAlbumPredicate);
	NSManagedObjectContext *moc=[self managedObjectContext];ASSERTNOTNIL(moc);
	NSFetchRequest *request=[[[NSFetchRequest alloc] init] autorelease];ASSERTNOTNIL(request);
	NSEntityDescription *entity=[NSEntityDescription entityForName: @"Condition" inManagedObjectContext: moc];ASSERTNOTNIL(entity);
	[request setEntity: entity];
	[request setPredicate: smartAlbumPredicate];
	NSError *error=nil;
	NSArray *array=[moc executeFetchRequest: request error: &error];ASSERTNOTNIL(array);
	return [array count];
	}
	

//———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

- (NSString*) extendedLabel
	{
	NSInteger								numberInAlbum;


	NSString *str=nil;
	NSString *labelStr=[self valueForKey: @"label"];
	if (labelStr != nil)
		{
		NSString *albumTypeStr=@"";ASSERTNOTNIL(albumTypeStr);
		if ([self isSmartAlbum])
			{
			numberInAlbum=[self numberOfConditionsInSmartAlbum];
			}
		else
			{
			numberInAlbum=[[self valueForKeyPath: @"conditions"] count];
			albumTypeStr=[NSString stringWithUTF8String: "📘"];
			}
		str=[NSString stringWithFormat: @"%@%@ (%ld)", albumTypeStr, labelStr, (long) numberInAlbum];
		}
	
	return (str);
	}
	
	
@end
